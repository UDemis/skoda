# -*- coding: utf-8 -*-
from django.db import models
from snippets.core.fields import FileField

class Banner(models.Model):
    banner = FileField(u'Баннер', max_length=255, upload_to='banners')
    title = models.CharField(u'Заголок', max_length=255)
    # link = models.URLField(u'Ссылка', blank=True)
    link = models.CharField(u'Ссылка', max_length=200, blank=True)
    in_new = models.BooleanField(u'Открыть в новом окне', default=True)

    class Meta:
        verbose_name = u'Баннер'
        verbose_name_plural = u'Баннеры'

    def __unicode__(self):
        return self.title

    def is_image(self):
        return self.banner.mimetype[0] in ['image/jpeg', 'image/gif']

    def is_flash(self):
        return self.banner.mimetype[0] in ['application/x-shockwave-flash']