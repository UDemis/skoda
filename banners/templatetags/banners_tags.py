# -*- coding: utf-8 -*-
from django import template
from banners.models import Banner

register = template.Library()

@register.inclusion_tag("banners/banners_tag.html")
def banners(limit=2):
    banners = Banner.objects.all()[:limit]
    return {'object_list':banners}

