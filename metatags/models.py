# -*- coding: utf-8 -*-
from django.db import models


class MetaTags(models.Model):
    url = models.CharField(u'URL страницы', max_length=255, unique=True)
    title = models.TextField(u'Title', blank=True)
    description = models.TextField(u'Description', blank=True)
    keywords = models.TextField(u'Keywords', blank=True)

    class Meta:
        verbose_name = u'метатеги'
        verbose_name_plural = u'метатеги'