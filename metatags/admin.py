# -*- coding: utf-8 -*-
from django.contrib import admin
from metatags.models import MetaTags


class MetaTagsAdmin(admin.ModelAdmin):
    list_display = ('url', 'title', 'description', 'keywords')
    list_display_links = ('url', 'title', 'description', 'keywords')

admin.site.register(MetaTags, MetaTagsAdmin)