# -*- coding: utf-8 -*-
from metatags.models import MetaTags


def meta_tags(request):
    try:
        meta_tags = MetaTags.objects.get(url=request.path)
    except MetaTags.DoesNotExist:
        return {}
    return {
        "meta_tags": meta_tags,
    }