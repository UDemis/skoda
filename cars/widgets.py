# -*- coding: utf-8 -*-
from django import forms
from django.forms.util import flatatt
from django.utils.html import mark_safe
from django.contrib.admin.templatetags.admin_static import static

class PositionWidget(forms.TextInput):
    input_type = 'hidden'

    def __init__(self, bg_img='', bg_img_width=100, bg_img_height=100, attrs=None):
        self.bg_img = bg_img
        self.bg_img_width = bg_img_width
        self.bg_img_height = bg_img_height
        super(PositionWidget, self).__init__(attrs)

    def render(self, name, value, *args, **kwargs):
        output = [super(PositionWidget, self).render(name, value, *args, **kwargs)]
        output.append('<div class="%s"><div class="%s"></div></div>' %
                      ('set-position-container','set-position-close'))
        output.append('<a href="%s" data-width="%s" data-height="%s" class="set-position-link">%s</a>' %
                      (self.bg_img, self.bg_img_width, self.bg_img_height, u'Указать позицию',))
        return mark_safe(''.join(output))

    @property
    def media(self):
        js = ["jquery.easyModal.js", "jquery.setPosition.init.js"]
        css = ["jquery.setPosition.css"]
        return forms.Media(js=[static("cars/js/%s" % path) for path in js],
                           css={'all':[static("cars/css/%s" % path) for path in css]})

class LinkWidget(forms.Widget):
    hidden = False
    def __init__(self, title, attrs=None):
        self.title = title
        super(LinkWidget, self).__init__(attrs)
        if 'href' not in self.attrs:
            self.attrs['href'] = '#'

    def render(self, name, value, attrs=None):
        if self.hidden:
            return ''
        final_attrs = self.build_attrs(attrs, name=name)
        return mark_safe('<a%s>%s</a>' % (flatatt(final_attrs), self.title))
    
class SelectReadOnlyWidget(forms.MultiWidget):
    
    def __init__(self, attrs=None, choices=()):
        widgets = [forms.Select, forms.HiddenInput]
        super(SelectReadOnlyWidget, self).__init__(widgets, attrs)
        self.choices = list(choices)

    def render(self, name, value, attrs=None):
        if self.is_localized:
            for widget in self.widgets:
                widget.is_localized = self.is_localized
        output = []
        final_attrs = self.build_attrs(attrs)
        readonly_attrs = self.build_attrs({'disabled':'disabled'})
        output.append(self.widgets[0].render('', value, readonly_attrs, choices=self.choices))
        output.append(self.widgets[1].render(name, value, final_attrs))
        return mark_safe(self.format_output(output))

    def value_from_datadict(self, data, files, name):
        return self.widgets[1].value_from_datadict(data, files, name)

class CharacteristicsWidget(forms.CheckboxSelectMultiple):

    def value_from_datadict(self, data, files, name):
        return ','.join(data.getlist(name))