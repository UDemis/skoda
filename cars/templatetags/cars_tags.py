# -*- coding: utf-8 -*-
from django import template
from django.template.loader import get_template
from django.utils.safestring import mark_safe
from mptt.templatetags.mptt_tags import cache_tree_children
from cars.models import Car

register = template.Library()

@register.simple_tag(takes_context=True)
def cars_menu_tree(context):
    template_name = 'cars/menu.html'

    def _render_node(template, node, context):
        # bits = []
        # for child in node.get_children():
        #     bits.append(_render_node(template,child,context))
        context['node'] = node
        context['children'] = node.get_children()
        return template.render(context)

    qs = Car.objects.all().order_by('lft')
    nodes = cache_tree_children(qs)
    t = get_template(template_name)
    context = template.Context(context)
    bits = []
    for node in nodes:
        bits.append(_render_node(t,node,context))
    return ''.join(bits)

@register.simple_tag(takes_context=True)
def cars_menu(context, template_name='cars/menu_footer.html'):
    cars = Car.objects.filter(level=1)
    t = get_template(template_name)
    context = template.Context(context)
    context.update({'cars':cars})
    return t.render(context)

@register.filter
def jsafe(value):
    return value.replace('// <![CDATA[','').replace('// ]]>','')