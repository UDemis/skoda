# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns
from cars.views import CarDetail, CarExteriorPointDetail, CarInteriorDetail, CarInteriorPointDetail, \
    CarTechnologyDetail, CarTechnologyPointDetail, CarGalleryDetail, CarView360Detail, CarList, CarInStockList, \
    CarPriceListDetail, CarOthersDetail, CarExteriorDetail, CarPromoList

urlpatterns = patterns('',
    url(r'^$', CarList.as_view(), name='cars_car_list'),
    url(r'^auto-in-stock/$', CarInStockList.as_view(), name='cars_carinstock_list'),
    url(r'^promo/$', CarPromoList.as_view(), name='cars_promo'),
    url(r'^rapid2/$', CarPromoList.as_view(), name='cars_promo'),
    url(r'^(?P<slug>[-\w]+)/$', CarDetail.as_view(), name='cars_car_detail'),
    url(r'^(?P<slug>[-\w]+)/exterior/$', CarExteriorDetail.as_view(), name='cars_carexterior_detail'),
    url(r'^(?P<car>[-\w]+)/exterior/(?P<pk>\d+)/$', CarExteriorPointDetail.as_view(), name='cars_carexteriorpoint_detail'),
    url(r'^(?P<slug>[-\w]+)/interior/$', CarInteriorDetail.as_view(), name='cars_carinterior_detail'),
    url(r'^(?P<car>[-\w]+)/interior/(?P<slug>[-\w]+)/(?P<pk>\d+)/$', CarInteriorPointDetail.as_view(), name='cars_carinteriorpoint_detail'),
    url(r'^(?P<slug>[-\w]+)/technology/$', CarTechnologyDetail.as_view(), name='cars_technology_detail'),
    url(r'^(?P<car>[-\w]+)/technology/(?P<slug>[-\w]+)/(?P<pk>\d+)/$', CarTechnologyPointDetail.as_view(), name='cars_technologypoint_detail'),
    url(r'^(?P<slug>[-\w]+)/gallery/$', CarGalleryDetail.as_view(), name='cars_gallery_detail'),
    url(r'^(?P<slug>[-\w]+)/360/$', CarView360Detail.as_view(), name='cars_view360_detail'),
    url(r'^(?P<slug>[-\w]+)/pricelist/$', CarPriceListDetail.as_view(), name='cars_pricelist_detail'),
    url(r'^(?P<car>[-\w]+)/(?P<slug>[-\w]+)/$', CarOthersDetail.as_view(), name='cars_others_detail'),
)