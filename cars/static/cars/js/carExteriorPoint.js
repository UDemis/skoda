/**
 * Created by metalpower.
 * Date: 10.02.14
 */
(function($){
    $(function(){
        var mediaUrl = '/media/';
        var frontImgField = $("#id_img_exterior_point_front");
        var frontImgFieldLabel = $("label[for='id_img_exterior_point_front']").text();
        var backImgField = $("#id_img_exterior_point_back");
        var backImgFieldLabel = $("label[for='id_img_exterior_point_back']").text();
        var $positionFields = $(".position a");
        $positionFields.click(function(e){
            var view = $(this).parent().siblings('.view').find('select').val();
            if (!frontImgField.val() || !backImgField.val()){
                e.stopImmediatePropagation();
                alert('Сначала заполните следующие поля: "'+frontImgFieldLabel+'", "'+backImgFieldLabel+'"');
            } else {
                if (view == 'front'){
                    $(this).attr('href', mediaUrl+frontImgField.val());
                } else if (view == 'back') {
                    $(this).attr('href', mediaUrl+backImgField.val());
                }
            }
            e.preventDefault();
        });
    });
})(django.jQuery);