/**
 * Created by metalpower.
 * Date: 10.02.14
 */
(function($){
    $(function(){
        var mediaUrl = '/media/';
        var imgField = $("#id_img");
        var $positionFields = $(".position a");
        $positionFields.click(function(e){
            $(this).attr('href', mediaUrl+imgField.val());
            e.preventDefault();
        });
    });
})(django.jQuery);