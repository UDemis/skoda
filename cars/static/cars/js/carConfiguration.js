/**
 * Created by metalpower.
 * Date: 10.02.14
 */
function addRowToTable(win){
    var container = document.getElementById('carconfiguration-group');
    var table = container.getElementsByTagName('TABLE')[0];
    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);
    row.className = "grp-row grp-row-even";
    for (var i=1; i<arguments.length; i++){
        var arg = arguments[i];
        var cell = row.insertCell(i-1);
        cell.innerHTML = arg;
    }
    win.close();
}

function updateRowToTable(win){
    var name = windowname_to_id(win.name);
    var link = document.getElementById(name);
    var row = link.parentNode.parentNode;
    for (var i=1; i<arguments.length; i++){
        var arg = arguments[i];
        var cell = row.cells[i-1];
        cell.innerHTML = arg;
    }
    win.close();
}

function deleteRowToTable(win){
    var name = windowname_to_id(win.name);
    var link = document.getElementById(name);
    var row = link.parentNode.parentNode;
    var table = row.parentNode;
    table.deleteRow(row.rowIndex-1);
    win.close();
}

function showInPopup(triggeringLink) {
    var href = triggeringLink.href;
    if (href.indexOf('?') == -1) {
        href += '?_popup=1';
    } else {
        href  += '&_popup=1';
    }
    var win = window.open(href, 'new', 'height=500,width=1200,resizable=yes,scrollbars=yes');
    win.focus();
    return false;
}