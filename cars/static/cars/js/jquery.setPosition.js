/**
 * Created with PyCharm.
 * User: 1
 * Date: 09.02.14
 * Time: 13:24
 * To change this template use File | Settings | File Templates.
 */
(function($) {
    $.fn.setPosition = function(){
        var defaults = {

        };
        var newPoint = function(x, y){
            return $("<div></div>").addClass('set_position_point').css({left:x,top:y});
        };
        var positionVal = function(x,y){
            return [x,y].join(',');
        };
        var getPositionVal = function(str){
            var xy = str.split(',');
            if (xy.length != 2){
                return [null,null];
            }
            return [parseInt(xy[0]),parseInt(xy[1])]
        };
        return this.each(function(){
//            var field = $(this);
//            var container = field.next();
            var container = $(this);
            var field = container.prev();
            var containerOffset = container.offset();
            var pointXY = getPositionVal(field.val());
            var pointX = pointXY[0];
            var pointY = pointXY[1];
            var point = null;
            var pointMoveHandler = function(event){
                var self = this;
                var x1 = event.clientX;
                var y1 = event.clientY;
                var x3 = parseInt($(self).css('left'));
                var y3 = parseInt($(self).css('top'));
                container.bind('mousemove', function(event1) {
                    var x2 = event1.clientX;
                    var y2 = event1.clientY;
                    var distX = x2 - x1;
                    var distY = y2 - y1;
                    var newX = x3 + distX;
                    var newY = y3 + distY;
                    $(self).css('left', newX);
                    $(self).css('top', newY);
                    field.val(positionVal(newX,newY));
                });
            };
            container.attr('unselectable','on');
            container.bind('selectstart',false);
            if (pointX != null && pointY != null){
                point = newPoint(pointX, pointY);
                container.append(point);
                point.bind('mousedown', pointMoveHandler);
                container.bind('mouseup', function() {
                    container.unbind('mousemove');
                });
            } else {
                container.bind('click', function(e){
                    var pointX = e.pageX - containerOffset.left;
                    var pointY = e.pageY - containerOffset.top;
                    var point = newPoint(pointX, pointY);
                    field.val(positionVal(pointX,pointY));
                    $(this).append(point);
                    $(this).unbind('click');
                    point.bind('mousedown', pointMoveHandler);
                    container.bind('mouseup', function() {
                        container.unbind('mousemove');
                    });
                });
            }
        });
    };
    $(function(){
        $(".set_position").setPosition();
    });
})(django.jQuery);


