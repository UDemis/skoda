/**
 * Created with PyCharm.
 * User: 1
 * Date: 09.02.14
 * Time: 13:24
 * To change this template use File | Settings | File Templates.
 */
(function($) {
    $.fn.setPosition = function(options){
        options = options || {};
        var defaults = {
            imgUrl: '',
            width: '',
            height: '',
            pointClass: 'set-position-point',
            pointShiftLeft: 10,
            pointShiftTop: 10
        };
        options = $.extend(defaults, options);
        var newPoint = function(x, y){
            return $("<div></div>").addClass(options.pointClass).css({left:x,top:y});
        };
        var positionVal = function(x,y){
            return [Math.round(x)-13,Math.round(y)-13].join(',');
        };
        var getPositionVal = function(str){
            var xy = str.split(',');
            if (xy.length != 2){
                return [null,null];
            }
            return [parseInt(xy[0])+13,parseInt(xy[1])+13]
        };
        return this.each(function(){
            var container = $(this);
            var img = container.find('img');
            var field = container.prev();
            var containerOffset = container.offset();
            var pointXY = getPositionVal(field.val());
            var pointX = pointXY[0];
            var pointY = pointXY[1];
            var point = null;
            container.attr('unselectable','on');
            container.bind('selectstart',false);
            if (pointX != null && pointY != null){
                point = newPoint(pointX, pointY);
                container.append(point);
            }
            if (!img.length){
                img = $("<img>").attr({width:options.width, height:options.height});
                container.prepend(img);
            }
            img.attr("src",options.imgUrl);
            img.bind('click', function(e){
                var pointX = e.pageX - containerOffset.left - options.pointShiftLeft;
                var pointY = e.pageY - containerOffset.top - options.pointShiftTop;
                if (!point){
                    point = newPoint(pointX, pointY);
                    container.append(point);
                } else {
                    point.css('left', pointX);
                    point.css('top', pointY);
                }
                field.val(positionVal(pointX,pointY));
            });
        });
    };
    $(function(){
        $(".set-position-link").on('click',function(e){
            e.preventDefault();
            var self = $(this);
            var container = self.prev();
            var url = self.attr('href');
            var width = self.data('width');
            var height = self.data('height');
            if (container.data('modal') != true){
                container.css({width:width,height:height});
                container.easyModal({
                    closeButtonClass: '.set-position-close',
                    overlayClose: false
                });
                container.data('modal', true);
            }
            container.trigger('openModal');
            container.setPosition({width:width, height:height,
                                    imgUrl:url,
                                   pointShiftLeft:10, pointShiftTop:10});
        })
    });
})(django.jQuery);


