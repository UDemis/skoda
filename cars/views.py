# -*- coding: utf-8 -*-
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.shortcuts import Http404, HttpResponseRedirect, HttpResponsePermanentRedirect
from cars.models import Car, CarExteriorPoint, CarInteriorPoint, CarTechnologyPoint, CarInStock, CarOthers


class CarList(ListView):
    queryset = Car.objects.filter(level=1).order_by('parent')
    template_name = 'cars/car_list.html'

    def get_context_data(self, **kwargs):
        context = super(CarList, self).get_context_data(**kwargs)
        context.update({
            'characteristics': Car.CHARACTERISTIC_CHOICES
        })
        return context


class CarInStockList(ListView):
    queryset = CarInStock.objects.select_related().all()


class CarPromoList(ListView):
    queryset = CarInStock.objects.select_related().filter(in_promo=True).order_by('car__title',
                                                                                  'configuration_text',
                                                                                  'configuration__title')
    template_name = 'cars/promo.html'


class CarDetail(DetailView):
    model = Car
    template_name = 'cars/car_detail.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.has_exterior():
            return HttpResponsePermanentRedirect(self.object.get_exterior_url())
        others_first = self.object.others.first()
        if others_first:
            return HttpResponsePermanentRedirect(others_first.get_absolute_url())
        if self.object.has_pricelist():
            return HttpResponsePermanentRedirect(self.object.get_pricelist_url())
        return super(CarDetail, self).get(request, *args, **kwargs)


class CarExteriorDetail(DetailView):
    model = Car
    template_name = 'cars/car_detail.html'

    def get_object(self, queryset=None):
        obj = super(CarExteriorDetail, self).get_object(queryset)
        if not obj.has_exterior():
            raise Http404
        return obj


class CarInteriorDetail(DetailView):
    model = Car
    template_name = 'cars/car_interior_detail.html'

    def get_object(self, queryset=None):
        obj = super(CarInteriorDetail, self).get_object(queryset)
        if not obj.has_configurations():
            raise Http404
        return obj


class CarTechnologyDetail(DetailView):
    model = Car
    template_name = 'cars/car_technology_detail.html'

    def get_object(self, queryset=None):
        obj = super(CarTechnologyDetail, self).get_object(queryset)
        if not obj.has_technologies():
            raise Http404
        return obj


class CarGalleryDetail(DetailView):
    model = Car
    template_name = 'cars/car_gallery_detail.html'

    def get_object(self, queryset=None):
        obj = super(CarGalleryDetail, self).get_object(queryset)
        if not obj.has_gallery():
            raise Http404
        return obj


class CarExteriorPointDetail(DetailView):
    model = CarExteriorPoint
    template_name = 'cars/car_exteriorpoint_detail.html'


class CarInteriorPointDetail(DetailView):
    model = CarInteriorPoint
    template_name = 'cars/car_interiorpoint_detail.html'


class CarTechnologyPointDetail(DetailView):
    model = CarTechnologyPoint
    template_name = 'cars/car_technologypoint_detail.html'


class CarView360Detail(DetailView):
    model = Car
    template_name = 'cars/car_view360_detail.html'

    def get_object(self, queryset=None):
        obj = super(CarView360Detail, self).get_object(queryset)
        if not obj.view360:
            raise Http404
        return obj


class CarPriceListDetail(DetailView):
    model = Car
    template_name = 'cars/car_pricelist_detail.html'

    def get_object(self, queryset=None):
        obj = super(CarPriceListDetail, self).get_object(queryset)
        if not obj.has_pricelist():
            raise Http404
        return obj


class CarOthersDetail(DetailView):
    model = CarOthers
    template_name = 'cars/car_others_detail.html'

    def get_object(self, queryset=None):
        car_slug = self.kwargs.get('car', None)
        if car_slug is None:
            raise Http404
        qs = self.get_queryset()
        qs = qs.filter(car__slug=car_slug)
        obj = super(CarOthersDetail, self).get_object(qs)
        if obj.link:
            raise Http404
        if self.kwargs.get('slug', None) == 'design':
            obj.postdesc = ' Дизайн.'
        if self.kwargs.get('slug', None) == 'roominess':
            obj.postdesc = ' Вместительность.'
        if self.kwargs.get('slug', None) == 'tehnologii' or self.kwargs.get('slug',
                                                                            None) == 'tehnologii2' or self.kwargs.get(
                'slug', None) == 'tehnology' or self.kwargs.get('slug', None) == 'technology2':
            obj.postdesc = ' Технологии.'
        if self.kwargs.get('slug', None) == 'safety2' or self.kwargs.get('slug', None) == 'safety' or self.kwargs.get(
                'slug', None) == 'bezopasnost':
            obj.postdesc = ' Безопасность.'
        if self.kwargs.get('slug', None) == 'infotainment':
            obj.postdesc = ' Медиасистемы.'
        if self.kwargs.get('slug', None) == 'comfort':
            obj.postdesc = ' Комфорт.'
        if self.kwargs.get('slug', None) == 'clever-inside':
            obj.postdesc = ' Clever Inside.'
        if self.kwargs.get('slug', None) == 'skoda-yeti-adventure':
            obj.postdesc = ' Skoda Yeti Adventure.'
        if self.kwargs.get('slug', None) == 'awards-press-reviews':
            obj.postdesc = ' Награды и обзоры.'

        return obj

