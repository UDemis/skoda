# -*- coding: utf-8 -*-
from django.db.models import Count
from django.contrib.sitemaps import Sitemap
from cars.models import Car, CarOthers

sitemaps = {}


class CarSitemap(Sitemap):

    def items(self):
        return Car.objects.filter(level=1)


class CarInteriorSitemap(Sitemap):

    def items(self):
        return Car.objects.filter(level=1).annotate(conf=Count('configurations')).filter(conf__gt=0)

    def location(self, obj):
        return obj.get_interior_url()


class CarTechnologySitemap(Sitemap):

    def items(self):
        return Car.objects.filter(level=1).annotate(conf=Count('technologies')).filter(conf__gt=0)

    def location(self, obj):
        return obj.get_technology_url()


class CarGallerySitemap(Sitemap):

        def items(self):
            return Car.objects.filter(level=1).annotate(conf=Count('images')).filter(conf__gt=0)

        def location(self, obj):
            return obj.get_gallery_url()


class CarPricelistSitemap(Sitemap):

    def items(self):
        return Car.objects.filter(level=1).annotate(conf=Count('pricelist')).filter(conf__gt=0)

    def location(self, obj):
        return obj.get_pricelist_url()


class CarView360Sitemap(Sitemap):

    def items(self):
        return Car.objects.filter(level=1).exclude(view360='')

    def location(self, obj):
        return obj.get_view360_url()


class CarOthersSitemap(Sitemap):

    def items(self):
        return CarOthers.objects.filter(link='')

    def location(self, obj):
        return obj.get_absolute_url()


sitemaps = {
    'cars': CarSitemap,
    'cars_interior': CarInteriorSitemap,
    'cars_technology': CarTechnologySitemap,
    'cars_gallery': CarGallerySitemap,
    'cars_pricelist': CarPricelistSitemap,
    'cars_view360': CarView360Sitemap,
    'cars_others': CarOthersSitemap,
}




