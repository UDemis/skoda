# -*- coding: utf-8 -*-
from django.contrib import admin
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_protect
from django.utils.decorators import method_decorator
from django.template.response import SimpleTemplateResponse

from mptt.admin import MPTTModelAdmin
from snippets.seo.admin import MetaDataInline
from snippets.core.admin import MPTTOrderingMixin
from cars.models import Car, CarColor, CarExteriorPoint, CarConfiguration, CarInteriorPoint, \
                        CarTechnology, CarTechnologyPoint, CarGallery, CarInStock, CarConfigurationPrice, CarOthers
from cars.forms import CarAdminForm, CarExteriorPointAdminForm, CarInteriorPointAdminForm, \
                        CarTechnologyForm, CarTechnologyInlineForm, CarTechnologyPointAdminForm, \
                        CarConfigurationForm, CarConfigurationInlineForm, CarInStockAdminForm

from mobile.models import CarMobileContent

csrf_protect_m = method_decorator(csrf_protect)

class CarColorInline(admin.TabularInline):
    model = CarColor
    extra = 0
    classes = ('grp-collapse grp-closed',)

class CarExteriorPointInline(admin.StackedInline):
    model = CarExteriorPoint
    extra = 0
    fields = ('title','view','img','description','fulltext','position')
    form = CarExteriorPointAdminForm
    classes = ('grp-collapse grp-closed',)
    inline_classes = ('grp-collapse grp-closed',)

    class Media:
        js = ("cars/js/carExteriorPoint.js",)

class CarConfigurationInline(admin.TabularInline):
    model = CarConfiguration
    classes = ('grp-collapse grp-closed',)
    form = CarConfigurationInlineForm
    extra = 1

class CarTechnologyInline(admin.TabularInline):
    model = CarTechnology
    # can_delete = False
    form = CarTechnologyInlineForm
    classes = ('grp-collapse grp-closed',)

    def get_extra(self, request, obj=None, **kwargs):
        return len(self.model.KIND_CHOICES)

    def get_max_num(self, request, obj=None, **kwargs):
        return len(self.model.KIND_CHOICES)

    def get_formset(self, request, obj=None, **kwargs):
        Formset = super(CarTechnologyInline, self).get_formset(request, obj, **kwargs)
        exist = []
        if obj:
            exist = [f[0] for f in obj.technologies.all().values_list('kind')]
        initial = [{'kind': kind} for kind, kind_name in self.model.KIND_CHOICES if kind not in exist]
        class InitialFormset(Formset):
            def __init__(self, *args, **kwargs):
                super(InitialFormset, self).__init__(*args, **kwargs)
                self.initial_extra = initial
        return InitialFormset

class CarGalleryInline(admin.TabularInline):
    model = CarGallery
    classes = ('grp-collapse grp-closed',)

class CarConfigurationPriceInline(admin.StackedInline):
    model = CarConfigurationPrice
    classes = ('grp-collapse grp-closed',)
    inline_classes = ('grp-collapse grp-open',)

class CarOthersInline(admin.StackedInline):
    model = CarOthers
    prepopulated_fields = {'slug':('title',)}
    extra = 1
    classes = ('grp-collapse grp-closed',)
    inline_classes = ('grp-collapse grp-closed',)
    sortable_field_name = "position"

class CarMobileContentInline(admin.StackedInline):
    model = CarMobileContent
    extra = 1
    classes = ('grp-collapse grp-closed',)
    inline_classes = ('grp-collapse grp-closed',)
    sortable_field_name = "position"

class CarAdmin(MPTTOrderingMixin, MPTTModelAdmin):
    list_display = ('title','car_actions',)
    prepopulated_fields = {'slug':('title',)}
    form = CarAdminForm
    fieldsets = (
        ('', {
            'fields': ('title', 'slug', ('price', 'price_to'), ('interior_space','luggage_space'),
                       'petrol_consumption','emission_co2','description', 'fulltext', 'characteristics','configurator_link',
                       'catalog_link','view360','accessories', 'img', 'img_exterior_bg', 'img_exterior_point_front',
                       'img_exterior_point_back', 'parent',),
            }),
    )
    add_fields = ('title', 'slug', 'parent', 'price', 'img','img_euroncap')
    mptt_level_indent = 30
    inlines = [CarColorInline, CarExteriorPointInline, CarConfigurationInline, CarTechnologyInline, CarGalleryInline,
               CarConfigurationPriceInline, CarOthersInline, MetaDataInline, CarMobileContentInline]

    class Media:
        js = ("cars/js/carConfiguration.js",)
        css = {'all':('bootstrap/css/bootstrap-glyphicons.css',)}

    def car_actions(self, obj):
        movedown = '&nbsp;&nbsp;&nbsp;'
        moveup = ''
        if obj.get_next_sibling():
            movedown = "<a href='%s'><span class='glyphicon glyphicon-arrow-down'></span></a>" % (reverse('admin:cars_car_movedown', args=(obj.pk,)),)
        if obj.get_previous_sibling():
            moveup = "<a href='%s'><span class='glyphicon glyphicon-arrow-up'></a>" % (reverse('admin:cars_car_moveup', args=(obj.pk,)),)
        add_child = "<a href='%s?parent=%s'>%s</a>" % (reverse('admin:cars_car_add'), obj.pk, u"Добавить")
        if obj.level > 0:
            return ' '.join((movedown, moveup))
        return ' '.join((add_child, movedown, moveup))
    car_actions.short_description = u'Действия'
    car_actions.allow_tags = True

    def get_fieldsets(self, request, obj=None):
        parent = request.GET.get('parent', None)
        if obj is None:
            if parent is None:
                return ((None,{'fields':self.add_fields}),)
        elif obj.parent is None:
            return ((None,{'fields':self.add_fields}),)
        return super(CarAdmin, self).get_fieldsets(request, obj)

    def get_form(self, request, obj=None, **kwargs):
        parent = request.GET.get('parent', None)
        if obj is None:
            if parent is None:
                kwargs['fields'] = self.add_fields
        elif obj.parent is None:
            kwargs['fields'] = self.add_fields
        return super(CarAdmin, self).get_form(request, obj, **kwargs)

    def get_prepopulated_fields(self, request, obj=None):
        parent = request.GET.get('parent', None)
        if obj is None:
            if parent is None:
                return {}
        if obj is not None and obj.parent is None:
            return {}
        return self.prepopulated_fields

    def get_inline_instances(self, request, obj=None):
        parent = request.GET.get('parent', None)
        if obj is None:
            if parent is None:
                return []
        if obj is not None and obj.parent is None:
            return []
        return super(CarAdmin, self).get_inline_instances(request, obj)

class CarInteriorPointInline(admin.StackedInline):
    model = CarInteriorPoint
    extra = 0
    form = CarInteriorPointAdminForm
    classes = ('grp-collapse grp-open',)

    class Media:
        js = ("cars/js/carInteriorPoint.js",)

class CarConfigurationAdmin(admin.ModelAdmin):
    inlines = [CarInteriorPointInline]

    def get_form(self, request, obj=None, **kwargs):
        if 'only_inlines' in request.GET and obj:
            kwargs.update({'form':CarConfigurationForm})
            return super(CarConfigurationAdmin, self).get_form(request, obj, **kwargs)
        return super(CarConfigurationAdmin, self).get_form(request, obj, **kwargs)

    def response_change(self, request, obj):
        if admin.options.IS_POPUP_VAR in request.POST:
            return SimpleTemplateResponse('admin/cars/carconfiguration/popup_response.html',{})
        return super(CarConfigurationAdmin, self).response_change(request, obj)

class CarTechnologyPointInline(admin.StackedInline):
    model = CarTechnologyPoint
    classes = ('grp-collapse grp-open',)
    form = CarTechnologyPointAdminForm
    extra = 1

    class Media:
        js = ("cars/js/carTechnologyPoint.js",)

class CarTechnologyAdmin(admin.ModelAdmin):
    inlines = [CarTechnologyPointInline]

    def get_form(self, request, obj=None, **kwargs):
        if 'only_inlines' in request.GET and obj:
            kwargs.update({'form':CarTechnologyForm})
            return super(CarTechnologyAdmin, self).get_form(request, obj, **kwargs)
        return super(CarTechnologyAdmin, self).get_form(request, obj, **kwargs)
    
    def response_change(self, request, obj):
        if 'only_inlines' in request.GET and admin.options.IS_POPUP_VAR in request.POST:
            return SimpleTemplateResponse('admin/cars/cartechnology/popup_response.html', {})
        return super(CarTechnologyAdmin, self).response_change(request, obj)

class CarInStockAdmin(admin.ModelAdmin):
    list_display = ('car','configuration_display', 'color_display', 'engine', 'year','price')
    fields = ('car',('configuration', 'configuration_text'),('color', 'color_text'),'colorInterior','engine', 'year', 'price', 'options', 'profit', 'in_promo')
    add_fields = ('car',)
    form = CarInStockAdminForm
    list_select_related = True

    def configuration_display(self, obj):
        return obj.configuration_display
    configuration_display.short_description = u'Комплектация'

    def color_display(self, obj):
        return obj.color_display
    color_display.short_description = u'Цвет кузова'

    def get_fieldsets(self, request, obj=None):
        if obj is None:
            return ((None,{'fields':self.add_fields}),)
        return super(CarInStockAdmin, self).get_fieldsets(request, obj)
    
    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ('car',)
        return super(CarInStockAdmin, self).get_readonly_fields(request, obj)

admin.site.register(Car, CarAdmin)
admin.site.register(CarConfiguration, CarConfigurationAdmin)
admin.site.register(CarTechnology, CarTechnologyAdmin)
admin.site.register(CarInStock, CarInStockAdmin)