# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'CarOthers.meta_title'
        db.add_column(u'cars_carothers', 'meta_title',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'CarOthers.meta_keywords'
        db.add_column(u'cars_carothers', 'meta_keywords',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'CarOthers.meta_description'
        db.add_column(u'cars_carothers', 'meta_description',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'CarOthers.meta_title'
        db.delete_column(u'cars_carothers', 'meta_title')

        # Deleting field 'CarOthers.meta_keywords'
        db.delete_column(u'cars_carothers', 'meta_keywords')

        # Deleting field 'CarOthers.meta_description'
        db.delete_column(u'cars_carothers', 'meta_description')


    models = {
        u'cars.car': {
            'Meta': {'object_name': 'Car'},
            'accessories': ('snippets.core.fields.FileField', [], {'max_length': '255', 'blank': 'True'}),
            'catalog_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'characteristics': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'max_length': '100'}),
            'configurator_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'emission_co2': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'fulltext': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'img_euroncap': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'img_exterior_bg': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'img_exterior_point_back': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'img_exterior_point_front': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'interior_space': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'luggage_space': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['cars.Car']"}),
            'petrol_consumption': ('django.db.models.fields.FloatField', [], {'null': 'True'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'price_to': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'view360': ('snippets.core.fields.FileField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'cars.carcolor': {
            'Meta': {'object_name': 'CarColor'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'colors'", 'to': u"orm['cars.Car']"}),
            'color': ('colorful.fields.RGBColorField', [], {'max_length': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img_back': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'img_front': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'cars.carconfiguration': {
            'Meta': {'object_name': 'CarConfiguration'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'configurations'", 'to': u"orm['cars.Car']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'title'"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'cars.carconfigurationprice': {
            'Meta': {'object_name': 'CarConfigurationPrice'},
            'car': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'pricelist'", 'unique': 'True', 'to': u"orm['cars.Car']"}),
            'content': ('tinymce.models.HTMLField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'cars.carexteriorpoint': {
            'Meta': {'ordering': "['view', 'title']", 'object_name': 'CarExteriorPoint'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'exterior_points'", 'to': u"orm['cars.Car']"}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'fulltext': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'position': ('cars.fields.PositionField', [], {'max_length': "'50'"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'view': ('django.db.models.fields.CharField', [], {'default': "'front'", 'max_length': "'10'"})
        },
        u'cars.cargallery': {
            'Meta': {'object_name': 'CarGallery'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['cars.Car']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'cars.carinstock': {
            'Meta': {'ordering': "['car']", 'object_name': 'CarInStock'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cars.Car']"}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cars.CarColor']", 'null': 'True', 'blank': 'True'}),
            'colorInterior': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'color_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'configuration': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cars.CarConfiguration']", 'null': 'True', 'blank': 'True'}),
            'configuration_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'engine': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_promo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'options': ('django.db.models.fields.TextField', [], {}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'profit': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'year': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2016'})
        },
        u'cars.carinteriorpoint': {
            'Meta': {'ordering': "['title']", 'object_name': 'CarInteriorPoint'},
            'configuration': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'interior_points'", 'to': u"orm['cars.CarConfiguration']"}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'fulltext': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'position': ('cars.fields.PositionField', [], {'max_length': "'50'"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'cars.carothers': {
            'Meta': {'ordering': "['position']", 'unique_together': "(('car', 'slug'),)", 'object_name': 'CarOthers'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'others'", 'to': u"orm['cars.Car']"}),
            'content': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'html': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'meta_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'meta_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'cars.cartechnology': {
            'Meta': {'object_name': 'CarTechnology'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'technologies'", 'to': u"orm['cars.Car']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'kind': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'cars.cartechnologypoint': {
            'Meta': {'ordering': "['title']", 'object_name': 'CarTechnologyPoint'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'fulltext': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'position': ('cars.fields.PositionField', [], {'max_length': "'50'"}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'points'", 'to': u"orm['cars.CarTechnology']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['cars']