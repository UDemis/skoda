# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Car'
        db.create_table(u'cars_car', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=255, blank=True)),
            ('price', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('price_to', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('interior_space', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('luggage_space', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('petrol_consumption', self.gf('django.db.models.fields.FloatField')(null=True)),
            ('emission_co2', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('characteristics', self.gf('django.db.models.fields.CommaSeparatedIntegerField')(max_length=100)),
            ('configurator_link', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('catalog_link', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('fulltext', self.gf('tinymce.models.HTMLField')(blank=True)),
            ('view360', self.gf('snippets.core.fields.FileField')(max_length=255, blank=True)),
            ('accessories', self.gf('snippets.core.fields.FileField')(max_length=255, blank=True)),
            ('img', self.gf('snippets.core.fields.ImageField')(max_length=255)),
            ('img_exterior_bg', self.gf('snippets.core.fields.ImageField')(max_length=255, blank=True)),
            ('img_exterior_point_front', self.gf('snippets.core.fields.ImageField')(max_length=255, blank=True)),
            ('img_exterior_point_back', self.gf('snippets.core.fields.ImageField')(max_length=255, blank=True)),
            ('parent', self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='children', null=True, to=orm['cars.Car'])),
            (u'lft', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            (u'rght', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            (u'tree_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            (u'level', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
        ))
        db.send_create_signal(u'cars', ['Car'])

        # Adding model 'CarColor'
        db.create_table(u'cars_carcolor', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(related_name='colors', to=orm['cars.Car'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('color', self.gf('colorful.fields.RGBColorField')(max_length=7)),
            ('img_front', self.gf('snippets.core.fields.ImageField')(max_length=255)),
            ('img_back', self.gf('snippets.core.fields.ImageField')(max_length=255)),
        ))
        db.send_create_signal(u'cars', ['CarColor'])

        # Adding model 'CarConfiguration'
        db.create_table(u'cars_carconfiguration', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(related_name='configurations', to=orm['cars.Car'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('slug', self.gf('autoslug.fields.AutoSlugField')(unique_with=(), max_length=50, populate_from='title')),
            ('price', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('img', self.gf('snippets.core.fields.ImageField')(max_length=255)),
        ))
        db.send_create_signal(u'cars', ['CarConfiguration'])

        # Adding model 'CarTechnology'
        db.create_table(u'cars_cartechnology', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(related_name='technologies', to=orm['cars.Car'])),
            ('kind', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('img', self.gf('snippets.core.fields.ImageField')(max_length=255)),
        ))
        db.send_create_signal(u'cars', ['CarTechnology'])

        # Adding model 'CarGallery'
        db.create_table(u'cars_cargallery', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(related_name='images', to=orm['cars.Car'])),
            ('img', self.gf('snippets.core.fields.ImageField')(max_length=255)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
        ))
        db.send_create_signal(u'cars', ['CarGallery'])

        # Adding model 'CarExteriorPoint'
        db.create_table(u'cars_carexteriorpoint', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('fulltext', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('img', self.gf('snippets.core.fields.ImageField')(max_length=255)),
            ('position', self.gf('cars.fields.PositionField')(max_length='50')),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(related_name='exterior_points', to=orm['cars.Car'])),
            ('view', self.gf('django.db.models.fields.CharField')(default='front', max_length='10')),
        ))
        db.send_create_signal(u'cars', ['CarExteriorPoint'])

        # Adding model 'CarInteriorPoint'
        db.create_table(u'cars_carinteriorpoint', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('fulltext', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('img', self.gf('snippets.core.fields.ImageField')(max_length=255)),
            ('position', self.gf('cars.fields.PositionField')(max_length='50')),
            ('configuration', self.gf('django.db.models.fields.related.ForeignKey')(related_name='interior_points', to=orm['cars.CarConfiguration'])),
        ))
        db.send_create_signal(u'cars', ['CarInteriorPoint'])

        # Adding model 'CarTechnologyPoint'
        db.create_table(u'cars_cartechnologypoint', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('fulltext', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('img', self.gf('snippets.core.fields.ImageField')(max_length=255)),
            ('position', self.gf('cars.fields.PositionField')(max_length='50')),
            ('technology', self.gf('django.db.models.fields.related.ForeignKey')(related_name='points', to=orm['cars.CarTechnology'])),
        ))
        db.send_create_signal(u'cars', ['CarTechnologyPoint'])

        # Adding model 'CarConfigurationPrice'
        db.create_table(u'cars_carconfigurationprice', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('car', self.gf('django.db.models.fields.related.OneToOneField')(related_name='pricelist', unique=True, to=orm['cars.Car'])),
            ('content', self.gf('tinymce.models.HTMLField')()),
            ('img', self.gf('snippets.core.fields.ImageField')(max_length=255, blank=True)),
        ))
        db.send_create_signal(u'cars', ['CarConfigurationPrice'])

        # Adding model 'CarOthers'
        db.create_table(u'cars_carothers', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(related_name='others', to=orm['cars.Car'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('link', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('content', self.gf('tinymce.models.HTMLField')(blank=True)),
            ('img', self.gf('snippets.core.fields.ImageField')(max_length=255, blank=True)),
        ))
        db.send_create_signal(u'cars', ['CarOthers'])

        # Adding unique constraint on 'CarOthers', fields ['car', 'slug']
        db.create_unique(u'cars_carothers', ['car_id', 'slug'])

        # Adding model 'CarInStock'
        db.create_table(u'cars_carinstock', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cars.Car'])),
            ('configuration', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cars.CarConfiguration'], null=True)),
            ('color', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cars.CarColor'], null=True)),
            ('colorInterior', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('engine', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('year', self.gf('django.db.models.fields.PositiveIntegerField')(default=2014)),
            ('price', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal(u'cars', ['CarInStock'])


    def backwards(self, orm):
        # Removing unique constraint on 'CarOthers', fields ['car', 'slug']
        db.delete_unique(u'cars_carothers', ['car_id', 'slug'])

        # Deleting model 'Car'
        db.delete_table(u'cars_car')

        # Deleting model 'CarColor'
        db.delete_table(u'cars_carcolor')

        # Deleting model 'CarConfiguration'
        db.delete_table(u'cars_carconfiguration')

        # Deleting model 'CarTechnology'
        db.delete_table(u'cars_cartechnology')

        # Deleting model 'CarGallery'
        db.delete_table(u'cars_cargallery')

        # Deleting model 'CarExteriorPoint'
        db.delete_table(u'cars_carexteriorpoint')

        # Deleting model 'CarInteriorPoint'
        db.delete_table(u'cars_carinteriorpoint')

        # Deleting model 'CarTechnologyPoint'
        db.delete_table(u'cars_cartechnologypoint')

        # Deleting model 'CarConfigurationPrice'
        db.delete_table(u'cars_carconfigurationprice')

        # Deleting model 'CarOthers'
        db.delete_table(u'cars_carothers')

        # Deleting model 'CarInStock'
        db.delete_table(u'cars_carinstock')


    models = {
        u'cars.car': {
            'Meta': {'object_name': 'Car'},
            'accessories': ('snippets.core.fields.FileField', [], {'max_length': '255', 'blank': 'True'}),
            'catalog_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'characteristics': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'max_length': '100'}),
            'configurator_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'emission_co2': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'fulltext': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'img_exterior_bg': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'img_exterior_point_back': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'img_exterior_point_front': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'interior_space': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'luggage_space': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['cars.Car']"}),
            'petrol_consumption': ('django.db.models.fields.FloatField', [], {'null': 'True'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'price_to': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'view360': ('snippets.core.fields.FileField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'cars.carcolor': {
            'Meta': {'object_name': 'CarColor'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'colors'", 'to': u"orm['cars.Car']"}),
            'color': ('colorful.fields.RGBColorField', [], {'max_length': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img_back': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'img_front': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'cars.carconfiguration': {
            'Meta': {'object_name': 'CarConfiguration'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'configurations'", 'to': u"orm['cars.Car']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'title'"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'cars.carconfigurationprice': {
            'Meta': {'object_name': 'CarConfigurationPrice'},
            'car': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'pricelist'", 'unique': 'True', 'to': u"orm['cars.Car']"}),
            'content': ('tinymce.models.HTMLField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'cars.carexteriorpoint': {
            'Meta': {'ordering': "['view', 'title']", 'object_name': 'CarExteriorPoint'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'exterior_points'", 'to': u"orm['cars.Car']"}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'fulltext': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'position': ('cars.fields.PositionField', [], {'max_length': "'50'"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'view': ('django.db.models.fields.CharField', [], {'default': "'front'", 'max_length': "'10'"})
        },
        u'cars.cargallery': {
            'Meta': {'object_name': 'CarGallery'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['cars.Car']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'cars.carinstock': {
            'Meta': {'object_name': 'CarInStock'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cars.Car']"}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cars.CarColor']", 'null': 'True'}),
            'colorInterior': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'configuration': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cars.CarConfiguration']", 'null': 'True'}),
            'engine': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'year': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2014'})
        },
        u'cars.carinteriorpoint': {
            'Meta': {'ordering': "['title']", 'object_name': 'CarInteriorPoint'},
            'configuration': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'interior_points'", 'to': u"orm['cars.CarConfiguration']"}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'fulltext': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'position': ('cars.fields.PositionField', [], {'max_length': "'50'"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'cars.carothers': {
            'Meta': {'unique_together': "(('car', 'slug'),)", 'object_name': 'CarOthers'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'others'", 'to': u"orm['cars.Car']"}),
            'content': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'cars.cartechnology': {
            'Meta': {'object_name': 'CarTechnology'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'technologies'", 'to': u"orm['cars.Car']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'kind': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'cars.cartechnologypoint': {
            'Meta': {'ordering': "['title']", 'object_name': 'CarTechnologyPoint'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'fulltext': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'position': ('cars.fields.PositionField', [], {'max_length': "'50'"}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'points'", 'to': u"orm['cars.CarTechnology']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['cars']