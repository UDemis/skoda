# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from snippets.pages.site import site, PageType
from cars.models import Car, CarInStock

class CarPageType(PageType):

    def get_path(self, *args, **kwargs):
        return reverse('cars_car_list')

class CarInStockPageType(PageType):

    def get_path(self, *args, **kwargs):
        return reverse('cars_carinstock_list')

site.register(Car, CarPageType)
site.register(CarInStock, CarInStockPageType)