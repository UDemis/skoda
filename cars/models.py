# -*- coding: utf-8 -*-
from django.db import models
from django.utils.timezone import now
from mptt.models import MPTTModel, TreeForeignKey
from snippets.core.fields import ImageField, FileField, RichTextField
from colorful.fields import RGBColorField
from autoslug import AutoSlugField
from cars.fields import PositionField


class Car(MPTTModel):
    CHARACTERISTIC_CHOICES = (
        (0, u'Хетчбек'),
        (1, u'Седан/Лифтбек'),
        (2, u'Универсал'),
        (3, u'Универсал/Минивэн'),
        (4, u'Кроссовер/Внедорожник'),
        (5, u'Спорт'),
        (6, u'4x4'),
    )
    title = models.CharField(u'Название', max_length=255)
    slug = models.SlugField(u'Путь', max_length=255, blank=True)
    price = models.PositiveIntegerField(u'Цена (от), р', null=True)
    price_to = models.PositiveIntegerField(u'Цена (до), р', null=True)
    interior_space = models.PositiveIntegerField(u'Объем салона, л', null=True)
    luggage_space = models.PositiveIntegerField(u'Объем багажного отделения, л', null=True)
    petrol_consumption = models.FloatField(u'Расход топлива, л/100км', null=True)
    emission_co2 = models.PositiveIntegerField(u'Выброс СО2, г/км', null=True)
    characteristics = models.CommaSeparatedIntegerField(u'Характеристики', max_length=100)
    configurator_link = models.URLField(u'Ссылка на конфигуратор', blank=True)
    catalog_link = models.URLField(u'Ссылка на каталог', blank=True)
    description = models.TextField(u'Краткое описание', blank=True)
    fulltext = RichTextField(u'Полное описание', blank=True)
    view360 = FileField(u'Вид 360°', max_length=255, upload_to='cars', blank=True)
    accessories = FileField(u'Аксессуары', max_length=255, upload_to='cars', blank=True)
    img = ImageField(u'Изображение (для меню)', max_length=255, upload_to='cars')
    img_exterior_bg = ImageField(u'Фоновое изображение для внешнего вида', max_length=255, upload_to='cars', blank=True)
    img_exterior_point_front = ImageField(u'Внешний вид, спереди', max_length=255, upload_to='cars', blank=True)
    img_exterior_point_back = ImageField(u'Внешний вид, сзади', max_length=255, upload_to='cars', blank=True)
    img_euroncap = ImageField(u'Изображение(Euroncap)', max_length=255, upload_to='cars', blank=True)

    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')

    class Meta:
        verbose_name = u'Модель'
        verbose_name_plural = u'Автомобили'

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('cars_car_detail', (), {'slug': self.slug})

    @models.permalink
    def get_exterior_url(self):
        return ('cars_carexterior_detail', (), {'slug': self.slug})

    @models.permalink
    def get_interior_url(self):
        return ('cars_carinterior_detail', (), {'slug': self.slug})

    @models.permalink
    def get_view360_url(self):
        return ('cars_view360_detail', (), {'slug': self.slug})

    @models.permalink
    def get_gallery_url(self):
        return ('cars_gallery_detail', (), {'slug': self.slug})

    @models.permalink
    def get_technology_url(self):
        return ('cars_technology_detail', (), {'slug': self.slug})

    @models.permalink
    def get_pricelist_url(self):
        return ('cars_pricelist_detail', (), {'slug': self.slug})

    def get_exterior_points_front(self):
        return self.exterior_points.filter(view='front')

    def get_exterior_points_back(self):
        return self.exterior_points.filter(view='back')

    def has_exterior(self):
        return self.img_exterior_point_front and self.img_exterior_point_back

    def has_configurations(self):
        return self.configurations.count() > 0

    def has_technologies(self):
        return self.technologies.count() > 0

    def has_gallery(self):
        return self.images.count() > 0

    def has_pricelist(self):
        return hasattr(self, 'pricelist')

    def has_others(self):
        return self.others.count() > 0


class CarColor(models.Model):
    car = models.ForeignKey(Car, verbose_name=u'Модель', related_name='colors')
    title = models.CharField(u'Название', max_length=255)
    color = RGBColorField(u'Цвет')
    img_front = ImageField(u'Вид спереди', max_length=255, upload_to='cars')
    img_back = ImageField(u'Вид сзади', max_length=255, upload_to='cars')

    class Meta:
        verbose_name = u'Цвет'
        verbose_name_plural = u'Цвета'

    def __unicode__(self):
        return self.title


class CarConfiguration(models.Model):
    car = models.ForeignKey(Car, verbose_name=u'Модель', related_name='configurations')
    title = models.CharField(u'Название', max_length=255)
    slug = AutoSlugField(populate_from='title')
    price = models.PositiveIntegerField(u'Цена, р', null=True)
    img = ImageField(u'Фото интерьера', max_length=255, upload_to='cars')

    class Meta:
        verbose_name = u'Комплектация'
        verbose_name_plural = u'Комплектации'

    def __unicode__(self):
        return self.title


class CarTechnology(models.Model):
    KIND_CHOICES = (
        ('comfort', u'Комфорт'),
        ('ecology', u'Экологичность'),
        ('optic', u'Оптика'),
        ('luggage', u'Объем багажного отделения'),
        ('engine', u'Двигатель'),
        ('audio-and-navigation', u'Аудиосистема и навигация'),
        ('safety', u'Безопасность'),
    )
    car = models.ForeignKey(Car, verbose_name=u'Модель', related_name='technologies')
    kind = models.CharField(u'Вид', max_length=50, choices=KIND_CHOICES)
    img = ImageField(u'Изображение', max_length=255, upload_to='cars')

    class Meta:
        verbose_name = u'Технология'
        verbose_name_plural = u'Технологии'

    def __unicode__(self):
        return self.get_kind_display()


class CarGallery(models.Model):
    car = models.ForeignKey(Car, verbose_name=u'Модель', related_name='images')
    img = ImageField(u'Изображение', max_length=255, upload_to='cars')
    title = models.CharField(u'Описание', max_length=255, blank=True)

    class Meta:
        verbose_name = u'Изображение'
        verbose_name_plural = u'Галерея'

    def __unicode__(self):
        return self.img


class PointBase(models.Model):
    title = models.CharField(u'Название', max_length=255)
    description = models.TextField(u'Краткое описание', blank=True)
    fulltext = models.TextField(u'Полное описание', blank=True)
    img = ImageField(u'Изображение', max_length=255, upload_to='cars')
    position = PositionField(u'Позиция', max_length='50')

    class Meta:
        abstract = True

    def __unicode__(self):
        return self.title

    @property
    def left(self):
        return self.position.split(',')[0]

    @property
    def top(self):
        return self.position.split(',')[-1]

    def _get_next_prev_qs(self):
        return self._meta.model.objects.all()

    def get_next(self):
        qs = self._get_next_prev_qs()
        try:
            next_obj = qs.filter(pk__gt=self.pk).order_by('pk')[:1][0]
        except IndexError:
            try:
                next_obj = qs.exclude(pk=self.pk).order_by('pk')[:1][0]
            except IndexError:
                return None
        return next_obj

    def get_prev(self):
        qs = self._get_next_prev_qs()
        try:
            prev_obj = qs.filter(pk__lt=self.pk).order_by('-pk')[:1][0]
        except IndexError:
            try:
                prev_obj = qs.exclude(pk=self.pk).order_by('-pk')[:1][0]
            except IndexError:
                return None
        return prev_obj


class CarExteriorPoint(PointBase):
    VIEW_CHOICES = (
        ('front', u'Спереди'),
        ('back', u'Сзади'),
    )
    car = models.ForeignKey(Car, verbose_name=u'Модель', related_name='exterior_points')
    view = models.CharField(u'Вид', max_length='10', choices=VIEW_CHOICES, default=VIEW_CHOICES[0][0])

    class Meta:
        verbose_name = u'Описание элемента'
        verbose_name_plural = u'Внешний вид'
        ordering = ['view', 'title']

    @models.permalink
    def get_absolute_url(self):
        return ('cars_carexteriorpoint_detail', (), {'car': self.car.slug, 'pk': self.pk})


    def _get_next_prev_qs(self):
        return self._meta.model.objects.filter(car=self.car, view=self.view)


class CarInteriorPoint(PointBase):
    configuration = models.ForeignKey(CarConfiguration, verbose_name=u'Комплектация', related_name='interior_points')

    class Meta:
        verbose_name = u'Описание элемента'
        verbose_name_plural = u'Салон'
        ordering = ['title']

    @models.permalink
    def get_absolute_url(self):
        return ('cars_carinteriorpoint_detail', (), {'car': self.configuration.car.slug,
                                                     'slug': self.configuration.slug,
                                                     'pk': self.pk})

    def _get_next_prev_qs(self):
        return self._meta.model.objects.filter(configuration=self.configuration)


class CarTechnologyPoint(PointBase):
    technology = models.ForeignKey(CarTechnology, verbose_name=u'Технология', related_name='points')

    class Meta:
        verbose_name = u'Описание элемента'
        verbose_name_plural = u'Описание элементов'
        ordering = ['title']

    @models.permalink
    def get_absolute_url(self):
        return ('cars_technologypoint_detail', (), {'car': self.technology.car.slug,
                                                    'slug': self.technology.kind,
                                                    'pk': self.pk})


    def _get_next_prev_qs(self):
        return self._meta.model.objects.filter(technology=self.technology)


class CarConfigurationPrice(models.Model):
    car = models.OneToOneField(Car, verbose_name=u'Модель', limit_choices_to={'level': 1}, related_name='pricelist')
    content = RichTextField(u'Текст')
    img = ImageField(u'Изображение', max_length=255, upload_to='cars', blank=True)

    class Meta:
        verbose_name = u'Содержание'
        verbose_name_plural = u'Комплектации и цены'

    def get_absolute_url(self):
        return self.car.get_pricelist_url()

    @property
    def title(self):
        return '%s %s' % (self.car.title, self._meta.verbose_name_plural)

    def __unicode__(self):
        return self.title


class CarOthers(models.Model):
    car = models.ForeignKey(Car, verbose_name=u'Модель', limit_choices_to={'level': 1}, related_name='others')
    title = models.CharField(u'Название', max_length=255)
    slug = models.SlugField(u'Путь')
    link = models.URLField(u'Внешняя ссылка', blank=True)
    content = RichTextField(u'Текст', blank=True)
    html = models.TextField(u'HTML код', blank=True)
    img = ImageField(u'Изображение', max_length=255, upload_to='cars', blank=True)
    position = models.PositiveSmallIntegerField(u"Позиция")
    meta_title = models.CharField(u'Заголовок страницы', max_length=255, blank=True)
    meta_keywords = models.CharField(u'Keywords', max_length=255, blank=True)
    meta_description = models.TextField(u'Description', blank=True)

    class Meta:
        verbose_name = u'Содержание'
        verbose_name_plural = u'Иное описание'
        unique_together = (('car', 'slug'),)
        ordering = ['position']

    @models.permalink
    def _get_absolute_url(self):
        return ('cars_others_detail', (), {'car': self.car.slug,
                                           'slug': self.slug})

    def get_absolute_url(self):
        if self.link:
            return self.link
        return self._get_absolute_url()

    def __unicode__(self):
        return self.title


class CarInStock(models.Model):
    car = models.ForeignKey(Car, verbose_name=u'Модель', limit_choices_to={'level': 1})
    configuration = models.ForeignKey(CarConfiguration, verbose_name=u'Комплектация', null=True, blank=True)
    configuration_text = models.CharField(u'Другая', max_length=255, blank=True)
    color = models.ForeignKey(CarColor, verbose_name=u'Цвет кузова', null=True, blank=True)
    color_text = models.CharField(u'Другой', max_length=255, blank=True)
    colorInterior = models.CharField(u'Цвет салона', max_length=255)
    engine = models.CharField(u'Двигатель, КПП', max_length=255)
    year = models.PositiveIntegerField(u'Год выпуска', default=now().year)
    price = models.PositiveIntegerField(u'Цена, р', default=0)
    options = models.TextField(u'Опции')
    profit = models.PositiveIntegerField(u'Выгода, р', default=0)
    in_promo = models.BooleanField(u'Отображать в промо разделе', default=False)

    class Meta:
        ordering = ['car']
        verbose_name = u'Автомобиль'
        verbose_name_plural = u'Автомобили в наличии'

    @property
    def configuration_display(self):
        if self.configuration:
            return self.configuration
        return self.configuration_text

    @property
    def color_display(self):
        if self.color:
            return self.color
        return self.color_text

    def __unicode__(self):
        return '%s %s' % (self.car.title, self.configuration_display)