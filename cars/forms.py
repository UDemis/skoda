# -*- coding: utf-8 -*-
from django import forms
from django.core.urlresolvers import reverse
from cars.models import Car, CarExteriorPoint, CarInteriorPoint, CarTechnology, CarTechnologyPoint, CarConfiguration, \
                        CarInStock
from cars.widgets import PositionWidget, LinkWidget, SelectReadOnlyWidget, CharacteristicsWidget

class CarAdminForm(forms.ModelForm):

    class Meta:
        model = Car

    def __init__(self, *args, **kwargs):
        super(CarAdminForm, self).__init__(*args, **kwargs)
        self.fields['parent'].widget = forms.HiddenInput()
        self.fields['parent'].label = ''
        if 'characteristics' in self.fields:
            self.fields['characteristics'].widget = CharacteristicsWidget(choices=Car.CHARACTERISTIC_CHOICES)

class CarExteriorPointAdminForm(forms.ModelForm):
    position = forms.CharField(label=u'Позиция', widget=PositionWidget(bg_img_width=600, bg_img_height=300))

    class Meta:
        model = CarExteriorPoint

class CarConfigurationForm(forms.ModelForm):
    img = forms.CharField(widget=forms.HiddenInput(), label='')

    class Meta:
        model = CarConfiguration
        fields = ['img']

class CarConfigurationInlineForm(forms.ModelForm):
    link = forms.CharField(label=u'Элементы', required=False,
                           widget=LinkWidget(u'Редактировать элементы', attrs={'onclick':'return showInPopup(this);'}))

    class Meta:
        model = CarConfiguration

    def __init__(self, *args, **kwargs):
        super(CarConfigurationInlineForm, self).__init__(*args, **kwargs)
        if 'instance' in kwargs:
            instance = kwargs['instance']
            url = reverse('admin:cars_carconfiguration_change', args=(instance.pk,))
            self.fields['link'].widget.attrs['href'] = '%s?only_inlines=1' % url
        else:
            self.fields['link'].widget.hidden = True


class CarInteriorPointAdminForm(forms.ModelForm):
    position = forms.CharField(label=u'Позиция', widget=PositionWidget(bg_img_width=940, bg_img_height=470))

    class Meta:
        model = CarInteriorPoint

class CarTechnologyInlineForm(forms.ModelForm):
    link = forms.CharField(label=u'Элементы', required=False,
                           widget=LinkWidget(u'Редактировать элементы', attrs={'onclick':'return showInPopup(this);'}))
    class Meta:
        model = CarTechnology

    def __init__(self, *args, **kwargs):
        super(CarTechnologyInlineForm, self).__init__(*args, **kwargs)
        self.fields['kind'].widget = SelectReadOnlyWidget(choices=CarTechnology.KIND_CHOICES)
        if 'instance' in kwargs:
            instance = kwargs['instance']
            url = reverse('admin:cars_cartechnology_change', args=(instance.pk,))
            self.fields['link'].widget.attrs['href'] = '%s?only_inlines=1' % url
        else:
            self.fields['link'].widget.hidden = True

class CarTechnologyForm(forms.ModelForm):
    img = forms.CharField(widget=forms.HiddenInput(), label='')

    class Meta:
        model = CarTechnology
        fields = ['img']

class CarTechnologyPointAdminForm(forms.ModelForm):
    position = forms.CharField(label=u'Позиция', widget=PositionWidget(bg_img_width=940, bg_img_height=430))

    class Meta:
        model = CarTechnologyPoint


class CarInStockAdminForm(forms.ModelForm):

    class Meta:
        model = CarInStock

    def __init__(self, *args, **kwargs):
        super(CarInStockAdminForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            qs = self.fields['configuration'].queryset
            self.fields['configuration'].queryset = qs.filter(car=self.instance.car)
            qs = self.fields['color'].queryset
            self.fields['color'].queryset = qs.filter(car=self.instance.car)

    def clean(self):
        cd = super(CarInStockAdminForm, self).clean()
        if 'configuration' in self.fields and not cd.get('configuration') and not cd.get('configuration_text'):
            self._errors["configuration"] = self.error_class([u'Заполните одно из полей'])
            self._errors["configuration_text"] = self.error_class([''])
            del cd['configuration']
            del cd['configuration_text']
        if 'color' in self.fields and not cd.get('color') and not cd.get('color_text'):
            self._errors["color"] = self.error_class([u'Заполните одно из полей'])
            self._errors["color_text"] = self.error_class([''])
            del cd['color']
            del cd['color_text']
        return cd
