# -*- coding: utf-8 -*-
from haystack import indexes
from cars.models import Car, CarInteriorPoint, CarExteriorPoint, CarTechnologyPoint, CarConfigurationPrice, CarOthers

class CarIndex(indexes.SearchIndex, indexes.Indexable):
        text = indexes.CharField(document=True, use_template=True)
        title = indexes.CharField(model_attr='title')

        def get_model(self):
            return Car

        def index_queryset(self, using=None):
            return self.get_model().objects.filter(level=1)


class CarPriceListIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')

    def get_model(self):
        return CarConfigurationPrice

    def index_queryset(self, using=None):
        return self.get_model().objects.all()


class CarInteriorPointIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')

    def get_model(self):
        return CarInteriorPoint

    def index_queryset(self, using=None):
        return self.get_model().objects.all()

class CarExteriorPointIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')

    def get_model(self):
        return CarExteriorPoint

    def index_queryset(self, using=None):
        return self.get_model().objects.all()

class CarTechnologyPointIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')

    def get_model(self):
        return CarTechnologyPoint

    def index_queryset(self, using=None):
        return self.get_model().objects.all()

class CarOthersIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')

    def get_model(self):
        return CarOthers

    def index_queryset(self, using=None):
        return self.get_model().objects.all()