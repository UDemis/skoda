# -*- coding: utf-8 -*-
from django.db.models import CommaSeparatedIntegerField

class PositionField(CommaSeparatedIntegerField):
    pass

try:
    from south.modelsinspector import add_introspection_rules
    add_introspection_rules([], ["^cars\.fields\.PositionField"])
except ImportError:
    pass