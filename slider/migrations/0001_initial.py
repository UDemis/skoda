# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Slide'
        db.create_table(u'slider_slide', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('img', self.gf('snippets.core.fields.ImageField')(max_length=255)),
            ('img_preview', self.gf('snippets.core.fields.ImageField')(max_length=255, blank=True)),
            ('position', self.gf('django.db.models.fields.CommaSeparatedIntegerField')(max_length=50)),
        ))
        db.send_create_signal(u'slider', ['Slide'])


    def backwards(self, orm):
        # Deleting model 'Slide'
        db.delete_table(u'slider_slide')


    models = {
        u'slider.slide': {
            'Meta': {'object_name': 'Slide'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'img_preview': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'position': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['slider']