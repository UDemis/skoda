# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Slide.order'
        db.add_column(u'slider_slide', 'order',
                      self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1, db_index=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Slide.order'
        db.delete_column(u'slider_slide', 'order')


    models = {
        u'slider.slide': {
            'Meta': {'ordering': "['order']", 'object_name': 'Slide'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'img_preview': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1', 'db_index': 'True'}),
            'position': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['slider']