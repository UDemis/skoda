# -*- coding: utf-8 -*-
from django import template
from slider.models import Slide

register = template.Library()

@register.inclusion_tag("slider/slide_list_tag.html")
def slider():
    slides = Slide.objects.all()
    return {'object_list':slides}