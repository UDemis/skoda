# -*- coding: utf-8 -*-
from django.db import models
from snippets.core.fields import ImageField

class Sortable(models.Model):
    order = models.PositiveSmallIntegerField(u"Порядок", default=1, db_index=True, editable=False)

    class Meta:
        abstract = True
        ordering = ['order']

    def get_previous(self):
        order_field =  self._meta.get_field_by_name('order')[0]
        try:
            prev_obj = self._get_next_or_previous_by_FIELD(order_field, False)
        except:
            prev_obj = None
        return prev_obj

    def get_next(self):
        order_field =  self._meta.get_field_by_name('order')[0]
        try:
            next_obj = self._get_next_or_previous_by_FIELD(order_field, True)
        except:
            next_obj = None
        return next_obj

    def move_up(self):
        prev_obj = self.get_previous()
        print self, prev_obj
        if prev_obj:
            prev_order = prev_obj.order
            prev_obj.order = self.order
            if self.order == prev_order:
                prev_obj.order += 1
            self.order = prev_order
            self.save()
            prev_obj.save()

    def move_down(self):
        next_obj = self.get_next()
        if next_obj:
            next_order = next_obj.order
            next_obj.order = self.order
            if self.order == next_order:
                next_obj.order -= 1
            self.order = next_order
            self.save()
            next_obj.save()

    def save(self, *args, **kwargs):
        if not self.pk:
            try:
                self.order = self.__class__.objects.aggregate(models.Max('order'))['order__max'] + 1
            except (TypeError, IndexError):
                pass
        super(Sortable, self).save(*args, **kwargs)

class Slide(Sortable):
    title = models.CharField(u'Заголовок', max_length=255)
    url = models.URLField(u'Ссылка')
    img = ImageField('Изображение', max_length=255, upload_to='slider')
    img_preview = ImageField('Изображение (для препросмотра)', max_length=255, upload_to='slider', blank=True)
    position = models.CommaSeparatedIntegerField(u'Позиция', max_length=50)

    class Meta:
        verbose_name = u'Слайд'
        verbose_name_plural = u'Слайдер'
        ordering = ['order']

    def __unicode__(self):
        return self.title

    @property
    def left(self):
        return self.position.split(',')[0]

    @property
    def top(self):
        return self.position.split(',')[-1]
