/**
 * Created by metalpower.
 * Date: 10.02.14
 */
(function($){
    $(function(){
        var mediaUrl = '/media/';
        var imgField = $("#id_img");
        var imgFieldLabel = $("label[for='id_img']").text();
        var $positionFields = $(".position a");
        $positionFields.click(function(e){
            if (!imgField.val()){
                e.stopImmediatePropagation();
                alert('Сначала заполните следующие поля: "'+imgFieldLabel+'"');
            } else {
                $(this).attr('href', mediaUrl+imgField.val());
            }
            e.preventDefault();
        });
    });
})(django.jQuery);