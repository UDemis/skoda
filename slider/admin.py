# -*- coding: utf-8 -*-
from functools import update_wrapper
from django.db import transaction
from django.contrib import admin
from django.contrib.admin.util import unquote
from django.core.urlresolvers import reverse
from django.shortcuts import Http404, HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect
from django.utils.decorators import method_decorator
from django.utils.html import escape
from django.utils.text import force_text
from django.utils.translation import ugettext_lazy as _
from slider.models import Slide
from cars.widgets import PositionWidget

csrf_protect_m = method_decorator(csrf_protect)

class SortableAdmin(admin.ModelAdmin):
    def sortable(self, obj):
        opts = self.model._meta
        info = opts.app_label, opts.model_name
        movedown = '&nbsp;'
        moveup = ''
        if obj.get_next():
            movedown = "<a href='%s'><span class='glyphicon glyphicon-arrow-down'></span></a>" % (reverse('admin:%s_%s_movedown' % info, args=(obj.pk,)),)
        if obj.get_previous():
            moveup = "<a href='%s'><span class='glyphicon glyphicon-arrow-up'></a>" % (reverse('admin:%s_%s_moveup'  % info, args=(obj.pk,)),)
        return ' '.join((movedown, moveup))
    sortable.short_description = u'Действия'
    sortable.allow_tags = True

    class Media:
        css = {'all':('bootstrap/css/bootstrap-glyphicons.css',)}


    def get_urls(self):
        from django.conf.urls import patterns, url

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)
            return update_wrapper(wrapper, view)

        info = self.model._meta.app_label, self.model._meta.model_name

        urlpatterns = super(SortableAdmin, self).get_urls()

        urlpatterns = patterns('',
           url(r'^(.+)/moveup/$',
               wrap(self.move_up),
               name='%s_%s_moveup' % info),
           url(r'^(.+)/movedown/$',
               wrap(self.move_down),
               name='%s_%s_movedown' % info),
           ) + urlpatterns
        return urlpatterns

    @csrf_protect_m
    @transaction.atomic
    def move_up(self, request, object_id, extra_context=None):
        opts = self.model._meta
        obj = self.get_object(request, unquote(object_id))

        if obj is None:
            raise Http404(
                _('%(name)s object with primary key %(key)r does not exist.') %
                {'name': force_text(opts.verbose_name), 'key': escape(object_id)}
            )

        obj.move_up()
        post_url = reverse('admin:%s_%s_changelist' %
                           (opts.app_label, opts.model_name),
                           current_app=self.admin_site.name)
        return HttpResponseRedirect(post_url)

    @csrf_protect_m
    @transaction.atomic
    def move_down(self, request, object_id, extra_context=None):
        opts = self.model._meta
        obj = self.get_object(request, unquote(object_id))

        if obj is None:
            raise Http404(
                _('%(name)s object with primary key %(key)r does not exist.') %
                {'name': force_text(opts.verbose_name), 'key': escape(object_id)}
            )

        obj.move_down()
        post_url = reverse('admin:%s_%s_changelist' %
                           (opts.app_label, opts.model_name),
                           current_app=self.admin_site.name)
        return HttpResponseRedirect(post_url)

class SliderAdmin(SortableAdmin):
    list_display = ('title','url','sortable')

    class Media(SortableAdmin.Media):
        js = ("slider/js/sliderAdminPosition.js",)

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'position':
            kwargs.update({'widget':PositionWidget(bg_img_width=940, bg_img_height=430)})
        return super(SliderAdmin, self).formfield_for_dbfield(db_field, **kwargs)

admin.site.register(Slide, SliderAdmin)