var _selectDealerCenter = 0;
var _selectModel = '';

$(document).ready(function(){
    $("body").on("click", ".test-drive-trigger", function(){
        _selectDealerCenter = $(this).data("select") || 0;
        _selectModel = $(this).data('model') || '';

        $("body").append("<a href='/ajax/getFormTemplate.php?FORM_CODE=TEST_DRIVE' class='fancybox-ajax' id='_kdxTmpTrigger'></a>");

        $("#_kdxTmpTrigger").trigger("click").remove();

        return false;
    });

    $("body").on("click", ".reserve-car-trigger", function(){
        var strXmlID = $(this).data('xml-id') || '';

        $("body").append('<a href="/ajax/getFormTemplate.php?FORM_CODE=RESERVE_AVAILABLE_CAR&XML_ID=' + (strXmlID) + '&TPL=available.car.reserve" class="fancybox-ajax" id="_kdxTmpTrigger"></a>');
        $("#_kdxTmpTrigger").trigger("click").remove();

        return false;
    });

    $('body').on('click', '.available-car-information-trigger', function(){
        var strXmlID = $(this).data('xml-id') || '';

        $("body").append('<a href="/ajax/getFormTemplate.php?FORM_CODE=AVAILABLE_CAR_INFORMATION&XML_ID=' + (strXmlID) + '" class="fancybox-ajax" id="_kdxTmpTrigger"></a>');
        $("#_kdxTmpTrigger").trigger("click").remove();

        return false;
    });

    $("body").on("click", ".service-trigger", function(){
        _selectDealerCenter = $(this).data("select") || 0;
        _selectModel = $(this).data('model') || '';

        $("body").append("<a href='/ajax/getFormTemplate.php?FORM_CODE=SERVICE' class='fancybox-ajax' id='_kdxTmpTrigger'></a>");

        $("#_kdxTmpTrigger").trigger("click").remove();

        return false;
    });

    $("body").on("click", ".feedback-trigger", function(){
        _selectDealerCenter = $(this).data("select") || 0;
        _selectModel = $(this).data('model') || '';

        $("body").append("<a href='/ajax/getFormTemplate.php?FORM_CODE=FEEDBACK' class='fancybox-ajax' id='_kdxTmpTrigger'></a>");

        $("#_kdxTmpTrigger").trigger("click").remove();

        return false;
    });

    $("body").on("click", ".callback-trigger", function(){
        _selectDealerCenter = $(this).data("select") || 0;
        _selectModel = $(this).data('model') || '';

        $("body").append("<a href='/ajax/getFormTemplate.php?FORM_CODE=CALLBACK' class='fancybox-ajax' id='_kdxTmpTrigger'></a>");

        $("#_kdxTmpTrigger").trigger("click").remove();

        return false;
    });
    var url = window.location.pathname;
    var events = {
        'CALLBACK'		:	{
            category		:	'CallbackForm',
            events			:	{
                open			:	'CALLBACKFORMOPEN',
                submit			:	'CALLBACKFORMSUBMIT',
                success			:	'CALLBACKFORMSUCCESS'
            }
        },
        'TEST_DRIVE'	:	{
            category		:	'TestdriveForm',
            events			:	{
                open			:	'TESTDRIVEFORMOPEN',
                submit			:	'TESTDRIVEFORMSUBMIT',
                success			:	'TESTDRIVEFORMSUCCESS'
            }
        },
        'SERVICE'	:	{
            category		:	'MaintenanceForm',
            events			:	{
                open			:	'MAINTENANCEFORMOPEN',
                submit			:	'MAINTENANCEFORMSUBMIT',
                success			:	'MAINTENANCEFORMSUCCESS'
            }
        },
        'FEEDBACK'	:	{
            category		:	'EmailSendForm',
            events			:	{
                open			:	'EMAILSENDFORMOPEN',
                submit			:	'EMAILSENDFORMSUBMIT',
                success			:	'EMAILSENDFORMSUCCESS'
            }
        },
        'VACANCY'	:	{
            category		:	'VacancyForm',
            events			:	{
                submit			:	'VACANCYFORMSUBMIT',
                success			:	'VACANCYFORMSUCCESS'
            }
        },
        'RESERVE_AVAILABLE_CAR'	:	{
            category		:	'AvailNewCarOrderForm',
            events			:	{
                open			:	'AVAILNEWCARORDERFORMOPEN',
                submit			:	'AVAILNEWCARORDERFORMSUBMIT',
                success			:	'AVAILNEWCARORDERFORMSUCCESS'
            }
        },

    };

    function YaSend(YA_label){
        /*if((typeof window['yaCounter33038139']) !== 'undefined' &&
            send_to_kdx){
            yaCounter33038139.reachGoal(YA_label);
        }
        if((typeof window[yam_id]) !== 'undefined' &&
            send_to_dealer){
            window[yam_id].reachGoal(YA_label);
        }*/
    }

    function GaSend(category, action, label, value){
        if(typeof window['ga'] !== 'undefined'){
            if(typeof value === 'undefined'){
                if(send_to_kdx) {
                    ga('KDX.send', 'event', category, action, label);
                }
                if(send_to_dealer){
                    ga('send', 'event', category, action, label);
                }
            }else{
                if(send_to_kdx) {
                    ga('KDX.send', 'event', category, action, label, value);
                }
                if(send_to_dealer){
                    ga('send', 'event', category, action, label, value);

                }
            }
        }
    }

    $('a[href="http://cc-cloud.skoda-auto.com/rus/rus/ru-ru"]').on('click',function(){
        GaSend('GoToConfig', 'click', url);
        YaSend('GOTOCONFIG');
    });

    $('a[href$=".pdf"]').on('click',function(){
        GaSend('PDF', 'Download', $(this).attr('href'));
        YaSend('PDFDOWNLOAD');
    });

    $('a[href="/skoda-world/120-years/"]').on('click', function(){
        GaSend('GoToPromo', 'click', url);
        YaSend('GOTOPROMO');
    });

    $('body').on("kdx-load", "form", function(){
        var strFormCode = $(this).attr('name');

        /*тут обрабатывается открытие формы*/
        if((typeof events[strFormCode]) != 'undefined' &&
            (typeof events[strFormCode].events.open) != 'undefined'
        ){
            GaSend(events[strFormCode].category, 'open', url);
            YaSend(events[strFormCode].events.open);
        }
        /**/

        $(this).parent().kdxForm({
            strMessageWrapper: ( typeof $(this).data('message-wrapper') !== 'undefined' ) ? $(this).data('message-wrapper') : '.form_content',
            strError: $(this).data('error-class') || 'has_error',
            strSubmitClass: '',
            objSubmitOpacity: {
                'ACTIVE':  'Y',
                'OPACITY': .5
            },
            OnBeforeFormSend: function(){
                if((typeof events[strFormCode]) != 'undefined'){
                    GaSend(events[strFormCode].category, 'submit', url);
                    YaSend(events[strFormCode].events.submit);
                }
            },
            OnFormSuccess   : function(){
                if((typeof events[strFormCode]) != 'undefined'){
                    GaSend(events[strFormCode].category, 'success', url);
                    YaSend(events[strFormCode].events.success);
                }
            }
        });

        kdxTools.Form.DatePickerInit(); // Инициализация DatePciker'a

        if(_selectDealerCenter > 0){
            $(".field-DEALER_CENTER select option").removeAttr("selected");
            $(".field-DEALER_CENTER select option[value='" + _selectDealerCenter + "']").prop("selected", true);

            _selectDealerCenter = 0;
        }

        if(_selectModel !== ''){
            $(".field-MODEL_JSON select option").removeAttr("selected");
            $(".field-MODEL_JSON select option[data-model='" + _selectModel + "']").prop("selected", true);

            _selectModel = '';
        }
    });
});