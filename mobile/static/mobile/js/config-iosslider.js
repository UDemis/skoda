$(document).ready(function() {
    // start ios
    $('.ios').iosSlider({
        desktopClickDrag: true,
        snapToChildren: true,
        infiniteSlider: true,
        autoSlide: false,
        autoSlideTimer: 3000,
        autoSlideTransTimer: 500,
        onSliderLoaded: sliderLoadedChange,
        onSlideChange: sliderLoadedChange,
        autoSlideHoverPause: false,
        responsiveSlideContainer: true,
        navSlideSelector: $('.iosSliderButtons .button'),
    });

    // arrows
    $('.ios .arrow.prev').on('click', function(){$(this).parents('.ios').iosSlider('prevSlide');});
    $('.ios .arrow.next').on('click', function(){$(this).parents('.ios').iosSlider('nextSlide');});

    function sliderLoadedChange(args) {

        /* indicator */
        $('.iosSliderButtons .button').removeClass('selected');
        $('.iosSliderButtons .button:eq(' + (args.currentSlideNumber - 1) + ')').addClass('selected');

        if (args.sliderObject.parents('.ios').hasClass('variable_height')) {
            var targetHeight = args.currentSlideObject.innerHeight();
            args.sliderObject.parent().stop().height(targetHeight).find('.slider').height(targetHeight);
        }

    }

    $(window).resize(function(){
        $('.ios').iosSlider('update');
    });

    $('.modelpageFeatures span').click(function(){
        $('.ios').iosSlider('update');
    });

    $.each($('.ios'), function(key, dom){
        if($(dom).find('.item').length === 1) {
            $(dom).iosSlider('destroy');
            $(dom).find('.arrow').remove();
        }
    });
});