//(function($){
$(document).ready(function(){
    var objStorage = {};

    var _kdxForm = {
        init: function(jWrapper, objOptions) {
            _that = this;

            if($(jWrapper).hasClass('loaded'))
                return false;

            $(jWrapper).addClass('loaded');

            var uniqID = 'kdx-form-' + Math.floor(Math.random() * 10000);
            $(jWrapper).find('form').attr('id', uniqID);

            _that.settings = $.extend({
                url:      '/ajax/newFormResult.php',
                jWrapper: $(jWrapper),
                jForm:    $(jWrapper).find("form"),
                strCode:  $(jWrapper).find("form").data('CODE'),
                type:     'POST',
                files:    {},
                strError: 'has_error',
                strMessageWrapper: '',
                strSubmitClass: 'hideme',
                objSubmitOpacity: {
                    'ACTIVE':  'N',
                    'OPACITY': .5
                },

                // Events Area. That may be called from Init.
                OnAfterFormDataCollect: function(){},

                OnBeforeFormSend: function(){},
                OnAfterFormSend:  function(){},

                OnFormSuccess: function(){},
                OnFormError:   function(){}

            }, objOptions);

            $(_that.settings.jWrapper).find("input[type='file']").on("change", function(e){
                var uniqID = $(this).parents('form');
                objStorage[uniqID].files[$(this).attr("name")] = e.target.files[0];
            });

            objStorage[uniqID] = _that.settings;

            $(_that.settings.jForm).on("submit", function(e){
                _that.settings = objStorage[$(this).attr('id')];

                e.preventDefault();
                e.stopPropagation();

                var objCollect = _that.collect();

                _that.settings.OnAfterFormDataCollect(objCollect);
                kdxTools.GetEventHandler('dForm:OnAfterFormDataCollect', _that.settings.strCode, objCollect);

                var formData = new FormData();

                $.each(objCollect, function(key, object){
                    formData.append(object['name'], object['value']);
                });

                $.each(_that.settings.files, function(strFieldName, rawFileData){
                    formData.append(strFieldName, rawFileData);
                });

                $.ajax({
                    url:  _that.settings.url,
                    type: _that.settings.type,

                    dataType: 'JSON',
                    data: formData,

                    processData: false,
                    contentType: false,

                    beforeSend: function(){
                        if(_that.settings.objSubmitOpacity['ACTIVE'] === 'Y')
                            $(_that.settings.jForm).find('input[type="submit"]').fadeTo(100, _that.settings.objSubmitOpacity['OPACITY']);

                        $(_that.settings.jForm).find('input[type="submit"]').addClass(_that.settings.strSubmitClass).prop('disabled', true);

                        _that.settings.OnBeforeFormSend();
                        kdxTools.GetEventHandler('dForm:OnBeforeFormSend', _that.settings.strCode);
                    },

                    success: function(json){
                        $(_that.settings.jForm).find('.' + _that.settings.strError).removeClass(_that.settings.strError);

                        if(_that.settings.objSubmitOpacity['ACTIVE'] === 'Y')
                            $(_that.settings.jForm).find('input[type="submit"]').fadeTo(100, 1);

                        $(_that.settings.jForm).find('input[type="submit"]').removeClass(_that.settings.strSubmitClass).prop('disabled', false);

                        _that.settings.OnAfterFormSend(json);
                        kdxTools.GetEventHandler('dForm:OnAfterFormSend', _that.settings.strCode, json);

                        if(json['STATUS'] === 'success'){
                            _that.settings.OnFormSuccess(json);
                            kdxTools.GetEventHandler('dForm:OnFormSuccess', _that.settings.strCode, json);

                            var strEventName = kdxTools.Form.GetEventName(_that.settings.strCode);

                            /*if( !kdxTools.Form.IsAllEventsPrevented() && !kdxTools.Form.IsSingleEventPrevented(strEventName) ) {

                                kdxTools.Form.GoogleAnalyticsSend({
                                    type: 'event',
                                    name: strEventName,
                                    action: 'Submit',
                                    cat: 'KdxForms'
                                });
                                kdxTools.Form.YandexMetrika.Send(strEventName, {});

                            }*/

                            var target = $(_that.settings.jWrapper);

                            if(_that.settings.strMessageWrapper !== ''){
                                var tmpTarget = $(target).find(_that.settings.strMessageWrapper);

                                if(tmpTarget.length === 1)
                                    target = tmpTarget;
                            }

                            $(target).removeClass('hideme').html("<div class='form_msg'>" + json['MESSAGE'] + "</div>");
                        }
                        else{
                            kdxTools.GetEventHandler('dForm:OnFormError', _that.settings._alias, json);
                            _that.settings.OnFormError(json);

                            if(typeof json['CAPTCHA'] !== 'undefined'){
                                $(_that.settings.jForm).find('input[name="captcha_sid"]').val(json['CAPTCHA']['CODE']);
                                $(_that.settings.jForm).find('.captcha-input').val("");
                                $(_that.settings.jForm).find('.captcha-img').attr("src", json['CAPTCHA']['SRC']);
                            }

                            if(typeof json['ERRORS'] !== 'undefined'){
                                $.each(json['ERRORS'], function(strFieldCode, objFieldDescription){
                                    $(_that.settings.jForm).find(".field-" + strFieldCode).addClass(_that.settings.strError);
                                });
                            }
                        }
                    },

                    error: function(){
                        if(_that.settings.objSubmitOpacity['ACTIVE'] === 'Y')
                            $(_that.settings.jForm).find('input[type="submit"]').fadeTo(100, 1);

                        $(_that.settings.jForm).find('input[type="submit"]').removeClass(_that.settings.strSubmitClass).prop('disabled', false);

                        _that.settings.OnAfterFormSend();
                        kdxTools.GetEventHandler('dForm:OnAfterFormSend', _that.settings.strCode, false);

                        kdxTools.GetEventHandler('dForm:OnFormError', _that.settings.strCode, false);
                        _that.settings.OnFormError();
                    }
                });

            });

            return jWrapper;
        },
        collect: function(){
            return $(this.settings.jForm).serializeArray();
        }
    };

    $.fn.kdxForm = function(objOptions){
        return _kdxForm.init(this, objOptions);
    };
});
//}(jQuery));