$(document).ready(function(){
    $('.btn-choose-car').on('click',function () {
        var self = $(this),
            $url = self.data('car-url'),
            $slug = self.data('car-slug');
        if ($slug)
        {
            $.get($url,'',function(data) {
                $('.available-cars-ajax-wrapper').html(data);
            });
        }
    })
})
