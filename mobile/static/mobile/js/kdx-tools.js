var kdxTools = {
    GetEventHandler: function(){
        var strEventCall = arguments[0] || '';

        if(strEventCall !== ''){
            var objEvent = (strEventCall).split(':');

            if(typeof window[objEvent[0]] === 'object'){
                if(typeof window[objEvent[0]][objEvent[1]] === 'function'){
                    var sortedArguments = [];

                    $.each(arguments, function(key, value){
                        if(key !== 0)
                            sortedArguments.push(value);
                    });

                    window[objEvent[0]][objEvent[1]].apply(null, sortedArguments);
                }
            }
        }
    },
    Countdown: {
        Process: function(strID, onComplete, onStop){
            var intValue = parseInt($("#" + strID).text());
            var strStop  = $("#" + strID).data('stop') || 'N';

            intValue--;

            if(strStop === 'Y')
                onStop();
            else {
                $("#" + strID).text(intValue);

                if (intValue === 0)
                    onComplete();
                else
                    setTimeout(function () {
                        kdxTools.Countdown.Process(strID, onComplete, onStop);
                    }, 1000);
            }
        }
    },
    Form: {
        objDefaultEventNames: {
            SERVICE: 'Service',
            TEST_DRIVE: 'TestDrive',
            CALLBACK: 'Callback',
            FEEDBACK: 'Feedback',
            VACANCY: 'Vacancy',
            AVAILABLE_CAR_INFORMATION: 'AVNInformation',
            RESERVE_AVAILABLE_CAR: 'AVNReserve'
        },
        boolMobile: false,

        SetMobile: function(){
            this.boolMobile = true;
        },

        IsMobile: function(){
            return this.boolMobile;
        },

        GetEventName: function(strFormCode, strPostfix){
            if( typeof strPostfix === 'undefined' )
                strPostfix = '_Success';

            if( this.IsMobile() )
                strPostfix += '_Mobile';

            return ( ( typeof this.objDefaultEventNames[strFormCode] !== 'undefined' ) ? (this.objDefaultEventNames[strFormCode]) : (strFormCode) ) + strPostfix;
        },

        GetCurDealerDays: function(){
            if(typeof window['objDealersData'] !== 'undefined') {
                var intCurDealerID = parseInt(window['objDealersData']['intCurDealerID']) || 0;

                if(intCurDealerID > 0){
                    if(typeof window['objDealersData']['objDealers'][intCurDealerID] !== 'undefined'){
                        return window['objDealersData']['objDealers'][intCurDealerID]['DAYS'];
                    }
                }
            }

            return false;
        },
        GetWeekDays: function(){
            return ['sut', 'mon', 'tue', 'wed', 'thu', 'fri', 'sun'];
        },
        SetCurDealer: function(intCurDealerID) {
            var intCurDealerID = parseInt(intCurDealerID) || 0;

            if(typeof window['objDealersData'] !== 'undefined')
                window['objDealersData']['intCurDealerID'] = intCurDealerID;
        },
        SetDealersData: function(objData){
            window['objDealersData'] = objData;
        },
        DatePickerInit: function(){
            $('.datepicker').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                showOn: "both",
                buttonImage: "/static/mobile/img/calendar.png",
                buttonImageOnly: true,
                dateFormat: "dd.mm.yy",
                monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
                monthNamesShort: [ "Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек" ],
                dayNames: [ "Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота" ],
                dayNamesShort: [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
                dayNamesMin: [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
                firstDay: 1,
                beforeShowDay: function(date){
                    var canShow = true;

                    var objDealerWorkDays = kdxTools.Form.GetCurDealerDays();

                    if(objDealerWorkDays !== false){
                        var arNames  = kdxTools.Form.GetWeekDays();
                        var strToday = arNames[ date.getDay() ];

                        if(typeof objDealerWorkDays[strToday] === 'undefined')
                            canShow = false;
                    }

                    return [canShow, "", ""];
                }
            });

            $("#ui-datepicker-div").hide(); // Не знаю почему он вылезает сразу

            kdxTools.Form.DatePickerUpdate(); // Сразу же обновляем, затем, что бы применить настройки минимальных и максимальных дней.
        },
        DatePickerUpdate: function(){
            if(typeof window['objDealersData'] !== 'undefined') {
                var intCurDealerID = parseInt(window['objDealersData']['intCurDealerID']) || 0;

                if(intCurDealerID > 0) {
                    $('.datepicker').datepicker('option', 'minDate', window['objDealersData']['objDealers'][intCurDealerID]['MIN']);
                    $('.datepicker').datepicker('option', 'maxDate', window['objDealersData']['objDealers'][intCurDealerID]['MAX']);
                }
            }
        },
        PreventPhoneInput: function(){
            $('body').on("keyup", ".field-PHONE input", function(){
                var val = $(this).val();
                var newVal = val.replace(/[^\+0-9\(\)\+\#\-]/g, "");

                if(val != newVal)
                    $(this).val(newVal);
            });
        },

        YandexMetrika: {
            strCounterName: false,

            Init: function(strCounterName){
                this.strCounterName = strCounterName;
            },

            HaveCounter: function(){
                return !(this.strCounterName === false);
            },

            GetCounter: function(){
                return window[this.strCounterName];
            },

            Send: function(strTarget, objParams){
                if( typeof strTarget !== 'string' )
                    return false;
                if( strTarget == '' )
                    return false;
                if( !this.HaveCounter() )
                    return false;
                if( typeof this.GetCounter() === 'undefined' ){
                    this.strCounterName = false;
                    return false;
                }

                if(typeof objParams !== 'object')
                    objParams = {};

                var objCounter = this.GetCounter();

                objCounter['reachGoal'](strTarget, objParams);
            }
        },

        IsAllEventsPrevented: function(){
            return (typeof dForm === 'object' && typeof dForm['boolPreventDefaultEvent'] === 'boolean' && dForm['boolPreventDefaultEvents'] === true);
        },
        IsSingleEventPrevented: function(strFormCode){
            return ( typeof dForm === 'object' && typeof dForm['arPreventDefaultEvent'] === 'array' && ($.inArray(strFormCode, dForm['arPreventDefaultEvent']) > -1) );
        },

        GoogleAnalyticsSend: function(arConfig){
            var isFull = false;

            if(typeof arConfig !== 'object') // Если конфигурация - не объект
                return false;

            if(typeof arConfig.type === 'undefined') // Если конфигурация не соедржит описание типа события
                return false;

            if(arConfig.type != 'event' && arConfig.type != 'pageview') // Доступные действия
                return false;

            if(arConfig.type != 'pageview'){ // Если у нас НЕ просмотр страницы с 1 параметром
                if(typeof arConfig.cat === 'undefined') // То должны быть все остальные параметры
                    return false;
                else
                    isFull = true;
            }

            if(typeof ga !== 'undefined'){ // Один тип отправки
                if(isFull){ // Если у нас не pageview с 1 параметром
                    if(typeof arConfig.name === 'undefined'){
                        if(typeof arConfig.action === 'undefined')
                            ga('send', arConfig.type, arConfig.cat);
                        else
                            ga('send', arConfig.type, arConfig.cat, arConfig.action);
                    }
                    else
                        ga('send', arConfig.type, arConfig.cat, arConfig.action, arConfig.name);
                }
                else // Если pageview
                    ga('send', arConfig.type);
            }
            else if(typeof _gaq !== 'undefined'){ // Второй тип отправки
                var tmpAr = []; // Массив для отправки в gaq
                var eventType = '_track' + ((arConfig.type).replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1){ return $1.toUpperCase(); })); // Заменяем первый символ на прописной у события

                tmpAr.push(eventType);

                if(isFull){
                    tmpAr.push(arConfig.cat);
                    if(typeof arConfig.action !== 'undefined')
                        tmpAr.push(arConfig.action);

                    if(typeof arConfig.name !== 'undefined')
                        tmpAr.push(arConfig.name);
                }

                _gaq.push(tmpAr);
            }

            return false;
        }
    },
    Ajax: {
        Pagination: {
            Init: function(){
                $('body').on('click', '.ajax-pagination a', function(e){
                    e.preventDefault();
                    e.stopPropagation();

                    var _that = this;

                    if($(_that).hasClass('waiting'))
                        return false;

                    $(_that).addClass('waiting');

                    var domParent = $(_that).parents('.ajax-pagination');
                    var domWrapper = $($(domParent).data('wrapper'));

                    $(domWrapper).addClass('relative').append("<div class='absolute-loader'></div>");

                    $.ajax({
                        url: $(_that).attr('href'),
                        success: function(strHTML){
                            if (window.history && window.history.pushState)
                                window.history.pushState(null, null, $(_that).attr("href"));
                            $(domWrapper).replaceWith(strHTML);
                        },
                        error: function(){
                            $(domWrapper).removeClass('relative').find('.absolute-loader').remove();
                        }
                    });
                });
            }
        }
    },
    Spin: {
        Push: function(objParent){
            var spinner = new Spinner().spin();
            $(objParent).append(spinner.el);
        }
    }
};

$(document).ready(function(){
    $("body").on("change", "#kdx-tools-no-more", function(){
        var strCookieName = $(this).data('cookie-name');

        if($(this).is(':checked'))
            $.cookie(strCookieName, 'Y', {
                path: '/'
            });
        else
            $.removeCookie(strCookieName, {
                path: '/'
            });
    });

    $("body").on("kdx-countdown", ".kdx-tools-countdown", function(){
        var strID = $(this).attr("id") || "";

        if(strID === "") {
            strID = "_kdxToolsCountdown" + (Math.floor(Math.random() * (1000 - 100 + 1)) + 100);

            $(this).attr("id", strID);
        }

        kdxTools.Countdown.Process(strID, function(){
            $.fancybox.close();
        }, function(){
            $(".pop_close_msg").remove();
        });
    });

    $('body').on('change', '.select-DEALER_CENTER', function(){
        var intCurDealerID = parseInt( $(this).find("option:selected").val() ) || 0;

        kdxTools.Form.SetCurDealer(intCurDealerID);
        kdxTools.Form.DatePickerUpdate();
    });

    kdxTools.Ajax.Pagination.Init();
});