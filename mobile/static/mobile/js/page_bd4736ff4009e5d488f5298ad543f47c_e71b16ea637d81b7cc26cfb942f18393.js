
; /* Start:/local/templates/.default/components/dws/available.cars.filter/mobile/script.js*/
var _kdxCurModel = '';
var boolBlockStateChange = false;
var infiniteScrollStart  = false;

$(document).ready(function(){
    $('body').on('click', '.available-section-trigger', function(e){
        e.preventDefault();
        e.stopPropagation();

        if(_kdxCurModel === $(this).attr("href"))
            return false;

        if(_kdxCurModel === '' && $(this).hasClass('btn_black')){
            _kdxCurModel = $(this).attr("href");
            return false
        }

        _kdxCurModel = $(this).attr("href");

        var _that = this;

        $.ajax({
            url: _kdxComplexPath + '_kdx_filter/',
            data: {
                'MODEL': $(this).attr("href")
            },
            type: 'POST',
            beforeSend: function(){
                $(".filter_set").addClass('relative').append("<div class='absolute-loader'><center class='spin-placeholder'></center></div>");
                $('.available-cars-ajax-wrapper').addClass('relative').append("<div class='absolute-loader' style='top: -36px; padding-bottom: 36px;'></div>");
                kdxTools.Spin.Push($(".spin-placeholder"));
            },
            success: function(strFilterHTML){
                $(".avn_choose_item .btn.btn_black").addClass('white').addClass('has_arrow').removeClass('btn_black').text('Выбрать');
                $(_that).removeClass('has_arrow').removeClass('white').addClass('btn_black').text('Выбран');

                $(".filter_set").replaceWith(strFilterHTML);
                $(".filter_set").addClass('relative').append("<div class='absolute-loader'><center class='spin-placeholder'></center></div>");
                kdxTools.Spin.Push($(".spin-placeholder"));

                $('.filter_set input:first').trigger('change');
            },
            error: function(){
                $(".filter_set").removeClass('relative').find('.absolute-loader').remove();
                $('.available-cars-ajax-wrapper').removeClass('relative').find('.absolute-loader').remove();
            }
        });
    });

    $('body').on('change', '.filter_set input', function(){
        $.ajax({
            url: _kdxComplexPath + '?' + $(".filter_set form").serialize(),
            beforeSend: function(){
                boolBlockStateChange = true;
                window.History.pushState(null, $('title').text(), '?' + $(".filter_set form").serialize());

                if(!$('.available-cars-ajax-wrapper').hasClass('relative')) {
                    $('.available-cars-ajax-wrapper').addClass('relative').append("<div class='absolute-loader'><center class='spin-placeholder'></center></div>");
                    kdxTools.Spin.Push($(".spin-placeholder"));
                }
            },
            success: function(strListHTML){
                $(".available-cars-ajax-wrapper").replaceWith(strListHTML);

                if($('.filter_set').hasClass('relative'))
                    $('.filter_set').removeClass('relative').find('.absolute-loader').remove();
            },
            error: function(){
                $('.available-cars-ajax-wrapper').removeClass('relative').find('.absolute-loader').remove();
            }
        })
    });

    $(window).on("statechange", function(){
        if(!boolBlockStateChange)
            window.location.href = window.History.getState().hash;

        boolBlockStateChange = false;
    });


    $(window).scroll(function() {
        var scroll_pos = $(window).scrollTop();
        if (scroll_pos >= 0.9*($(document).height() - $(window).height())) {

            if( !infiniteScrollStart && $(".scroll-ajax-pagination").length === 1 ){
                infiniteScrollStart = true;

                var strHref = $(".scroll-ajax-pagination").attr('href') + '&NO_WRAPPER=Y';

                if( strHref.indexOf('MODEL') < 0 )
                    strHref += '&MODEL=' + $('.filter_set input[name="MODEL"]').val();

                $.ajax({
                    url: strHref,
                    success: function(strHTML){
                        infiniteScrollStart = false;

                        $(".scroll-ajax-pagination").remove();
                        $('.available-cars-ajax-wrapper .avn_list_wrapper').append(strHTML);
                    },
                    error: function(){
                        infiniteScrollStart = false;
                    }
                });
            }
        }
    });

    $('body').on('click', '.avn_choose_img', function(){
        $(this).parents('.avn_choose_item').find('.available-section-trigger').trigger('click');
    });
});
/* End */
;; /* /local/templates/.default/components/dws/available.cars.filter/mobile/script.js*/
