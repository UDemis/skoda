$(document).ready(function () {
    if ($(".homepagetisImage > div.images-wrap > img").length > 1) {
        var carousel = new SwipeCarouselItems($(".homepagetisImage"), "> div.images-wrap", "> div.images-wrap > img", "");
        carousel.init();
        $(".homepagetisImage a.prev").bind("click", function() {
            carousel.prev();
        });
        $(".homepagetisImage a.next").bind("click", function() {
            carousel.next();
        });
        $(".homepagetisbCurrent span").removeClass("dotCurrent").slice(0, 1).addClass("dotCurrent");
        $(".homepagetisbLabel a").hide().slice(0, 1).show();
        $(".homepagetisImage").bind("changeposition", function(type, ev, inst) {
            var pos = carousel.position();
            $(".homepagetisbCurrent span").removeClass("dotCurrent").slice(pos, pos + 1).addClass("dotCurrent");
            $(".homepagetisbLabel a").hide().slice(pos, pos + 1).show();
        });
    }
    else {
        $(".homepagetisImage a.prev").hide();
        $(".homepagetisImage a.next").hide();
        $(".homepagetisbCurrent span").hide();
    }
});

$(document).ready(function () {
    var container = $("#newsContainer");
    if (container.length > 0) {
        var carousel = new SwipeCarouselItems(container, ">div.swipe-wrap", ">div.swipe-wrap >div", "");
        carousel.init();

        container.bind("changeposition", function (type, ev, inst) {
            var pos = carousel.position();
            $(".homepageNewsCurrent span").removeClass("dotCurrent").slice(pos, pos + 1).addClass("dotCurrent");
        });
    }

    $('.js_inner_accor_link').click(function(e){
        $(this).toggleClass('opened').next('.inner_accor_content').toggle();
        $(this).parent('li').siblings().find('.js_inner_accor_link').removeClass('opened').next('.inner_accor_content').hide();

        var h_h = $('.header').outerHeight();
        var w_top = $(this).offset();

        $(window).scrollTop(w_top.top - h_h);

        e.preventDefault();
    });


    $('.js_inner_tab').click(function(){
        $(this).addClass('active').siblings().removeClass('active');
        var data = $(this).attr('data-tab');
        $(this).parent().nextAll('.inner_tabs_content.'+data).addClass('active').siblings('.inner_tabs_content').removeClass('active');
    });

    // CUSTOM CHECKBOX/RADIO
    function customCheck(checkTarget){
        if(checkTarget.is(':checked')){
            if(checkTarget.is(':radio'))
                checkTarget.parent().siblings().removeClass('checked');
            checkTarget.parent().addClass('checked');
        }
        else
            checkTarget.parent().removeClass('checked');
    }

    // check defaults or F5
    $('.js_custom_check input').each(function(){
        customCheck($(this));
    });

    // on change
    $('body').on('change','.js_custom_check input',function(){
        customCheck($(this));
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    $('body').on('click', '.js_filter_row_opener', function(){
        $(this).toggleClass('opened').next('.filter_row_content').toggle();
        $(this).parent('.filter_row').siblings().find('.js_filter_row_opener').removeClass('opened').next('.filter_row_content').hide();
    });

    $('body').on('click', '.js_filter_show', function(){
        $(this).hide().parents('.filter_set').find('.filter_set_content').show();
        $(this).siblings('.js_filter_hide').show();
    });
    $('body').on('click', '.js_filter_hide', function(){
        $(this).hide().parents('.filter_set').find('.filter_set_content').hide();
        $(this).siblings('.js_filter_show').show();
    });


    $('body').on('click', '.js_show_form', function(){
        var _that = this;
        if($(_that).hasClass('waiting'))
            return false;

        if($(_that).find('.arrow_small').hasClass('opened')){
            $(_that).find('.arrow_small').toggleClass('opened');
            $(_that).parents('.avn_item').find('.avn_item_form').toggle();

            return false;
        }

        $(_that).addClass('waiting');

        $.ajax({
            url: '/ajax/getFormTemplate.php?FORM_CODE=AVAILABLE_CAR_INFORMATION&XML_ID=' + $(_that).find('a').data('xml-id') + '&TPL=mobile',
            type: 'POST',
            success: function(strFormHTML){
                $(_that).removeClass('waiting');

                $(_that).find('.arrow_small').toggleClass('opened');
                $(_that).parents('.avn_item').find('.avn_item_form').html(strFormHTML);
                $(_that).parents('.avn_item').find('.avn_item_form').toggle();
            },
            error: function(){
                $(_that).removeClass('waiting');
            }
        });
    });

    $('body').on('click', '.js_show_form2', function(){
        var _that = this;
        if($(_that).hasClass('waiting'))
            return false;

        if($(_that).find('.arrow_small').hasClass('opened')){
            $(this).find('.arrow_small').toggleClass('opened');
            $(this).next('.avn_item_form').toggle();

            return false;
        }

        $(_that).addClass('waiting');

        $.ajax({
            url: '/ajax/getFormTemplate.php?FORM_CODE=AVAILABLE_CAR_INFORMATION&XML_ID=' + $(_that).find('span').data('xml-id') + '&TPL=mobile',
            type: 'POST',
            success: function(strFormHTML){
                $(_that).removeClass('waiting');

                $(_that).find('.arrow_small').toggleClass('opened');
                $(_that).next('.avn_item_form').html(strFormHTML);
                $(_that).next('.avn_item_form').toggle();
            },
            error: function(){
                $(_that).removeClass('waiting')
            }
        });
    });

    $('body').on('click', '.js_tabs', function(e){
        $(this).addClass('current').siblings('.js_tabs').removeClass('current');
        var attr = $(this).find('a').attr('href');
        $(attr).addClass('current').siblings('.tabs_item').removeClass('current');
        e.preventDefault();
    });

    $('.version-msg_close').on('click', function(){
        $('.version-msg').css('display','none');
    });
    $(window).scroll(function() {
    if ($(this).scrollTop() > 50) {
        $('.version-msg').css('display','none');
    }
    if ($(this).scrollTop() < 5) {
        $('.version-msg').css('display','inline');
    }
});
});

