/*
 * jQuery JavaScript Library v1.9.1
 * http://jquery.com/
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 *
 * Copyright 2005, 2012 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2013-2-4
 */
(function(bW, bU) {
    var aT, bk, E = typeof bU,
        Y = bW.document,
        az = bW.location,
        b = bW.jQuery,
        a = bW.$,
        t = {}, x = [],
        H = "1.9.1",
        w = x.concat,
        B = x.push,
        D = x.slice,
        z = x.indexOf,
        F = t.toString,
        y = t.hasOwnProperty,
        G = H.trim,
        ay = function(b3, e) {
            return new ay.fn.init(b3, e, bk)
        }, A = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        C = /\S+/g,
        bA = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        bp = /^(?:(<[\w\W]+>)[^>]*|#([\w-]*))$/,
        bv = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
        bH = /^[\],:{}\s]*$/,
        bG = /(?:^|:|,)(?:\s*\[)+/g,
        bI = /\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,
        bJ = /"[^"\\\r\n]*"|true|false|null|-?(?:\d+\.|)\d+(?:[eE][+-]?\d+|)/g,
        bc = /^-ms-/,
        aR = /-([\da-z])/gi,
        ab = function(e, b3) {
            return b3.toUpperCase()
        }, v = function(e) {
            if (Y.addEventListener || e.type === "load" || Y.readyState === "complete") {
                W();
                ay.ready()
            }
        }, W = function() {
            if (Y.addEventListener) {
                Y.removeEventListener("DOMContentLoaded", v, false);
                bW.removeEventListener("load", v, false)
            } else {
                Y.detachEvent("onreadystatechange", v);
                bW.detachEvent("onload", v)
            }
        };
    ay.fn = ay.prototype = {
        jquery: H,
        constructor: ay,
        init: function(b6, e, b5) {
            var b4, b3;
            if (!b6) {
                return this
            }
            if (typeof b6 === "string") {
                if (b6.charAt(0) === "<" && b6.charAt(b6.length - 1) === ">" && b6.length >= 3) {
                    b4 = [null, b6, null]
                } else {
                    b4 = bp.exec(b6)
                } if (b4 && (b4[1] || !e)) {
                    if (b4[1]) {
                        e = e instanceof ay ? e[0] : e;
                        ay.merge(this, ay.parseHTML(b4[1], e && e.nodeType ? e.ownerDocument || e : Y, true));
                        if (bv.test(b4[1]) && ay.isPlainObject(e)) {
                            for (b4 in e) {
                                if (ay.isFunction(this[b4])) {
                                    this[b4](e[b4])
                                } else {
                                    this.attr(b4, e[b4])
                                }
                            }
                        }
                        return this
                    } else {
                        b3 = Y.getElementById(b4[2]);
                        if (b3 && b3.parentNode) {
                            if (b3.id !== b4[2]) {
                                return b5.find(b6)
                            }
                            this.length = 1;
                            this[0] = b3
                        }
                        this.context = Y;
                        this.selector = b6;
                        return this
                    }
                } else {
                    if (!e || e.jquery) {
                        return (e || b5).find(b6)
                    } else {
                        return this.constructor(e).find(b6)
                    }
                }
            } else {
                if (b6.nodeType) {
                    this.context = this[0] = b6;
                    this.length = 1;
                    return this
                } else {
                    if (ay.isFunction(b6)) {
                        return b5.ready(b6)
                    }
                }
            } if (b6.selector !== bU) {
                this.selector = b6.selector;
                this.context = b6.context
            }
            return ay.makeArray(b6, this)
        },
        selector: "",
        length: 0,
        size: function() {
            return this.length
        },
        toArray: function() {
            return D.call(this)
        },
        get: function(e) {
            return e == null ? this.toArray() : (e < 0 ? this[this.length + e] : this[e])
        },
        pushStack: function(e) {
            var b3 = ay.merge(this.constructor(), e);
            b3.prevObject = this;
            b3.context = this.context;
            return b3
        },
        each: function(b3, e) {
            return ay.each(this, b3, e)
        },
        ready: function(e) {
            ay.ready.promise().done(e);
            return this
        },
        slice: function() {
            return this.pushStack(D.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(e) {
            var b4 = this.length,
                b3 = +e + (e < 0 ? b4 : 0);
            return this.pushStack(b3 >= 0 && b3 < b4 ? [this[b3]] : [])
        },
        map: function(e) {
            return this.pushStack(ay.map(this, function(b3, b4) {
                return e.call(b3, b4, b3)
            }))
        },
        end: function() {
            return this.prevObject || this.constructor(null)
        },
        push: B,
        sort: [].sort,
        splice: [].splice
    };
    ay.fn.init.prototype = ay.fn;
    ay.extend = ay.fn.extend = function() {
        var ca, b4, b3, b8, b9, e, cb = arguments[0] || {}, b6 = 1,
            b7 = arguments.length,
            b5 = false;
        if (typeof cb === "boolean") {
            b5 = cb;
            cb = arguments[1] || {};
            b6 = 2
        }
        if (typeof cb !== "object" && !ay.isFunction(cb)) {
            cb = {}
        }
        if (b7 === b6) {
            cb = this;
            --b6
        }
        for (; b6 < b7; b6++) {
            if ((b9 = arguments[b6]) != null) {
                for (b8 in b9) {
                    ca = cb[b8];
                    b3 = b9[b8];
                    if (cb === b3) {
                        continue
                    }
                    if (b5 && b3 && (ay.isPlainObject(b3) || (b4 = ay.isArray(b3)))) {
                        if (b4) {
                            b4 = false;
                            e = ca && ay.isArray(ca) ? ca : []
                        } else {
                            e = ca && ay.isPlainObject(ca) ? ca : {}
                        }
                        cb[b8] = ay.extend(b5, e, b3)
                    } else {
                        if (b3 !== bU) {
                            cb[b8] = b3
                        }
                    }
                }
            }
        }
        return cb
    };
    ay.extend({
        noConflict: function(e) {
            if (bW.$ === ay) {
                bW.$ = a
            }
            if (e && bW.jQuery === ay) {
                bW.jQuery = b
            }
            return ay
        },
        isReady: false,
        readyWait: 1,
        holdReady: function(e) {
            if (e) {
                ay.readyWait++
            } else {
                ay.ready(true)
            }
        },
        ready: function(e) {
            if (e === true ? --ay.readyWait : ay.isReady) {
                return
            }
            if (!Y.body) {
                return setTimeout(ay.ready)
            }
            ay.isReady = true;
            if (e !== true && --ay.readyWait > 0) {
                return
            }
            aT.resolveWith(Y, [ay]);
            if (ay.fn.trigger) {
                ay(Y).trigger("ready").off("ready")
            }
        },
        isFunction: function(e) {
            return ay.type(e) === "function"
        },
        isArray: Array.isArray || function(e) {
            return ay.type(e) === "array"
        },
        isWindow: function(e) {
            return e != null && e == e.window
        },
        isNumeric: function(e) {
            return !isNaN(parseFloat(e)) && isFinite(e)
        },
        type: function(e) {
            if (e == null) {
                return String(e)
            }
            return typeof e === "object" || typeof e === "function" ? t[F.call(e)] || "object" : typeof e
        },
        isPlainObject: function(b5) {
            if (!b5 || ay.type(b5) !== "object" || b5.nodeType || ay.isWindow(b5)) {
                return false
            }
            try {
                if (b5.constructor && !y.call(b5, "constructor") && !y.call(b5.constructor.prototype, "isPrototypeOf")) {
                    return false
                }
            } catch (b3) {
                return false
            }
            var b4;
            for (b4 in b5) {}
            return b4 === bU || y.call(b5, b4)
        },
        isEmptyObject: function(b3) {
            var e;
            for (e in b3) {
                return false
            }
            return true
        },
        error: function(e) {
            throw new Error(e)
        },
        parseHTML: function(b3, e, b4) {
            if (!b3 || typeof b3 !== "string") {
                return null
            }
            if (typeof e === "boolean") {
                b4 = e;
                e = false
            }
            e = e || Y;
            var b5 = bv.exec(b3),
                b6 = !b4 && [];
            if (b5) {
                return [e.createElement(b5[1])]
            }
            b5 = ay.buildFragment([b3], e, b6);
            if (b6) {
                ay(b6).remove()
            }
            return ay.merge([], b5.childNodes)
        },
        parseJSON: function(e) {
            if (bW.JSON && bW.JSON.parse) {
                return bW.JSON.parse(e)
            }
            if (e === null) {
                return e
            }
            if (typeof e === "string") {
                e = ay.trim(e);
                if (e) {
                    if (bH.test(e.replace(bI, "@").replace(bJ, "]").replace(bG, ""))) {
                        return (new Function("return " + e))()
                    }
                }
            }
            ay.error("Invalid JSON: " + e)
        },
        parseXML: function(b3) {
            var b6, b5;
            if (!b3 || typeof b3 !== "string") {
                return null
            }
            try {
                if (bW.DOMParser) {
                    b5 = new DOMParser();
                    b6 = b5.parseFromString(b3, "text/xml")
                } else {
                    b6 = new ActiveXObject("Microsoft.XMLDOM");
                    b6.async = "false";
                    b6.loadXML(b3)
                }
            } catch (b4) {
                b6 = bU
            }
            if (!b6 || !b6.documentElement || b6.getElementsByTagName("parsererror").length) {
                ay.error("Invalid XML: " + b3)
            }
            return b6
        },
        noop: function() {},
        globalEval: function(e) {
            if (e && ay.trim(e)) {
                (bW.execScript || function(b3) {
                    bW["eval"].call(bW, b3)
                })(e)
            }
        },
        camelCase: function(e) {
            return e.replace(bc, "ms-").replace(aR, ab)
        },
        nodeName: function(e, b3) {
            return e.nodeName && e.nodeName.toLowerCase() === b3.toLowerCase()
        },
        each: function(b7, b3, e) {
            var b8, b4 = 0,
                b6 = b7.length,
                b5 = au(b7);
            if (e) {
                if (b5) {
                    for (; b4 < b6; b4++) {
                        b8 = b3.apply(b7[b4], e);
                        if (b8 === false) {
                            break
                        }
                    }
                } else {
                    for (b4 in b7) {
                        b8 = b3.apply(b7[b4], e);
                        if (b8 === false) {
                            break
                        }
                    }
                }
            } else {
                if (b5) {
                    for (; b4 < b6; b4++) {
                        b8 = b3.call(b7[b4], b4, b7[b4]);
                        if (b8 === false) {
                            break
                        }
                    }
                } else {
                    for (b4 in b7) {
                        b8 = b3.call(b7[b4], b4, b7[b4]);
                        if (b8 === false) {
                            break
                        }
                    }
                }
            }
            return b7
        },
        trim: G && !G.call("\uFEFF\xA0") ? function(e) {
            return e == null ? "" : G.call(e)
        } : function(e) {
            return e == null ? "" : (e + "").replace(bA, "")
        },
        makeArray: function(e, b3) {
            var b4 = b3 || [];
            if (e != null) {
                if (au(Object(e))) {
                    ay.merge(b4, typeof e === "string" ? [e] : e)
                } else {
                    B.call(b4, e)
                }
            }
            return b4
        },
        inArray: function(b3, e, b4) {
            var b5;
            if (e) {
                if (z) {
                    return z.call(e, b3, b4)
                }
                b5 = e.length;
                b4 = b4 ? b4 < 0 ? Math.max(0, b5 + b4) : b4 : 0;
                for (; b4 < b5; b4++) {
                    if (b4 in e && e[b4] === b3) {
                        return b4
                    }
                }
            }
            return -1
        },
        merge: function(e, b6) {
            var b5 = b6.length,
                b3 = e.length,
                b4 = 0;
            if (typeof b5 === "number") {
                for (; b4 < b5; b4++) {
                    e[b3++] = b6[b4]
                }
            } else {
                while (b6[b4] !== bU) {
                    e[b3++] = b6[b4++]
                }
            }
            e.length = b3;
            return e
        },
        grep: function(b3, e, b5) {
            var b8, b7 = [],
                b4 = 0,
                b6 = b3.length;
            b5 = !! b5;
            for (; b4 < b6; b4++) {
                b8 = !! e(b3[b4], b4);
                if (b5 !== b8) {
                    b7.push(b3[b4])
                }
            }
            return b7
        },
        map: function(b4, b3, e) {
            var b9, b5 = 0,
                b7 = b4.length,
                b6 = au(b4),
                b8 = [];
            if (b6) {
                for (; b5 < b7; b5++) {
                    b9 = b3(b4[b5], b5, e);
                    if (b9 != null) {
                        b8[b8.length] = b9
                    }
                }
            } else {
                for (b5 in b4) {
                    b9 = b3(b4[b5], b5, e);
                    if (b9 != null) {
                        b8[b8.length] = b9
                    }
                }
            }
            return w.apply([], b8)
        },
        guid: 1,
        proxy: function(b4, b3) {
            var e, b5, b6;
            if (typeof b3 === "string") {
                b6 = b4[b3];
                b3 = b4;
                b4 = b6
            }
            if (!ay.isFunction(b4)) {
                return bU
            }
            e = D.call(arguments, 2);
            b5 = function() {
                return b4.apply(b3 || this, e.concat(D.call(arguments)))
            };
            b5.guid = b4.guid = b4.guid || ay.guid++;
            return b5
        },
        access: function(b4, b6, b8, cb, b3, b5, ca) {
            var b7 = 0,
                b9 = b4.length,
                e = b8 == null;
            if (ay.type(b8) === "object") {
                b3 = true;
                for (b7 in b8) {
                    ay.access(b4, b6, b7, b8[b7], true, b5, ca)
                }
            } else {
                if (cb !== bU) {
                    b3 = true;
                    if (!ay.isFunction(cb)) {
                        ca = true
                    }
                    if (e) {
                        if (ca) {
                            b6.call(b4, cb);
                            b6 = null
                        } else {
                            e = b6;
                            b6 = function(cc, cd, ce) {
                                return e.call(ay(cc), ce)
                            }
                        }
                    }
                    if (b6) {
                        for (; b7 < b9; b7++) {
                            b6(b4[b7], b8, ca ? cb : cb.call(b4[b7], b7, b6(b4[b7], b8)))
                        }
                    }
                }
            }
            return b3 ? b4 : e ? b6.call(b4) : b9 ? b6(b4[0], b8) : b5
        },
        now: function() {
            return (new Date()).getTime()
        }
    });
    ay.ready.promise = function(b5) {
        if (!aT) {
            aT = ay.Deferred();
            if (Y.readyState === "complete") {
                setTimeout(ay.ready)
            } else {
                if (Y.addEventListener) {
                    Y.addEventListener("DOMContentLoaded", v, false);
                    bW.addEventListener("load", v, false)
                } else {
                    Y.attachEvent("onreadystatechange", v);
                    bW.attachEvent("onload", v);
                    var b6 = false;
                    try {
                        b6 = bW.frameElement == null && Y.documentElement
                    } catch (b4) {}
                    if (b6 && b6.doScroll) {
                        (function b3() {
                            if (!ay.isReady) {
                                try {
                                    b6.doScroll("left")
                                } catch (b7) {
                                    return setTimeout(b3, 50)
                                }
                                W();
                                ay.ready()
                            }
                        })()
                    }
                }
            }
        }
        return aT.promise(b5)
    };
    ay.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(e, b3) {
        t["[object " + b3 + "]"] = b3.toLowerCase()
    });

    function au(b3) {
        var e = b3.length,
            b4 = ay.type(b3);
        if (ay.isWindow(b3)) {
            return false
        }
        if (b3.nodeType === 1 && e) {
            return true
        }
        return b4 === "array" || b4 !== "function" && (e === 0 || typeof e === "number" && e > 0 && (e - 1) in b3)
    }
    bk = ay(Y);
    var aE = {};

    function K(b3) {
        var e = aE[b3] = {};
        ay.each(b3.match(C) || [], function(b4, b5) {
            e[b5] = true
        });
        return e
    }
    ay.Callbacks = function(ca) {
        ca = typeof ca === "string" ? (aE[ca] || K(ca)) : ay.extend({}, ca);
        var b4, b9, b3, b6, b5, b7, b8 = [],
            cc = !ca.once && [],
            e = function(cd) {
                b9 = ca.memory && cd;
                b3 = true;
                b5 = b7 || 0;
                b7 = 0;
                b6 = b8.length;
                b4 = true;
                for (; b8 && b5 < b6; b5++) {
                    if (b8[b5].apply(cd[0], cd[1]) === false && ca.stopOnFalse) {
                        b9 = false;
                        break
                    }
                }
                b4 = false;
                if (b8) {
                    if (cc) {
                        if (cc.length) {
                            e(cc.shift())
                        }
                    } else {
                        if (b9) {
                            b8 = []
                        } else {
                            cb.disable()
                        }
                    }
                }
            }, cb = {
                add: function() {
                    if (b8) {
                        var ce = b8.length;
                        (function cd(cf) {
                            ay.each(cf, function(cg, ch) {
                                var ci = ay.type(ch);
                                if (ci === "function") {
                                    if (!ca.unique || !cb.has(ch)) {
                                        b8.push(ch)
                                    }
                                } else {
                                    if (ch && ch.length && ci !== "string") {
                                        cd(ch)
                                    }
                                }
                            })
                        })(arguments);
                        if (b4) {
                            b6 = b8.length
                        } else {
                            if (b9) {
                                b7 = ce;
                                e(b9)
                            }
                        }
                    }
                    return this
                },
                remove: function() {
                    if (b8) {
                        ay.each(arguments, function(cd, ce) {
                            var cf;
                            while ((cf = ay.inArray(ce, b8, cf)) > -1) {
                                b8.splice(cf, 1);
                                if (b4) {
                                    if (cf <= b6) {
                                        b6--
                                    }
                                    if (cf <= b5) {
                                        b5--
                                    }
                                }
                            }
                        })
                    }
                    return this
                },
                has: function(cd) {
                    return cd ? ay.inArray(cd, b8) > -1 : !! (b8 && b8.length)
                },
                empty: function() {
                    b8 = [];
                    return this
                },
                disable: function() {
                    b8 = cc = b9 = bU;
                    return this
                },
                disabled: function() {
                    return !b8
                },
                lock: function() {
                    cc = bU;
                    if (!b9) {
                        cb.disable()
                    }
                    return this
                },
                locked: function() {
                    return !cc
                },
                fireWith: function(ce, cd) {
                    cd = cd || [];
                    cd = [ce, cd.slice ? cd.slice() : cd];
                    if (b8 && (!b3 || cc)) {
                        if (b4) {
                            cc.push(cd)
                        } else {
                            e(cd)
                        }
                    }
                    return this
                },
                fire: function() {
                    cb.fireWith(this, arguments);
                    return this
                },
                fired: function() {
                    return !!b3
                }
            };
        return cb
    };
    ay.extend({
        Deferred: function(b3) {
            var b6 = [
                    ["resolve", "done", ay.Callbacks("once memory"), "resolved"],
                    ["reject", "fail", ay.Callbacks("once memory"), "rejected"],
                    ["notify", "progress", ay.Callbacks("memory")]
                ],
                b5 = "pending",
                b4 = {
                    state: function() {
                        return b5
                    },
                    always: function() {
                        e.done(arguments).fail(arguments);
                        return this
                    },
                    then: function() {
                        var b7 = arguments;
                        return ay.Deferred(function(b8) {
                            ay.each(b6, function(cb, cc) {
                                var b9 = cc[0],
                                    ca = ay.isFunction(b7[cb]) && b7[cb];
                                e[cc[1]](function() {
                                    var cd = ca && ca.apply(this, arguments);
                                    if (cd && ay.isFunction(cd.promise)) {
                                        cd.promise().done(b8.resolve).fail(b8.reject).progress(b8.notify)
                                    } else {
                                        b8[b9 + "With"](this === b4 ? b8.promise() : this, ca ? [cd] : arguments)
                                    }
                                })
                            });
                            b7 = null
                        }).promise()
                    },
                    promise: function(b7) {
                        return b7 != null ? ay.extend(b7, b4) : b4
                    }
                }, e = {};
            b4.pipe = b4.then;
            ay.each(b6, function(b7, ca) {
                var b8 = ca[2],
                    b9 = ca[3];
                b4[ca[1]] = b8.add;
                if (b9) {
                    b8.add(function() {
                        b5 = b9
                    }, b6[b7 ^ 1][2].disable, b6[2][2].lock)
                }
                e[ca[0]] = function() {
                    e[ca[0] + "With"](this === e ? b4 : this, arguments);
                    return this
                };
                e[ca[0] + "With"] = b8.fireWith
            });
            b4.promise(e);
            if (b3) {
                b3.call(e, e)
            }
            return e
        },
        when: function(ca) {
            var b3 = 0,
                b9 = D.call(arguments),
                b4 = b9.length,
                b7 = b4 !== 1 || (ca && ay.isFunction(ca.promise)) ? b4 : 0,
                e = b7 === 1 ? ca : ay.Deferred(),
                cb = function(cd, cc, ce) {
                    return function(cf) {
                        cc[cd] = this;
                        ce[cd] = arguments.length > 1 ? D.call(arguments) : cf;
                        if (ce === b6) {
                            e.notifyWith(cc, ce)
                        } else {
                            if (!(--b7)) {
                                e.resolveWith(cc, ce)
                            }
                        }
                    }
                }, b6, b5, b8;
            if (b4 > 1) {
                b6 = new Array(b4);
                b5 = new Array(b4);
                b8 = new Array(b4);
                for (; b3 < b4; b3++) {
                    if (b9[b3] && ay.isFunction(b9[b3].promise)) {
                        b9[b3].promise().done(cb(b3, b8, b9)).fail(e.reject).progress(cb(b3, b5, b6))
                    } else {
                        --b7
                    }
                }
            }
            if (!b7) {
                e.resolveWith(b8, b9)
            }
            return e.promise()
        }
    });
    ay.support = (function() {
        var ce, b4, b3, ca, cd, b8, cc, b7, cb, b9, b5 = Y.createElement("div");
        b5.setAttribute("className", "t");
        b5.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
        b4 = b5.getElementsByTagName("*");
        b3 = b5.getElementsByTagName("a")[0];
        if (!b4 || !b3 || !b4.length) {
            return {}
        }
        cd = Y.createElement("select");
        cc = cd.appendChild(Y.createElement("option"));
        ca = b5.getElementsByTagName("input")[0];
        b3.style.cssText = "top:1px;float:left;opacity:.5";
        ce = {
            getSetAttribute: b5.className !== "t",
            leadingWhitespace: b5.firstChild.nodeType === 3,
            tbody: !b5.getElementsByTagName("tbody").length,
            htmlSerialize: !! b5.getElementsByTagName("link").length,
            style: /top/.test(b3.getAttribute("style")),
            hrefNormalized: b3.getAttribute("href") === "/a",
            opacity: /^0.5/.test(b3.style.opacity),
            cssFloat: !! b3.style.cssFloat,
            checkOn: !! ca.value,
            optSelected: cc.selected,
            enctype: !! Y.createElement("form").enctype,
            html5Clone: Y.createElement("nav").cloneNode(true).outerHTML !== "<:nav></:nav>",
            boxModel: Y.compatMode === "CSS1Compat",
            deleteExpando: true,
            noCloneEvent: true,
            inlineBlockNeedsLayout: false,
            shrinkWrapBlocks: false,
            reliableMarginRight: true,
            boxSizingReliable: true,
            pixelPosition: false
        };
        ca.checked = true;
        ce.noCloneChecked = ca.cloneNode(true).checked;
        cd.disabled = true;
        ce.optDisabled = !cc.disabled;
        try {
            delete b5.test
        } catch (b6) {
            ce.deleteExpando = false
        }
        ca = Y.createElement("input");
        ca.setAttribute("value", "");
        ce.input = ca.getAttribute("value") === "";
        ca.value = "t";
        ca.setAttribute("type", "radio");
        ce.radioValue = ca.value === "t";
        ca.setAttribute("checked", "t");
        ca.setAttribute("name", "t");
        b8 = Y.createDocumentFragment();
        b8.appendChild(ca);
        ce.appendChecked = ca.checked;
        ce.checkClone = b8.cloneNode(true).cloneNode(true).lastChild.checked;
        if (b5.attachEvent) {
            b5.attachEvent("onclick", function() {
                ce.noCloneEvent = false
            });
            b5.cloneNode(true).click()
        }
        for (b9 in {
            submit: true,
            change: true,
            focusin: true
        }) {
            b5.setAttribute(b7 = "on" + b9, "t");
            ce[b9 + "Bubbles"] = b7 in bW || b5.attributes[b7].expando === false
        }
        b5.style.backgroundClip = "content-box";
        b5.cloneNode(true).style.backgroundClip = "";
        ce.clearCloneStyle = b5.style.backgroundClip === "content-box";
        ay(function() {
            var cf, ch, ci, cg = "padding:0;margin:0;border:0;display:block;box-sizing:content-box;-moz-box-sizing:content-box;-webkit-box-sizing:content-box;",
                e = Y.getElementsByTagName("body")[0];
            if (!e) {
                return
            }
            cf = Y.createElement("div");
            cf.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px";
            e.appendChild(cf).appendChild(b5);
            b5.innerHTML = "<table><tr><td></td><td>t</td></tr></table>";
            ci = b5.getElementsByTagName("td");
            ci[0].style.cssText = "padding:0;margin:0;border:0;display:none";
            cb = (ci[0].offsetHeight === 0);
            ci[0].style.display = "";
            ci[1].style.display = "none";
            ce.reliableHiddenOffsets = cb && (ci[0].offsetHeight === 0);
            b5.innerHTML = "";
            b5.style.cssText = "box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;";
            ce.boxSizing = (b5.offsetWidth === 4);
            ce.doesNotIncludeMarginInBodyOffset = (e.offsetTop !== 1);
            if (bW.getComputedStyle) {
                ce.pixelPosition = (bW.getComputedStyle(b5, null) || {}).top !== "1%";
                ce.boxSizingReliable = (bW.getComputedStyle(b5, null) || {
                    width: "4px"
                }).width === "4px";
                ch = b5.appendChild(Y.createElement("div"));
                ch.style.cssText = b5.style.cssText = cg;
                ch.style.marginRight = ch.style.width = "0";
                b5.style.width = "1px";
                ce.reliableMarginRight = !parseFloat((bW.getComputedStyle(ch, null) || {}).marginRight)
            }
            if (typeof b5.style.zoom !== E) {
                b5.innerHTML = "";
                b5.style.cssText = cg + "width:1px;padding:1px;display:inline;zoom:1";
                ce.inlineBlockNeedsLayout = (b5.offsetWidth === 3);
                b5.style.display = "block";
                b5.innerHTML = "<div></div>";
                b5.firstChild.style.width = "5px";
                ce.shrinkWrapBlocks = (b5.offsetWidth !== 3);
                if (ce.inlineBlockNeedsLayout) {
                    e.style.zoom = 1
                }
            }
            e.removeChild(cf);
            cf = b5 = ci = ch = null
        });
        b4 = cd = b8 = cc = b3 = ca = null;
        return ce
    })();
    var aK = /(?:\{[\s\S]*\}|\[[\s\S]*\])$/,
        bd = /([A-Z])/g;

    function ar(b4, b9, b3, ca) {
        if (!ay.acceptData(b4)) {
            return
        }
        var cc, cb, b7 = ay.expando,
            b5 = typeof b9 === "string",
            b8 = b4.nodeType,
            e = b8 ? ay.cache : b4,
            b6 = b8 ? b4[b7] : b4[b7] && b7;
        if ((!b6 || !e[b6] || (!ca && !e[b6].data)) && b5 && b3 === bU) {
            return
        }
        if (!b6) {
            if (b8) {
                b4[b7] = b6 = x.pop() || ay.guid++
            } else {
                b6 = b7
            }
        }
        if (!e[b6]) {
            e[b6] = {};
            if (!b8) {
                e[b6].toJSON = ay.noop
            }
        }
        if (typeof b9 === "object" || typeof b9 === "function") {
            if (ca) {
                e[b6] = ay.extend(e[b6], b9)
            } else {
                e[b6].data = ay.extend(e[b6].data, b9)
            }
        }
        cc = e[b6];
        if (!ca) {
            if (!cc.data) {
                cc.data = {}
            }
            cc = cc.data
        }
        if (b3 !== bU) {
            cc[ay.camelCase(b9)] = b3
        }
        if (b5) {
            cb = cc[b9];
            if (cb == null) {
                cb = cc[ay.camelCase(b9)]
            }
        } else {
            cb = cc
        }
        return cb
    }

    function at(b3, b8, b9) {
        if (!ay.acceptData(b3)) {
            return
        }
        var b4, b7, ca, b6 = b3.nodeType,
            e = b6 ? ay.cache : b3,
            b5 = b6 ? b3[ay.expando] : ay.expando;
        if (!e[b5]) {
            return
        }
        if (b8) {
            ca = b9 ? e[b5] : e[b5].data;
            if (ca) {
                if (!ay.isArray(b8)) {
                    if (b8 in ca) {
                        b8 = [b8]
                    } else {
                        b8 = ay.camelCase(b8);
                        if (b8 in ca) {
                            b8 = [b8]
                        } else {
                            b8 = b8.split(" ")
                        }
                    }
                } else {
                    b8 = b8.concat(ay.map(b8, ay.camelCase))
                }
                for (b4 = 0, b7 = b8.length; b4 < b7; b4++) {
                    delete ca[b8[b4]]
                }
                if (!(b9 ? av : ay.isEmptyObject)(ca)) {
                    return
                }
            }
        }
        if (!b9) {
            delete e[b5].data;
            if (!av(e[b5])) {
                return
            }
        }
        if (b6) {
            ay.cleanData([b3], true)
        } else {
            if (ay.support.deleteExpando || e != e.window) {
                delete e[b5]
            } else {
                e[b5] = null
            }
        }
    }
    ay.extend({
        cache: {},
        expando: "jQuery" + (H + Math.random()).replace(/\D/g, ""),
        noData: {
            embed: true,
            object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
            applet: true
        },
        hasData: function(e) {
            e = e.nodeType ? ay.cache[e[ay.expando]] : e[ay.expando];
            return !!e && !av(e)
        },
        data: function(b3, b4, e) {
            return ar(b3, b4, e)
        },
        removeData: function(e, b3) {
            return at(e, b3)
        },
        _data: function(b3, b4, e) {
            return ar(b3, b4, e, true)
        },
        _removeData: function(e, b3) {
            return at(e, b3, true)
        },
        acceptData: function(e) {
            if (e.nodeType && e.nodeType !== 1 && e.nodeType !== 9) {
                return false
            }
            var b3 = e.nodeName && ay.noData[e.nodeName.toLowerCase()];
            return !b3 || b3 !== true && e.getAttribute("classid") === b3
        }
    });
    ay.fn.extend({
        data: function(b6, b8) {
            var e, b7, b4 = this[0],
                b5 = 0,
                b3 = null;
            if (b6 === bU) {
                if (this.length) {
                    b3 = ay.data(b4);
                    if (b4.nodeType === 1 && !ay._data(b4, "parsedAttrs")) {
                        e = b4.attributes;
                        for (; b5 < e.length; b5++) {
                            b7 = e[b5].name;
                            if (!b7.indexOf("data-")) {
                                b7 = ay.camelCase(b7.slice(5));
                                U(b4, b7, b3[b7])
                            }
                        }
                        ay._data(b4, "parsedAttrs", true)
                    }
                }
                return b3
            }
            if (typeof b6 === "object") {
                return this.each(function() {
                    ay.data(this, b6)
                })
            }
            return ay.access(this, function(b9) {
                if (b9 === bU) {
                    return b4 ? U(b4, b6, ay.data(b4, b6)) : null
                }
                this.each(function() {
                    ay.data(this, b6, b9)
                })
            }, null, b8, arguments.length > 1, null, true)
        },
        removeData: function(e) {
            return this.each(function() {
                ay.removeData(this, e)
            })
        }
    });

    function U(b5, b6, b3) {
        if (b3 === bU && b5.nodeType === 1) {
            var b7 = "data-" + b6.replace(bd, "-$1").toLowerCase();
            b3 = b5.getAttribute(b7);
            if (typeof b3 === "string") {
                try {
                    b3 = b3 === "true" ? true : b3 === "false" ? false : b3 === "null" ? null : +b3 + "" === b3 ? +b3 : aK.test(b3) ? ay.parseJSON(b3) : b3
                } catch (b4) {}
                ay.data(b5, b6, b3)
            } else {
                b3 = bU
            }
        }
        return b3
    }

    function av(b3) {
        var e;
        for (e in b3) {
            if (e === "data" && ay.isEmptyObject(b3[e])) {
                continue
            }
            if (e !== "toJSON") {
                return false
            }
        }
        return true
    }
    ay.extend({
        queue: function(b3, b5, e) {
            var b4;
            if (b3) {
                b5 = (b5 || "fx") + "queue";
                b4 = ay._data(b3, b5);
                if (e) {
                    if (!b4 || ay.isArray(e)) {
                        b4 = ay._data(b3, b5, ay.makeArray(e))
                    } else {
                        b4.push(e)
                    }
                }
                return b4 || []
            }
        },
        dequeue: function(e, b8) {
            b8 = b8 || "fx";
            var b6 = ay.queue(e, b8),
                b7 = b6.length,
                b3 = b6.shift(),
                b4 = ay._queueHooks(e, b8),
                b5 = function() {
                    ay.dequeue(e, b8)
                };
            if (b3 === "inprogress") {
                b3 = b6.shift();
                b7--
            }
            b4.cur = b3;
            if (b3) {
                if (b8 === "fx") {
                    b6.unshift("inprogress")
                }
                delete b4.stop;
                b3.call(e, b5, b4)
            }
            if (!b7 && b4) {
                b4.empty.fire()
            }
        },
        _queueHooks: function(e, b4) {
            var b3 = b4 + "queueHooks";
            return ay._data(e, b3) || ay._data(e, b3, {
                    empty: ay.Callbacks("once memory").add(function() {
                        ay._removeData(e, b4 + "queue");
                        ay._removeData(e, b3)
                    })
                })
        }
    });
    ay.fn.extend({
        queue: function(b4, e) {
            var b3 = 2;
            if (typeof b4 !== "string") {
                e = b4;
                b4 = "fx";
                b3--
            }
            if (arguments.length < b3) {
                return ay.queue(this[0], b4)
            }
            return e === bU ? this : this.each(function() {
                var b5 = ay.queue(this, b4, e);
                ay._queueHooks(this, b4);
                if (b4 === "fx" && b5[0] !== "inprogress") {
                    ay.dequeue(this, b4)
                }
            })
        },
        dequeue: function(e) {
            return this.each(function() {
                ay.dequeue(this, e)
            })
        },
        delay: function(e, b3) {
            e = ay.fx ? ay.fx.speeds[e] || e : e;
            b3 = b3 || "fx";
            return this.queue(b3, function(b5, b4) {
                var b6 = setTimeout(b5, e);
                b4.stop = function() {
                    clearTimeout(b6)
                }
            })
        },
        clearQueue: function(e) {
            return this.queue(e || "fx", [])
        },
        promise: function(b9, b6) {
            var b8, e = 1,
                b3 = ay.Deferred(),
                b4 = this,
                b5 = this.length,
                b7 = function() {
                    if (!(--e)) {
                        b3.resolveWith(b4, [b4])
                    }
                };
            if (typeof b9 !== "string") {
                b6 = b9;
                b9 = bU
            }
            b9 = b9 || "fx";
            while (b5--) {
                b8 = ay._data(b4[b5], b9 + "queueHooks");
                if (b8 && b8.empty) {
                    e++;
                    b8.empty.add(b7)
                }
            }
            b7();
            return b3.promise(b6)
        }
    });
    var aB, r, aN = /[\t\r\n]/g,
        br = /\r/g,
        aX = /^(?:input|select|textarea|button|object)$/i,
        aP = /^(?:a|area)$/i,
        aJ = /^(?:checked|selected|autofocus|autoplay|async|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped)$/i,
        bF = /^(?:checked|selected)$/i,
        aj = ay.support.getSetAttribute,
        ak = ay.support.input;
    ay.fn.extend({
        attr: function(e, b3) {
            return ay.access(this, ay.attr, e, b3, arguments.length > 1)
        },
        removeAttr: function(e) {
            return this.each(function() {
                ay.removeAttr(this, e)
            })
        },
        prop: function(e, b3) {
            return ay.access(this, ay.prop, e, b3, arguments.length > 1)
        },
        removeProp: function(e) {
            e = ay.propFix[e] || e;
            return this.each(function() {
                try {
                    this[e] = bU;
                    delete this[e]
                } catch (b3) {}
            })
        },
        addClass: function(ca) {
            var e, b5, b4, b3, b7, b6 = 0,
                b8 = this.length,
                b9 = typeof ca === "string" && ca;
            if (ay.isFunction(ca)) {
                return this.each(function(cb) {
                    ay(this).addClass(ca.call(this, cb, this.className))
                })
            }
            if (b9) {
                e = (ca || "").match(C) || [];
                for (; b6 < b8; b6++) {
                    b5 = this[b6];
                    b4 = b5.nodeType === 1 && (b5.className ? (" " + b5.className + " ").replace(aN, " ") : " ");
                    if (b4) {
                        b7 = 0;
                        while ((b3 = e[b7++])) {
                            if (b4.indexOf(" " + b3 + " ") < 0) {
                                b4 += b3 + " "
                            }
                        }
                        b5.className = ay.trim(b4)
                    }
                }
            }
            return this
        },
        removeClass: function(ca) {
            var e, b5, b4, b3, b7, b6 = 0,
                b8 = this.length,
                b9 = arguments.length === 0 || typeof ca === "string" && ca;
            if (ay.isFunction(ca)) {
                return this.each(function(cb) {
                    ay(this).removeClass(ca.call(this, cb, this.className))
                })
            }
            if (b9) {
                e = (ca || "").match(C) || [];
                for (; b6 < b8; b6++) {
                    b5 = this[b6];
                    b4 = b5.nodeType === 1 && (b5.className ? (" " + b5.className + " ").replace(aN, " ") : "");
                    if (b4) {
                        b7 = 0;
                        while ((b3 = e[b7++])) {
                            while (b4.indexOf(" " + b3 + " ") >= 0) {
                                b4 = b4.replace(" " + b3 + " ", " ")
                            }
                        }
                        b5.className = ca ? ay.trim(b4) : ""
                    }
                }
            }
            return this
        },
        toggleClass: function(b5, b3) {
            var b4 = typeof b5,
                e = typeof b3 === "boolean";
            if (ay.isFunction(b5)) {
                return this.each(function(b6) {
                    ay(this).toggleClass(b5.call(this, b6, this.className, b3), b3)
                })
            }
            return this.each(function() {
                if (b4 === "string") {
                    var b6, b8 = 0,
                        b9 = ay(this),
                        ca = b3,
                        b7 = b5.match(C) || [];
                    while ((b6 = b7[b8++])) {
                        ca = e ? ca : !b9.hasClass(b6);
                        b9[ca ? "addClass" : "removeClass"](b6)
                    }
                } else {
                    if (b4 === E || b4 === "boolean") {
                        if (this.className) {
                            ay._data(this, "__className__", this.className)
                        }
                        this.className = this.className || b5 === false ? "" : ay._data(this, "__className__") || ""
                    }
                }
            })
        },
        hasClass: function(b5) {
            var e = " " + b5 + " ",
                b3 = 0,
                b4 = this.length;
            for (; b3 < b4; b3++) {
                if (this[b3].nodeType === 1 && (" " + this[b3].className + " ").replace(aN, " ").indexOf(e) >= 0) {
                    return true
                }
            }
            return false
        },
        val: function(b6) {
            var b5, b3, b4, e = this[0];
            if (!arguments.length) {
                if (e) {
                    b3 = ay.valHooks[e.type] || ay.valHooks[e.nodeName.toLowerCase()];
                    if (b3 && "get" in b3 && (b5 = b3.get(e, "value")) !== bU) {
                        return b5
                    }
                    b5 = e.value;
                    return typeof b5 === "string" ? b5.replace(br, "") : b5 == null ? "" : b5
                }
                return
            }
            b4 = ay.isFunction(b6);
            return this.each(function(b7) {
                var b9, b8 = ay(this);
                if (this.nodeType !== 1) {
                    return
                }
                if (b4) {
                    b9 = b6.call(this, b7, b8.val())
                } else {
                    b9 = b6
                } if (b9 == null) {
                    b9 = ""
                } else {
                    if (typeof b9 === "number") {
                        b9 += ""
                    } else {
                        if (ay.isArray(b9)) {
                            b9 = ay.map(b9, function(ca) {
                                return ca == null ? "" : ca + ""
                            })
                        }
                    }
                }
                b3 = ay.valHooks[this.type] || ay.valHooks[this.nodeName.toLowerCase()];
                if (!b3 || !("set" in b3) || b3.set(this, b9, "value") === bU) {
                    this.value = b9
                }
            })
        }
    });
    ay.extend({
        valHooks: {
            option: {
                get: function(e) {
                    var b3 = e.attributes.value;
                    return !b3 || b3.specified ? e.value : e.text
                }
            },
            select: {
                get: function(e) {
                    var b9, b7, b8 = e.options,
                        b4 = e.selectedIndex,
                        b6 = e.type === "select-one" || b4 < 0,
                        ca = b6 ? null : [],
                        b5 = b6 ? b4 + 1 : b8.length,
                        b3 = b4 < 0 ? b5 : b6 ? b4 : 0;
                    for (; b3 < b5; b3++) {
                        b7 = b8[b3];
                        if ((b7.selected || b3 === b4) && (ay.support.optDisabled ? !b7.disabled : b7.getAttribute("disabled") === null) && (!b7.parentNode.disabled || !ay.nodeName(b7.parentNode, "optgroup"))) {
                            b9 = ay(b7).val();
                            if (b6) {
                                return b9
                            }
                            ca.push(b9)
                        }
                    }
                    return ca
                },
                set: function(e, b3) {
                    var b4 = ay.makeArray(b3);
                    ay(e).find("option").each(function() {
                        this.selected = ay.inArray(ay(this).val(), b4) >= 0
                    });
                    if (!b4.length) {
                        e.selectedIndex = -1
                    }
                    return b4
                }
            }
        },
        attr: function(e, b4, b8) {
            var b3, b5, b7, b6 = e.nodeType;
            if (!e || b6 === 3 || b6 === 8 || b6 === 2) {
                return
            }
            if (typeof e.getAttribute === E) {
                return ay.prop(e, b4, b8)
            }
            b5 = b6 !== 1 || !ay.isXMLDoc(e);
            if (b5) {
                b4 = b4.toLowerCase();
                b3 = ay.attrHooks[b4] || (aJ.test(b4) ? r : aB)
            }
            if (b8 !== bU) {
                if (b8 === null) {
                    ay.removeAttr(e, b4)
                } else {
                    if (b3 && b5 && "set" in b3 && (b7 = b3.set(e, b8, b4)) !== bU) {
                        return b7
                    } else {
                        e.setAttribute(b4, b8 + "");
                        return b8
                    }
                }
            } else {
                if (b3 && b5 && "get" in b3 && (b7 = b3.get(e, b4)) !== null) {
                    return b7
                } else {
                    if (typeof e.getAttribute !== E) {
                        b7 = e.getAttribute(b4)
                    }
                    return b7 == null ? bU : b7
                }
            }
        },
        removeAttr: function(b3, b7) {
            var b5, b6, b4 = 0,
                e = b7 && b7.match(C);
            if (e && b3.nodeType === 1) {
                while ((b5 = e[b4++])) {
                    b6 = ay.propFix[b5] || b5;
                    if (aJ.test(b5)) {
                        if (!aj && bF.test(b5)) {
                            b3[ay.camelCase("default-" + b5)] = b3[b6] = false
                        } else {
                            b3[b6] = false
                        }
                    } else {
                        ay.attr(b3, b5, "")
                    }
                    b3.removeAttribute(aj ? b5 : b6)
                }
            }
        },
        attrHooks: {
            type: {
                set: function(e, b4) {
                    if (!ay.support.radioValue && b4 === "radio" && ay.nodeName(e, "input")) {
                        var b3 = e.value;
                        e.setAttribute("type", b4);
                        if (b3) {
                            e.value = b3
                        }
                        return b4
                    }
                }
            }
        },
        propFix: {
            tabindex: "tabIndex",
            readonly: "readOnly",
            "for": "htmlFor",
            "class": "className",
            maxlength: "maxLength",
            cellspacing: "cellSpacing",
            cellpadding: "cellPadding",
            rowspan: "rowSpan",
            colspan: "colSpan",
            usemap: "useMap",
            frameborder: "frameBorder",
            contenteditable: "contentEditable"
        },
        prop: function(e, b4, b8) {
            var b7, b3, b5, b6 = e.nodeType;
            if (!e || b6 === 3 || b6 === 8 || b6 === 2) {
                return
            }
            b5 = b6 !== 1 || !ay.isXMLDoc(e);
            if (b5) {
                b4 = ay.propFix[b4] || b4;
                b3 = ay.propHooks[b4]
            }
            if (b8 !== bU) {
                if (b3 && "set" in b3 && (b7 = b3.set(e, b8, b4)) !== bU) {
                    return b7
                } else {
                    return (e[b4] = b8)
                }
            } else {
                if (b3 && "get" in b3 && (b7 = b3.get(e, b4)) !== null) {
                    return b7
                } else {
                    return e[b4]
                }
            }
        },
        propHooks: {
            tabIndex: {
                get: function(b3) {
                    var e = b3.getAttributeNode("tabindex");
                    return e && e.specified ? parseInt(e.value, 10) : aX.test(b3.nodeName) || aP.test(b3.nodeName) && b3.href ? 0 : bU
                }
            }
        }
    });
    r = {
        get: function(b4, b5) {
            var b6 = ay.prop(b4, b5),
                e = typeof b6 === "boolean" && b4.getAttribute(b5),
                b3 = typeof b6 === "boolean" ? ak && aj ? e != null : bF.test(b5) ? b4[ay.camelCase("default-" + b5)] : !! e : b4.getAttributeNode(b5);
            return b3 && b3.value !== false ? b5.toLowerCase() : bU
        },
        set: function(e, b4, b3) {
            if (b4 === false) {
                ay.removeAttr(e, b3)
            } else {
                if (ak && aj || !bF.test(b3)) {
                    e.setAttribute(!aj && ay.propFix[b3] || b3, b3)
                } else {
                    e[ay.camelCase("default-" + b3)] = e[b3] = true
                }
            }
            return b3
        }
    };
    if (!ak || !aj) {
        ay.attrHooks.value = {
            get: function(e, b3) {
                var b4 = e.getAttributeNode(b3);
                return ay.nodeName(e, "input") ? e.defaultValue : b4 && b4.specified ? b4.value : bU
            },
            set: function(e, b4, b3) {
                if (ay.nodeName(e, "input")) {
                    e.defaultValue = b4
                } else {
                    return aB && aB.set(e, b4, b3)
                }
            }
        }
    }
    if (!aj) {
        aB = ay.valHooks.button = {
            get: function(e, b3) {
                var b4 = e.getAttributeNode(b3);
                return b4 && (b3 === "id" || b3 === "name" || b3 === "coords" ? b4.value !== "" : b4.specified) ? b4.value : bU
            },
            set: function(e, b5, b3) {
                var b4 = e.getAttributeNode(b3);
                if (!b4) {
                    e.setAttributeNode((b4 = e.ownerDocument.createAttribute(b3)))
                }
                b4.value = b5 += "";
                return b3 === "value" || b5 === e.getAttribute(b3) ? b5 : bU
            }
        };
        ay.attrHooks.contenteditable = {
            get: aB.get,
            set: function(e, b4, b3) {
                aB.set(e, b4 === "" ? false : b4, b3)
            }
        };
        ay.each(["width", "height"], function(e, b3) {
            ay.attrHooks[b3] = ay.extend(ay.attrHooks[b3], {
                set: function(b4, b5) {
                    if (b5 === "") {
                        b4.setAttribute(b3, "auto");
                        return b5
                    }
                }
            })
        })
    }
    if (!ay.support.hrefNormalized) {
        ay.each(["href", "src", "width", "height"], function(e, b3) {
            ay.attrHooks[b3] = ay.extend(ay.attrHooks[b3], {
                get: function(b4) {
                    var b5 = b4.getAttribute(b3, 2);
                    return b5 == null ? bU : b5
                }
            })
        });
        ay.each(["href", "src"], function(e, b3) {
            ay.propHooks[b3] = {
                get: function(b4) {
                    return b4.getAttribute(b3, 4)
                }
            }
        })
    }
    if (!ay.support.style) {
        ay.attrHooks.style = {
            get: function(e) {
                return e.style.cssText || bU
            },
            set: function(e, b3) {
                return (e.style.cssText = b3 + "")
            }
        }
    }
    if (!ay.support.optSelected) {
        ay.propHooks.selected = ay.extend(ay.propHooks.selected, {
            get: function(e) {
                var b3 = e.parentNode;
                if (b3) {
                    b3.selectedIndex;
                    if (b3.parentNode) {
                        b3.parentNode.selectedIndex
                    }
                }
                return null
            }
        })
    }
    if (!ay.support.enctype) {
        ay.propFix.enctype = "encoding"
    }
    if (!ay.support.checkOn) {
        ay.each(["radio", "checkbox"], function() {
            ay.valHooks[this] = {
                get: function(e) {
                    return e.getAttribute("value") === null ? "on" : e.value
                }
            }
        })
    }
    ay.each(["radio", "checkbox"], function() {
        ay.valHooks[this] = ay.extend(ay.valHooks[this], {
            set: function(e, b3) {
                if (ay.isArray(b3)) {
                    return (e.checked = ay.inArray(ay(e).val(), b3) >= 0)
                }
            }
        })
    });
    var aZ = /^(?:input|select|textarea)$/i,
        a7 = /^key/,
        bb = /^(?:mouse|contextmenu)|click/,
        aY = /^(?:focusinfocus|focusoutblur)$/,
        bC = /^([^.]*)(?:\.(.+)|)$/;

    function aW() {
        return true
    }

    function aV() {
        return false
    }
    ay.event = {
        global: {},
        add: function(b3, ci, b9, e, cd) {
            var cg, b6, cf, b8, ce, b5, b7, ca, ch, cb, cc, b4 = ay._data(b3);
            if (!b4) {
                return
            }
            if (b9.handler) {
                b8 = b9;
                b9 = b8.handler;
                cd = b8.selector
            }
            if (!b9.guid) {
                b9.guid = ay.guid++
            }
            if (!(b6 = b4.events)) {
                b6 = b4.events = {}
            }
            if (!(b5 = b4.handle)) {
                b5 = b4.handle = function(cj) {
                    return typeof ay !== E && (!cj || ay.event.triggered !== cj.type) ? ay.event.dispatch.apply(b5.elem, arguments) : bU
                };
                b5.elem = b3
            }
            ci = (ci || "").match(C) || [""];
            cf = ci.length;
            while (cf--) {
                cg = bC.exec(ci[cf]) || [];
                ch = cc = cg[1];
                cb = (cg[2] || "").split(".").sort();
                ce = ay.event.special[ch] || {};
                ch = (cd ? ce.delegateType : ce.bindType) || ch;
                ce = ay.event.special[ch] || {};
                b7 = ay.extend({
                    type: ch,
                    origType: cc,
                    data: e,
                    handler: b9,
                    guid: b9.guid,
                    selector: cd,
                    needsContext: cd && ay.expr.match.needsContext.test(cd),
                    namespace: cb.join(".")
                }, b8);
                if (!(ca = b6[ch])) {
                    ca = b6[ch] = [];
                    ca.delegateCount = 0;
                    if (!ce.setup || ce.setup.call(b3, e, cb, b5) === false) {
                        if (b3.addEventListener) {
                            b3.addEventListener(ch, b5, false)
                        } else {
                            if (b3.attachEvent) {
                                b3.attachEvent("on" + ch, b5)
                            }
                        }
                    }
                }
                if (ce.add) {
                    ce.add.call(b3, b7);
                    if (!b7.handler.guid) {
                        b7.handler.guid = b9.guid
                    }
                }
                if (cd) {
                    ca.splice(ca.delegateCount++, 0, b7)
                } else {
                    ca.push(b7)
                }
                ay.event.global[ch] = true
            }
            b3 = null
        },
        remove: function(e, ci, b6, cd, b9) {
            var b8, b5, cg, cb, cf, b4, ce, b7, ch, ca, cc, b3 = ay.hasData(e) && ay._data(e);
            if (!b3 || !(b4 = b3.events)) {
                return
            }
            ci = (ci || "").match(C) || [""];
            cf = ci.length;
            while (cf--) {
                cg = bC.exec(ci[cf]) || [];
                ch = cc = cg[1];
                ca = (cg[2] || "").split(".").sort();
                if (!ch) {
                    for (ch in b4) {
                        ay.event.remove(e, ch + ci[cf], b6, cd, true)
                    }
                    continue
                }
                ce = ay.event.special[ch] || {};
                ch = (cd ? ce.delegateType : ce.bindType) || ch;
                b7 = b4[ch] || [];
                cg = cg[2] && new RegExp("(^|\\.)" + ca.join("\\.(?:.*\\.|)") + "(\\.|$)");
                cb = b8 = b7.length;
                while (b8--) {
                    b5 = b7[b8];
                    if ((b9 || cc === b5.origType) && (!b6 || b6.guid === b5.guid) && (!cg || cg.test(b5.namespace)) && (!cd || cd === b5.selector || cd === "**" && b5.selector)) {
                        b7.splice(b8, 1);
                        if (b5.selector) {
                            b7.delegateCount--
                        }
                        if (ce.remove) {
                            ce.remove.call(e, b5)
                        }
                    }
                }
                if (cb && !b7.length) {
                    if (!ce.teardown || ce.teardown.call(e, ca, b3.handle) === false) {
                        ay.removeEvent(e, ch, b3.handle)
                    }
                    delete b4[ch]
                }
            }
            if (ay.isEmptyObject(b4)) {
                delete b3.handle;
                ay._removeData(e, "events")
            }
        },
        trigger: function(b8, b5, b7, cd) {
            var ca, ce, b4, b3, cf, cg, cb, b9 = [b7 || Y],
                ch = y.call(b8, "type") ? b8.type : b8,
                cc = y.call(b8, "namespace") ? b8.namespace.split(".") : [];
            b4 = cg = b7 = b7 || Y;
            if (b7.nodeType === 3 || b7.nodeType === 8) {
                return
            }
            if (aY.test(ch + ay.event.triggered)) {
                return
            }
            if (ch.indexOf(".") >= 0) {
                cc = ch.split(".");
                ch = cc.shift();
                cc.sort()
            }
            ce = ch.indexOf(":") < 0 && "on" + ch;
            b8 = b8[ay.expando] ? b8 : new ay.Event(ch, typeof b8 === "object" && b8);
            b8.isTrigger = true;
            b8.namespace = cc.join(".");
            b8.namespace_re = b8.namespace ? new RegExp("(^|\\.)" + cc.join("\\.(?:.*\\.|)") + "(\\.|$)") : null;
            b8.result = bU;
            if (!b8.target) {
                b8.target = b7
            }
            b5 = b5 == null ? [b8] : ay.makeArray(b5, [b8]);
            cf = ay.event.special[ch] || {};
            if (!cd && cf.trigger && cf.trigger.apply(b7, b5) === false) {
                return
            }
            if (!cd && !cf.noBubble && !ay.isWindow(b7)) {
                b3 = cf.delegateType || ch;
                if (!aY.test(b3 + ch)) {
                    b4 = b4.parentNode
                }
                for (; b4; b4 = b4.parentNode) {
                    b9.push(b4);
                    cg = b4
                }
                if (cg === (b7.ownerDocument || Y)) {
                    b9.push(cg.defaultView || cg.parentWindow || bW)
                }
            }
            cb = 0;
            while ((b4 = b9[cb++]) && !b8.isPropagationStopped()) {
                b8.type = cb > 1 ? b3 : cf.bindType || ch;
                ca = (ay._data(b4, "events") || {})[b8.type] && ay._data(b4, "handle");
                if (ca) {
                    ca.apply(b4, b5)
                }
                ca = ce && b4[ce];
                if (ca && ay.acceptData(b4) && ca.apply && ca.apply(b4, b5) === false) {
                    b8.preventDefault()
                }
            }
            b8.type = ch;
            if (!cd && !b8.isDefaultPrevented()) {
                if ((!cf._default || cf._default.apply(b7.ownerDocument, b5) === false) && !(ch === "click" && ay.nodeName(b7, "a")) && ay.acceptData(b7)) {
                    if (ce && b7[ch] && !ay.isWindow(b7)) {
                        cg = b7[ce];
                        if (cg) {
                            b7[ce] = null
                        }
                        ay.event.triggered = ch;
                        try {
                            b7[ch]()
                        } catch (b6) {}
                        ay.event.triggered = bU;
                        if (cg) {
                            b7[ce] = cg
                        }
                    }
                }
            }
            return b8.result
        },
        dispatch: function(b3) {
            b3 = ay.event.fix(b3);
            var b7, ca, b4, b9, b8, b5 = [],
                e = D.call(arguments),
                b6 = (ay._data(this, "events") || {})[b3.type] || [],
                cb = ay.event.special[b3.type] || {};
            e[0] = b3;
            b3.delegateTarget = this;
            if (cb.preDispatch && cb.preDispatch.call(this, b3) === false) {
                return
            }
            b5 = ay.event.handlers.call(this, b3, b6);
            b7 = 0;
            while ((b9 = b5[b7++]) && !b3.isPropagationStopped()) {
                b3.currentTarget = b9.elem;
                b8 = 0;
                while ((b4 = b9.handlers[b8++]) && !b3.isImmediatePropagationStopped()) {
                    if (!b3.namespace_re || b3.namespace_re.test(b4.namespace)) {
                        b3.handleObj = b4;
                        b3.data = b4.data;
                        ca = ((ay.event.special[b4.origType] || {}).handle || b4.handler).apply(b9.elem, e);
                        if (ca !== bU) {
                            if ((b3.result = ca) === false) {
                                b3.preventDefault();
                                b3.stopPropagation()
                            }
                        }
                    }
                }
            }
            if (cb.postDispatch) {
                cb.postDispatch.call(this, b3)
            }
            return b3.result
        },
        handlers: function(b4, b7) {
            var ca, b5, b9, b8, b6 = [],
                b3 = b7.delegateCount,
                e = b4.target;
            if (b3 && e.nodeType && (!b4.button || b4.type !== "click")) {
                for (; e != this; e = e.parentNode || this) {
                    if (e.nodeType === 1 && (e.disabled !== true || b4.type !== "click")) {
                        b9 = [];
                        for (b8 = 0; b8 < b3; b8++) {
                            b5 = b7[b8];
                            ca = b5.selector + " ";
                            if (b9[ca] === bU) {
                                b9[ca] = b5.needsContext ? ay(ca, this).index(e) >= 0 : ay.find(ca, this, null, [e]).length
                            }
                            if (b9[ca]) {
                                b9.push(b5)
                            }
                        }
                        if (b9.length) {
                            b6.push({
                                elem: e,
                                handlers: b9
                            })
                        }
                    }
                }
            }
            if (b3 < b7.length) {
                b6.push({
                    elem: this,
                    handlers: b7.slice(b3)
                })
            }
            return b6
        },
        fix: function(b3) {
            if (b3[ay.expando]) {
                return b3
            }
            var b5, b7, e, b8 = b3.type,
                b6 = b3,
                b4 = this.fixHooks[b8];
            if (!b4) {
                this.fixHooks[b8] = b4 = bb.test(b8) ? this.mouseHooks : a7.test(b8) ? this.keyHooks : {}
            }
            e = b4.props ? this.props.concat(b4.props) : this.props;
            b3 = new ay.Event(b6);
            b5 = e.length;
            while (b5--) {
                b7 = e[b5];
                b3[b7] = b6[b7]
            }
            if (!b3.target) {
                b3.target = b6.srcElement || Y
            }
            if (b3.target.nodeType === 3) {
                b3.target = b3.target.parentNode
            }
            b3.metaKey = !! b3.metaKey;
            return b4.filter ? b4.filter(b3, b6) : b3
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(e, b3) {
                if (e.which == null) {
                    e.which = b3.charCode != null ? b3.charCode : b3.keyCode
                }
                return e
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(b5, b8) {
                var e, b6, b4, b3 = b8.button,
                    b7 = b8.fromElement;
                if (b5.pageX == null && b8.clientX != null) {
                    b6 = b5.target.ownerDocument || Y;
                    b4 = b6.documentElement;
                    e = b6.body;
                    b5.pageX = b8.clientX + (b4 && b4.scrollLeft || e && e.scrollLeft || 0) - (b4 && b4.clientLeft || e && e.clientLeft || 0);
                    b5.pageY = b8.clientY + (b4 && b4.scrollTop || e && e.scrollTop || 0) - (b4 && b4.clientTop || e && e.clientTop || 0)
                }
                if (!b5.relatedTarget && b7) {
                    b5.relatedTarget = b7 === b5.target ? b8.toElement : b7
                }
                if (!b5.which && b3 !== bU) {
                    b5.which = (b3 & 1 ? 1 : (b3 & 2 ? 3 : (b3 & 4 ? 2 : 0)))
                }
                return b5
            }
        },
        special: {
            load: {
                noBubble: true
            },
            click: {
                trigger: function() {
                    if (ay.nodeName(this, "input") && this.type === "checkbox" && this.click) {
                        this.click();
                        return false
                    }
                }
            },
            focus: {
                trigger: function() {
                    if (this !== Y.activeElement && this.focus) {
                        try {
                            this.focus();
                            return false
                        } catch (b3) {}
                    }
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    if (this === Y.activeElement && this.blur) {
                        this.blur();
                        return false
                    }
                },
                delegateType: "focusout"
            },
            beforeunload: {
                postDispatch: function(e) {
                    if (e.result !== bU) {
                        e.originalEvent.returnValue = e.result
                    }
                }
            }
        },
        simulate: function(b7, b5, b6, b3) {
            var b4 = ay.extend(new ay.Event(), b6, {
                type: b7,
                isSimulated: true,
                originalEvent: {}
            });
            if (b3) {
                ay.event.trigger(b4, null, b5)
            } else {
                ay.event.dispatch.call(b5, b4)
            } if (b4.isDefaultPrevented()) {
                b6.preventDefault()
            }
        }
    };
    ay.removeEvent = Y.removeEventListener ? function(e, b4, b3) {
        if (e.removeEventListener) {
            e.removeEventListener(b4, b3, false)
        }
    } : function(e, b5, b3) {
        var b4 = "on" + b5;
        if (e.detachEvent) {
            if (typeof e[b4] === E) {
                e[b4] = null
            }
            e.detachEvent(b4, b3)
        }
    };
    ay.Event = function(b3, e) {
        if (!(this instanceof ay.Event)) {
            return new ay.Event(b3, e)
        }
        if (b3 && b3.type) {
            this.originalEvent = b3;
            this.type = b3.type;
            this.isDefaultPrevented = (b3.defaultPrevented || b3.returnValue === false || b3.getPreventDefault && b3.getPreventDefault()) ? aW : aV
        } else {
            this.type = b3
        } if (e) {
            ay.extend(this, e)
        }
        this.timeStamp = b3 && b3.timeStamp || ay.now();
        this[ay.expando] = true
    };
    ay.Event.prototype = {
        isDefaultPrevented: aV,
        isPropagationStopped: aV,
        isImmediatePropagationStopped: aV,
        preventDefault: function() {
            var b3 = this.originalEvent;
            this.isDefaultPrevented = aW;
            if (!b3) {
                return
            }
            if (b3.preventDefault) {
                b3.preventDefault()
            } else {
                b3.returnValue = false
            }
        },
        stopPropagation: function() {
            var b3 = this.originalEvent;
            this.isPropagationStopped = aW;
            if (!b3) {
                return
            }
            if (b3.stopPropagation) {
                b3.stopPropagation()
            }
            b3.cancelBubble = true
        },
        stopImmediatePropagation: function() {
            this.isImmediatePropagationStopped = aW;
            this.stopPropagation()
        }
    };
    ay.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout"
    }, function(b3, e) {
        ay.event.special[b3] = {
            delegateType: e,
            bindType: e,
            handle: function(b4) {
                var b7, b8 = this,
                    b6 = b4.relatedTarget,
                    b5 = b4.handleObj;
                if (!b6 || (b6 !== b8 && !ay.contains(b8, b6))) {
                    b4.type = b5.origType;
                    b7 = b5.handler.apply(this, arguments);
                    b4.type = e
                }
                return b7
            }
        }
    });
    if (!ay.support.submitBubbles) {
        ay.event.special.submit = {
            setup: function() {
                if (ay.nodeName(this, "form")) {
                    return false
                }
                ay.event.add(this, "click._submit keypress._submit", function(b3) {
                    var b4 = b3.target,
                        b5 = ay.nodeName(b4, "input") || ay.nodeName(b4, "button") ? b4.form : bU;
                    if (b5 && !ay._data(b5, "submitBubbles")) {
                        ay.event.add(b5, "submit._submit", function(e) {
                            e._submit_bubble = true
                        });
                        ay._data(b5, "submitBubbles", true)
                    }
                })
            },
            postDispatch: function(e) {
                if (e._submit_bubble) {
                    delete e._submit_bubble;
                    if (this.parentNode && !e.isTrigger) {
                        ay.event.simulate("submit", this.parentNode, e, true)
                    }
                }
            },
            teardown: function() {
                if (ay.nodeName(this, "form")) {
                    return false
                }
                ay.event.remove(this, "._submit")
            }
        }
    }
    if (!ay.support.changeBubbles) {
        ay.event.special.change = {
            setup: function() {
                if (aZ.test(this.nodeName)) {
                    if (this.type === "checkbox" || this.type === "radio") {
                        ay.event.add(this, "propertychange._change", function(e) {
                            if (e.originalEvent.propertyName === "checked") {
                                this._just_changed = true
                            }
                        });
                        ay.event.add(this, "click._change", function(e) {
                            if (this._just_changed && !e.isTrigger) {
                                this._just_changed = false
                            }
                            ay.event.simulate("change", this, e, true)
                        })
                    }
                    return false
                }
                ay.event.add(this, "beforeactivate._change", function(b3) {
                    var b4 = b3.target;
                    if (aZ.test(b4.nodeName) && !ay._data(b4, "changeBubbles")) {
                        ay.event.add(b4, "change._change", function(e) {
                            if (this.parentNode && !e.isSimulated && !e.isTrigger) {
                                ay.event.simulate("change", this.parentNode, e, true)
                            }
                        });
                        ay._data(b4, "changeBubbles", true)
                    }
                })
            },
            handle: function(b3) {
                var e = b3.target;
                if (this !== e || b3.isSimulated || b3.isTrigger || (e.type !== "radio" && e.type !== "checkbox")) {
                    return b3.handleObj.handler.apply(this, arguments)
                }
            },
            teardown: function() {
                ay.event.remove(this, "._change");
                return !aZ.test(this.nodeName)
            }
        }
    }
    if (!ay.support.focusinBubbles) {
        ay.each({
            focus: "focusin",
            blur: "focusout"
        }, function(b5, b3) {
            var e = 0,
                b4 = function(b6) {
                    ay.event.simulate(b3, b6.target, ay.event.fix(b6), true)
                };
            ay.event.special[b3] = {
                setup: function() {
                    if (e++ === 0) {
                        Y.addEventListener(b5, b4, true)
                    }
                },
                teardown: function() {
                    if (--e === 0) {
                        Y.removeEventListener(b5, b4, true)
                    }
                }
            }
        })
    }
    ay.fn.extend({
        on: function(b8, b6, e, b3, b4) {
            var b7, b5;
            if (typeof b8 === "object") {
                if (typeof b6 !== "string") {
                    e = e || b6;
                    b6 = bU
                }
                for (b7 in b8) {
                    this.on(b7, b6, e, b8[b7], b4)
                }
                return this
            }
            if (e == null && b3 == null) {
                b3 = b6;
                e = b6 = bU
            } else {
                if (b3 == null) {
                    if (typeof b6 === "string") {
                        b3 = e;
                        e = bU
                    } else {
                        b3 = e;
                        e = b6;
                        b6 = bU
                    }
                }
            } if (b3 === false) {
                b3 = aV
            } else {
                if (!b3) {
                    return this
                }
            } if (b4 === 1) {
                b5 = b3;
                b3 = function(b9) {
                    ay().off(b9);
                    return b5.apply(this, arguments)
                };
                b3.guid = b5.guid || (b5.guid = ay.guid++)
            }
            return this.each(function() {
                ay.event.add(this, b8, b3, e, b6)
            })
        },
        one: function(b5, b4, e, b3) {
            return this.on(b5, b4, e, b3, 1)
        },
        off: function(b6, b4, e) {
            var b3, b5;
            if (b6 && b6.preventDefault && b6.handleObj) {
                b3 = b6.handleObj;
                ay(b6.delegateTarget).off(b3.namespace ? b3.origType + "." + b3.namespace : b3.origType, b3.selector, b3.handler);
                return this
            }
            if (typeof b6 === "object") {
                for (b5 in b6) {
                    this.off(b5, b4, b6[b5])
                }
                return this
            }
            if (b4 === false || typeof b4 === "function") {
                e = b4;
                b4 = bU
            }
            if (e === false) {
                e = aV
            }
            return this.each(function() {
                ay.event.remove(this, b6, e, b4)
            })
        },
        bind: function(b4, e, b3) {
            return this.on(b4, null, e, b3)
        },
        unbind: function(b3, e) {
            return this.off(b3, null, e)
        },
        delegate: function(b4, b5, e, b3) {
            return this.on(b5, b4, e, b3)
        },
        undelegate: function(b3, b4, e) {
            return arguments.length === 1 ? this.off(b3, "**") : this.off(b4, b3 || "**", e)
        },
        trigger: function(b3, e) {
            return this.each(function() {
                ay.event.trigger(b3, e, this)
            })
        },
        triggerHandler: function(b4, e) {
            var b3 = this[0];
            if (b3) {
                return ay.event.trigger(b4, e, b3, true)
            }
        }
    });
    /*
     * Sizzle CSS Selector Engine
     * Copyright 2012 jQuery Foundation and other contributors
     * Released under the MIT license
     * http://sizzlejs.com/
     */
    (function(dg, de) {
        var cu, b7, cq, cs, cy, ca, ct, cH, c2, ck, cj, cl, cO, cN, cC, cd, c8, cp = "sizzle" + -(new Date()),
            cJ = dg.document,
            da = {}, ci = 0,
            cm = 0,
            b9 = cf(),
            db = cf(),
            cb = cf(),
            c9 = typeof de,
            cE = 1 << 31,
            b4 = [],
            cI = b4.pop,
            cL = b4.push,
            c7 = b4.slice,
            cw = b4.indexOf || function(e) {
                    var dh = 0,
                        di = this.length;
                    for (; dh < di; dh++) {
                        if (this[dh] === e) {
                            return dh
                        }
                    }
                    return -1
                }, df = "[\\x20\\t\\r\\n\\f]",
            b8 = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
            cv = b8.replace("w", "w#"),
            cG = "([*^$|!~]?=)",
            b6 = "\\[" + df + "*(" + b8 + ")" + df + "*(?:" + cG + df + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + cv + ")|)|)" + df + "*\\]",
            cK = ":(" + b8 + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + b6.replace(3, 8) + ")*)|.*)\\)|)",
            cZ = new RegExp("^" + df + "+|((?:^|[^\\\\])(?:\\\\.)*)" + df + "+$", "g"),
            cQ = new RegExp("^" + df + "*," + df + "*"),
            cP = new RegExp("^" + df + "*([\\x20\\t\\r\\n\\f>+~])" + df + "*"),
            cW = new RegExp(cK),
            cT = new RegExp("^" + cv + "$"),
            cD = {
                ID: new RegExp("^#(" + b8 + ")"),
                CLASS: new RegExp("^\\.(" + b8 + ")"),
                NAME: new RegExp("^\\[name=['\"]?(" + b8 + ")['\"]?\\]"),
                TAG: new RegExp("^(" + b8.replace("w", "w*") + ")"),
                ATTR: new RegExp("^" + b6),
                PSEUDO: new RegExp("^" + cK),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + df + "*(even|odd|(([+-]|)(\\d*)n|)" + df + "*(?:([+-]|)" + df + "*(\\d+)|))" + df + "*\\)|)", "i"),
                needsContext: new RegExp("^" + df + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + df + "*((?:-\\d)?\\d*)" + df + "*\\)|)(?=[^-]|$)", "i")
            }, cY = /[\x20\t\r\n\f]*[+~]/,
            cV = /^[^{]+\{\s*\[native code/,
            cX = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            cU = /^(?:input|select|textarea|button)$/i,
            cS = /^h\d$/i,
            cR = /'|\\/g,
            cM = /\=[\x20\t\r\n\f]*([^'"\]]*)[\x20\t\r\n\f]*\]/g,
            c0 = /\\([\da-fA-F]{1,6}[\x20\t\r\n\f]?|.)/g,
            cr = function(e, dh) {
                var di = "0x" + dh - 65536;
                return di !== di ? dh : di < 0 ? String.fromCharCode(di + 65536) : String.fromCharCode(di >> 10 | 55296, di & 1023 | 56320)
            };
        try {
            c7.call(cJ.documentElement.childNodes, 0)[0].nodeType
        } catch (cn) {
            c7 = function(dh) {
                var e, di = [];
                while ((e = this[dh++])) {
                    di.push(e)
                }
                return di
            }
        }

        function cx(e) {
            return cV.test(e + "")
        }

        function cf() {
            var e, dh = [];
            return (e = function(di, dj) {
                if (dh.push(di += " ") > cq.cacheLength) {
                    delete e[dh.shift()]
                }
                return (e[di] = dj)
            })
        }

        function cz(e) {
            e[cp] = true;
            return e
        }

        function b5(dj) {
            var dh = ck.createElement("div");
            try {
                return dj(dh)
            } catch (di) {
                return false
            } finally {
                dh = null
            }
        }

        function c6(dv, e, dt, du) {
            var dl, dh, dk, dq, dj, di, dr, dp, dm, dn;
            if ((e ? e.ownerDocument || e : cJ) !== ck) {
                c2(e)
            }
            e = e || ck;
            dt = dt || [];
            if (!dv || typeof dv !== "string") {
                return dt
            }
            if ((dq = e.nodeType) !== 1 && dq !== 9) {
                return []
            }
            if (!cl && !du) {
                if ((dl = cX.exec(dv))) {
                    if ((dk = dl[1])) {
                        if (dq === 9) {
                            dh = e.getElementById(dk);
                            if (dh && dh.parentNode) {
                                if (dh.id === dk) {
                                    dt.push(dh);
                                    return dt
                                }
                            } else {
                                return dt
                            }
                        } else {
                            if (e.ownerDocument && (dh = e.ownerDocument.getElementById(dk)) && cd(e, dh) && dh.id === dk) {
                                dt.push(dh);
                                return dt
                            }
                        }
                    } else {
                        if (dl[2]) {
                            cL.apply(dt, c7.call(e.getElementsByTagName(dv), 0));
                            return dt
                        } else {
                            if ((dk = dl[3]) && da.getByClassName && e.getElementsByClassName) {
                                cL.apply(dt, c7.call(e.getElementsByClassName(dk), 0));
                                return dt
                            }
                        }
                    }
                }
                if (da.qsa && !cO.test(dv)) {
                    dr = true;
                    dp = cp;
                    dm = e;
                    dn = dq === 9 && dv;
                    if (dq === 1 && e.nodeName.toLowerCase() !== "object") {
                        di = dc(dv);
                        if ((dr = e.getAttribute("id"))) {
                            dp = dr.replace(cR, "\\$&")
                        } else {
                            e.setAttribute("id", dp)
                        }
                        dp = "[id='" + dp + "'] ";
                        dj = di.length;
                        while (dj--) {
                            di[dj] = dp + dd(di[dj])
                        }
                        dm = cY.test(dv) && e.parentNode || e;
                        dn = di.join(",")
                    }
                    if (dn) {
                        try {
                            cL.apply(dt, c7.call(dm.querySelectorAll(dn), 0));
                            return dt
                        } catch (ds) {} finally {
                            if (!dr) {
                                e.removeAttribute("id")
                            }
                        }
                    }
                }
            }
            return c1(dv.replace(cZ, "$1"), e, dt, du)
        }
        cy = c6.isXML = function(dh) {
            var e = dh && (dh.ownerDocument || dh).documentElement;
            return e ? e.nodeName !== "HTML" : false
        };
        c2 = c6.setDocument = function(dh) {
            var e = dh ? dh.ownerDocument || dh : cJ;
            if (e === ck || e.nodeType !== 9 || !e.documentElement) {
                return ck
            }
            ck = e;
            cj = e.documentElement;
            cl = cy(e);
            da.tagNameNoComments = b5(function(di) {
                di.appendChild(e.createComment(""));
                return !di.getElementsByTagName("*").length
            });
            da.attributes = b5(function(di) {
                di.innerHTML = "<select></select>";
                var dj = typeof di.lastChild.getAttribute("multiple");
                return dj !== "boolean" && dj !== "string"
            });
            da.getByClassName = b5(function(di) {
                di.innerHTML = "<div class='hidden e'></div><div class='hidden'></div>";
                if (!di.getElementsByClassName || !di.getElementsByClassName("e").length) {
                    return false
                }
                di.lastChild.className = "e";
                return di.getElementsByClassName("e").length === 2
            });
            da.getByName = b5(function(di) {
                di.id = cp + 0;
                di.innerHTML = "<a name='" + cp + "'></a><div name='" + cp + "'></div>";
                cj.insertBefore(di, cj.firstChild);
                var dj = e.getElementsByName && e.getElementsByName(cp).length === 2 + e.getElementsByName(cp + 0).length;
                da.getIdNotName = !e.getElementById(cp);
                cj.removeChild(di);
                return dj
            });
            cq.attrHandle = b5(function(di) {
                di.innerHTML = "<a href='#'></a>";
                return di.firstChild && typeof di.firstChild.getAttribute !== c9 && di.firstChild.getAttribute("href") === "#"
            }) ? {} : {
                href: function(di) {
                    return di.getAttribute("href", 2)
                },
                type: function(di) {
                    return di.getAttribute("type")
                }
            };
            if (da.getIdNotName) {
                cq.find.ID = function(dj, di) {
                    if (typeof di.getElementById !== c9 && !cl) {
                        var dk = di.getElementById(dj);
                        return dk && dk.parentNode ? [dk] : []
                    }
                };
                cq.filter.ID = function(dj) {
                    var di = dj.replace(c0, cr);
                    return function(dk) {
                        return dk.getAttribute("id") === di
                    }
                }
            } else {
                cq.find.ID = function(dj, di) {
                    if (typeof di.getElementById !== c9 && !cl) {
                        var dk = di.getElementById(dj);
                        return dk ? dk.id === dj || typeof dk.getAttributeNode !== c9 && dk.getAttributeNode("id").value === dj ? [dk] : de : []
                    }
                };
                cq.filter.ID = function(dj) {
                    var di = dj.replace(c0, cr);
                    return function(dk) {
                        var dl = typeof dk.getAttributeNode !== c9 && dk.getAttributeNode("id");
                        return dl && dl.value === di
                    }
                }
            }
            cq.find.TAG = da.tagNameNoComments ? function(dj, di) {
                if (typeof di.getElementsByTagName !== c9) {
                    return di.getElementsByTagName(dj)
                }
            } : function(dm, di) {
                var dj, dn = [],
                    dk = 0,
                    dl = di.getElementsByTagName(dm);
                if (dm === "*") {
                    while ((dj = dl[dk++])) {
                        if (dj.nodeType === 1) {
                            dn.push(dj)
                        }
                    }
                    return dn
                }
                return dl
            };
            cq.find.NAME = da.getByName && function(dj, di) {
                if (typeof di.getElementsByName !== c9) {
                    return di.getElementsByName(name)
                }
            };
            cq.find.CLASS = da.getByClassName && function(di, dj) {
                if (typeof dj.getElementsByClassName !== c9 && !cl) {
                    return dj.getElementsByClassName(di)
                }
            };
            cN = [];
            cO = [":focus"];
            if ((da.qsa = cx(e.querySelectorAll))) {
                b5(function(di) {
                    di.innerHTML = "<select><option selected=''></option></select>";
                    if (!di.querySelectorAll("[selected]").length) {
                        cO.push("\\[" + df + "*(?:checked|disabled|ismap|multiple|readonly|selected|value)")
                    }
                    if (!di.querySelectorAll(":checked").length) {
                        cO.push(":checked")
                    }
                });
                b5(function(di) {
                    di.innerHTML = "<input type='hidden' i=''/>";
                    if (di.querySelectorAll("[i^='']").length) {
                        cO.push("[*^$]=" + df + "*(?:\"\"|'')")
                    }
                    if (!di.querySelectorAll(":enabled").length) {
                        cO.push(":enabled", ":disabled")
                    }
                    di.querySelectorAll("*,:x");
                    cO.push(",.*:")
                })
            }
            if ((da.matchesSelector = cx((cC = cj.matchesSelector || cj.mozMatchesSelector || cj.webkitMatchesSelector || cj.oMatchesSelector || cj.msMatchesSelector)))) {
                b5(function(di) {
                    da.disconnectedMatch = cC.call(di, "div");
                    cC.call(di, "[s!='']:x");
                    cN.push("!=", cK)
                })
            }
            cO = new RegExp(cO.join("|"));
            cN = new RegExp(cN.join("|"));
            cd = cx(cj.contains) || cj.compareDocumentPosition ? function(di, dk) {
                var dj = di.nodeType === 9 ? di.documentElement : di,
                    dl = dk && dk.parentNode;
                return di === dl || !! (dl && dl.nodeType === 1 && (dj.contains ? dj.contains(dl) : di.compareDocumentPosition && di.compareDocumentPosition(dl) & 16))
            } : function(di, dj) {
                if (dj) {
                    while ((dj = dj.parentNode)) {
                        if (dj === di) {
                            return true
                        }
                    }
                }
                return false
            };
            c8 = cj.compareDocumentPosition ? function(di, dj) {
                var dk;
                if (di === dj) {
                    ct = true;
                    return 0
                }
                if ((dk = dj.compareDocumentPosition && di.compareDocumentPosition && di.compareDocumentPosition(dj))) {
                    if (dk & 1 || di.parentNode && di.parentNode.nodeType === 11) {
                        if (di === e || cd(cJ, di)) {
                            return -1
                        }
                        if (dj === e || cd(cJ, dj)) {
                            return 1
                        }
                        return 0
                    }
                    return dk & 4 ? -1 : 1
                }
                return di.compareDocumentPosition ? -1 : 1
            } : function(di, dl) {
                var dp, dq = 0,
                    dk = di.parentNode,
                    dn = dl.parentNode,
                    dj = [di],
                    dm = [dl];
                if (di === dl) {
                    ct = true;
                    return 0
                } else {
                    if (!dk || !dn) {
                        return di === e ? -1 : dl === e ? 1 : dk ? -1 : dn ? 1 : 0
                    } else {
                        if (dk === dn) {
                            return c5(di, dl)
                        }
                    }
                }
                dp = di;
                while ((dp = dp.parentNode)) {
                    dj.unshift(dp)
                }
                dp = dl;
                while ((dp = dp.parentNode)) {
                    dm.unshift(dp)
                }
                while (dj[dq] === dm[dq]) {
                    dq++
                }
                return dq ? c5(dj[dq], dm[dq]) : dj[dq] === cJ ? -1 : dm[dq] === cJ ? 1 : 0
            };
            ct = false;
            [0, 0].sort(c8);
            da.detectDuplicates = ct;
            return ck
        };
        c6.matches = function(dh, e) {
            return c6(dh, null, null, e)
        };
        c6.matchesSelector = function(di, dj) {
            if ((di.ownerDocument || di) !== ck) {
                c2(di)
            }
            dj = dj.replace(cM, "='$1']");
            if (da.matchesSelector && !cl && (!cN || !cN.test(dj)) && !cO.test(dj)) {
                try {
                    var dk = cC.call(di, dj);
                    if (dk || da.disconnectedMatch || di.document && di.document.nodeType !== 11) {
                        return dk
                    }
                } catch (dh) {}
            }
            return c6(dj, ck, null, [di]).length > 0
        };
        c6.contains = function(e, dh) {
            if ((e.ownerDocument || e) !== ck) {
                c2(e)
            }
            return cd(e, dh)
        };
        c6.attr = function(e, dh) {
            var di;
            if ((e.ownerDocument || e) !== ck) {
                c2(e)
            }
            if (!cl) {
                dh = dh.toLowerCase()
            }
            if ((di = cq.attrHandle[dh])) {
                return di(e)
            }
            if (cl || da.attributes) {
                return e.getAttribute(dh)
            }
            return ((di = e.getAttributeNode(dh)) || e.getAttribute(dh)) && e[dh] === true ? dh : di && di.specified ? di.value : null
        };
        c6.error = function(e) {
            throw new Error("Syntax error, unrecognized expression: " + e)
        };
        c6.uniqueSort = function(dk) {
            var dh, e = [],
                di = 1,
                dj = 0;
            ct = !da.detectDuplicates;
            dk.sort(c8);
            if (ct) {
                for (;
                    (dh = dk[di]); di++) {
                    if (dh === dk[di - 1]) {
                        dj = e.push(di)
                    }
                }
                while (dj--) {
                    dk.splice(e[dj], 1)
                }
            }
            return dk
        };

        function c5(e, dh) {
            var di = dh && e,
                dj = di && (~dh.sourceIndex || cE) - (~e.sourceIndex || cE);
            if (dj) {
                return dj
            }
            if (di) {
                while ((di = di.nextSibling)) {
                    if (di === dh) {
                        return -1
                    }
                }
            }
            return e ? 1 : -1
        }

        function cg(e) {
            return function(dh) {
                var di = dh.nodeName.toLowerCase();
                return di === "input" && dh.type === e
            }
        }

        function ce(e) {
            return function(dh) {
                var di = dh.nodeName.toLowerCase();
                return (di === "input" || di === "button") && dh.type === e
            }
        }

        function ch(e) {
            return cz(function(dh) {
                dh = +dh;
                return cz(function(dm, dk) {
                    var dj, dl = e([], dm.length, dh),
                        di = dl.length;
                    while (di--) {
                        if (dm[(dj = dl[di])]) {
                            dm[dj] = !(dk[dj] = dm[dj])
                        }
                    }
                })
            })
        }
        cs = c6.getText = function(e) {
            var di, dk = "",
                dh = 0,
                dj = e.nodeType;
            if (!dj) {
                for (;
                    (di = e[dh]); dh++) {
                    dk += cs(di)
                }
            } else {
                if (dj === 1 || dj === 9 || dj === 11) {
                    if (typeof e.textContent === "string") {
                        return e.textContent
                    } else {
                        for (e = e.firstChild; e; e = e.nextSibling) {
                            dk += cs(e)
                        }
                    }
                } else {
                    if (dj === 3 || dj === 4) {
                        return e.nodeValue
                    }
                }
            }
            return dk
        };
        cq = c6.selectors = {
            cacheLength: 50,
            createPseudo: cz,
            match: cD,
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: true
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: true
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(e) {
                    e[1] = e[1].replace(c0, cr);
                    e[3] = (e[4] || e[5] || "").replace(c0, cr);
                    if (e[2] === "~=") {
                        e[3] = " " + e[3] + " "
                    }
                    return e.slice(0, 4)
                },
                CHILD: function(e) {
                    e[1] = e[1].toLowerCase();
                    if (e[1].slice(0, 3) === "nth") {
                        if (!e[3]) {
                            c6.error(e[0])
                        }
                        e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * (e[3] === "even" || e[3] === "odd"));
                        e[5] = +((e[7] + e[8]) || e[3] === "odd")
                    } else {
                        if (e[3]) {
                            c6.error(e[0])
                        }
                    }
                    return e
                },
                PSEUDO: function(dh) {
                    var e, di = !dh[5] && dh[2];
                    if (cD.CHILD.test(dh[0])) {
                        return null
                    }
                    if (dh[4]) {
                        dh[2] = dh[4]
                    } else {
                        if (di && cW.test(di) && (e = dc(di, true)) && (e = di.indexOf(")", di.length - e) - di.length)) {
                            dh[0] = dh[0].slice(0, e);
                            dh[2] = di.slice(0, e)
                        }
                    }
                    return dh.slice(0, 3)
                }
            },
            filter: {
                TAG: function(e) {
                    if (e === "*") {
                        return function() {
                            return true
                        }
                    }
                    e = e.replace(c0, cr).toLowerCase();
                    return function(dh) {
                        return dh.nodeName && dh.nodeName.toLowerCase() === e
                    }
                },
                CLASS: function(e) {
                    var dh = b9[e + " "];
                    return dh || (dh = new RegExp("(^|" + df + ")" + e + "(" + df + "|$)")) && b9(e, function(di) {
                            return dh.test(di.className || (typeof di.getAttribute !== c9 && di.getAttribute("class")) || "")
                        })
                },
                ATTR: function(dh, di, e) {
                    return function(dj) {
                        var dk = c6.attr(dj, dh);
                        if (dk == null) {
                            return di === "!="
                        }
                        if (!di) {
                            return true
                        }
                        dk += "";
                        return di === "=" ? dk === e : di === "!=" ? dk !== e : di === "^=" ? e && dk.indexOf(e) === 0 : di === "*=" ? e && dk.indexOf(e) > -1 : di === "$=" ? e && dk.slice(-e.length) === e : di === "~=" ? (" " + dk + " ").indexOf(e) > -1 : di === "|=" ? dk === e || dk.slice(0, e.length + 1) === e + "-" : false
                    }
                },
                CHILD: function(dm, dn, e, dh, dj) {
                    var dl = dm.slice(0, 3) !== "nth",
                        di = dm.slice(-4) !== "last",
                        dk = dn === "of-type";
                    return dh === 1 && dj === 0 ? function(dp) {
                        return !!dp.parentNode
                    } : function(dt, dq, dB) {
                        var dp, dx, dv, dr, dw, dz, ds = dl !== di ? "nextSibling" : "previousSibling",
                            dy = dt.parentNode,
                            du = dk && dt.nodeName.toLowerCase(),
                            dA = !dB && !dk;
                        if (dy) {
                            if (dl) {
                                while (ds) {
                                    dv = dt;
                                    while ((dv = dv[ds])) {
                                        if (dk ? dv.nodeName.toLowerCase() === du : dv.nodeType === 1) {
                                            return false
                                        }
                                    }
                                    dz = ds = dm === "only" && !dz && "nextSibling"
                                }
                                return true
                            }
                            dz = [di ? dy.firstChild : dy.lastChild];
                            if (di && dA) {
                                dx = dy[cp] || (dy[cp] = {});
                                dp = dx[dm] || [];
                                dw = dp[0] === ci && dp[1];
                                dr = dp[0] === ci && dp[2];
                                dv = dw && dy.childNodes[dw];
                                while ((dv = ++dw && dv && dv[ds] || (dr = dw = 0) || dz.pop())) {
                                    if (dv.nodeType === 1 && ++dr && dv === dt) {
                                        dx[dm] = [ci, dw, dr];
                                        break
                                    }
                                }
                            } else {
                                if (dA && (dp = (dt[cp] || (dt[cp] = {}))[dm]) && dp[0] === ci) {
                                    dr = dp[1]
                                } else {
                                    while ((dv = ++dw && dv && dv[ds] || (dr = dw = 0) || dz.pop())) {
                                        if ((dk ? dv.nodeName.toLowerCase() === du : dv.nodeType === 1) && ++dr) {
                                            if (dA) {
                                                (dv[cp] || (dv[cp] = {}))[dm] = [ci, dr]
                                            }
                                            if (dv === dt) {
                                                break
                                            }
                                        }
                                    }
                                }
                            }
                            dr -= dj;
                            return dr === dh || (dr % dh === 0 && dr / dh >= 0)
                        }
                    }
                },
                PSEUDO: function(dj, dh) {
                    var e, di = cq.pseudos[dj] || cq.setFilters[dj.toLowerCase()] || c6.error("unsupported pseudo: " + dj);
                    if (di[cp]) {
                        return di(dh)
                    }
                    if (di.length > 1) {
                        e = [dj, dj, "", dh];
                        return cq.setFilters.hasOwnProperty(dj.toLowerCase()) ? cz(function(dp, dn) {
                            var dl, dm = di(dp, dh),
                                dk = dm.length;
                            while (dk--) {
                                dl = cw.call(dp, dm[dk]);
                                dp[dl] = !(dn[dl] = dm[dk])
                            }
                        }) : function(dk) {
                            return di(dk, 0, e)
                        }
                    }
                    return di
                }
            },
            pseudos: {
                not: cz(function(dj) {
                    var e = [],
                        di = [],
                        dh = ca(dj.replace(cZ, "$1"));
                    return dh[cp] ? cz(function(dp, dn, dk, dr) {
                        var dl, dq = dh(dp, null, dr, []),
                            dm = dp.length;
                        while (dm--) {
                            if ((dl = dq[dm])) {
                                dp[dm] = !(dn[dm] = dl)
                            }
                        }
                    }) : function(dl, dk, dm) {
                        e[0] = dl;
                        dh(e, null, dm, di);
                        return !di.pop()
                    }
                }),
                has: cz(function(e) {
                    return function(dh) {
                        return c6(e, dh).length > 0
                    }
                }),
                contains: cz(function(e) {
                    return function(dh) {
                        return (dh.textContent || dh.innerText || cs(dh)).indexOf(e) > -1
                    }
                }),
                lang: cz(function(e) {
                    if (!cT.test(e || "")) {
                        c6.error("unsupported lang: " + e)
                    }
                    e = e.replace(c0, cr).toLowerCase();
                    return function(dh) {
                        var di;
                        do {
                            if ((di = cl ? dh.getAttribute("xml:lang") || dh.getAttribute("lang") : dh.lang)) {
                                di = di.toLowerCase();
                                return di === e || di.indexOf(e + "-") === 0
                            }
                        } while ((dh = dh.parentNode) && dh.nodeType === 1);
                        return false
                    }
                }),
                target: function(e) {
                    var dh = dg.location && dg.location.hash;
                    return dh && dh.slice(1) === e.id
                },
                root: function(e) {
                    return e === cj
                },
                focus: function(e) {
                    return e === ck.activeElement && (!ck.hasFocus || ck.hasFocus()) && !! (e.type || e.href || ~e.tabIndex)
                },
                enabled: function(e) {
                    return e.disabled === false
                },
                disabled: function(e) {
                    return e.disabled === true
                },
                checked: function(e) {
                    var dh = e.nodeName.toLowerCase();
                    return (dh === "input" && !! e.checked) || (dh === "option" && !! e.selected)
                },
                selected: function(e) {
                    if (e.parentNode) {
                        e.parentNode.selectedIndex
                    }
                    return e.selected === true
                },
                empty: function(e) {
                    for (e = e.firstChild; e; e = e.nextSibling) {
                        if (e.nodeName > "@" || e.nodeType === 3 || e.nodeType === 4) {
                            return false
                        }
                    }
                    return true
                },
                parent: function(e) {
                    return !cq.pseudos.empty(e)
                },
                header: function(e) {
                    return cS.test(e.nodeName)
                },
                input: function(e) {
                    return cU.test(e.nodeName)
                },
                button: function(e) {
                    var dh = e.nodeName.toLowerCase();
                    return dh === "input" && e.type === "button" || dh === "button"
                },
                text: function(dh) {
                    var e;
                    return dh.nodeName.toLowerCase() === "input" && dh.type === "text" && ((e = dh.getAttribute("type")) == null || e.toLowerCase() === dh.type)
                },
                first: ch(function() {
                    return [0]
                }),
                last: ch(function(dh, e) {
                    return [e - 1]
                }),
                eq: ch(function(di, dh, e) {
                    return [e < 0 ? e + dh : e]
                }),
                even: ch(function(di, dh) {
                    var e = 0;
                    for (; e < dh; e += 2) {
                        di.push(e)
                    }
                    return di
                }),
                odd: ch(function(di, dh) {
                    var e = 1;
                    for (; e < dh; e += 2) {
                        di.push(e)
                    }
                    return di
                }),
                lt: ch(function(dj, di, e) {
                    var dh = e < 0 ? e + di : e;
                    for (; --dh >= 0;) {
                        dj.push(dh)
                    }
                    return dj
                }),
                gt: ch(function(dj, di, e) {
                    var dh = e < 0 ? e + di : e;
                    for (; ++dh < di;) {
                        dj.push(dh)
                    }
                    return dj
                })
            }
        };
        for (cu in {
            radio: true,
            checkbox: true,
            file: true,
            password: true,
            image: true
        }) {
            cq.pseudos[cu] = cg(cu)
        }
        for (cu in {
            submit: true,
            reset: true
        }) {
            cq.pseudos[cu] = ce(cu)
        }

        function dc(dm, dk) {
            var dj, di, dp, dq, dn, dh, dl, e = db[dm + " "];
            if (e) {
                return dk ? 0 : e.slice(0)
            }
            dn = dm;
            dh = [];
            dl = cq.preFilter;
            while (dn) {
                if (!dj || (di = cQ.exec(dn))) {
                    if (di) {
                        dn = dn.slice(di[0].length) || dn
                    }
                    dh.push(dp = [])
                }
                dj = false;
                if ((di = cP.exec(dn))) {
                    dj = di.shift();
                    dp.push({
                        value: dj,
                        type: di[0].replace(cZ, " ")
                    });
                    dn = dn.slice(dj.length)
                }
                for (dq in cq.filter) {
                    if ((di = cD[dq].exec(dn)) && (!dl[dq] || (di = dl[dq](di)))) {
                        dj = di.shift();
                        dp.push({
                            value: dj,
                            type: dq,
                            matches: di
                        });
                        dn = dn.slice(dj.length)
                    }
                }
                if (!dj) {
                    break
                }
            }
            return dk ? dn.length : dn ? c6.error(dm) : db(dm, dh).slice(0)
        }

        function dd(dj) {
            var e = 0,
                dh = dj.length,
                di = "";
            for (; e < dh; e++) {
                di += dj[e].value
            }
            return di
        }

        function b3(dl, di, e) {
            var dj = di.dir,
                dh = e && dj === "parentNode",
                dk = cm++;
            return di.first ? function(dn, dm, dp) {
                while ((dn = dn[dj])) {
                    if (dn.nodeType === 1 || dh) {
                        return dl(dn, dm, dp)
                    }
                }
            } : function(dr, dn, dt) {
                var dp, dm, ds, dq = ci + " " + dk;
                if (dt) {
                    while ((dr = dr[dj])) {
                        if (dr.nodeType === 1 || dh) {
                            if (dl(dr, dn, dt)) {
                                return true
                            }
                        }
                    }
                } else {
                    while ((dr = dr[dj])) {
                        if (dr.nodeType === 1 || dh) {
                            ds = dr[cp] || (dr[cp] = {});
                            if ((dm = ds[dj]) && dm[0] === dq) {
                                if ((dp = dm[1]) === true || dp === b7) {
                                    return dp === true
                                }
                            } else {
                                dm = ds[dj] = [dq];
                                dm[1] = dl(dr, dn, dt) || b7;
                                if (dm[1] === true) {
                                    return true
                                }
                            }
                        }
                    }
                }
            }
        }

        function co(e) {
            return e.length > 1 ? function(di, dh, dk) {
                var dj = e.length;
                while (dj--) {
                    if (!e[dj](di, dh, dk)) {
                        return false
                    }
                }
                return true
            } : e[0]
        }

        function cc(dp, dl, di, e, dq) {
            var dh, dn = [],
                dj = 0,
                dk = dp.length,
                dm = dl != null;
            for (; dj < dk; dj++) {
                if ((dh = dp[dj])) {
                    if (!di || di(dh, e, dq)) {
                        dn.push(dh);
                        if (dm) {
                            dl.push(dj)
                        }
                    }
                }
            }
            return dn
        }

        function c4(dk, dl, e, dh, di, dj) {
            if (dh && !dh[cp]) {
                dh = c4(dh)
            }
            if (di && !di[cp]) {
                di = c4(di, dj)
            }
            return cz(function(dx, dw, dm, dz) {
                var dy, dq, dn, dv = [],
                    dt = [],
                    du = dw.length,
                    dp = dx || cF(dl || "*", dm.nodeType ? [dm] : dm, []),
                    dr = dk && (dx || !dl) ? cc(dp, dv, dk, dm, dz) : dp,
                    ds = e ? di || (dx ? dk : du || dh) ? [] : dw : dr;
                if (e) {
                    e(dr, ds, dm, dz)
                }
                if (dh) {
                    dy = cc(ds, dt);
                    dh(dy, [], dm, dz);
                    dq = dy.length;
                    while (dq--) {
                        if ((dn = dy[dq])) {
                            ds[dt[dq]] = !(dr[dt[dq]] = dn)
                        }
                    }
                }
                if (dx) {
                    if (di || dk) {
                        if (di) {
                            dy = [];
                            dq = ds.length;
                            while (dq--) {
                                if ((dn = ds[dq])) {
                                    dy.push((dr[dq] = dn))
                                }
                            }
                            di(null, (ds = []), dy, dz)
                        }
                        dq = ds.length;
                        while (dq--) {
                            if ((dn = ds[dq]) && (dy = di ? cw.call(dx, dn) : dv[dq]) > -1) {
                                dx[dy] = !(dw[dy] = dn)
                            }
                        }
                    }
                } else {
                    ds = cc(ds === dw ? ds.splice(du, ds.length) : ds);
                    if (di) {
                        di(null, dw, ds, dz)
                    } else {
                        cL.apply(dw, ds)
                    }
                }
            })
        }

        function cB(dr) {
            var e, dp, dj, dl = dr.length,
                dk = cq.relative[dr[0].type],
                di = dk || cq.relative[" "],
                dh = dk ? 1 : 0,
                dn = b3(function(ds) {
                    return ds === e
                }, di, true),
                dm = b3(function(ds) {
                    return cw.call(e, ds) > -1
                }, di, true),
                dq = [
                    function(dt, ds, du) {
                        return (!dk && (du || ds !== cH)) || ((e = ds).nodeType ? dn(dt, ds, du) : dm(dt, ds, du))
                    }
                ];
            for (; dh < dl; dh++) {
                if ((dp = cq.relative[dr[dh].type])) {
                    dq = [b3(co(dq), dp)]
                } else {
                    dp = cq.filter[dr[dh].type].apply(null, dr[dh].matches);
                    if (dp[cp]) {
                        dj = ++dh;
                        for (; dj < dl; dj++) {
                            if (cq.relative[dr[dj].type]) {
                                break
                            }
                        }
                        return c4(dh > 1 && co(dq), dh > 1 && dd(dr.slice(0, dh - 1)).replace(cZ, "$1"), dp, dh < dj && cB(dr.slice(dh, dj)), dj < dl && cB((dr = dr.slice(dj))), dj < dl && dd(dr))
                    }
                    dq.push(dp)
                }
            }
            return co(dq)
        }

        function cA(di, dk) {
            var dj = 0,
                dh = dk.length > 0,
                e = di.length > 0,
                dl = function(dz, dm, dC, dy, ds) {
                    var dq, du, dw, dA = [],
                        dv = 0,
                        dt = "0",
                        dB = dz && [],
                        dx = ds != null,
                        dn = cH,
                        dr = dz || e && cq.find.TAG("*", ds && dm.parentNode || dm),
                        dp = (ci += dn == null ? 1 : Math.random() || 0.1);
                    if (dx) {
                        cH = dm !== ck && dm;
                        b7 = dj
                    }
                    for (;
                        (dq = dr[dt]) != null; dt++) {
                        if (e && dq) {
                            du = 0;
                            while ((dw = di[du++])) {
                                if (dw(dq, dm, dC)) {
                                    dy.push(dq);
                                    break
                                }
                            }
                            if (dx) {
                                ci = dp;
                                b7 = ++dj
                            }
                        }
                        if (dh) {
                            if ((dq = !dw && dq)) {
                                dv--
                            }
                            if (dz) {
                                dB.push(dq)
                            }
                        }
                    }
                    dv += dt;
                    if (dh && dt !== dv) {
                        du = 0;
                        while ((dw = dk[du++])) {
                            dw(dB, dA, dm, dC)
                        }
                        if (dz) {
                            if (dv > 0) {
                                while (dt--) {
                                    if (!(dB[dt] || dA[dt])) {
                                        dA[dt] = cI.call(dy)
                                    }
                                }
                            }
                            dA = cc(dA)
                        }
                        cL.apply(dy, dA);
                        if (dx && !dz && dA.length > 0 && (dv + dk.length) > 1) {
                            c6.uniqueSort(dy)
                        }
                    }
                    if (dx) {
                        ci = dp;
                        cH = dn
                    }
                    return dB
                };
            return dh ? cz(dl) : dl
        }
        ca = c6.compile = function(dk, di) {
            var dj, dl = [],
                dh = [],
                e = cb[dk + " "];
            if (!e) {
                if (!di) {
                    di = dc(dk)
                }
                dj = di.length;
                while (dj--) {
                    e = cB(di[dj]);
                    if (e[cp]) {
                        dl.push(e)
                    } else {
                        dh.push(e)
                    }
                }
                e = cb(dk, cA(dh, dl))
            }
            return e
        };

        function cF(dk, e, dj) {
            var dh = 0,
                di = e.length;
            for (; dh < di; dh++) {
                c6(dk, e[dh], dj)
            }
            return dj
        }

        function c1(dm, e, dk, dl) {
            var di, dp, dn, dq, dh, dj = dc(dm);
            if (!dl) {
                if (dj.length === 1) {
                    dp = dj[0] = dj[0].slice(0);
                    if (dp.length > 2 && (dn = dp[0]).type === "ID" && e.nodeType === 9 && !cl && cq.relative[dp[1].type]) {
                        e = cq.find.ID(dn.matches[0].replace(c0, cr), e)[0];
                        if (!e) {
                            return dk
                        }
                        dm = dm.slice(dp.shift().value.length)
                    }
                    di = cD.needsContext.test(dm) ? 0 : dp.length;
                    while (di--) {
                        dn = dp[di];
                        if (cq.relative[(dq = dn.type)]) {
                            break
                        }
                        if ((dh = cq.find[dq])) {
                            if ((dl = dh(dn.matches[0].replace(c0, cr), cY.test(dp[0].type) && e.parentNode || e))) {
                                dp.splice(di, 1);
                                dm = dl.length && dd(dp);
                                if (!dm) {
                                    cL.apply(dk, c7.call(dl, 0));
                                    return dk
                                }
                                break
                            }
                        }
                    }
                }
            }
            ca(dm, dj)(dl, e, cl, dk, cY.test(dm));
            return dk
        }
        cq.pseudos.nth = cq.pseudos.eq;

        function c3() {}
        cq.filters = c3.prototype = cq.pseudos;
        cq.setFilters = new c3();
        c2();
        c6.attr = ay.attr;
        ay.find = c6;
        ay.expr = c6.selectors;
        ay.expr[":"] = ay.expr.pseudos;
        ay.unique = c6.uniqueSort;
        ay.text = c6.getText;
        ay.isXMLDoc = c6.isXML;
        ay.contains = c6.contains
    })(bW);
    var bD = /Until$/,
        bm = /^(?:parents|prev(?:Until|All))/,
        ax = /^.[^:#\[\.,]*$/,
        be = ay.expr.match.needsContext,
        ao = {
            children: true,
            contents: true,
            next: true,
            prev: true
        };
    ay.fn.extend({
        find: function(b5) {
            var e, b4, b6, b3 = this.length;
            if (typeof b5 !== "string") {
                b6 = this;
                return this.pushStack(ay(b5).filter(function() {
                    for (e = 0; e < b3; e++) {
                        if (ay.contains(b6[e], this)) {
                            return true
                        }
                    }
                }))
            }
            b4 = [];
            for (e = 0; e < b3; e++) {
                ay.find(b5, this[e], b4)
            }
            b4 = this.pushStack(b3 > 1 ? ay.unique(b4) : b4);
            b4.selector = (this.selector ? this.selector + " " : "") + b5;
            return b4
        },
        has: function(b4) {
            var e, b5 = ay(b4, this),
                b3 = b5.length;
            return this.filter(function() {
                for (e = 0; e < b3; e++) {
                    if (ay.contains(this, b5[e])) {
                        return true
                    }
                }
            })
        },
        not: function(e) {
            return this.pushStack(bX(this, e, false))
        },
        filter: function(e) {
            return this.pushStack(bX(this, e, true))
        },
        is: function(e) {
            return !!e && (typeof e === "string" ? be.test(e) ? ay(e, this.context).index(this[0]) >= 0 : ay.filter(e, this).length > 0 : this.filter(e).length > 0)
        },
        closest: function(b8, e) {
            var b3, b4 = 0,
                b5 = this.length,
                b7 = [],
                b6 = be.test(b8) || typeof b8 !== "string" ? ay(b8, e || this.context) : 0;
            for (; b4 < b5; b4++) {
                b3 = this[b4];
                while (b3 && b3.ownerDocument && b3 !== e && b3.nodeType !== 11) {
                    if (b6 ? b6.index(b3) > -1 : ay.find.matchesSelector(b3, b8)) {
                        b7.push(b3);
                        break
                    }
                    b3 = b3.parentNode
                }
            }
            return this.pushStack(b7.length > 1 ? ay.unique(b7) : b7)
        },
        index: function(e) {
            if (!e) {
                return (this[0] && this[0].parentNode) ? this.first().prevAll().length : -1
            }
            if (typeof e === "string") {
                return ay.inArray(this[0], ay(e))
            }
            return ay.inArray(e.jquery ? e[0] : e, this)
        },
        add: function(b4, b3) {
            var b5 = typeof b4 === "string" ? ay(b4, b3) : ay.makeArray(b4 && b4.nodeType ? [b4] : b4),
                e = ay.merge(this.get(), b5);
            return this.pushStack(ay.unique(e))
        },
        addBack: function(e) {
            return this.add(e == null ? this.prevObject : this.prevObject.filter(e))
        }
    });
    ay.fn.andSelf = ay.fn.addBack;

    function bP(e, b3) {
        do {
            e = e[b3]
        } while (e && e.nodeType !== 1);
        return e
    }
    ay.each({
        parent: function(e) {
            var b3 = e.parentNode;
            return b3 && b3.nodeType !== 11 ? b3 : null
        },
        parents: function(e) {
            return ay.dir(e, "parentNode")
        },
        parentsUntil: function(e, b3, b4) {
            return ay.dir(e, "parentNode", b4)
        },
        next: function(e) {
            return bP(e, "nextSibling")
        },
        prev: function(e) {
            return bP(e, "previousSibling")
        },
        nextAll: function(e) {
            return ay.dir(e, "nextSibling")
        },
        prevAll: function(e) {
            return ay.dir(e, "previousSibling")
        },
        nextUntil: function(e, b3, b4) {
            return ay.dir(e, "nextSibling", b4)
        },
        prevUntil: function(e, b3, b4) {
            return ay.dir(e, "previousSibling", b4)
        },
        siblings: function(e) {
            return ay.sibling((e.parentNode || {}).firstChild, e)
        },
        children: function(e) {
            return ay.sibling(e.firstChild)
        },
        contents: function(e) {
            return ay.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : ay.merge([], e.childNodes)
        }
    }, function(b3, e) {
        ay.fn[b3] = function(b6, b5) {
            var b4 = ay.map(this, e, b6);
            if (!bD.test(b3)) {
                b5 = b6
            }
            if (b5 && typeof b5 === "string") {
                b4 = ay.filter(b5, b4)
            }
            b4 = this.length > 1 && !ao[b3] ? ay.unique(b4) : b4;
            if (this.length > 1 && bm.test(b3)) {
                b4 = b4.reverse()
            }
            return this.pushStack(b4)
        }
    });
    ay.extend({
        filter: function(b3, e, b4) {
            if (b4) {
                b3 = ":not(" + b3 + ")"
            }
            return e.length === 1 ? ay.find.matchesSelector(e[0], b3) ? [e[0]] : [] : ay.find.matches(b3, e)
        },
        dir: function(b4, b3, b6) {
            var b5 = [],
                e = b4[b3];
            while (e && e.nodeType !== 9 && (b6 === bU || e.nodeType !== 1 || !ay(e).is(b6))) {
                if (e.nodeType === 1) {
                    b5.push(e)
                }
                e = e[b3]
            }
            return b5
        },
        sibling: function(b3, e) {
            var b4 = [];
            for (; b3; b3 = b3.nextSibling) {
                if (b3.nodeType === 1 && b3 !== e) {
                    b4.push(b3)
                }
            }
            return b4
        }
    });

    function bX(e, b5, b4) {
        b5 = b5 || 0;
        if (ay.isFunction(b5)) {
            return ay.grep(e, function(b6, b7) {
                var b8 = !! b5.call(b6, b7, b6);
                return b8 === b4
            })
        } else {
            if (b5.nodeType) {
                return ay.grep(e, function(b6) {
                    return (b6 === b5) === b4
                })
            } else {
                if (typeof b5 === "string") {
                    var b3 = ay.grep(e, function(b6) {
                        return b6.nodeType === 1
                    });
                    if (ax.test(b5)) {
                        return ay.filter(b5, b3, !b4)
                    } else {
                        b5 = ay.filter(b5, b3)
                    }
                }
            }
        }
        return ay.grep(e, function(b6) {
            return (ay.inArray(b6, b5) >= 0) === b4
        })
    }

    function L(e) {
        var b3 = aC.split("|"),
            b4 = e.createDocumentFragment();
        if (b4.createElement) {
            while (b3.length) {
                b4.createElement(b3.pop())
            }
        }
        return b4
    }
    var aC = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
        a5 = / jQuery\d+="(?:null|\d+)"/g,
        bh = new RegExp("<(?:" + aC + ")[\\s/>]", "i"),
        a8 = /^\s+/,
        bK = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
        by = /<([\w:]+)/,
        bz = /<tbody/i,
        a4 = /<|&#?\w+;/,
        bg = /<(?:script|style|link)/i,
        aA = /^(?:checkbox|radio)$/i,
        aM = /checked\s*(?:[^=]|=\s*.checked.)/i,
        bt = /^$|\/(?:java|ecma)script/i,
        bu = /^true\/(.*)/,
        aO = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
        bY = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            legend: [1, "<fieldset>", "</fieldset>"],
            area: [1, "<map>", "</map>"],
            param: [1, "<object>", "</object>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: ay.support.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
        }, bL = L(Y),
        af = bL.appendChild(Y.createElement("div"));
    bY.optgroup = bY.option;
    bY.tbody = bY.tfoot = bY.colgroup = bY.caption = bY.thead;
    bY.th = bY.td;
    ay.fn.extend({
        text: function(e) {
            return ay.access(this, function(b3) {
                return b3 === bU ? ay.text(this) : this.empty().append((this[0] && this[0].ownerDocument || Y).createTextNode(b3))
            }, null, e, arguments.length)
        },
        wrapAll: function(e) {
            if (ay.isFunction(e)) {
                return this.each(function(b4) {
                    ay(this).wrapAll(e.call(this, b4))
                })
            }
            if (this[0]) {
                var b3 = ay(e, this[0].ownerDocument).eq(0).clone(true);
                if (this[0].parentNode) {
                    b3.insertBefore(this[0])
                }
                b3.map(function() {
                    var b4 = this;
                    while (b4.firstChild && b4.firstChild.nodeType === 1) {
                        b4 = b4.firstChild
                    }
                    return b4
                }).append(this)
            }
            return this
        },
        wrapInner: function(e) {
            if (ay.isFunction(e)) {
                return this.each(function(b3) {
                    ay(this).wrapInner(e.call(this, b3))
                })
            }
            return this.each(function() {
                var b4 = ay(this),
                    b3 = b4.contents();
                if (b3.length) {
                    b3.wrapAll(e)
                } else {
                    b4.append(e)
                }
            })
        },
        wrap: function(e) {
            var b3 = ay.isFunction(e);
            return this.each(function(b4) {
                ay(this).wrapAll(b3 ? e.call(this, b4) : e)
            })
        },
        unwrap: function() {
            return this.parent().each(function() {
                if (!ay.nodeName(this, "body")) {
                    ay(this).replaceWith(this.childNodes)
                }
            }).end()
        },
        append: function() {
            return this.domManip(arguments, true, function(e) {
                if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                    this.appendChild(e)
                }
            })
        },
        prepend: function() {
            return this.domManip(arguments, true, function(e) {
                if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                    this.insertBefore(e, this.firstChild)
                }
            })
        },
        before: function() {
            return this.domManip(arguments, false, function(e) {
                if (this.parentNode) {
                    this.parentNode.insertBefore(e, this)
                }
            })
        },
        after: function() {
            return this.domManip(arguments, false, function(e) {
                if (this.parentNode) {
                    this.parentNode.insertBefore(e, this.nextSibling)
                }
            })
        },
        remove: function(b5, b4) {
            var e, b3 = 0;
            for (;
                (e = this[b3]) != null; b3++) {
                if (!b5 || ay.filter(b5, [e]).length > 0) {
                    if (!b4 && e.nodeType === 1) {
                        ay.cleanData(ai(e))
                    }
                    if (e.parentNode) {
                        if (b4 && ay.contains(e.ownerDocument, e)) {
                            bM(ai(e, "script"))
                        }
                        e.parentNode.removeChild(e)
                    }
                }
            }
            return this
        },
        empty: function() {
            var e, b3 = 0;
            for (;
                (e = this[b3]) != null; b3++) {
                if (e.nodeType === 1) {
                    ay.cleanData(ai(e, false))
                }
                while (e.firstChild) {
                    e.removeChild(e.firstChild)
                }
                if (e.options && ay.nodeName(e, "select")) {
                    e.options.length = 0
                }
            }
            return this
        },
        clone: function(e, b3) {
            e = e == null ? false : e;
            b3 = b3 == null ? e : b3;
            return this.map(function() {
                return ay.clone(this, e, b3)
            })
        },
        html: function(e) {
            return ay.access(this, function(b7) {
                var b4 = this[0] || {}, b5 = 0,
                    b6 = this.length;
                if (b7 === bU) {
                    return b4.nodeType === 1 ? b4.innerHTML.replace(a5, "") : bU
                }
                if (typeof b7 === "string" && !bg.test(b7) && (ay.support.htmlSerialize || !bh.test(b7)) && (ay.support.leadingWhitespace || !a8.test(b7)) && !bY[(by.exec(b7) || ["", ""])[1].toLowerCase()]) {
                    b7 = b7.replace(bK, "<$1></$2>");
                    try {
                        for (; b5 < b6; b5++) {
                            b4 = this[b5] || {};
                            if (b4.nodeType === 1) {
                                ay.cleanData(ai(b4, false));
                                b4.innerHTML = b7
                            }
                        }
                        b4 = 0
                    } catch (b3) {}
                }
                if (b4) {
                    this.empty().append(b7)
                }
            }, null, e, arguments.length)
        },
        replaceWith: function(b3) {
            var e = ay.isFunction(b3);
            if (!e && typeof b3 !== "string") {
                b3 = ay(b3).not(this).detach()
            }
            return this.domManip([b3], true, function(b4) {
                var b5 = this.nextSibling,
                    b6 = this.parentNode;
                if (b6) {
                    ay(this).remove();
                    b6.insertBefore(b4, b5)
                }
            })
        },
        detach: function(e) {
            return this.remove(e, true)
        },
        domManip: function(e, cf, b3) {
            e = w.apply([], e);
            var b5, cc, b7, cd, b4, b6, b8 = 0,
                cb = this.length,
                ce = this,
                b9 = cb - 1,
                cg = e[0],
                ca = ay.isFunction(cg);
            if (ca || !(cb <= 1 || typeof cg !== "string" || ay.support.checkClone || !aM.test(cg))) {
                return this.each(function(ch) {
                    var ci = ce.eq(ch);
                    if (ca) {
                        e[0] = cg.call(this, ch, cf ? ci.html() : bU)
                    }
                    ci.domManip(e, cf, b3)
                })
            }
            if (cb) {
                b6 = ay.buildFragment(e, this[0].ownerDocument, false, this);
                b5 = b6.firstChild;
                if (b6.childNodes.length === 1) {
                    b6 = b5
                }
                if (b5) {
                    cf = cf && ay.nodeName(b5, "tr");
                    cd = ay.map(ai(b6, "script"), X);
                    b7 = cd.length;
                    for (; b8 < cb; b8++) {
                        cc = b6;
                        if (b8 !== b9) {
                            cc = ay.clone(cc, true, true);
                            if (b7) {
                                ay.merge(cd, ai(cc, "script"))
                            }
                        }
                        b3.call(cf && ay.nodeName(this[b8], "table") ? ac(this[b8], "tbody") : this[b8], cc, b8)
                    }
                    if (b7) {
                        b4 = cd[cd.length - 1].ownerDocument;
                        ay.map(cd, aU);
                        for (b8 = 0; b8 < b7; b8++) {
                            cc = cd[b8];
                            if (bt.test(cc.type || "") && !ay._data(cc, "globalEval") && ay.contains(b4, cc)) {
                                if (cc.src) {
                                    ay.ajax({
                                        url: cc.src,
                                        type: "GET",
                                        dataType: "script",
                                        async: false,
                                        global: false,
                                        "throws": true
                                    })
                                } else {
                                    ay.globalEval((cc.text || cc.textContent || cc.innerHTML || "").replace(aO, ""))
                                }
                            }
                        }
                    }
                    b6 = b5 = null
                }
            }
            return this
        }
    });

    function ac(e, b3) {
        return e.getElementsByTagName(b3)[0] || e.appendChild(e.ownerDocument.createElement(b3))
    }

    function X(b3) {
        var e = b3.getAttributeNode("type");
        b3.type = (e && e.specified) + "/" + b3.type;
        return b3
    }

    function aU(e) {
        var b3 = bu.exec(e.type);
        if (b3) {
            e.type = b3[1]
        } else {
            e.removeAttribute("type")
        }
        return e
    }

    function bM(b3, b5) {
        var e, b4 = 0;
        for (;
            (e = b3[b4]) != null; b4++) {
            ay._data(e, "globalEval", !b5 || ay._data(b5[b4], "globalEval"))
        }
    }

    function u(b8, b3) {
        if (b3.nodeType !== 1 || !ay.hasData(b8)) {
            return
        }
        var b9, b5, b6, b7 = ay._data(b8),
            e = ay._data(b3, b7),
            b4 = b7.events;
        if (b4) {
            delete e.handle;
            e.events = {};
            for (b9 in b4) {
                for (b5 = 0, b6 = b4[b9].length; b5 < b6; b5++) {
                    ay.event.add(b3, b9, b4[b9][b5])
                }
            }
        }
        if (e.data) {
            e.data = ay.extend({}, e.data)
        }
    }

    function ad(b7, b4) {
        var b6, b5, b3;
        if (b4.nodeType !== 1) {
            return
        }
        b6 = b4.nodeName.toLowerCase();
        if (!ay.support.noCloneEvent && b4[ay.expando]) {
            b3 = ay._data(b4);
            for (b5 in b3.events) {
                ay.removeEvent(b4, b5, b3.handle)
            }
            b4.removeAttribute(ay.expando)
        }
        if (b6 === "script" && b4.text !== b7.text) {
            X(b4).text = b7.text;
            aU(b4)
        } else {
            if (b6 === "object") {
                if (b4.parentNode) {
                    b4.outerHTML = b7.outerHTML
                }
                if (ay.support.html5Clone && (b7.innerHTML && !ay.trim(b4.innerHTML))) {
                    b4.innerHTML = b7.innerHTML
                }
            } else {
                if (b6 === "input" && aA.test(b7.type)) {
                    b4.defaultChecked = b4.checked = b7.checked;
                    if (b4.value !== b7.value) {
                        b4.value = b7.value
                    }
                } else {
                    if (b6 === "option") {
                        b4.defaultSelected = b4.selected = b7.defaultSelected
                    } else {
                        if (b6 === "input" || b6 === "textarea") {
                            b4.defaultValue = b7.defaultValue
                        }
                    }
                }
            }
        }
    }
    ay.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(e, b3) {
        ay.fn[e] = function(b9) {
            var b4, b5 = 0,
                b8 = [],
                b6 = ay(b9),
                b7 = b6.length - 1;
            for (; b5 <= b7; b5++) {
                b4 = b5 === b7 ? this : this.clone(true);
                ay(b6[b5])[b3](b4);
                B.apply(b8, b4.get())
            }
            return this.pushStack(b8)
        }
    });

    function ai(e, b7) {
        var b4, b3, b6 = 0,
            b5 = typeof e.getElementsByTagName !== E ? e.getElementsByTagName(b7 || "*") : typeof e.querySelectorAll !== E ? e.querySelectorAll(b7 || "*") : bU;
        if (!b5) {
            for (b5 = [], b4 = e.childNodes || e;
                 (b3 = b4[b6]) != null; b6++) {
                if (!b7 || ay.nodeName(b3, b7)) {
                    b5.push(b3)
                } else {
                    ay.merge(b5, ai(b3, b7))
                }
            }
        }
        return b7 === bU || b7 && ay.nodeName(e, b7) ? ay.merge([e], b5) : b5
    }

    function ae(e) {
        if (aA.test(e.type)) {
            e.defaultChecked = e.checked
        }
    }
    ay.extend({
        clone: function(b6, b3, b4) {
            var b5, b9, e, b7, ca, b8 = ay.contains(b6.ownerDocument, b6);
            if (ay.support.html5Clone || ay.isXMLDoc(b6) || !bh.test("<" + b6.nodeName + ">")) {
                e = b6.cloneNode(true)
            } else {
                af.innerHTML = b6.outerHTML;
                af.removeChild(e = af.firstChild)
            } if ((!ay.support.noCloneEvent || !ay.support.noCloneChecked) && (b6.nodeType === 1 || b6.nodeType === 11) && !ay.isXMLDoc(b6)) {
                b5 = ai(e);
                ca = ai(b6);
                for (b7 = 0;
                     (b9 = ca[b7]) != null; ++b7) {
                    if (b5[b7]) {
                        ad(b9, b5[b7])
                    }
                }
            }
            if (b3) {
                if (b4) {
                    ca = ca || ai(b6);
                    b5 = b5 || ai(e);
                    for (b7 = 0;
                         (b9 = ca[b7]) != null; b7++) {
                        u(b9, b5[b7])
                    }
                } else {
                    u(b6, e)
                }
            }
            b5 = ai(e, "script");
            if (b5.length > 0) {
                bM(b5, !b8 && ai(b6, "script"))
            }
            b5 = ca = b9 = null;
            return e
        },
        buildFragment: function(b5, b3, cb, cc) {
            var b7, b4, e, cf, cd, ce, cg, b8 = b5.length,
                ca = L(b3),
                b9 = [],
                b6 = 0;
            for (; b6 < b8; b6++) {
                b4 = b5[b6];
                if (b4 || b4 === 0) {
                    if (ay.type(b4) === "object") {
                        ay.merge(b9, b4.nodeType ? [b4] : b4)
                    } else {
                        if (!a4.test(b4)) {
                            b9.push(b3.createTextNode(b4))
                        } else {
                            cf = cf || ca.appendChild(b3.createElement("div"));
                            cd = (by.exec(b4) || ["", ""])[1].toLowerCase();
                            cg = bY[cd] || bY._default;
                            cf.innerHTML = cg[1] + b4.replace(bK, "<$1></$2>") + cg[2];
                            b7 = cg[0];
                            while (b7--) {
                                cf = cf.lastChild
                            }
                            if (!ay.support.leadingWhitespace && a8.test(b4)) {
                                b9.push(b3.createTextNode(a8.exec(b4)[0]))
                            }
                            if (!ay.support.tbody) {
                                b4 = cd === "table" && !bz.test(b4) ? cf.firstChild : cg[1] === "<table>" && !bz.test(b4) ? cf : 0;
                                b7 = b4 && b4.childNodes.length;
                                while (b7--) {
                                    if (ay.nodeName((ce = b4.childNodes[b7]), "tbody") && !ce.childNodes.length) {
                                        b4.removeChild(ce)
                                    }
                                }
                            }
                            ay.merge(b9, cf.childNodes);
                            cf.textContent = "";
                            while (cf.firstChild) {
                                cf.removeChild(cf.firstChild)
                            }
                            cf = ca.lastChild
                        }
                    }
                }
            }
            if (cf) {
                ca.removeChild(cf)
            }
            if (!ay.support.appendChecked) {
                ay.grep(ai(b9, "input"), ae)
            }
            b6 = 0;
            while ((b4 = b9[b6++])) {
                if (cc && ay.inArray(b4, cc) !== -1) {
                    continue
                }
                e = ay.contains(b4.ownerDocument, b4);
                cf = ai(ca.appendChild(b4), "script");
                if (e) {
                    bM(cf)
                }
                if (cb) {
                    b7 = 0;
                    while ((b4 = cf[b7++])) {
                        if (bt.test(b4.type || "")) {
                            cb.push(b4)
                        }
                    }
                }
            }
            cf = null;
            return ca
        },
        cleanData: function(b7, e) {
            var b6, cc, b9, b4, b8 = 0,
                ca = ay.expando,
                b3 = ay.cache,
                b5 = ay.support.deleteExpando,
                cb = ay.event.special;
            for (;
                (b6 = b7[b8]) != null; b8++) {
                if (e || ay.acceptData(b6)) {
                    b9 = b6[ca];
                    b4 = b9 && b3[b9];
                    if (b4) {
                        if (b4.events) {
                            for (cc in b4.events) {
                                if (cb[cc]) {
                                    ay.event.remove(b6, cc)
                                } else {
                                    ay.removeEvent(b6, cc, b4.handle)
                                }
                            }
                        }
                        if (b3[b9]) {
                            delete b3[b9];
                            if (b5) {
                                delete b6[ca]
                            } else {
                                if (typeof b6.removeAttribute !== E) {
                                    b6.removeAttribute(ca)
                                } else {
                                    b6[ca] = null
                                }
                            }
                            x.push(b9)
                        }
                    }
                }
            }
        }
    });
    var ap, al, T, aI = /alpha\([^)]*\)/i,
        bl = /opacity\s*=\s*([^)]*)/,
        bn = /^(top|right|bottom|left)$/,
        aS = /^(none|table(?!-c[ea]).+)/,
        ba = /^margin/,
        bj = new RegExp("^(" + A + ")(.*)$", "i"),
        bi = new RegExp("^(" + A + ")(?!px)[a-z%]+$", "i"),
        bq = new RegExp("^([+-])=(" + A + ")", "i"),
        aa = {
            BODY: "block"
        }, S = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        }, Q = {
            letterSpacing: 0,
            fontWeight: 400
        }, P = ["Top", "Right", "Bottom", "Left"],
        R = ["Webkit", "O", "Moz", "ms"];

    function bV(b6, b4) {
        if (b4 in b6) {
            return b4
        }
        var e = b4.charAt(0).toUpperCase() + b4.slice(1),
            b5 = b4,
            b3 = R.length;
        while (b3--) {
            b4 = R[b3] + e;
            if (b4 in b6) {
                return b4
            }
        }
        return b5
    }

    function aw(b3, e) {
        b3 = e || b3;
        return ay.css(b3, "display") === "none" || !ay.contains(b3.ownerDocument, b3)
    }

    function bO(b4, b8) {
        var e, b3, b5, b9 = [],
            b6 = 0,
            b7 = b4.length;
        for (; b6 < b7; b6++) {
            b3 = b4[b6];
            if (!b3.style) {
                continue
            }
            b9[b6] = ay._data(b3, "olddisplay");
            e = b3.style.display;
            if (b8) {
                if (!b9[b6] && e === "none") {
                    b3.style.display = ""
                }
                if (b3.style.display === "" && aw(b3)) {
                    b9[b6] = ay._data(b3, "olddisplay", O(b3.nodeName))
                }
            } else {
                if (!b9[b6]) {
                    b5 = aw(b3);
                    if (e && e !== "none" || !b5) {
                        ay._data(b3, "olddisplay", b5 ? e : ay.css(b3, "display"))
                    }
                }
            }
        }
        for (b6 = 0; b6 < b7; b6++) {
            b3 = b4[b6];
            if (!b3.style) {
                continue
            }
            if (!b8 || b3.style.display === "none" || b3.style.display === "") {
                b3.style.display = b8 ? b9[b6] || "" : "none"
            }
        }
        return b4
    }
    ay.fn.extend({
        css: function(e, b3) {
            return ay.access(this, function(b4, b8, ca) {
                var b6, b9, b7 = {}, b5 = 0;
                if (ay.isArray(b8)) {
                    b9 = al(b4);
                    b6 = b8.length;
                    for (; b5 < b6; b5++) {
                        b7[b8[b5]] = ay.css(b4, b8[b5], false, b9)
                    }
                    return b7
                }
                return ca !== bU ? ay.style(b4, b8, ca) : ay.css(b4, b8)
            }, e, b3, arguments.length > 1)
        },
        show: function() {
            return bO(this, true)
        },
        hide: function() {
            return bO(this)
        },
        toggle: function(b3) {
            var e = typeof b3 === "boolean";
            return this.each(function() {
                if (e ? b3 : aw(this)) {
                    ay(this).show()
                } else {
                    ay(this).hide()
                }
            })
        }
    });
    ay.extend({
        cssHooks: {
            opacity: {
                get: function(b3, e) {
                    if (e) {
                        var b4 = T(b3, "opacity");
                        return b4 === "" ? "1" : b4
                    }
                }
            }
        },
        cssNumber: {
            columnCount: true,
            fillOpacity: true,
            fontWeight: true,
            lineHeight: true,
            opacity: true,
            orphans: true,
            widows: true,
            zIndex: true,
            zoom: true
        },
        cssProps: {
            "float": ay.support.cssFloat ? "cssFloat" : "styleFloat"
        },
        style: function(b4, b7, cc, b5) {
            if (!b4 || b4.nodeType === 3 || b4.nodeType === 8 || !b4.style) {
                return
            }
            var b9, cb, b6, b8 = ay.camelCase(b7),
                ca = b4.style;
            b7 = ay.cssProps[b8] || (ay.cssProps[b8] = bV(ca, b8));
            b6 = ay.cssHooks[b7] || ay.cssHooks[b8];
            if (cc !== bU) {
                cb = typeof cc;
                if (cb === "string" && (b9 = bq.exec(cc))) {
                    cc = (b9[1] + 1) * b9[2] + parseFloat(ay.css(b4, b7));
                    cb = "number"
                }
                if (cc == null || cb === "number" && isNaN(cc)) {
                    return
                }
                if (cb === "number" && !ay.cssNumber[b8]) {
                    cc += "px"
                }
                if (!ay.support.clearCloneStyle && cc === "" && b7.indexOf("background") === 0) {
                    ca[b7] = "inherit"
                }
                if (!b6 || !("set" in b6) || (cc = b6.set(b4, cc, b5)) !== bU) {
                    try {
                        ca[b7] = cc
                    } catch (b3) {}
                }
            } else {
                if (b6 && "get" in b6 && (b9 = b6.get(b4, false, b5)) !== bU) {
                    return b9
                }
                return ca[b7]
            }
        },
        css: function(e, b5, b3, b8) {
            var b6, b9, b4, b7 = ay.camelCase(b5);
            b5 = ay.cssProps[b7] || (ay.cssProps[b7] = bV(e.style, b7));
            b4 = ay.cssHooks[b5] || ay.cssHooks[b7];
            if (b4 && "get" in b4) {
                b9 = b4.get(e, true, b3)
            }
            if (b9 === bU) {
                b9 = T(e, b5, b8)
            }
            if (b9 === "normal" && b5 in Q) {
                b9 = Q[b5]
            }
            if (b3 === "" || b3) {
                b6 = parseFloat(b9);
                return b3 === true || ay.isNumeric(b6) ? b6 || 0 : b9
            }
            return b9
        },
        swap: function(b4, b7, b3, e) {
            var b8, b5, b6 = {};
            for (b5 in b7) {
                b6[b5] = b4.style[b5];
                b4.style[b5] = b7[b5]
            }
            b8 = b3.apply(b4, e || []);
            for (b5 in b7) {
                b4.style[b5] = b6[b5]
            }
            return b8
        }
    });
    if (bW.getComputedStyle) {
        al = function(e) {
            return bW.getComputedStyle(e, null)
        };
        T = function(b4, b7, e) {
            var ca, b6, b5, b3 = e || al(b4),
                b8 = b3 ? b3.getPropertyValue(b7) || b3[b7] : bU,
                b9 = b4.style;
            if (b3) {
                if (b8 === "" && !ay.contains(b4.ownerDocument, b4)) {
                    b8 = ay.style(b4, b7)
                }
                if (bi.test(b8) && ba.test(b7)) {
                    ca = b9.width;
                    b6 = b9.minWidth;
                    b5 = b9.maxWidth;
                    b9.minWidth = b9.maxWidth = b9.width = b8;
                    b8 = b3.width;
                    b9.width = ca;
                    b9.minWidth = b6;
                    b9.maxWidth = b5
                }
            }
            return b8
        }
    } else {
        if (Y.documentElement.currentStyle) {
            al = function(e) {
                return e.currentStyle
            };
            T = function(b4, b6, e) {
                var b5, b8, b9, b3 = e || al(b4),
                    b7 = b3 ? b3[b6] : bU,
                    ca = b4.style;
                if (b7 == null && ca && ca[b6]) {
                    b7 = ca[b6]
                }
                if (bi.test(b7) && !bn.test(b6)) {
                    b5 = ca.left;
                    b8 = b4.runtimeStyle;
                    b9 = b8 && b8.left;
                    if (b9) {
                        b8.left = b4.currentStyle.left
                    }
                    ca.left = b6 === "fontSize" ? "1em" : b7;
                    b7 = ca.pixelLeft + "px";
                    ca.left = b5;
                    if (b9) {
                        b8.left = b9
                    }
                }
                return b7 === "" ? "auto" : b7
            }
        }
    }

    function bN(e, b5, b4) {
        var b3 = bj.exec(b5);
        return b3 ? Math.max(0, b3[1] - (b4 || 0)) + (b3[2] || "px") : b5
    }

    function q(e, b6, b3, b5, b7) {
        var b4 = b3 === (b5 ? "border" : "content") ? 4 : b6 === "width" ? 1 : 0,
            b8 = 0;
        for (; b4 < 4; b4 += 2) {
            if (b3 === "margin") {
                b8 += ay.css(e, b3 + P[b4], true, b7)
            }
            if (b5) {
                if (b3 === "content") {
                    b8 -= ay.css(e, "padding" + P[b4], true, b7)
                }
                if (b3 !== "margin") {
                    b8 -= ay.css(e, "border" + P[b4] + "Width", true, b7)
                }
            } else {
                b8 += ay.css(e, "padding" + P[b4], true, b7);
                if (b3 !== "padding") {
                    b8 += ay.css(e, "border" + P[b4] + "Width", true, b7)
                }
            }
        }
        return b8
    }

    function am(e, b5, b3) {
        var b8 = true,
            b7 = b5 === "width" ? e.offsetWidth : e.offsetHeight,
            b6 = al(e),
            b4 = ay.support.boxSizing && ay.css(e, "boxSizing", false, b6) === "border-box";
        if (b7 <= 0 || b7 == null) {
            b7 = T(e, b5, b6);
            if (b7 < 0 || b7 == null) {
                b7 = e.style[b5]
            }
            if (bi.test(b7)) {
                return b7
            }
            b8 = b4 && (ay.support.boxSizingReliable || b7 === e.style[b5]);
            b7 = parseFloat(b7) || 0
        }
        return (b7 + q(e, b5, b3 || (b4 ? "border" : "content"), b8, b6)) + "px"
    }

    function O(b4) {
        var b3 = Y,
            e = aa[b4];
        if (!e) {
            e = d(b4, b3);
            if (e === "none" || !e) {
                ap = (ap || ay("<iframe frameborder='0' width='0' height='0'/>").css("cssText", "display:block !important")).appendTo(b3.documentElement);
                b3 = (ap[0].contentWindow || ap[0].contentDocument).document;
                b3.write("<!doctype html><html><body>");
                b3.close();
                e = d(b4, b3);
                ap.detach()
            }
            aa[b4] = e
        }
        return e
    }

    function d(b5, b3) {
        var b4 = ay(b3.createElement(b5)).appendTo(b3.body),
            e = ay.css(b4[0], "display");
        b4.remove();
        return e
    }
    ay.each(["height", "width"], function(e, b3) {
        ay.cssHooks[b3] = {
            get: function(b5, b4, b6) {
                if (b4) {
                    return b5.offsetWidth === 0 && aS.test(ay.css(b5, "display")) ? ay.swap(b5, S, function() {
                        return am(b5, b3, b6)
                    }) : am(b5, b3, b6)
                }
            },
            set: function(b4, b7, b5) {
                var b6 = b5 && al(b4);
                return bN(b4, b7, b5 ? q(b4, b3, b5, ay.support.boxSizing && ay.css(b4, "boxSizing", false, b6) === "border-box", b6) : 0)
            }
        }
    });
    if (!ay.support.opacity) {
        ay.cssHooks.opacity = {
            get: function(b3, e) {
                return bl.test((e && b3.currentStyle ? b3.currentStyle.filter : b3.style.filter) || "") ? (0.01 * parseFloat(RegExp.$1)) + "" : e ? "1" : ""
            },
            set: function(b3, b7) {
                var b6 = b3.style,
                    e = b3.currentStyle,
                    b5 = ay.isNumeric(b7) ? "alpha(opacity=" + b7 * 100 + ")" : "",
                    b4 = e && e.filter || b6.filter || "";
                b6.zoom = 1;
                if ((b7 >= 1 || b7 === "") && ay.trim(b4.replace(aI, "")) === "" && b6.removeAttribute) {
                    b6.removeAttribute("filter");
                    if (b7 === "" || e && !e.filter) {
                        return
                    }
                }
                b6.filter = aI.test(b4) ? b4.replace(aI, b5) : b4 + " " + b5
            }
        }
    }
    ay(function() {
        if (!ay.support.reliableMarginRight) {
            ay.cssHooks.marginRight = {
                get: function(b3, e) {
                    if (e) {
                        return ay.swap(b3, {
                            display: "inline-block"
                        }, T, [b3, "marginRight"])
                    }
                }
            }
        }
        if (!ay.support.pixelPosition && ay.fn.position) {
            ay.each(["top", "left"], function(e, b3) {
                ay.cssHooks[b3] = {
                    get: function(b5, b4) {
                        if (b4) {
                            b4 = T(b5, b3);
                            return bi.test(b4) ? ay(b5).position()[b3] + "px" : b4
                        }
                    }
                }
            })
        }
    });
    if (ay.expr && ay.expr.filters) {
        ay.expr.filters.hidden = function(e) {
            return e.offsetWidth <= 0 && e.offsetHeight <= 0 || (!ay.support.reliableHiddenOffsets && ((e.style && e.style.display) || ay.css(e, "display")) === "none")
        };
        ay.expr.filters.visible = function(e) {
            return !ay.expr.filters.hidden(e)
        }
    }
    ay.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(e, b3) {
        ay.cssHooks[e + b3] = {
            expand: function(b7) {
                var b5 = 0,
                    b4 = {}, b6 = typeof b7 === "string" ? b7.split(" ") : [b7];
                for (; b5 < 4; b5++) {
                    b4[e + P[b5] + b3] = b6[b5] || b6[b5 - 2] || b6[0]
                }
                return b4
            }
        };
        if (!ba.test(e)) {
            ay.cssHooks[e + b3].set = bN
        }
    });
    var aH = /%20/g,
        aL = /\[\]$/,
        aQ = /\r?\n/g,
        bx = /^(?:submit|button|image|reset|file)$/i,
        bw = /^(?:input|select|textarea|keygen)/i;
    ay.fn.extend({
        serialize: function() {
            return ay.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var e = ay.prop(this, "elements");
                return e ? ay.makeArray(e) : this
            }).filter(function() {
                var e = this.type;
                return this.name && !ay(this).is(":disabled") && bw.test(this.nodeName) && !bx.test(e) && (this.checked || !aA.test(e))
            }).map(function(b3, e) {
                var b4 = ay(this).val();
                return b4 == null ? null : ay.isArray(b4) ? ay.map(b4, function(b5) {
                    return {
                        name: e.name,
                        value: b5.replace(aQ, "\r\n")
                    }
                }) : {
                    name: e.name,
                    value: b4.replace(aQ, "\r\n")
                }
            }).get()
        }
    });
    ay.param = function(e, b6) {
        var b4, b5 = [],
            b3 = function(b7, b8) {
                b8 = ay.isFunction(b8) ? b8() : (b8 == null ? "" : b8);
                b5[b5.length] = encodeURIComponent(b7) + "=" + encodeURIComponent(b8)
            };
        if (b6 === bU) {
            b6 = ay.ajaxSettings && ay.ajaxSettings.traditional
        }
        if (ay.isArray(e) || (e.jquery && !ay.isPlainObject(e))) {
            ay.each(e, function() {
                b3(this.name, this.value)
            })
        } else {
            for (b4 in e) {
                s(b4, e[b4], b6, b3)
            }
        }
        return b5.join("&").replace(aH, "+")
    };

    function s(b5, b4, b6, e) {
        var b3;
        if (ay.isArray(b4)) {
            ay.each(b4, function(b7, b8) {
                if (b6 || aL.test(b5)) {
                    e(b5, b8)
                } else {
                    s(b5 + "[" + (typeof b8 === "object" ? b7 : "") + "]", b8, b6, e)
                }
            })
        } else {
            if (!b6 && ay.type(b4) === "object") {
                for (b3 in b4) {
                    s(b5 + "[" + b3 + "]", b4[b3], b6, e)
                }
            } else {
                e(b5, b4)
            }
        }
    }
    ay.each(("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu").split(" "), function(e, b3) {
        ay.fn[b3] = function(b4, b5) {
            return arguments.length > 0 ? this.on(b3, null, b4, b5) : this.trigger(b3)
        }
    });
    ay.fn.hover = function(b3, e) {
        return this.mouseenter(b3).mouseleave(e || b3)
    };
    var m, l, g = ay.now(),
        h = /\?/,
        a2 = /#.*$/,
        bB = /([?&])_=[^&]*/,
        a3 = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg,
        a9 = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        bf = /^(?:GET|HEAD)$/,
        bo = /^\/\//,
        bE = /^([\w.+-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,
        c = ay.fn.load,
        aF = {}, bR = {}, n = "*/".concat("*");
    try {
        l = az.href
    } catch (Z) {
        l = Y.createElement("a");
        l.href = "";
        l = l.href
    }
    m = bE.exec(l.toLowerCase()) || [];

    function f(e) {
        return function(b4, b6) {
            if (typeof b4 !== "string") {
                b6 = b4;
                b4 = "*"
            }
            var b3, b7 = 0,
                b5 = b4.toLowerCase().match(C) || [];
            if (ay.isFunction(b6)) {
                while ((b3 = b5[b7++])) {
                    if (b3[0] === "+") {
                        b3 = b3.slice(1) || "*";
                        (e[b3] = e[b3] || []).unshift(b6)
                    } else {
                        (e[b3] = e[b3] || []).push(b6)
                    }
                }
            }
        }
    }

    function aq(b8, b5, b6, b4) {
        var b3 = {}, b7 = (b8 === bR);

        function e(b9) {
            var ca;
            b3[b9] = true;
            ay.each(b8[b9] || [], function(cb, cd) {
                var cc = cd(b5, b6, b4);
                if (typeof cc === "string" && !b7 && !b3[cc]) {
                    b5.dataTypes.unshift(cc);
                    e(cc);
                    return false
                } else {
                    if (b7) {
                        return !(ca = cc)
                    }
                }
            });
            return ca
        }
        return e(b5.dataTypes[0]) || !b3["*"] && e("*")
    }

    function j(b6, b5) {
        var e, b4, b3 = ay.ajaxSettings.flatOptions || {};
        for (b4 in b5) {
            if (b5[b4] !== bU) {
                (b3[b4] ? b6 : (e || (e = {})))[b4] = b5[b4]
            }
        }
        if (e) {
            ay.extend(true, b6, e)
        }
        return b6
    }
    ay.fn.load = function(b9, b4, e) {
        if (typeof b9 !== "string" && c) {
            return c.apply(this, arguments)
        }
        var b6, b5, b8, b7 = this,
            b3 = b9.indexOf(" ");
        if (b3 >= 0) {
            b6 = b9.slice(b3, b9.length);
            b9 = b9.slice(0, b3)
        }
        if (ay.isFunction(b4)) {
            e = b4;
            b4 = bU
        } else {
            if (b4 && typeof b4 === "object") {
                b8 = "POST"
            }
        } if (b7.length > 0) {
            ay.ajax({
                url: b9,
                type: b8,
                dataType: "html",
                data: b4
            }).done(function(ca) {
                b5 = arguments;
                b7.html(b6 ? ay("<div>").append(ay.parseHTML(ca)).find(b6) : ca)
            }).complete(e && function(ca, cb) {
                b7.each(e, b5 || [ca.responseText, cb, ca])
            })
        }
        return this
    };
    ay.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, b3) {
        ay.fn[b3] = function(b4) {
            return this.on(b3, b4)
        }
    });
    ay.each(["get", "post"], function(e, b3) {
        ay[b3] = function(b7, b5, b4, b6) {
            if (ay.isFunction(b5)) {
                b6 = b6 || b4;
                b4 = b5;
                b5 = bU
            }
            return ay.ajax({
                url: b7,
                type: b3,
                dataType: b6,
                data: b5,
                success: b4
            })
        }
    });
    ay.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: l,
            type: "GET",
            isLocal: a9.test(m[1]),
            global: true,
            processData: true,
            async: true,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": n,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText"
            },
            converters: {
                "* text": bW.String,
                "text html": true,
                "text json": ay.parseJSON,
                "text xml": ay.parseXML
            },
            flatOptions: {
                url: true,
                context: true
            }
        },
        ajaxSetup: function(b3, e) {
            return e ? j(j(b3, ay.ajaxSettings), e) : j(ay.ajaxSettings, b3)
        },
        ajaxPrefilter: f(aF),
        ajaxTransport: f(bR),
        ajax: function(cp, cd) {
            if (typeof cp === "object") {
                cd = cp;
                cp = bU
            }
            cd = cd || {};
            var ce, cb, b3, ci, cn, b9, co, ch, cj = ay.ajaxSetup({}, cd),
                b4 = cj.context || cj,
                ca = cj.context && (b4.nodeType || b4.jquery) ? ay(b4) : ay.event,
                b6 = ay.Deferred(),
                b5 = ay.Callbacks("once memory"),
                cl = cj.statusCode || {}, cf = {}, cg = {}, ck = 0,
                cm = "canceled",
                cc = {
                    readyState: 0,
                    getResponseHeader: function(e) {
                        var cq;
                        if (ck === 2) {
                            if (!ch) {
                                ch = {};
                                while ((cq = a3.exec(ci))) {
                                    ch[cq[1].toLowerCase()] = cq[2]
                                }
                            }
                            cq = ch[e.toLowerCase()]
                        }
                        return cq == null ? null : cq
                    },
                    getAllResponseHeaders: function() {
                        return ck === 2 ? ci : null
                    },
                    setRequestHeader: function(cq, cr) {
                        var e = cq.toLowerCase();
                        if (!ck) {
                            cq = cg[e] = cg[e] || cq;
                            cf[cq] = cr
                        }
                        return this
                    },
                    overrideMimeType: function(e) {
                        if (!ck) {
                            cj.mimeType = e
                        }
                        return this
                    },
                    statusCode: function(cq) {
                        var e;
                        if (cq) {
                            if (ck < 2) {
                                for (e in cq) {
                                    cl[e] = [cl[e], cq[e]]
                                }
                            } else {
                                cc.always(cq[cc.status])
                            }
                        }
                        return this
                    },
                    abort: function(cq) {
                        var e = cq || cm;
                        if (co) {
                            co.abort(e)
                        }
                        b7(0, e);
                        return this
                    }
                };
            b6.promise(cc).complete = b5.add;
            cc.success = cc.done;
            cc.error = cc.fail;
            cj.url = ((cp || cj.url || l) + "").replace(a2, "").replace(bo, m[1] + "//");
            cj.type = cd.method || cd.type || cj.method || cj.type;
            cj.dataTypes = ay.trim(cj.dataType || "*").toLowerCase().match(C) || [""];
            if (cj.crossDomain == null) {
                ce = bE.exec(cj.url.toLowerCase());
                cj.crossDomain = !! (ce && (ce[1] !== m[1] || ce[2] !== m[2] || (ce[3] || (ce[1] === "http:" ? 80 : 443)) != (m[3] || (m[1] === "http:" ? 80 : 443))))
            }
            if (cj.data && cj.processData && typeof cj.data !== "string") {
                cj.data = ay.param(cj.data, cj.traditional)
            }
            aq(aF, cj, cd, cc);
            if (ck === 2) {
                return cc
            }
            b9 = cj.global;
            if (b9 && ay.active++ === 0) {
                ay.event.trigger("ajaxStart")
            }
            cj.type = cj.type.toUpperCase();
            cj.hasContent = !bf.test(cj.type);
            b3 = cj.url;
            if (!cj.hasContent) {
                if (cj.data) {
                    b3 = (cj.url += (h.test(b3) ? "&" : "?") + cj.data);
                    delete cj.data
                }
                if (cj.cache === false) {
                    cj.url = bB.test(b3) ? b3.replace(bB, "$1_=" + g++) : b3 + (h.test(b3) ? "&" : "?") + "_=" + g++
                }
            }
            if (cj.ifModified) {
                if (ay.lastModified[b3]) {
                    cc.setRequestHeader("If-Modified-Since", ay.lastModified[b3])
                }
                if (ay.etag[b3]) {
                    cc.setRequestHeader("If-None-Match", ay.etag[b3])
                }
            }
            if (cj.data && cj.hasContent && cj.contentType !== false || cd.contentType) {
                cc.setRequestHeader("Content-Type", cj.contentType)
            }
            cc.setRequestHeader("Accept", cj.dataTypes[0] && cj.accepts[cj.dataTypes[0]] ? cj.accepts[cj.dataTypes[0]] + (cj.dataTypes[0] !== "*" ? ", " + n + "; q=0.01" : "") : cj.accepts["*"]);
            for (cb in cj.headers) {
                cc.setRequestHeader(cb, cj.headers[cb])
            }
            if (cj.beforeSend && (cj.beforeSend.call(b4, cc, cj) === false || ck === 2)) {
                return cc.abort()
            }
            cm = "abort";
            for (cb in {
                success: 1,
                error: 1,
                complete: 1
            }) {
                cc[cb](cj[cb])
            }
            co = aq(bR, cj, cd, cc);
            if (!co) {
                b7(-1, "No Transport")
            } else {
                cc.readyState = 1;
                if (b9) {
                    ca.trigger("ajaxSend", [cc, cj])
                }
                if (cj.async && cj.timeout > 0) {
                    cn = setTimeout(function() {
                        cc.abort("timeout")
                    }, cj.timeout)
                }
                try {
                    ck = 1;
                    co.send(cf, b7)
                } catch (b8) {
                    if (ck < 2) {
                        b7(-1, b8)
                    } else {
                        throw b8
                    }
                }
            }

            function b7(cw, ct, cv, cq) {
                var cr, cy, e, cu, cs, cx = ct;
                if (ck === 2) {
                    return
                }
                ck = 2;
                if (cn) {
                    clearTimeout(cn)
                }
                co = bU;
                ci = cq || "";
                cc.readyState = cw > 0 ? 4 : 0;
                if (cv) {
                    cu = k(cj, cc, cv)
                }
                if (cw >= 200 && cw < 300 || cw === 304) {
                    if (cj.ifModified) {
                        cs = cc.getResponseHeader("Last-Modified");
                        if (cs) {
                            ay.lastModified[b3] = cs
                        }
                        cs = cc.getResponseHeader("etag");
                        if (cs) {
                            ay.etag[b3] = cs
                        }
                    }
                    if (cw === 204) {
                        cr = true;
                        cx = "nocontent"
                    } else {
                        if (cw === 304) {
                            cr = true;
                            cx = "notmodified"
                        } else {
                            cr = i(cj, cu);
                            cx = cr.state;
                            cy = cr.data;
                            e = cr.error;
                            cr = !e
                        }
                    }
                } else {
                    e = cx;
                    if (cw || !cx) {
                        cx = "error";
                        if (cw < 0) {
                            cw = 0
                        }
                    }
                }
                cc.status = cw;
                cc.statusText = (ct || cx) + "";
                if (cr) {
                    b6.resolveWith(b4, [cy, cx, cc])
                } else {
                    b6.rejectWith(b4, [cc, cx, e])
                }
                cc.statusCode(cl);
                cl = bU;
                if (b9) {
                    ca.trigger(cr ? "ajaxSuccess" : "ajaxError", [cc, cj, cr ? cy : e])
                }
                b5.fireWith(b4, [cc, cx]);
                if (b9) {
                    ca.trigger("ajaxComplete", [cc, cj]);
                    if (!(--ay.active)) {
                        ay.event.trigger("ajaxStop")
                    }
                }
            }
            return cc
        },
        getScript: function(b3, e) {
            return ay.get(b3, bU, e, "script")
        },
        getJSON: function(b4, b3, e) {
            return ay.get(b4, b3, e, "json")
        }
    });

    function k(ca, b7, b9) {
        var b6, b3, b5, cb, e = ca.contents,
            b4 = ca.dataTypes,
            b8 = ca.responseFields;
        for (cb in b8) {
            if (cb in b9) {
                b7[b8[cb]] = b9[cb]
            }
        }
        while (b4[0] === "*") {
            b4.shift();
            if (b3 === bU) {
                b3 = ca.mimeType || b7.getResponseHeader("Content-Type")
            }
        }
        if (b3) {
            for (cb in e) {
                if (e[cb] && e[cb].test(b3)) {
                    b4.unshift(cb);
                    break
                }
            }
        }
        if (b4[0] in b9) {
            b5 = b4[0]
        } else {
            for (cb in b9) {
                if (!b4[0] || ca.converters[cb + " " + b4[0]]) {
                    b5 = cb;
                    break
                }
                if (!b6) {
                    b6 = cb
                }
            }
            b5 = b5 || b6
        } if (b5) {
            if (b5 !== b4[0]) {
                b4.unshift(b5)
            }
            return b9[b5]
        }
    }

    function i(cc, cb) {
        var b4, b6, b3, cd, b5 = {}, b9 = 0,
            b7 = cc.dataTypes.slice(),
            ca = b7[0];
        if (cc.dataFilter) {
            cb = cc.dataFilter(cb, cc.dataType)
        }
        if (b7[1]) {
            for (b3 in cc.converters) {
                b5[b3.toLowerCase()] = cc.converters[b3]
            }
        }
        for (;
            (b6 = b7[++b9]);) {
            if (b6 !== "*") {
                if (ca !== "*" && ca !== b6) {
                    b3 = b5[ca + " " + b6] || b5["* " + b6];
                    if (!b3) {
                        for (b4 in b5) {
                            cd = b4.split(" ");
                            if (cd[1] === b6) {
                                b3 = b5[ca + " " + cd[0]] || b5["* " + cd[0]];
                                if (b3) {
                                    if (b3 === true) {
                                        b3 = b5[b4]
                                    } else {
                                        if (b5[b4] !== true) {
                                            b6 = cd[0];
                                            b7.splice(b9--, 0, b6)
                                        }
                                    }
                                    break
                                }
                            }
                        }
                    }
                    if (b3 !== true) {
                        if (b3 && cc["throws"]) {
                            cb = b3(cb)
                        } else {
                            try {
                                cb = b3(cb)
                            } catch (b8) {
                                return {
                                    state: "parsererror",
                                    error: b3 ? b8 : "No conversion from " + ca + " to " + b6
                                }
                            }
                        }
                    }
                }
                ca = b6
            }
        }
        return {
            state: "success",
            data: cb
        }
    }
    ay.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function(e) {
                ay.globalEval(e);
                return e
            }
        }
    });
    ay.ajaxPrefilter("script", function(e) {
        if (e.cache === bU) {
            e.cache = false
        }
        if (e.crossDomain) {
            e.type = "GET";
            e.global = false
        }
    });
    ay.ajaxTransport("script", function(b3) {
        if (b3.crossDomain) {
            var b4, e = Y.head || ay("head")[0] || Y.documentElement;
            return {
                send: function(b5, b6) {
                    b4 = Y.createElement("script");
                    b4.async = true;
                    if (b3.scriptCharset) {
                        b4.charset = b3.scriptCharset
                    }
                    b4.src = b3.url;
                    b4.onload = b4.onreadystatechange = function(b7, b8) {
                        if (b8 || !b4.readyState || /loaded|complete/.test(b4.readyState)) {
                            b4.onload = b4.onreadystatechange = null;
                            if (b4.parentNode) {
                                b4.parentNode.removeChild(b4)
                            }
                            b4 = null;
                            if (!b8) {
                                b6(200, "success")
                            }
                        }
                    };
                    e.insertBefore(b4, e.firstChild)
                },
                abort: function() {
                    if (b4) {
                        b4.onload(bU, true)
                    }
                }
            }
        }
    });
    var aD = [],
        a6 = /(=)\?(?=&|$)|\?\?/;
    ay.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var e = aD.pop() || (ay.expando + "_" + (g++));
            this[e] = true;
            return e
        }
    });
    ay.ajaxPrefilter("json jsonp", function(b8, b5, b3) {
        var e, b6, b7, b4 = b8.jsonp !== false && (a6.test(b8.url) ? "url" : typeof b8.data === "string" && !(b8.contentType || "").indexOf("application/x-www-form-urlencoded") && a6.test(b8.data) && "data");
        if (b4 || b8.dataTypes[0] === "jsonp") {
            e = b8.jsonpCallback = ay.isFunction(b8.jsonpCallback) ? b8.jsonpCallback() : b8.jsonpCallback;
            if (b4) {
                b8[b4] = b8[b4].replace(a6, "$1" + e)
            } else {
                if (b8.jsonp !== false) {
                    b8.url += (h.test(b8.url) ? "&" : "?") + b8.jsonp + "=" + e
                }
            }
            b8.converters["script json"] = function() {
                if (!b7) {
                    ay.error(e + " was not called")
                }
                return b7[0]
            };
            b8.dataTypes[0] = "json";
            b6 = bW[e];
            bW[e] = function() {
                b7 = arguments
            };
            b3.always(function() {
                bW[e] = b6;
                if (b8[e]) {
                    b8.jsonpCallback = b5.jsonpCallback;
                    aD.push(e)
                }
                if (b7 && ay.isFunction(b6)) {
                    b6(b7[0])
                }
                b7 = b6 = bU
            });
            return "script"
        }
    });
    var bZ, b2, b0 = 0,
        b1 = bW.ActiveXObject && function() {
                var e;
                for (e in bZ) {
                    bZ[e](bU, true)
                }
            };

    function M() {
        try {
            return new bW.XMLHttpRequest()
        } catch (b3) {}
    }

    function I() {
        try {
            return new bW.ActiveXObject("Microsoft.XMLHTTP")
        } catch (b3) {}
    }
    ay.ajaxSettings.xhr = bW.ActiveXObject ? function() {
        return !this.isLocal && M() || I()
    } : M;
    b2 = ay.ajaxSettings.xhr();
    ay.support.cors = !! b2 && ("withCredentials" in b2);
    b2 = ay.support.ajax = !! b2;
    if (b2) {
        ay.ajaxTransport(function(b3) {
            if (!b3.crossDomain || ay.support.cors) {
                var e;
                return {
                    send: function(b7, b4) {
                        var b6, b8, b9 = b3.xhr();
                        if (b3.username) {
                            b9.open(b3.type, b3.url, b3.async, b3.username, b3.password)
                        } else {
                            b9.open(b3.type, b3.url, b3.async)
                        } if (b3.xhrFields) {
                            for (b8 in b3.xhrFields) {
                                b9[b8] = b3.xhrFields[b8]
                            }
                        }
                        if (b3.mimeType && b9.overrideMimeType) {
                            b9.overrideMimeType(b3.mimeType)
                        }
                        if (!b3.crossDomain && !b7["X-Requested-With"]) {
                            b7["X-Requested-With"] = "XMLHttpRequest"
                        }
                        try {
                            for (b8 in b7) {
                                b9.setRequestHeader(b8, b7[b8])
                            }
                        } catch (b5) {}
                        b9.send((b3.hasContent && b3.data) || null);
                        e = function(ca, cd) {
                            var cg, ce, ch, cf;
                            try {
                                if (e && (cd || b9.readyState === 4)) {
                                    e = bU;
                                    if (b6) {
                                        b9.onreadystatechange = ay.noop;
                                        if (b1) {
                                            delete bZ[b6]
                                        }
                                    }
                                    if (cd) {
                                        if (b9.readyState !== 4) {
                                            b9.abort()
                                        }
                                    } else {
                                        cf = {};
                                        cg = b9.status;
                                        ce = b9.getAllResponseHeaders();
                                        if (typeof b9.responseText === "string") {
                                            cf.text = b9.responseText
                                        }
                                        try {
                                            ch = b9.statusText
                                        } catch (cb) {
                                            ch = ""
                                        }
                                        if (!cg && b3.isLocal && !b3.crossDomain) {
                                            cg = cf.text ? 200 : 404
                                        } else {
                                            if (cg === 1223) {
                                                cg = 204
                                            }
                                        }
                                    }
                                }
                            } catch (cc) {
                                if (!cd) {
                                    b4(-1, cc)
                                }
                            }
                            if (cf) {
                                b4(cg, ch, cf, ce)
                            }
                        };
                        if (!b3.async) {
                            e()
                        } else {
                            if (b9.readyState === 4) {
                                setTimeout(e)
                            } else {
                                b6 = ++b0;
                                if (b1) {
                                    if (!bZ) {
                                        bZ = {};
                                        ay(bW).unload(b1)
                                    }
                                    bZ[b6] = e
                                }
                                b9.onreadystatechange = e
                            }
                        }
                    },
                    abort: function() {
                        if (e) {
                            e(bU, true)
                        }
                    }
                }
            }
        })
    }
    var ag, bQ, a1 = /^(?:toggle|show|hide)$/,
        a0 = new RegExp("^(?:([+-])=|)(" + A + ")([a-z%]*)$", "i"),
        bs = /queueHooks$/,
        p = [V],
        bT = {
            "*": [
                function(b5, cb) {
                    var e, ca, b9 = this.createTween(b5, cb),
                        b4 = a0.exec(cb),
                        b8 = b9.cur(),
                        b7 = +b8 || 0,
                        b6 = 1,
                        b3 = 20;
                    if (b4) {
                        e = +b4[2];
                        ca = b4[3] || (ay.cssNumber[b5] ? "" : "px");
                        if (ca !== "px" && b7) {
                            b7 = ay.css(b9.elem, b5, true) || e || 1;
                            do {
                                b6 = b6 || ".5";
                                b7 = b7 / b6;
                                ay.style(b9.elem, b5, b7 + ca)
                            } while (b6 !== (b6 = b9.cur() / b8) && b6 !== 1 && --b3)
                        }
                        b9.unit = ca;
                        b9.start = b7;
                        b9.end = b4[1] ? b7 + (b4[1] + 1) * e : e
                    }
                    return b9
                }
            ]
        };

    function J() {
        setTimeout(function() {
            ag = bU
        });
        return (ag = ay.now())
    }

    function N(e, b3) {
        ay.each(b3, function(b7, b8) {
            var b4 = (bT[b7] || []).concat(bT["*"]),
                b5 = 0,
                b6 = b4.length;
            for (; b5 < b6; b5++) {
                if (b4[b5].call(e, b7, b8)) {
                    return
                }
            }
        })
    }

    function o(b4, b8, b7) {
        var ca, cb, b5 = 0,
            b6 = p.length,
            b3 = ay.Deferred().always(function() {
                delete cc.elem
            }),
            cc = function() {
                if (cb) {
                    return false
                }
                var cd = ag || J(),
                    ch = Math.max(0, e.startTime + e.duration - cd),
                    ci = ch / e.duration || 0,
                    cg = 1 - ci,
                    ce = 0,
                    cf = e.tweens.length;
                for (; ce < cf; ce++) {
                    e.tweens[ce].run(cg)
                }
                b3.notifyWith(b4, [e, cg, ch]);
                if (cg < 1 && cf) {
                    return ch
                } else {
                    b3.resolveWith(b4, [e]);
                    return false
                }
            }, e = b3.promise({
                elem: b4,
                props: ay.extend({}, b8),
                opts: ay.extend(true, {
                    specialEasing: {}
                }, b7),
                originalProperties: b8,
                originalOptions: b7,
                startTime: ag || J(),
                duration: b7.duration,
                tweens: [],
                createTween: function(ce, cd) {
                    var cf = ay.Tween(b4, e.opts, ce, cd, e.opts.specialEasing[ce] || e.opts.easing);
                    e.tweens.push(cf);
                    return cf
                },
                stop: function(cd) {
                    var ce = 0,
                        cf = cd ? e.tweens.length : 0;
                    if (cb) {
                        return this
                    }
                    cb = true;
                    for (; ce < cf; ce++) {
                        e.tweens[ce].run(1)
                    }
                    if (cd) {
                        b3.resolveWith(b4, [e, cd])
                    } else {
                        b3.rejectWith(b4, [e, cd])
                    }
                    return this
                }
            }),
            b9 = e.props;
        aG(b9, e.opts.specialEasing);
        for (; b5 < b6; b5++) {
            ca = p[b5].call(e, b4, b9, e.opts);
            if (ca) {
                return ca
            }
        }
        N(e, b9);
        if (ay.isFunction(e.opts.start)) {
            e.opts.start.call(b4, e)
        }
        ay.fx.timer(ay.extend(cc, {
            elem: b4,
            anim: e,
            queue: e.opts.queue
        }));
        return e.progress(e.opts.progress).done(e.opts.done, e.opts.complete).fail(e.opts.fail).always(e.opts.always)
    }

    function aG(b6, b7) {
        var b8, b5, b4, e, b3;
        for (b4 in b6) {
            b5 = ay.camelCase(b4);
            e = b7[b5];
            b8 = b6[b4];
            if (ay.isArray(b8)) {
                e = b8[1];
                b8 = b6[b4] = b8[0]
            }
            if (b4 !== b5) {
                b6[b5] = b8;
                delete b6[b4]
            }
            b3 = ay.cssHooks[b5];
            if (b3 && "expand" in b3) {
                b8 = b3.expand(b8);
                delete b6[b5];
                for (b4 in b8) {
                    if (!(b4 in b6)) {
                        b6[b4] = b8[b4];
                        b7[b4] = e
                    }
                }
            } else {
                b7[b5] = e
            }
        }
    }
    ay.Animation = ay.extend(o, {
        tweener: function(b6, e) {
            if (ay.isFunction(b6)) {
                e = b6;
                b6 = ["*"]
            } else {
                b6 = b6.split(" ")
            }
            var b5, b3 = 0,
                b4 = b6.length;
            for (; b3 < b4; b3++) {
                b5 = b6[b3];
                bT[b5] = bT[b5] || [];
                bT[b5].unshift(e)
            }
        },
        prefilter: function(e, b3) {
            if (b3) {
                p.unshift(e)
            } else {
                p.push(e)
            }
        }
    });

    function V(b4, ce, cb) {
        var cd, b8, b9, ci, b3, cg, ch, b7, ca, e = this,
            cf = b4.style,
            cc = {}, b5 = [],
            b6 = b4.nodeType && aw(b4);
        if (!cb.queue) {
            b7 = ay._queueHooks(b4, "fx");
            if (b7.unqueued == null) {
                b7.unqueued = 0;
                ca = b7.empty.fire;
                b7.empty.fire = function() {
                    if (!b7.unqueued) {
                        ca()
                    }
                }
            }
            b7.unqueued++;
            e.always(function() {
                e.always(function() {
                    b7.unqueued--;
                    if (!ay.queue(b4, "fx").length) {
                        b7.empty.fire()
                    }
                })
            })
        }
        if (b4.nodeType === 1 && ("height" in ce || "width" in ce)) {
            cb.overflow = [cf.overflow, cf.overflowX, cf.overflowY];
            if (ay.css(b4, "display") === "inline" && ay.css(b4, "float") === "none") {
                if (!ay.support.inlineBlockNeedsLayout || O(b4.nodeName) === "inline") {
                    cf.display = "inline-block"
                } else {
                    cf.zoom = 1
                }
            }
        }
        if (cb.overflow) {
            cf.overflow = "hidden";
            if (!ay.support.shrinkWrapBlocks) {
                e.always(function() {
                    cf.overflow = cb.overflow[0];
                    cf.overflowX = cb.overflow[1];
                    cf.overflowY = cb.overflow[2]
                })
            }
        }
        for (b8 in ce) {
            ci = ce[b8];
            if (a1.exec(ci)) {
                delete ce[b8];
                cg = cg || ci === "toggle";
                if (ci === (b6 ? "hide" : "show")) {
                    continue
                }
                b5.push(b8)
            }
        }
        b9 = b5.length;
        if (b9) {
            b3 = ay._data(b4, "fxshow") || ay._data(b4, "fxshow", {});
            if ("hidden" in b3) {
                b6 = b3.hidden
            }
            if (cg) {
                b3.hidden = !b6
            }
            if (b6) {
                ay(b4).show()
            } else {
                e.done(function() {
                    ay(b4).hide()
                })
            }
            e.done(function() {
                var cj;
                ay._removeData(b4, "fxshow");
                for (cj in cc) {
                    ay.style(b4, cj, cc[cj])
                }
            });
            for (b8 = 0; b8 < b9; b8++) {
                cd = b5[b8];
                ch = e.createTween(cd, b6 ? b3[cd] : 0);
                cc[cd] = b3[cd] || ay.style(b4, cd);
                if (!(cd in b3)) {
                    b3[cd] = ch.start;
                    if (b6) {
                        ch.end = ch.start;
                        ch.start = cd === "width" || cd === "height" ? 1 : 0
                    }
                }
            }
        }
    }

    function bS(b3, b5, b6, b4, e) {
        return new bS.prototype.init(b3, b5, b6, b4, e)
    }
    ay.Tween = bS;
    bS.prototype = {
        constructor: bS,
        init: function(b3, b5, b6, b4, e, b7) {
            this.elem = b3;
            this.prop = b6;
            this.easing = e || "swing";
            this.options = b5;
            this.start = this.now = this.cur();
            this.end = b4;
            this.unit = b7 || (ay.cssNumber[b6] ? "" : "px")
        },
        cur: function() {
            var e = bS.propHooks[this.prop];
            return e && e.get ? e.get(this) : bS.propHooks._default.get(this)
        },
        run: function(b4) {
            var e, b3 = bS.propHooks[this.prop];
            if (this.options.duration) {
                this.pos = e = ay.easing[this.easing](b4, this.options.duration * b4, 0, 1, this.options.duration)
            } else {
                this.pos = e = b4
            }
            this.now = (this.end - this.start) * e + this.start;
            if (this.options.step) {
                this.options.step.call(this.elem, this.now, this)
            }
            if (b3 && b3.set) {
                b3.set(this)
            } else {
                bS.propHooks._default.set(this)
            }
            return this
        }
    };
    bS.prototype.init.prototype = bS.prototype;
    bS.propHooks = {
        _default: {
            get: function(b3) {
                var e;
                if (b3.elem[b3.prop] != null && (!b3.elem.style || b3.elem.style[b3.prop] == null)) {
                    return b3.elem[b3.prop]
                }
                e = ay.css(b3.elem, b3.prop, "");
                return !e || e === "auto" ? 0 : e
            },
            set: function(e) {
                if (ay.fx.step[e.prop]) {
                    ay.fx.step[e.prop](e)
                } else {
                    if (e.elem.style && (e.elem.style[ay.cssProps[e.prop]] != null || ay.cssHooks[e.prop])) {
                        ay.style(e.elem, e.prop, e.now + e.unit)
                    } else {
                        e.elem[e.prop] = e.now
                    }
                }
            }
        }
    };
    bS.propHooks.scrollTop = bS.propHooks.scrollLeft = {
        set: function(e) {
            if (e.elem.nodeType && e.elem.parentNode) {
                e.elem[e.prop] = e.now
            }
        }
    };
    ay.each(["toggle", "show", "hide"], function(b3, b4) {
        var e = ay.fn[b4];
        ay.fn[b4] = function(b7, b6, b5) {
            return b7 == null || typeof b7 === "boolean" ? e.apply(this, arguments) : this.animate(ah(b4, true), b7, b6, b5)
        }
    });
    ay.fn.extend({
        fadeTo: function(b4, b5, b3, e) {
            return this.filter(aw).css("opacity", 0).show().end().animate({
                opacity: b5
            }, b4, b3, e)
        },
        animate: function(b7, b8, b4, e) {
            var b5 = ay.isEmptyObject(b7),
                b6 = ay.speed(b8, b4, e),
                b3 = function() {
                    var b9 = o(this, ay.extend({}, b7), b6);
                    b3.finish = function() {
                        b9.stop(true)
                    };
                    if (b5 || ay._data(this, "finish")) {
                        b9.stop(true)
                    }
                };
            b3.finish = b3;
            return b5 || b6.queue === false ? this.each(b3) : this.queue(b6.queue, b3)
        },
        stop: function(b5, e, b3) {
            var b4 = function(b6) {
                var b7 = b6.stop;
                delete b6.stop;
                b7(b3)
            };
            if (typeof b5 !== "string") {
                b3 = e;
                e = b5;
                b5 = bU
            }
            if (e && b5 !== false) {
                this.queue(b5 || "fx", [])
            }
            return this.each(function() {
                var b7 = true,
                    b8 = b5 != null && b5 + "queueHooks",
                    b9 = ay.timers,
                    b6 = ay._data(this);
                if (b8) {
                    if (b6[b8] && b6[b8].stop) {
                        b4(b6[b8])
                    }
                } else {
                    for (b8 in b6) {
                        if (b6[b8] && b6[b8].stop && bs.test(b8)) {
                            b4(b6[b8])
                        }
                    }
                }
                for (b8 = b9.length; b8--;) {
                    if (b9[b8].elem === this && (b5 == null || b9[b8].queue === b5)) {
                        b9[b8].anim.stop(b3);
                        b7 = false;
                        b9.splice(b8, 1)
                    }
                }
                if (b7 || !b3) {
                    ay.dequeue(this, b5)
                }
            })
        },
        finish: function(e) {
            if (e !== false) {
                e = e || "fx"
            }
            return this.each(function() {
                var b5, b3 = ay._data(this),
                    b7 = b3[e + "queue"],
                    b4 = b3[e + "queueHooks"],
                    b8 = ay.timers,
                    b6 = b7 ? b7.length : 0;
                b3.finish = true;
                ay.queue(this, e, []);
                if (b4 && b4.cur && b4.cur.finish) {
                    b4.cur.finish.call(this)
                }
                for (b5 = b8.length; b5--;) {
                    if (b8[b5].elem === this && b8[b5].queue === e) {
                        b8[b5].anim.stop(true);
                        b8.splice(b5, 1)
                    }
                }
                for (b5 = 0; b5 < b6; b5++) {
                    if (b7[b5] && b7[b5].finish) {
                        b7[b5].finish.call(this)
                    }
                }
                delete b3.finish
            })
        }
    });

    function ah(b5, b4) {
        var b6, e = {
            height: b5
        }, b3 = 0;
        b4 = b4 ? 1 : 0;
        for (; b3 < 4; b3 += 2 - b4) {
            b6 = P[b3];
            e["margin" + b6] = e["padding" + b6] = b5
        }
        if (b4) {
            e.opacity = e.width = b5
        }
        return e
    }
    ay.each({
        slideDown: ah("show"),
        slideUp: ah("hide"),
        slideToggle: ah("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(e, b3) {
        ay.fn[e] = function(b6, b5, b4) {
            return this.animate(b3, b6, b5, b4)
        }
    });
    ay.speed = function(b5, e, b3) {
        var b4 = b5 && typeof b5 === "object" ? ay.extend({}, b5) : {
            complete: b3 || !b3 && e || ay.isFunction(b5) && b5,
            duration: b5,
            easing: b3 && e || e && !ay.isFunction(e) && e
        };
        b4.duration = ay.fx.off ? 0 : typeof b4.duration === "number" ? b4.duration : b4.duration in ay.fx.speeds ? ay.fx.speeds[b4.duration] : ay.fx.speeds._default;
        if (b4.queue == null || b4.queue === true) {
            b4.queue = "fx"
        }
        b4.old = b4.complete;
        b4.complete = function() {
            if (ay.isFunction(b4.old)) {
                b4.old.call(this)
            }
            if (b4.queue) {
                ay.dequeue(this, b4.queue)
            }
        };
        return b4
    };
    ay.easing = {
        linear: function(e) {
            return e
        },
        swing: function(e) {
            return 0.5 - Math.cos(e * Math.PI) / 2
        }
    };
    ay.timers = [];
    ay.fx = bS.prototype.init;
    ay.fx.tick = function() {
        var b3, b4 = ay.timers,
            e = 0;
        ag = ay.now();
        for (; e < b4.length; e++) {
            b3 = b4[e];
            if (!b3() && b4[e] === b3) {
                b4.splice(e--, 1)
            }
        }
        if (!b4.length) {
            ay.fx.stop()
        }
        ag = bU
    };
    ay.fx.timer = function(e) {
        if (e() && ay.timers.push(e)) {
            ay.fx.start()
        }
    };
    ay.fx.interval = 13;
    ay.fx.start = function() {
        if (!bQ) {
            bQ = setInterval(ay.fx.tick, ay.fx.interval)
        }
    };
    ay.fx.stop = function() {
        clearInterval(bQ);
        bQ = null
    };
    ay.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    };
    ay.fx.step = {};
    if (ay.expr && ay.expr.filters) {
        ay.expr.filters.animated = function(e) {
            return ay.grep(ay.timers, function(b3) {
                return e === b3.elem
            }).length
        }
    }
    ay.fn.offset = function(b6) {
        if (arguments.length) {
            return b6 === bU ? this : this.each(function(b8) {
                ay.offset.setOffset(this, b6, b8)
            })
        }
        var b4, b7, e = {
                top: 0,
                left: 0
            }, b5 = this[0],
            b3 = b5 && b5.ownerDocument;
        if (!b3) {
            return
        }
        b4 = b3.documentElement;
        if (!ay.contains(b4, b5)) {
            return e
        }
        if (typeof b5.getBoundingClientRect !== E) {
            e = b5.getBoundingClientRect()
        }
        b7 = an(b3);
        return {
            top: e.top + (b7.pageYOffset || b4.scrollTop) - (b4.clientTop || 0),
            left: e.left + (b7.pageXOffset || b4.scrollLeft) - (b4.clientLeft || 0)
        }
    };
    ay.offset = {
        setOffset: function(ca, cc, cb) {
            var cd = ay.css(ca, "position");
            if (cd === "static") {
                ca.style.position = "relative"
            }
            var b5 = ay(ca),
                b7 = b5.offset(),
                b4 = ay.css(ca, "top"),
                b3 = ay.css(ca, "left"),
                e = (cd === "absolute" || cd === "fixed") && ay.inArray("auto", [b4, b3]) > -1,
                ce = {}, b8 = {}, b9, b6;
            if (e) {
                b8 = b5.position();
                b9 = b8.top;
                b6 = b8.left
            } else {
                b9 = parseFloat(b4) || 0;
                b6 = parseFloat(b3) || 0
            } if (ay.isFunction(cc)) {
                cc = cc.call(ca, cb, b7)
            }
            if (cc.top != null) {
                ce.top = (cc.top - b7.top) + b9
            }
            if (cc.left != null) {
                ce.left = (cc.left - b7.left) + b6
            }
            if ("using" in cc) {
                cc.using.call(ca, ce)
            } else {
                b5.css(ce)
            }
        }
    };
    ay.fn.extend({
        position: function() {
            if (!this[0]) {
                return
            }
            var b4, b3, b5 = {
                top: 0,
                left: 0
            }, e = this[0];
            if (ay.css(e, "position") === "fixed") {
                b3 = e.getBoundingClientRect()
            } else {
                b4 = this.offsetParent();
                b3 = this.offset();
                if (!ay.nodeName(b4[0], "html")) {
                    b5 = b4.offset()
                }
                b5.top += ay.css(b4[0], "borderTopWidth", true);
                b5.left += ay.css(b4[0], "borderLeftWidth", true)
            }
            return {
                top: b3.top - b5.top - ay.css(e, "marginTop", true),
                left: b3.left - b5.left - ay.css(e, "marginLeft", true)
            }
        },
        offsetParent: function() {
            return this.map(function() {
                var e = this.offsetParent || Y.documentElement;
                while (e && (!ay.nodeName(e, "html") && ay.css(e, "position") === "static")) {
                    e = e.offsetParent
                }
                return e || Y.documentElement
            })
        }
    });
    ay.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(e, b3) {
        var b4 = /Y/.test(b3);
        ay.fn[e] = function(b5) {
            return ay.access(this, function(b6, b7, b8) {
                var b9 = an(b6);
                if (b8 === bU) {
                    return b9 ? (b3 in b9) ? b9[b3] : b9.document.documentElement[b7] : b6[b7]
                }
                if (b9) {
                    b9.scrollTo(!b4 ? b8 : ay(b9).scrollLeft(), b4 ? b8 : ay(b9).scrollTop())
                } else {
                    b6[b7] = b8
                }
            }, e, b5, arguments.length, null)
        }
    });

    function an(e) {
        return ay.isWindow(e) ? e : e.nodeType === 9 ? e.defaultView || e.parentWindow : false
    }
    ay.each({
        Height: "height",
        Width: "width"
    }, function(e, b3) {
        ay.each({
            padding: "inner" + e,
            content: b3,
            "": "outer" + e
        }, function(b4, b5) {
            ay.fn[b5] = function(b8, b9) {
                var b6 = arguments.length && (b4 || typeof b8 !== "boolean"),
                    b7 = b4 || (b8 === true || b9 === true ? "margin" : "border");
                return ay.access(this, function(cb, cc, cd) {
                    var ca;
                    if (ay.isWindow(cb)) {
                        return cb.document.documentElement["client" + e]
                    }
                    if (cb.nodeType === 9) {
                        ca = cb.documentElement;
                        return Math.max(cb.body["scroll" + e], ca["scroll" + e], cb.body["offset" + e], ca["offset" + e], ca["client" + e])
                    }
                    return cd === bU ? ay.css(cb, cc, b7) : ay.style(cb, cc, cd, b7)
                }, b3, b6 ? b8 : bU, b6, null)
            }
        })
    });
    bW.jQuery = bW.$ = ay;
    if (typeof define === "function" && define.amd && define.amd.jQuery) {
        define("jquery", [], function() {
            return ay
        })
    }
})(window);
(function(a) {
    if (!a.support.cors && a.ajaxTransport && window.XDomainRequest) {
        var d = /^https?:\/\//i;
        var b = /^get|post$/i;
        var f = new RegExp("^" + location.protocol, "i");
        var c = /text\/html/i;
        var e = /\/json/i;
        var g = /\/xml/i;
        a.ajaxTransport("* text html xml json", function(i, j, h) {
            if (i.crossDomain && i.async && b.test(i.type) && d.test(i.url) && f.test(i.url)) {
                var l = null;
                var k = (j.dataType || "").toLowerCase();
                return {
                    send: function(n, m) {
                        l = new XDomainRequest();
                        if (/^\d+$/.test(j.timeout)) {
                            l.timeout = j.timeout
                        }
                        l.ontimeout = function() {
                            m(500, "timeout")
                        };
                        l.onload = function() {
                            var p = "Content-Length: " + l.responseText.length + "\r\nContent-Type: " + l.contentType;
                            var u = {
                                code: 200,
                                message: "success"
                            };
                            var t = {
                                text: l.responseText
                            };
                            try {
                                if (k === "html" || c.test(l.contentType)) {
                                    t.html = l.responseText
                                } else {
                                    if (k === "json" || (k !== "text" && e.test(l.contentType))) {
                                        try {
                                            t.json = a.parseJSON(l.responseText)
                                        } catch (r) {
                                            u.code = 500;
                                            u.message = "parseerror"
                                        }
                                    } else {
                                        if (k === "xml" || (k !== "text" && g.test(l.contentType))) {
                                            var q = new ActiveXObject("Microsoft.XMLDOM");
                                            q.async = false;
                                            try {
                                                q.loadXML(l.responseText)
                                            } catch (r) {
                                                q = undefined
                                            }
                                            if (!q || !q.documentElement || q.getElementsByTagName("parsererror").length) {
                                                u.code = 500;
                                                u.message = "parseerror";
                                                throw "Invalid XML: " + l.responseText
                                            }
                                            t.xml = q
                                        }
                                    }
                                }
                            } catch (s) {
                                throw s
                            } finally {
                                m(u.code, u.message, t, p)
                            }
                        };
                        l.onprogress = function() {};
                        l.onerror = function() {
                            m(500, "error", {
                                text: l.responseText
                            })
                        };
                        var o = "";
                        if (j.data) {
                            o = (a.type(j.data) === "string") ? j.data : a.param(j.data)
                        }
                        l.open(i.type, i.url);
                        l.send(o)
                    },
                    abort: function() {
                        if (l) {
                            l.abort()
                        }
                    }
                }
            }
        })
    }
})(jQuery);
(function() {
    (function(p) {
        var A = this || (0, eval)("this"),
            w = A.document,
            K = A.navigator,
            t = A.jQuery,
            C = A.JSON;
        (function(p) {
            "function" === typeof require && "object" === typeof exports && "object" === typeof module ? p(module.exports || exports) : "function" === typeof define && define.amd ? define(["exports"], p) : p(A.ko = {})
        })(function(z) {
            function G(a, c) {
                return null === a || typeof a in M ? a === c : !1
            }

            function N(a, c) {
                var d;
                return function() {
                    d || (d = setTimeout(function() {
                        d = p;
                        a()
                    }, c))
                }
            }

            function O(a, c) {
                var d;
                return function() {
                    clearTimeout(d);
                    d = setTimeout(a, c)
                }
            }

            function H(b, c, d, e) {
                a.d[b] = {
                    init: function(b, h, g, k, l) {
                        var n, r;
                        a.ba(function() {
                            var g = a.a.c(h()),
                                k = !d !== !g,
                                s = !r;
                            if (s || c || k !== n) {
                                s && a.ca.fa() && (r = a.a.lb(a.e.childNodes(b), !0)), k ? (s || a.e.U(b, a.a.lb(r)), a.gb(e ? e(l, g) : l, b)) : a.e.da(b), n = k
                            }
                        }, null, {
                            G: b
                        });
                        return {
                            controlsDescendantBindings: !0
                        }
                    }
                };
                a.g.aa[b] = !1;
                a.e.Q[b] = !0
            }
            var a = "undefined" !== typeof z ? z : {};
            a.b = function(b, c) {
                for (var d = b.split("."), e = a, f = 0; f < d.length - 1; f++) {
                    e = e[d[f]]
                }
                e[d[d.length - 1]] = c
            };
            a.s = function(a, c, d) {
                a[c] = d
            };
            a.version = "3.1.0";
            a.b("version", a.version);
            a.a = function() {
                function b(a, b) {
                    for (var c in a) {
                        a.hasOwnProperty(c) && b(c, a[c])
                    }
                }

                function c(a, b) {
                    if (b) {
                        for (var c in b) {
                            b.hasOwnProperty(c) && (a[c] = b[c])
                        }
                    }
                    return a
                }

                function d(a, b) {
                    a.__proto__ = b;
                    return a
                }
                var e = {
                        __proto__: []
                    }
                    instanceof Array, f = {}, h = {};
                f[K && /Firefox\/2/i.test(K.userAgent) ? "KeyboardEvent" : "UIEvents"] = ["keyup", "keydown", "keypress"];
                f.MouseEvents = "click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave".split(" ");
                b(f, function(a, b) {
                    if (b.length) {
                        for (var c = 0, d = b.length; c < d; c++) {
                            h[b[c]] = a
                        }
                    }
                });
                var g = {
                    propertychange: !0
                }, k = w && function() {
                        for (var a = 3, b = w.createElement("div"), c = b.getElementsByTagName("i"); b.innerHTML = "\x3c!--[if gt IE " + ++a + "]><i></i><![endif]--\x3e", c[0];) {}
                        return 4 < a ? a : p
                    }();
                return {
                    mb: ["authenticity_token", /^__RequestVerificationToken(_.*)?$/],
                    r: function(a, b) {
                        for (var c = 0, d = a.length; c < d; c++) {
                            b(a[c], c)
                        }
                    },
                    l: function(a, b) {
                        if ("function" == typeof Array.prototype.indexOf) {
                            return Array.prototype.indexOf.call(a, b)
                        }
                        for (var c = 0, d = a.length; c < d; c++) {
                            if (a[c] === b) {
                                return c
                            }
                        }
                        return -1
                    },
                    hb: function(a, b, c) {
                        for (var d = 0, e = a.length; d < e; d++) {
                            if (b.call(c, a[d], d)) {
                                return a[d]
                            }
                        }
                        return null
                    },
                    ma: function(b, c) {
                        var d = a.a.l(b, c);
                        0 < d ? b.splice(d, 1) : 0 === d && b.shift()
                    },
                    ib: function(b) {
                        b = b || [];
                        for (var c = [], d = 0, e = b.length; d < e; d++) {
                            0 > a.a.l(c, b[d]) && c.push(b[d])
                        }
                        return c
                    },
                    ya: function(a, b) {
                        a = a || [];
                        for (var c = [], d = 0, e = a.length; d < e; d++) {
                            c.push(b(a[d], d))
                        }
                        return c
                    },
                    la: function(a, b) {
                        a = a || [];
                        for (var c = [], d = 0, e = a.length; d < e; d++) {
                            b(a[d], d) && c.push(a[d])
                        }
                        return c
                    },
                    $: function(a, b) {
                        if (b instanceof Array) {
                            a.push.apply(a, b)
                        } else {
                            for (var c = 0, d = b.length; c < d; c++) {
                                a.push(b[c])
                            }
                        }
                        return a
                    },
                    Y: function(b, c, d) {
                        var e = a.a.l(a.a.Sa(b), c);
                        0 > e ? d && b.push(c) : d || b.splice(e, 1)
                    },
                    na: e,
                    extend: c,
                    ra: d,
                    sa: e ? d : c,
                    A: b,
                    Oa: function(a, b) {
                        if (!a) {
                            return a
                        }
                        var c = {}, d;
                        for (d in a) {
                            a.hasOwnProperty(d) && (c[d] = b(a[d], d, a))
                        }
                        return c
                    },
                    Fa: function(b) {
                        for (; b.firstChild;) {
                            a.removeNode(b.firstChild)
                        }
                    },
                    ec: function(b) {
                        b = a.a.R(b);
                        for (var c = w.createElement("div"), d = 0, e = b.length; d < e; d++) {
                            c.appendChild(a.M(b[d]))
                        }
                        return c
                    },
                    lb: function(b, c) {
                        for (var d = 0, e = b.length, g = []; d < e; d++) {
                            var k = b[d].cloneNode(!0);
                            g.push(c ? a.M(k) : k)
                        }
                        return g
                    },
                    U: function(b, c) {
                        a.a.Fa(b);
                        if (c) {
                            for (var d = 0, e = c.length; d < e; d++) {
                                b.appendChild(c[d])
                            }
                        }
                    },
                    Bb: function(b, c) {
                        var d = b.nodeType ? [b] : b;
                        if (0 < d.length) {
                            for (var e = d[0], g = e.parentNode, k = 0, h = c.length; k < h; k++) {
                                g.insertBefore(c[k], e)
                            }
                            k = 0;
                            for (h = d.length; k < h; k++) {
                                a.removeNode(d[k])
                            }
                        }
                    },
                    ea: function(a, b) {
                        if (a.length) {
                            for (b = 8 === b.nodeType && b.parentNode || b; a.length && a[0].parentNode !== b;) {
                                a.shift()
                            }
                            if (1 < a.length) {
                                var c = a[0],
                                    d = a[a.length - 1];
                                for (a.length = 0; c !== d;) {
                                    if (a.push(c), c = c.nextSibling, !c) {
                                        return
                                    }
                                }
                                a.push(d)
                            }
                        }
                        return a
                    },
                    Db: function(a, b) {
                        7 > k ? a.setAttribute("selected", b) : a.selected = b
                    },
                    ta: function(a) {
                        return null === a || a === p ? "" : a.trim ? a.trim() : a.toString().replace(/^[\s\xa0]+|[\s\xa0]+$/g, "")
                    },
                    oc: function(b, c) {
                        for (var d = [], e = (b || "").split(c), g = 0, k = e.length; g < k; g++) {
                            var h = a.a.ta(e[g]);
                            "" !== h && d.push(h)
                        }
                        return d
                    },
                    kc: function(a, b) {
                        a = a || "";
                        return b.length > a.length ? !1 : a.substring(0, b.length) === b
                    },
                    Sb: function(a, b) {
                        if (a === b) {
                            return !0
                        }
                        if (11 === a.nodeType) {
                            return !1
                        }
                        if (b.contains) {
                            return b.contains(3 === a.nodeType ? a.parentNode : a)
                        }
                        if (b.compareDocumentPosition) {
                            return 16 == (b.compareDocumentPosition(a) & 16)
                        }
                        for (; a && a != b;) {
                            a = a.parentNode
                        }
                        return !!a
                    },
                    Ea: function(b) {
                        return a.a.Sb(b, b.ownerDocument.documentElement)
                    },
                    eb: function(b) {
                        return !!a.a.hb(b, a.a.Ea)
                    },
                    B: function(a) {
                        return a && a.tagName && a.tagName.toLowerCase()
                    },
                    q: function(b, c, d) {
                        var e = k && g[c];
                        if (!e && t) {
                            t(b).bind(c, d)
                        } else {
                            if (e || "function" != typeof b.addEventListener) {
                                if ("undefined" != typeof b.attachEvent) {
                                    var h = function(a) {
                                        d.call(b, a)
                                    }, f = "on" + c;
                                    b.attachEvent(f, h);
                                    a.a.u.ja(b, function() {
                                        b.detachEvent(f, h)
                                    })
                                } else {
                                    throw Error("Browser doesn't support addEventListener or attachEvent")
                                }
                            } else {
                                b.addEventListener(c, d, !1)
                            }
                        }
                    },
                    ha: function(b, c) {
                        if (!b || !b.nodeType) {
                            throw Error("element must be a DOM node when calling triggerEvent")
                        }
                        var d;
                        "input" === a.a.B(b) && b.type && "click" == c.toLowerCase() ? (d = b.type, d = "checkbox" == d || "radio" == d) : d = !1;
                        if (t && !d) {
                            t(b).trigger(c)
                        } else {
                            if ("function" == typeof w.createEvent) {
                                if ("function" == typeof b.dispatchEvent) {
                                    d = w.createEvent(h[c] || "HTMLEvents"), d.initEvent(c, !0, !0, A, 0, 0, 0, 0, 0, !1, !1, !1, !1, 0, b), b.dispatchEvent(d)
                                } else {
                                    throw Error("The supplied element doesn't support dispatchEvent")
                                }
                            } else {
                                if (d && b.click) {
                                    b.click()
                                } else {
                                    if ("undefined" != typeof b.fireEvent) {
                                        b.fireEvent("on" + c)
                                    } else {
                                        throw Error("Browser doesn't support triggering events")
                                    }
                                }
                            }
                        }
                    },
                    c: function(b) {
                        return a.v(b) ? b() : b
                    },
                    Sa: function(b) {
                        return a.v(b) ? b.o() : b
                    },
                    ua: function(b, c, d) {
                        if (c) {
                            var e = /\S+/g,
                                g = b.className.match(e) || [];
                            a.a.r(c.match(e), function(b) {
                                a.a.Y(g, b, d)
                            });
                            b.className = g.join(" ")
                        }
                    },
                    Xa: function(b, c) {
                        var d = a.a.c(c);
                        if (null === d || d === p) {
                            d = ""
                        }
                        var e = a.e.firstChild(b);
                        !e || 3 != e.nodeType || a.e.nextSibling(e) ? a.e.U(b, [b.ownerDocument.createTextNode(d)]) : e.data = d;
                        a.a.Vb(b)
                    },
                    Cb: function(a, b) {
                        a.name = b;
                        if (7 >= k) {
                            try {
                                a.mergeAttributes(w.createElement("<input name='" + a.name + "'/>"), !1)
                            } catch (c) {}
                        }
                    },
                    Vb: function(a) {
                        9 <= k && (a = 1 == a.nodeType ? a : a.parentNode, a.style && (a.style.zoom = a.style.zoom))
                    },
                    Tb: function(a) {
                        if (k) {
                            var b = a.style.width;
                            a.style.width = 0;
                            a.style.width = b
                        }
                    },
                    ic: function(b, c) {
                        b = a.a.c(b);
                        c = a.a.c(c);
                        for (var d = [], e = b; e <= c; e++) {
                            d.push(e)
                        }
                        return d
                    },
                    R: function(a) {
                        for (var b = [], c = 0, d = a.length; c < d; c++) {
                            b.push(a[c])
                        }
                        return b
                    },
                    mc: 6 === k,
                    nc: 7 === k,
                    oa: k,
                    ob: function(b, c) {
                        for (var d = a.a.R(b.getElementsByTagName("input")).concat(a.a.R(b.getElementsByTagName("textarea"))), e = "string" == typeof c ? function(a) {
                            return a.name === c
                        } : function(a) {
                            return c.test(a.name)
                        }, g = [], k = d.length - 1; 0 <= k; k--) {
                            e(d[k]) && g.push(d[k])
                        }
                        return g
                    },
                    fc: function(b) {
                        return "string" == typeof b && (b = a.a.ta(b)) ? C && C.parse ? C.parse(b) : (new Function("return " + b))() : null
                    },
                    Ya: function(b, c, d) {
                        if (!C || !C.stringify) {
                            throw Error("Cannot find JSON.stringify(). Some browsers (e.g., IE < 8) don't support it natively, but you can overcome this by adding a script reference to json2.js, downloadable from http://www.json.org/json2.js")
                        }
                        return C.stringify(a.a.c(b), c, d)
                    },
                    gc: function(c, d, e) {
                        e = e || {};
                        var g = e.params || {}, k = e.includeFields || this.mb,
                            h = c;
                        if ("object" == typeof c && "form" === a.a.B(c)) {
                            for (var h = c.action, f = k.length - 1; 0 <= f; f--) {
                                for (var u = a.a.ob(c, k[f]), D = u.length - 1; 0 <= D; D--) {
                                    g[u[D].name] = u[D].value
                                }
                            }
                        }
                        d = a.a.c(d);
                        var y = w.createElement("form");
                        y.style.display = "none";
                        y.action = h;
                        y.method = "post";
                        for (var p in d) {
                            c = w.createElement("input"), c.name = p, c.value = a.a.Ya(a.a.c(d[p])), y.appendChild(c)
                        }
                        b(g, function(a, b) {
                            var c = w.createElement("input");
                            c.name = a;
                            c.value = b;
                            y.appendChild(c)
                        });
                        w.body.appendChild(y);
                        e.submitter ? e.submitter(y) : y.submit();
                        setTimeout(function() {
                            y.parentNode.removeChild(y)
                        }, 0)
                    }
                }
            }();
            a.b("utils", a.a);
            a.b("utils.arrayForEach", a.a.r);
            a.b("utils.arrayFirst", a.a.hb);
            a.b("utils.arrayFilter", a.a.la);
            a.b("utils.arrayGetDistinctValues", a.a.ib);
            a.b("utils.arrayIndexOf", a.a.l);
            a.b("utils.arrayMap", a.a.ya);
            a.b("utils.arrayPushAll", a.a.$);
            a.b("utils.arrayRemoveItem", a.a.ma);
            a.b("utils.extend", a.a.extend);
            a.b("utils.fieldsIncludedWithJsonPost", a.a.mb);
            a.b("utils.getFormFields", a.a.ob);
            a.b("utils.peekObservable", a.a.Sa);
            a.b("utils.postJson", a.a.gc);
            a.b("utils.parseJson", a.a.fc);
            a.b("utils.registerEventHandler", a.a.q);
            a.b("utils.stringifyJson", a.a.Ya);
            a.b("utils.range", a.a.ic);
            a.b("utils.toggleDomNodeCssClass", a.a.ua);
            a.b("utils.triggerEvent", a.a.ha);
            a.b("utils.unwrapObservable", a.a.c);
            a.b("utils.objectForEach", a.a.A);
            a.b("utils.addOrRemoveItem", a.a.Y);
            a.b("unwrap", a.a.c);
            Function.prototype.bind || (Function.prototype.bind = function(a) {
                var c = this,
                    d = Array.prototype.slice.call(arguments);
                a = d.shift();
                return function() {
                    return c.apply(a, d.concat(Array.prototype.slice.call(arguments)))
                }
            });
            a.a.f = new function() {
                function a(b, h) {
                    var g = b[d];
                    if (!g || "null" === g || !e[g]) {
                        if (!h) {
                            return p
                        }
                        g = b[d] = "ko" + c++;
                        e[g] = {}
                    }
                    return e[g]
                }
                var c = 0,
                    d = "__ko__" + (new Date).getTime(),
                    e = {};
                return {
                    get: function(c, d) {
                        var e = a(c, !1);
                        return e === p ? p : e[d]
                    },
                    set: function(c, d, e) {
                        if (e !== p || a(c, !1) !== p) {
                            a(c, !0)[d] = e
                        }
                    },
                    clear: function(a) {
                        var b = a[d];
                        return b ? (delete e[b], a[d] = null, !0) : !1
                    },
                    L: function() {
                        return c+++d
                    }
                }
            };
            a.b("utils.domData", a.a.f);
            a.b("utils.domData.clear", a.a.f.clear);
            a.a.u = new function() {
                function b(b, c) {
                    var e = a.a.f.get(b, d);
                    e === p && c && (e = [], a.a.f.set(b, d, e));
                    return e
                }

                function c(d) {
                    var e = b(d, !1);
                    if (e) {
                        for (var e = e.slice(0), k = 0; k < e.length; k++) {
                            e[k](d)
                        }
                    }
                    a.a.f.clear(d);
                    a.a.u.cleanExternalData(d);
                    if (f[d.nodeType]) {
                        for (e = d.firstChild; d = e;) {
                            e = d.nextSibling, 8 === d.nodeType && c(d)
                        }
                    }
                }
                var d = a.a.f.L(),
                    e = {
                        1: !0,
                        8: !0,
                        9: !0
                    }, f = {
                        1: !0,
                        9: !0
                    };
                return {
                    ja: function(a, c) {
                        if ("function" != typeof c) {
                            throw Error("Callback must be a function")
                        }
                        b(a, !0).push(c)
                    },
                    Ab: function(c, e) {
                        var k = b(c, !1);
                        k && (a.a.ma(k, e), 0 == k.length && a.a.f.set(c, d, p))
                    },
                    M: function(b) {
                        if (e[b.nodeType] && (c(b), f[b.nodeType])) {
                            var d = [];
                            a.a.$(d, b.getElementsByTagName("*"));
                            for (var k = 0, l = d.length; k < l; k++) {
                                c(d[k])
                            }
                        }
                        return b
                    },
                    removeNode: function(b) {
                        a.M(b);
                        b.parentNode && b.parentNode.removeChild(b)
                    },
                    cleanExternalData: function(a) {
                        t && "function" == typeof t.cleanData && t.cleanData([a])
                    }
                }
            };
            a.M = a.a.u.M;
            a.removeNode = a.a.u.removeNode;
            a.b("cleanNode", a.M);
            a.b("removeNode", a.removeNode);
            a.b("utils.domNodeDisposal", a.a.u);
            a.b("utils.domNodeDisposal.addDisposeCallback", a.a.u.ja);
            a.b("utils.domNodeDisposal.removeDisposeCallback", a.a.u.Ab);
            (function() {
                a.a.Qa = function(b) {
                    var c;
                    if (t) {
                        if (t.parseHTML) {
                            c = t.parseHTML(b) || []
                        } else {
                            if ((c = t.clean([b])) && c[0]) {
                                for (b = c[0]; b.parentNode && 11 !== b.parentNode.nodeType;) {
                                    b = b.parentNode
                                }
                                b.parentNode && b.parentNode.removeChild(b)
                            }
                        }
                    } else {
                        var d = a.a.ta(b).toLowerCase();
                        c = w.createElement("div");
                        d = d.match(/^<(thead|tbody|tfoot)/) && [1, "<table>", "</table>"] || !d.indexOf("<tr") && [2, "<table><tbody>", "</tbody></table>"] || (!d.indexOf("<td") || !d.indexOf("<th")) && [3, "<table><tbody><tr>", "</tr></tbody></table>"] || [0, "", ""];
                        b = "ignored<div>" + d[1] + b + d[2] + "</div>";
                        for ("function" == typeof A.innerShiv ? c.appendChild(A.innerShiv(b)) : c.innerHTML = b; d[0]--;) {
                            c = c.lastChild
                        }
                        c = a.a.R(c.lastChild.childNodes)
                    }
                    return c
                };
                a.a.Va = function(b, c) {
                    a.a.Fa(b);
                    c = a.a.c(c);
                    if (null !== c && c !== p) {
                        if ("string" != typeof c && (c = c.toString()), t) {
                            t(b).html(c)
                        } else {
                            for (var d = a.a.Qa(c), e = 0; e < d.length; e++) {
                                b.appendChild(d[e])
                            }
                        }
                    }
                }
            })();
            a.b("utils.parseHtmlFragment", a.a.Qa);
            a.b("utils.setHtml", a.a.Va);
            a.w = function() {
                function b(c, e) {
                    if (c) {
                        if (8 == c.nodeType) {
                            var f = a.w.xb(c.nodeValue);
                            null != f && e.push({
                                Rb: c,
                                cc: f
                            })
                        } else {
                            if (1 == c.nodeType) {
                                for (var f = 0, h = c.childNodes, g = h.length; f < g; f++) {
                                    b(h[f], e)
                                }
                            }
                        }
                    }
                }
                var c = {};
                return {
                    Na: function(a) {
                        if ("function" != typeof a) {
                            throw Error("You can only pass a function to ko.memoization.memoize()")
                        }
                        var b = (4294967296 * (1 + Math.random()) | 0).toString(16).substring(1) + (4294967296 * (1 + Math.random()) | 0).toString(16).substring(1);
                        c[b] = a;
                        return "\x3c!--[ko_memo:" + b + "]--\x3e"
                    },
                    Hb: function(a, b) {
                        var f = c[a];
                        if (f === p) {
                            throw Error("Couldn't find any memo with ID " + a + ". Perhaps it's already been unmemoized.")
                        }
                        try {
                            return f.apply(null, b || []), !0
                        } finally {
                            delete c[a]
                        }
                    },
                    Ib: function(c, e) {
                        var f = [];
                        b(c, f);
                        for (var h = 0, g = f.length; h < g; h++) {
                            var k = f[h].Rb,
                                l = [k];
                            e && a.a.$(l, e);
                            a.w.Hb(f[h].cc, l);
                            k.nodeValue = "";
                            k.parentNode && k.parentNode.removeChild(k)
                        }
                    },
                    xb: function(a) {
                        return (a = a.match(/^\[ko_memo\:(.*?)\]$/)) ? a[1] : null
                    }
                }
            }();
            a.b("memoization", a.w);
            a.b("memoization.memoize", a.w.Na);
            a.b("memoization.unmemoize", a.w.Hb);
            a.b("memoization.parseMemoText", a.w.xb);
            a.b("memoization.unmemoizeDomNodeAndDescendants", a.w.Ib);
            a.Ga = {
                throttle: function(b, c) {
                    b.throttleEvaluation = c;
                    var d = null;
                    return a.h({
                        read: b,
                        write: function(a) {
                            clearTimeout(d);
                            d = setTimeout(function() {
                                b(a)
                            }, c)
                        }
                    })
                },
                rateLimit: function(a, c) {
                    var d, e, f;
                    "number" == typeof c ? d = c : (d = c.timeout, e = c.method);
                    f = "notifyWhenChangesStop" == e ? O : N;
                    a.Ma(function(a) {
                        return f(a, d)
                    })
                },
                notify: function(a, c) {
                    a.equalityComparer = "always" == c ? null : G
                }
            };
            var M = {
                undefined: 1,
                "boolean": 1,
                number: 1,
                string: 1
            };
            a.b("extenders", a.Ga);
            a.Fb = function(b, c, d) {
                this.target = b;
                this.za = c;
                this.Qb = d;
                this.sb = !1;
                a.s(this, "dispose", this.F)
            };
            a.Fb.prototype.F = function() {
                this.sb = !0;
                this.Qb()
            };
            a.N = function() {
                a.a.sa(this, a.N.fn);
                this.H = {}
            };
            var F = "change";
            z = {
                V: function(b, c, d) {
                    var e = this;
                    d = d || F;
                    var f = new a.Fb(e, c ? b.bind(c) : b, function() {
                        a.a.ma(e.H[d], f)
                    });
                    e.o && e.o();
                    e.H[d] || (e.H[d] = []);
                    e.H[d].push(f);
                    return f
                },
                notifySubscribers: function(b, c) {
                    c = c || F;
                    if (this.qb(c)) {
                        try {
                            a.k.jb();
                            for (var d = this.H[c].slice(0), e = 0, f; f = d[e]; ++e) {
                                f.sb || f.za(b)
                            }
                        } finally {
                            a.k.end()
                        }
                    }
                },
                Ma: function(b) {
                    var c = this,
                        d = a.v(c),
                        e, f, h;
                    c.ia || (c.ia = c.notifySubscribers, c.notifySubscribers = function(a, b) {
                        b && b !== F ? "beforeChange" === b ? c.bb(a) : c.ia(a, b) : c.cb(a)
                    });
                    var g = b(function() {
                        d && h === c && (h = c());
                        e = !1;
                        c.Ka(f, h) && c.ia(f = h)
                    });
                    c.cb = function(a) {
                        e = !0;
                        h = a;
                        g()
                    };
                    c.bb = function(a) {
                        e || (f = a, c.ia(a, "beforeChange"))
                    }
                },
                qb: function(a) {
                    return this.H[a] && this.H[a].length
                },
                Wb: function() {
                    var b = 0;
                    a.a.A(this.H, function(a, d) {
                        b += d.length
                    });
                    return b
                },
                Ka: function(a, c) {
                    return !this.equalityComparer || !this.equalityComparer(a, c)
                },
                extend: function(b) {
                    var c = this;
                    b && a.a.A(b, function(b, e) {
                        var f = a.Ga[b];
                        "function" == typeof f && (c = f(c, e) || c)
                    });
                    return c
                }
            };
            a.s(z, "subscribe", z.V);
            a.s(z, "extend", z.extend);
            a.s(z, "getSubscriptionsCount", z.Wb);
            a.a.na && a.a.ra(z, Function.prototype);
            a.N.fn = z;
            a.tb = function(a) {
                return null != a && "function" == typeof a.V && "function" == typeof a.notifySubscribers
            };
            a.b("subscribable", a.N);
            a.b("isSubscribable", a.tb);
            a.ca = a.k = function() {
                function b(a) {
                    d.push(e);
                    e = a
                }

                function c() {
                    e = d.pop()
                }
                var d = [],
                    e, f = 0;
                return {
                    jb: b,
                    end: c,
                    zb: function(b) {
                        if (e) {
                            if (!a.tb(b)) {
                                throw Error("Only subscribable things can act as dependencies")
                            }
                            e.za(b, b.Kb || (b.Kb = ++f))
                        }
                    },
                    t: function(a, d, e) {
                        try {
                            return b(), a.apply(d, e || [])
                        } finally {
                            c()
                        }
                    },
                    fa: function() {
                        if (e) {
                            return e.ba.fa()
                        }
                    },
                    pa: function() {
                        if (e) {
                            return e.pa
                        }
                    }
                }
            }();
            a.b("computedContext", a.ca);
            a.b("computedContext.getDependenciesCount", a.ca.fa);
            a.b("computedContext.isInitial", a.ca.pa);
            a.m = function(b) {
                function c() {
                    if (0 < arguments.length) {
                        return c.Ka(d, arguments[0]) && (c.P(), d = arguments[0], c.O()), this
                    }
                    a.k.zb(c);
                    return d
                }
                var d = b;
                a.N.call(c);
                a.a.sa(c, a.m.fn);
                c.o = function() {
                    return d
                };
                c.O = function() {
                    c.notifySubscribers(d)
                };
                c.P = function() {
                    c.notifySubscribers(d, "beforeChange")
                };
                a.s(c, "peek", c.o);
                a.s(c, "valueHasMutated", c.O);
                a.s(c, "valueWillMutate", c.P);
                return c
            };
            a.m.fn = {
                equalityComparer: G
            };
            var E = a.m.hc = "__ko_proto__";
            a.m.fn[E] = a.m;
            a.a.na && a.a.ra(a.m.fn, a.N.fn);
            a.Ha = function(b, c) {
                return null === b || b === p || b[E] === p ? !1 : b[E] === c ? !0 : a.Ha(b[E], c)
            };
            a.v = function(b) {
                return a.Ha(b, a.m)
            };
            a.ub = function(b) {
                return "function" == typeof b && b[E] === a.m || "function" == typeof b && b[E] === a.h && b.Yb ? !0 : !1
            };
            a.b("observable", a.m);
            a.b("isObservable", a.v);
            a.b("isWriteableObservable", a.ub);
            a.T = function(b) {
                b = b || [];
                if ("object" != typeof b || !("length" in b)) {
                    throw Error("The argument passed when initializing an observable array must be an array, or null, or undefined.")
                }
                b = a.m(b);
                a.a.sa(b, a.T.fn);
                return b.extend({
                    trackArrayChanges: !0
                })
            };
            a.T.fn = {
                remove: function(b) {
                    for (var c = this.o(), d = [], e = "function" != typeof b || a.v(b) ? function(a) {
                        return a === b
                    } : b, f = 0; f < c.length; f++) {
                        var h = c[f];
                        e(h) && (0 === d.length && this.P(), d.push(h), c.splice(f, 1), f--)
                    }
                    d.length && this.O();
                    return d
                },
                removeAll: function(b) {
                    if (b === p) {
                        var c = this.o(),
                            d = c.slice(0);
                        this.P();
                        c.splice(0, c.length);
                        this.O();
                        return d
                    }
                    return b ? this.remove(function(c) {
                        return 0 <= a.a.l(b, c)
                    }) : []
                },
                destroy: function(b) {
                    var c = this.o(),
                        d = "function" != typeof b || a.v(b) ? function(a) {
                            return a === b
                        } : b;
                    this.P();
                    for (var e = c.length - 1; 0 <= e; e--) {
                        d(c[e]) && (c[e]._destroy = !0)
                    }
                    this.O()
                },
                destroyAll: function(b) {
                    return b === p ? this.destroy(function() {
                        return !0
                    }) : b ? this.destroy(function(c) {
                        return 0 <= a.a.l(b, c)
                    }) : []
                },
                indexOf: function(b) {
                    var c = this();
                    return a.a.l(c, b)
                },
                replace: function(a, c) {
                    var d = this.indexOf(a);
                    0 <= d && (this.P(), this.o()[d] = c, this.O())
                }
            };
            a.a.r("pop push reverse shift sort splice unshift".split(" "), function(b) {
                a.T.fn[b] = function() {
                    var a = this.o();
                    this.P();
                    this.kb(a, b, arguments);
                    a = a[b].apply(a, arguments);
                    this.O();
                    return a
                }
            });
            a.a.r(["slice"], function(b) {
                a.T.fn[b] = function() {
                    var a = this();
                    return a[b].apply(a, arguments)
                }
            });
            a.a.na && a.a.ra(a.T.fn, a.m.fn);
            a.b("observableArray", a.T);
            var I = "arrayChange";
            a.Ga.trackArrayChanges = function(b) {
                function c() {
                    if (!d) {
                        d = !0;
                        var c = b.notifySubscribers;
                        b.notifySubscribers = function(a, b) {
                            b && b !== F || ++f;
                            return c.apply(this, arguments)
                        };
                        var k = [].concat(b.o() || []);
                        e = null;
                        b.V(function(c) {
                            c = [].concat(c || []);
                            if (b.qb(I)) {
                                var d;
                                if (!e || 1 < f) {
                                    e = a.a.Aa(k, c, {
                                        sparse: !0
                                    })
                                }
                                d = e;
                                d.length && b.notifySubscribers(d, I)
                            }
                            k = c;
                            e = null;
                            f = 0
                        })
                    }
                }
                if (!b.kb) {
                    var d = !1,
                        e = null,
                        f = 0,
                        h = b.V;
                    b.V = b.subscribe = function(a, b, d) {
                        d === I && c();
                        return h.apply(this, arguments)
                    };
                    b.kb = function(b, c, l) {
                        function h(a, b, c) {
                            return r[r.length] = {
                                status: a,
                                value: b,
                                index: c
                            }
                        }
                        if (d && !f) {
                            var r = [],
                                m = b.length,
                                q = l.length,
                                s = 0;
                            switch (c) {
                                case "push":
                                    s = m;
                                case "unshift":
                                    for (c = 0; c < q; c++) {
                                        h("added", l[c], s + c)
                                    }
                                    break;
                                case "pop":
                                    s = m - 1;
                                case "shift":
                                    m && h("deleted", b[s], s);
                                    break;
                                case "splice":
                                    c = Math.min(Math.max(0, 0 > l[0] ? m + l[0] : l[0]), m);
                                    for (var m = 1 === q ? m : Math.min(c + (l[1] || 0), m), q = c + q - 2, s = Math.max(m, q), B = [], u = [], D = 2; c < s; ++c, ++D) {
                                        c < m && u.push(h("deleted", b[c], c)), c < q && B.push(h("added", l[D], c))
                                    }
                                    a.a.nb(u, B);
                                    break;
                                default:
                                    return
                            }
                            e = r
                        }
                    }
                }
            };
            a.ba = a.h = function(b, c, d) {
                function e() {
                    q = !0;
                    a.a.A(v, function(a, b) {
                        b.F()
                    });
                    v = {};
                    x = 0;
                    n = !1
                }

                function f() {
                    var a = g.throttleEvaluation;
                    a && 0 <= a ? (clearTimeout(t), t = setTimeout(h, a)) : g.wa ? g.wa() : h()
                }

                function h() {
                    if (!r && !q) {
                        if (y && y()) {
                            if (!m) {
                                p();
                                return
                            }
                        } else {
                            m = !1
                        }
                        r = !0;
                        try {
                            var b = v,
                                d = x;
                            a.k.jb({
                                za: function(a, c) {
                                    q || (d && b[c] ? (v[c] = b[c], ++x, delete b[c], --d) : v[c] || (v[c] = a.V(f), ++x))
                                },
                                ba: g,
                                pa: !x
                            });
                            v = {};
                            x = 0;
                            try {
                                var e = c ? s.call(c) : s()
                            } finally {
                                a.k.end(), d && a.a.A(b, function(a, b) {
                                    b.F()
                                }), n = !1
                            }
                            g.Ka(l, e) && (g.notifySubscribers(l, "beforeChange"), l = e, g.wa && !g.throttleEvaluation || g.notifySubscribers(l))
                        } finally {
                            r = !1
                        }
                        x || p()
                    }
                }

                function g() {
                    if (0 < arguments.length) {
                        if ("function" === typeof B) {
                            B.apply(c, arguments)
                        } else {
                            throw Error("Cannot write a value to a ko.computed unless you specify a 'write' option. If you wish to read the current value, don't pass any parameters.")
                        }
                        return this
                    }
                    n && h();
                    a.k.zb(g);
                    return l
                }

                function k() {
                    return n || 0 < x
                }
                var l, n = !0,
                    r = !1,
                    m = !1,
                    q = !1,
                    s = b;
                s && "object" == typeof s ? (d = s, s = d.read) : (d = d || {}, s || (s = d.read));
                if ("function" != typeof s) {
                    throw Error("Pass a function that returns the value of the ko.computed")
                }
                var B = d.write,
                    u = d.disposeWhenNodeIsRemoved || d.G || null,
                    D = d.disposeWhen || d.Da,
                    y = D,
                    p = e,
                    v = {}, x = 0,
                    t = null;
                c || (c = d.owner);
                a.N.call(g);
                a.a.sa(g, a.h.fn);
                g.o = function() {
                    n && !x && h();
                    return l
                };
                g.fa = function() {
                    return x
                };
                g.Yb = "function" === typeof d.write;
                g.F = function() {
                    p()
                };
                g.ga = k;
                var w = g.Ma;
                g.Ma = function(a) {
                    w.call(g, a);
                    g.wa = function() {
                        g.bb(l);
                        n = !0;
                        g.cb(g)
                    }
                };
                a.s(g, "peek", g.o);
                a.s(g, "dispose", g.F);
                a.s(g, "isActive", g.ga);
                a.s(g, "getDependenciesCount", g.fa);
                u && (m = !0, u.nodeType && (y = function() {
                    return !a.a.Ea(u) || D && D()
                }));
                !0 !== d.deferEvaluation && h();
                u && k() && u.nodeType && (p = function() {
                    a.a.u.Ab(u, p);
                    e()
                }, a.a.u.ja(u, p));
                return g
            };
            a.$b = function(b) {
                return a.Ha(b, a.h)
            };
            z = a.m.hc;
            a.h[z] = a.m;
            a.h.fn = {
                equalityComparer: G
            };
            a.h.fn[z] = a.h;
            a.a.na && a.a.ra(a.h.fn, a.N.fn);
            a.b("dependentObservable", a.h);
            a.b("computed", a.h);
            a.b("isComputed", a.$b);
            (function() {
                function b(a, f, h) {
                    h = h || new d;
                    a = f(a);
                    if ("object" != typeof a || null === a || a === p || a instanceof Date || a instanceof String || a instanceof Number || a instanceof Boolean) {
                        return a
                    }
                    var g = a instanceof Array ? [] : {};
                    h.save(a, g);
                    c(a, function(c) {
                        var d = f(a[c]);
                        switch (typeof d) {
                            case "boolean":
                            case "number":
                            case "string":
                            case "function":
                                g[c] = d;
                                break;
                            case "object":
                            case "undefined":
                                var n = h.get(d);
                                g[c] = n !== p ? n : b(d, f, h)
                        }
                    });
                    return g
                }

                function c(a, b) {
                    if (a instanceof Array) {
                        for (var c = 0; c < a.length; c++) {
                            b(c)
                        }
                        "function" == typeof a.toJSON && b("toJSON")
                    } else {
                        for (c in a) {
                            b(c)
                        }
                    }
                }

                function d() {
                    this.keys = [];
                    this.ab = []
                }
                a.Gb = function(c) {
                    if (0 == arguments.length) {
                        throw Error("When calling ko.toJS, pass the object you want to convert.")
                    }
                    return b(c, function(b) {
                        for (var c = 0; a.v(b) && 10 > c; c++) {
                            b = b()
                        }
                        return b
                    })
                };
                a.toJSON = function(b, c, d) {
                    b = a.Gb(b);
                    return a.a.Ya(b, c, d)
                };
                d.prototype = {
                    save: function(b, c) {
                        var d = a.a.l(this.keys, b);
                        0 <= d ? this.ab[d] = c : (this.keys.push(b), this.ab.push(c))
                    },
                    get: function(b) {
                        b = a.a.l(this.keys, b);
                        return 0 <= b ? this.ab[b] : p
                    }
                }
            })();
            a.b("toJS", a.Gb);
            a.b("toJSON", a.toJSON);
            (function() {
                a.i = {
                    p: function(b) {
                        switch (a.a.B(b)) {
                            case "option":
                                return !0 === b.__ko__hasDomDataOptionValue__ ? a.a.f.get(b, a.d.options.Pa) : 7 >= a.a.oa ? b.getAttributeNode("value") && b.getAttributeNode("value").specified ? b.value : b.text : b.value;
                            case "select":
                                return 0 <= b.selectedIndex ? a.i.p(b.options[b.selectedIndex]) : p;
                            default:
                                return b.value
                        }
                    },
                    X: function(b, c, d) {
                        switch (a.a.B(b)) {
                            case "option":
                                switch (typeof c) {
                                    case "string":
                                        a.a.f.set(b, a.d.options.Pa, p);
                                        "__ko__hasDomDataOptionValue__" in b && delete b.__ko__hasDomDataOptionValue__;
                                        b.value = c;
                                        break;
                                    default:
                                        a.a.f.set(b, a.d.options.Pa, c), b.__ko__hasDomDataOptionValue__ = !0, b.value = "number" === typeof c ? c : ""
                                }
                                break;
                            case "select":
                                if ("" === c || null === c) {
                                    c = p
                                }
                                for (var e = -1, f = 0, h = b.options.length, g; f < h; ++f) {
                                    if (g = a.i.p(b.options[f]), g == c || "" == g && c === p) {
                                        e = f;
                                        break
                                    }
                                }
                                if (d || 0 <= e || c === p && 1 < b.size) {
                                    b.selectedIndex = e
                                }
                                break;
                            default:
                                if (null === c || c === p) {
                                    c = ""
                                }
                                b.value = c
                        }
                    }
                }
            })();
            a.b("selectExtensions", a.i);
            a.b("selectExtensions.readValue", a.i.p);
            a.b("selectExtensions.writeValue", a.i.X);
            a.g = function() {
                function b(b) {
                    b = a.a.ta(b);
                    123 === b.charCodeAt(0) && (b = b.slice(1, -1));
                    var c = [],
                        d = b.match(e),
                        g, m, q = 0;
                    if (d) {
                        d.push(",");
                        for (var s = 0, B; B = d[s]; ++s) {
                            var u = B.charCodeAt(0);
                            if (44 === u) {
                                if (0 >= q) {
                                    g && c.push(m ? {
                                        key: g,
                                        value: m.join("")
                                    } : {
                                        unknown: g
                                    });
                                    g = m = q = 0;
                                    continue
                                }
                            } else {
                                if (58 === u) {
                                    if (!m) {
                                        continue
                                    }
                                } else {
                                    if (47 === u && s && 1 < B.length) {
                                        (u = d[s - 1].match(f)) && !h[u[0]] && (b = b.substr(b.indexOf(B) + 1), d = b.match(e), d.push(","), s = -1, B = "/")
                                    } else {
                                        if (40 === u || 123 === u || 91 === u) {
                                            ++q
                                        } else {
                                            if (41 === u || 125 === u || 93 === u) {
                                                --q
                                            } else {
                                                if (!g && !m) {
                                                    g = 34 === u || 39 === u ? B.slice(1, -1) : B;
                                                    continue
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            m ? m.push(B) : m = [B]
                        }
                    }
                    return c
                }
                var c = ["true", "false", "null", "undefined"],
                    d = /^(?:[$_a-z][$\w]*|(.+)(\.\s*[$_a-z][$\w]*|\[.+\]))$/i,
                    e = RegExp("\"(?:[^\"\\\\]|\\\\.)*\"|'(?:[^'\\\\]|\\\\.)*'|/(?:[^/\\\\]|\\\\.)*/w*|[^\\s:,/][^,\"'{}()/:[\\]]*[^\\s,\"'{}()/:[\\]]|[^\\s]", "g"),
                    f = /[\])"'A-Za-z0-9_$]+$/,
                    h = {
                        "in": 1,
                        "return": 1,
                        "typeof": 1
                    }, g = {};
                return {
                    aa: [],
                    W: g,
                    Ra: b,
                    qa: function(e, l) {
                        function f(b, e) {
                            var l, k = a.getBindingHandler(b);
                            if (k && k.preprocess ? e = k.preprocess(e, b, f) : 1) {
                                if (k = g[b]) {
                                    l = e, 0 <= a.a.l(c, l) ? l = !1 : (k = l.match(d), l = null === k ? !1 : k[1] ? "Object(" + k[1] + ")" + k[2] : l), k = l
                                }
                                k && m.push("'" + b + "':function(_z){" + l + "=_z}");
                                q && (e = "function(){return " + e + " }");
                                h.push("'" + b + "':" + e)
                            }
                        }
                        l = l || {};
                        var h = [],
                            m = [],
                            q = l.valueAccessors,
                            s = "string" === typeof e ? b(e) : e;
                        a.a.r(s, function(a) {
                            f(a.key || a.unknown, a.value)
                        });
                        m.length && f("_ko_property_writers", "{" + m.join(",") + " }");
                        return h.join(",")
                    },
                    bc: function(a, b) {
                        for (var c = 0; c < a.length; c++) {
                            if (a[c].key == b) {
                                return !0
                            }
                        }
                        return !1
                    },
                    va: function(b, c, d, e, g) {
                        if (b && a.v(b)) {
                            !a.ub(b) || g && b.o() === e || b(e)
                        } else {
                            if ((b = c.get("_ko_property_writers")) && b[d]) {
                                b[d](e)
                            }
                        }
                    }
                }
            }();
            a.b("expressionRewriting", a.g);
            a.b("expressionRewriting.bindingRewriteValidators", a.g.aa);
            a.b("expressionRewriting.parseObjectLiteral", a.g.Ra);
            a.b("expressionRewriting.preProcessBindings", a.g.qa);
            a.b("expressionRewriting._twoWayBindings", a.g.W);
            a.b("jsonExpressionRewriting", a.g);
            a.b("jsonExpressionRewriting.insertPropertyAccessorsIntoJson", a.g.qa);
            (function() {
                function b(a) {
                    return 8 == a.nodeType && h.test(f ? a.text : a.nodeValue)
                }

                function c(a) {
                    return 8 == a.nodeType && g.test(f ? a.text : a.nodeValue)
                }

                function d(a, d) {
                    for (var e = a, g = 1, k = []; e = e.nextSibling;) {
                        if (c(e) && (g--, 0 === g)) {
                            return k
                        }
                        k.push(e);
                        b(e) && g++
                    }
                    if (!d) {
                        throw Error("Cannot find closing comment tag to match: " + a.nodeValue)
                    }
                    return null
                }

                function e(a, b) {
                    var c = d(a, b);
                    return c ? 0 < c.length ? c[c.length - 1].nextSibling : a.nextSibling : null
                }
                var f = w && "\x3c!--test--\x3e" === w.createComment("test").text,
                    h = f ? /^\x3c!--\s*ko(?:\s+([\s\S]+))?\s*--\x3e$/ : /^\s*ko(?:\s+([\s\S]+))?\s*$/,
                    g = f ? /^\x3c!--\s*\/ko\s*--\x3e$/ : /^\s*\/ko\s*$/,
                    k = {
                        ul: !0,
                        ol: !0
                    };
                a.e = {
                    Q: {},
                    childNodes: function(a) {
                        return b(a) ? d(a) : a.childNodes
                    },
                    da: function(c) {
                        if (b(c)) {
                            c = a.e.childNodes(c);
                            for (var d = 0, e = c.length; d < e; d++) {
                                a.removeNode(c[d])
                            }
                        } else {
                            a.a.Fa(c)
                        }
                    },
                    U: function(c, d) {
                        if (b(c)) {
                            a.e.da(c);
                            for (var e = c.nextSibling, g = 0, k = d.length; g < k; g++) {
                                e.parentNode.insertBefore(d[g], e)
                            }
                        } else {
                            a.a.U(c, d)
                        }
                    },
                    yb: function(a, c) {
                        b(a) ? a.parentNode.insertBefore(c, a.nextSibling) : a.firstChild ? a.insertBefore(c, a.firstChild) : a.appendChild(c)
                    },
                    rb: function(c, d, e) {
                        e ? b(c) ? c.parentNode.insertBefore(d, e.nextSibling) : e.nextSibling ? c.insertBefore(d, e.nextSibling) : c.appendChild(d) : a.e.yb(c, d)
                    },
                    firstChild: function(a) {
                        return b(a) ? !a.nextSibling || c(a.nextSibling) ? null : a.nextSibling : a.firstChild
                    },
                    nextSibling: function(a) {
                        b(a) && (a = e(a));
                        return a.nextSibling && c(a.nextSibling) ? null : a.nextSibling
                    },
                    Xb: b,
                    lc: function(a) {
                        return (a = (f ? a.text : a.nodeValue).match(h)) ? a[1] : null
                    },
                    wb: function(d) {
                        if (k[a.a.B(d)]) {
                            var g = d.firstChild;
                            if (g) {
                                do {
                                    if (1 === g.nodeType) {
                                        var f;
                                        f = g.firstChild;
                                        var h = null;
                                        if (f) {
                                            do {
                                                if (h) {
                                                    h.push(f)
                                                } else {
                                                    if (b(f)) {
                                                        var q = e(f, !0);
                                                        q ? f = q : h = [f]
                                                    } else {
                                                        c(f) && (h = [f])
                                                    }
                                                }
                                            } while (f = f.nextSibling)
                                        }
                                        if (f = h) {
                                            for (h = g.nextSibling, q = 0; q < f.length; q++) {
                                                h ? d.insertBefore(f[q], h) : d.appendChild(f[q])
                                            }
                                        }
                                    }
                                } while (g = g.nextSibling)
                            }
                        }
                    }
                }
            })();
            a.b("virtualElements", a.e);
            a.b("virtualElements.allowedBindings", a.e.Q);
            a.b("virtualElements.emptyNode", a.e.da);
            a.b("virtualElements.insertAfter", a.e.rb);
            a.b("virtualElements.prepend", a.e.yb);
            a.b("virtualElements.setDomNodeChildren", a.e.U);
            (function() {
                a.J = function() {
                    this.Nb = {}
                };
                a.a.extend(a.J.prototype, {
                    nodeHasBindings: function(b) {
                        switch (b.nodeType) {
                            case 1:
                                return null != b.getAttribute("data-bind");
                            case 8:
                                return a.e.Xb(b);
                            default:
                                return !1
                        }
                    },
                    getBindings: function(a, c) {
                        var d = this.getBindingsString(a, c);
                        return d ? this.parseBindingsString(d, c, a) : null
                    },
                    getBindingAccessors: function(a, c) {
                        var d = this.getBindingsString(a, c);
                        return d ? this.parseBindingsString(d, c, a, {
                            valueAccessors: !0
                        }) : null
                    },
                    getBindingsString: function(b) {
                        switch (b.nodeType) {
                            case 1:
                                return b.getAttribute("data-bind");
                            case 8:
                                return a.e.lc(b);
                            default:
                                return null
                        }
                    },
                    parseBindingsString: function(b, c, d, e) {
                        try {
                            var f = this.Nb,
                                h = b + (e && e.valueAccessors || ""),
                                g;
                            if (!(g = f[h])) {
                                var k, l = "with($context){with($data||{}){return{" + a.g.qa(b, e) + "}}}";
                                k = new Function("$context", "$element", l);
                                g = f[h] = k
                            }
                            return g(c, d)
                        } catch (n) {
                            throw n.message = "Unable to parse bindings.\nBindings value: " + b + "\nMessage: " + n.message, n
                        }
                    }
                });
                a.J.instance = new a.J
            })();
            a.b("bindingProvider", a.J);
            (function() {
                function b(a) {
                    return function() {
                        return a
                    }
                }

                function c(a) {
                    return a()
                }

                function d(b) {
                    return a.a.Oa(a.k.t(b), function(a, c) {
                        return function() {
                            return b()[c]
                        }
                    })
                }

                function e(a, b) {
                    return d(this.getBindings.bind(this, a, b))
                }

                function f(b, c, d) {
                    var e, g = a.e.firstChild(c),
                        k = a.J.instance,
                        f = k.preprocessNode;
                    if (f) {
                        for (; e = g;) {
                            g = a.e.nextSibling(e), f.call(k, e)
                        }
                        g = a.e.firstChild(c)
                    }
                    for (; e = g;) {
                        g = a.e.nextSibling(e), h(b, e, d)
                    }
                }

                function h(b, c, d) {
                    var e = !0,
                        g = 1 === c.nodeType;
                    g && a.e.wb(c);
                    if (g && d || a.J.instance.nodeHasBindings(c)) {
                        e = k(c, null, b, d).shouldBindDescendants
                    }
                    e && !n[a.a.B(c)] && f(b, c, !g)
                }

                function g(b) {
                    var c = [],
                        d = {}, e = [];
                    a.a.A(b, function y(g) {
                        if (!d[g]) {
                            var k = a.getBindingHandler(g);
                            k && (k.after && (e.push(g), a.a.r(k.after, function(c) {
                                if (b[c]) {
                                    if (-1 !== a.a.l(e, c)) {
                                        throw Error("Cannot combine the following bindings, because they have a cyclic dependency: " + e.join(", "))
                                    }
                                    y(c)
                                }
                            }), e.length--), c.push({
                                key: g,
                                pb: k
                            }));
                            d[g] = !0
                        }
                    });
                    return c
                }

                function k(b, d, k, f) {
                    var h = a.a.f.get(b, r);
                    if (!d) {
                        if (h) {
                            throw Error("You cannot apply bindings multiple times to the same element.")
                        }
                        a.a.f.set(b, r, !0)
                    }!h && f && a.Eb(b, k);
                    var l;
                    if (d && "function" !== typeof d) {
                        l = d
                    } else {
                        var n = a.J.instance,
                            m = n.getBindingAccessors || e,
                            x = a.h(function() {
                                (l = d ? d(k, b) : m.call(n, b, k)) && k.D && k.D();
                                return l
                            }, null, {
                                G: b
                            });
                        l && x.ga() || (x = null)
                    }
                    var t;
                    if (l) {
                        var w = x ? function(a) {
                            return function() {
                                return c(x()[a])
                            }
                        } : function(a) {
                            return l[a]
                        }, z = function() {
                            return a.a.Oa(x ? x() : l, c)
                        };
                        z.get = function(a) {
                            return l[a] && c(w(a))
                        };
                        z.has = function(a) {
                            return a in l
                        };
                        f = g(l);
                        a.a.r(f, function(c) {
                            var d = c.pb.init,
                                e = c.pb.update,
                                g = c.key;
                            if (8 === b.nodeType && !a.e.Q[g]) {
                                throw Error("The binding '" + g + "' cannot be used with virtual elements")
                            }
                            try {
                                "function" == typeof d && a.k.t(function() {
                                    var a = d(b, w(g), z, k.$data, k);
                                    if (a && a.controlsDescendantBindings) {
                                        if (t !== p) {
                                            throw Error("Multiple bindings (" + t + " and " + g + ") are trying to control descendant bindings of the same element. You cannot use these bindings together on the same element.")
                                        }
                                        t = g
                                    }
                                }), "function" == typeof e && a.h(function() {
                                    e(b, w(g), z, k.$data, k)
                                }, null, {
                                    G: b
                                })
                            } catch (f) {
                                throw f.message = 'Unable to process binding "' + g + ": " + l[g] + '"\nMessage: ' + f.message, f
                            }
                        })
                    }
                    return {
                        shouldBindDescendants: t === p
                    }
                }

                function l(b) {
                    return b && b instanceof a.I ? b : new a.I(b)
                }
                a.d = {};
                var n = {
                    script: !0
                };
                a.getBindingHandler = function(b) {
                    return a.d[b]
                };
                a.I = function(b, c, d, e) {
                    var g = this,
                        k = "function" == typeof b && !a.v(b),
                        f, h = a.h(function() {
                            var f = k ? b() : b,
                                l = a.a.c(f);
                            c ? (c.D && c.D(), a.a.extend(g, c), h && (g.D = h)) : (g.$parents = [], g.$root = l, g.ko = a);
                            g.$rawData = f;
                            g.$data = l;
                            d && (g[d] = l);
                            e && e(g, c, l);
                            return g.$data
                        }, null, {
                            Da: function() {
                                return f && !a.a.eb(f)
                            },
                            G: !0
                        });
                    h.ga() && (g.D = h, h.equalityComparer = null, f = [], h.Jb = function(b) {
                        f.push(b);
                        a.a.u.ja(b, function(b) {
                            a.a.ma(f, b);
                            f.length || (h.F(), g.D = h = p)
                        })
                    })
                };
                a.I.prototype.createChildContext = function(b, c, d) {
                    return new a.I(b, this, c, function(a, b) {
                        a.$parentContext = b;
                        a.$parent = b.$data;
                        a.$parents = (b.$parents || []).slice(0);
                        a.$parents.unshift(a.$parent);
                        d && d(a)
                    })
                };
                a.I.prototype.extend = function(b) {
                    return new a.I(this.D || this.$data, this, null, function(c, d) {
                        c.$rawData = d.$rawData;
                        a.a.extend(c, "function" == typeof b ? b() : b)
                    })
                };
                var r = a.a.f.L(),
                    m = a.a.f.L();
                a.Eb = function(b, c) {
                    if (2 == arguments.length) {
                        a.a.f.set(b, m, c), c.D && c.D.Jb(b)
                    } else {
                        return a.a.f.get(b, m)
                    }
                };
                a.xa = function(b, c, d) {
                    1 === b.nodeType && a.e.wb(b);
                    return k(b, c, l(d), !0)
                };
                a.Lb = function(c, e, g) {
                    g = l(g);
                    return a.xa(c, "function" === typeof e ? d(e.bind(null, g, c)) : a.a.Oa(e, b), g)
                };
                a.gb = function(a, b) {
                    1 !== b.nodeType && 8 !== b.nodeType || f(l(a), b, !0)
                };
                a.fb = function(a, b) {
                    !t && A.jQuery && (t = A.jQuery);
                    if (b && 1 !== b.nodeType && 8 !== b.nodeType) {
                        throw Error("ko.applyBindings: first parameter should be your view model; second parameter should be a DOM node")
                    }
                    b = b || A.document.body;
                    h(l(a), b, !0)
                };
                a.Ca = function(b) {
                    switch (b.nodeType) {
                        case 1:
                        case 8:
                            var c = a.Eb(b);
                            if (c) {
                                return c
                            }
                            if (b.parentNode) {
                                return a.Ca(b.parentNode)
                            }
                    }
                    return p
                };
                a.Pb = function(b) {
                    return (b = a.Ca(b)) ? b.$data : p
                };
                a.b("bindingHandlers", a.d);
                a.b("applyBindings", a.fb);
                a.b("applyBindingsToDescendants", a.gb);
                a.b("applyBindingAccessorsToNode", a.xa);
                a.b("applyBindingsToNode", a.Lb);
                a.b("contextFor", a.Ca);
                a.b("dataFor", a.Pb)
            })();
            var L = {
                "class": "className",
                "for": "htmlFor"
            };
            a.d.attr = {
                update: function(b, c) {
                    var d = a.a.c(c()) || {};
                    a.a.A(d, function(c, d) {
                        d = a.a.c(d);
                        var h = !1 === d || null === d || d === p;
                        h && b.removeAttribute(c);
                        8 >= a.a.oa && c in L ? (c = L[c], h ? b.removeAttribute(c) : b[c] = d) : h || b.setAttribute(c, d.toString());
                        "name" === c && a.a.Cb(b, h ? "" : d.toString())
                    })
                }
            };
            (function() {
                a.d.checked = {
                    after: ["value", "attr"],
                    init: function(b, c, d) {
                        function e() {
                            return d.has("checkedValue") ? a.a.c(d.get("checkedValue")) : b.value
                        }

                        function f() {
                            var g = b.checked,
                                f = r ? e() : g;
                            if (!a.ca.pa() && (!k || g)) {
                                var h = a.k.t(c);
                                l ? n !== f ? (g && (a.a.Y(h, f, !0), a.a.Y(h, n, !1)), n = f) : a.a.Y(h, f, g) : a.g.va(h, d, "checked", f, !0)
                            }
                        }

                        function h() {
                            var d = a.a.c(c());
                            b.checked = l ? 0 <= a.a.l(d, e()) : g ? d : e() === d
                        }
                        var g = "checkbox" == b.type,
                            k = "radio" == b.type;
                        if (g || k) {
                            var l = g && a.a.c(c()) instanceof Array,
                                n = l ? e() : p,
                                r = k || l;
                            k && !b.name && a.d.uniqueName.init(b, function() {
                                return !0
                            });
                            a.ba(f, null, {
                                G: b
                            });
                            a.a.q(b, "click", f);
                            a.ba(h, null, {
                                G: b
                            })
                        }
                    }
                };
                a.g.W.checked = !0;
                a.d.checkedValue = {
                    update: function(b, c) {
                        b.value = a.a.c(c())
                    }
                }
            })();
            a.d.css = {
                update: function(b, c) {
                    var d = a.a.c(c());
                    "object" == typeof d ? a.a.A(d, function(c, d) {
                        d = a.a.c(d);
                        a.a.ua(b, c, d)
                    }) : (d = String(d || ""), a.a.ua(b, b.__ko__cssValue, !1), b.__ko__cssValue = d, a.a.ua(b, d, !0))
                }
            };
            a.d.enable = {
                update: function(b, c) {
                    var d = a.a.c(c());
                    d && b.disabled ? b.removeAttribute("disabled") : d || b.disabled || (b.disabled = !0)
                }
            };
            a.d.disable = {
                update: function(b, c) {
                    a.d.enable.update(b, function() {
                        return !a.a.c(c())
                    })
                }
            };
            a.d.event = {
                init: function(b, c, d, e, f) {
                    var h = c() || {};
                    a.a.A(h, function(g) {
                        "string" == typeof g && a.a.q(b, g, function(b) {
                            var h, n = c()[g];
                            if (n) {
                                try {
                                    var r = a.a.R(arguments);
                                    e = f.$data;
                                    r.unshift(e);
                                    h = n.apply(e, r)
                                } finally {
                                    !0 !== h && (b.preventDefault ? b.preventDefault() : b.returnValue = !1)
                                }!1 === d.get(g + "Bubble") && (b.cancelBubble = !0, b.stopPropagation && b.stopPropagation())
                            }
                        })
                    })
                }
            };
            a.d.foreach = {
                vb: function(b) {
                    return function() {
                        var c = b(),
                            d = a.a.Sa(c);
                        if (!d || "number" == typeof d.length) {
                            return {
                                foreach: c,
                                templateEngine: a.K.Ja
                            }
                        }
                        a.a.c(c);
                        return {
                            foreach: d.data,
                            as: d.as,
                            includeDestroyed: d.includeDestroyed,
                            afterAdd: d.afterAdd,
                            beforeRemove: d.beforeRemove,
                            afterRender: d.afterRender,
                            beforeMove: d.beforeMove,
                            afterMove: d.afterMove,
                            templateEngine: a.K.Ja
                        }
                    }
                },
                init: function(b, c) {
                    return a.d.template.init(b, a.d.foreach.vb(c))
                },
                update: function(b, c, d, e, f) {
                    return a.d.template.update(b, a.d.foreach.vb(c), d, e, f)
                }
            };
            a.g.aa.foreach = !1;
            a.e.Q.foreach = !0;
            a.d.hasfocus = {
                init: function(b, c, d) {
                    function e(e) {
                        b.__ko_hasfocusUpdating = !0;
                        var k = b.ownerDocument;
                        if ("activeElement" in k) {
                            var f;
                            try {
                                f = k.activeElement
                            } catch (h) {
                                f = k.body
                            }
                            e = f === b
                        }
                        k = c();
                        a.g.va(k, d, "hasfocus", e, !0);
                        b.__ko_hasfocusLastValue = e;
                        b.__ko_hasfocusUpdating = !1
                    }
                    var f = e.bind(null, !0),
                        h = e.bind(null, !1);
                    a.a.q(b, "focus", f);
                    a.a.q(b, "focusin", f);
                    a.a.q(b, "blur", h);
                    a.a.q(b, "focusout", h)
                },
                update: function(b, c) {
                    var d = !! a.a.c(c());
                    b.__ko_hasfocusUpdating || b.__ko_hasfocusLastValue === d || (d ? b.focus() : b.blur(), a.k.t(a.a.ha, null, [b, d ? "focusin" : "focusout"]))
                }
            };
            a.g.W.hasfocus = !0;
            a.d.hasFocus = a.d.hasfocus;
            a.g.W.hasFocus = !0;
            a.d.html = {
                init: function() {
                    return {
                        controlsDescendantBindings: !0
                    }
                },
                update: function(b, c) {
                    a.a.Va(b, c())
                }
            };
            H("if");
            H("ifnot", !1, !0);
            H("with", !0, !1, function(a, c) {
                return a.createChildContext(c)
            });
            var J = {};
            a.d.options = {
                init: function(b) {
                    if ("select" !== a.a.B(b)) {
                        throw Error("options binding applies only to SELECT elements")
                    }
                    for (; 0 < b.length;) {
                        b.remove(0)
                    }
                    return {
                        controlsDescendantBindings: !0
                    }
                },
                update: function(b, c, d) {
                    function e() {
                        return a.a.la(b.options, function(a) {
                            return a.selected
                        })
                    }

                    function f(a, b, c) {
                        var d = typeof b;
                        return "function" == d ? b(a) : "string" == d ? a[b] : c
                    }

                    function h(c, d) {
                        if (r.length) {
                            var e = 0 <= a.a.l(r, a.i.p(d[0]));
                            a.a.Db(d[0], e);
                            m && !e && a.k.t(a.a.ha, null, [b, "change"])
                        }
                    }
                    var g = 0 != b.length && b.multiple ? b.scrollTop : null,
                        k = a.a.c(c()),
                        l = d.get("optionsIncludeDestroyed");
                    c = {};
                    var n, r;
                    r = b.multiple ? a.a.ya(e(), a.i.p) : 0 <= b.selectedIndex ? [a.i.p(b.options[b.selectedIndex])] : [];
                    k && ("undefined" == typeof k.length && (k = [k]), n = a.a.la(k, function(b) {
                        return l || b === p || null === b || !a.a.c(b._destroy)
                    }), d.has("optionsCaption") && (k = a.a.c(d.get("optionsCaption")), null !== k && k !== p && n.unshift(J)));
                    var m = !1;
                    c.beforeRemove = function(a) {
                        b.removeChild(a)
                    };
                    k = h;
                    d.has("optionsAfterRender") && (k = function(b, c) {
                        h(0, c);
                        a.k.t(d.get("optionsAfterRender"), null, [c[0], b !== J ? b : p])
                    });
                    a.a.Ua(b, n, function(c, e, g) {
                        g.length && (r = g[0].selected ? [a.i.p(g[0])] : [], m = !0);
                        e = b.ownerDocument.createElement("option");
                        c === J ? (a.a.Xa(e, d.get("optionsCaption")), a.i.X(e, p)) : (g = f(c, d.get("optionsValue"), c), a.i.X(e, a.a.c(g)), c = f(c, d.get("optionsText"), g), a.a.Xa(e, c));
                        return [e]
                    }, c, k);
                    a.k.t(function() {
                        d.get("valueAllowUnset") && d.has("value") ? a.i.X(b, a.a.c(d.get("value")), !0) : (b.multiple ? r.length && e().length < r.length : r.length && 0 <= b.selectedIndex ? a.i.p(b.options[b.selectedIndex]) !== r[0] : r.length || 0 <= b.selectedIndex) && a.a.ha(b, "change")
                    });
                    a.a.Tb(b);
                    g && 20 < Math.abs(g - b.scrollTop) && (b.scrollTop = g)
                }
            };
            a.d.options.Pa = a.a.f.L();
            a.d.selectedOptions = {
                after: ["options", "foreach"],
                init: function(b, c, d) {
                    a.a.q(b, "change", function() {
                        var e = c(),
                            f = [];
                        a.a.r(b.getElementsByTagName("option"), function(b) {
                            b.selected && f.push(a.i.p(b))
                        });
                        a.g.va(e, d, "selectedOptions", f)
                    })
                },
                update: function(b, c) {
                    if ("select" != a.a.B(b)) {
                        throw Error("values binding applies only to SELECT elements")
                    }
                    var d = a.a.c(c());
                    d && "number" == typeof d.length && a.a.r(b.getElementsByTagName("option"), function(b) {
                        var c = 0 <= a.a.l(d, a.i.p(b));
                        a.a.Db(b, c)
                    })
                }
            };
            a.g.W.selectedOptions = !0;
            a.d.style = {
                update: function(b, c) {
                    var d = a.a.c(c() || {});
                    a.a.A(d, function(c, d) {
                        d = a.a.c(d);
                        b.style[c] = d || ""
                    })
                }
            };
            a.d.submit = {
                init: function(b, c, d, e, f) {
                    if ("function" != typeof c()) {
                        throw Error("The value for a submit binding must be a function")
                    }
                    a.a.q(b, "submit", function(a) {
                        var d, e = c();
                        try {
                            d = e.call(f.$data, b)
                        } finally {
                            !0 !== d && (a.preventDefault ? a.preventDefault() : a.returnValue = !1)
                        }
                    })
                }
            };
            a.d.text = {
                init: function() {
                    return {
                        controlsDescendantBindings: !0
                    }
                },
                update: function(b, c) {
                    a.a.Xa(b, c())
                }
            };
            a.e.Q.text = !0;
            a.d.uniqueName = {
                init: function(b, c) {
                    if (c()) {
                        var d = "ko_unique_" + ++a.d.uniqueName.Ob;
                        a.a.Cb(b, d)
                    }
                }
            };
            a.d.uniqueName.Ob = 0;
            a.d.value = {
                after: ["options", "foreach"],
                init: function(b, c, d) {
                    function e() {
                        g = !1;
                        var e = c(),
                            f = a.i.p(b);
                        a.g.va(e, d, "value", f)
                    }
                    var f = ["change"],
                        h = d.get("valueUpdate"),
                        g = !1;
                    h && ("string" == typeof h && (h = [h]), a.a.$(f, h), f = a.a.ib(f));
                    !a.a.oa || "input" != b.tagName.toLowerCase() || "text" != b.type || "off" == b.autocomplete || b.form && "off" == b.form.autocomplete || -1 != a.a.l(f, "propertychange") || (a.a.q(b, "propertychange", function() {
                        g = !0
                    }), a.a.q(b, "focus", function() {
                        g = !1
                    }), a.a.q(b, "blur", function() {
                        g && e()
                    }));
                    a.a.r(f, function(c) {
                        var d = e;
                        a.a.kc(c, "after") && (d = function() {
                            setTimeout(e, 0)
                        }, c = c.substring(5));
                        a.a.q(b, c, d)
                    })
                },
                update: function(b, c, d) {
                    var e = a.a.c(c());
                    c = a.i.p(b);
                    if (e !== c) {
                        if ("select" === a.a.B(b)) {
                            var f = d.get("valueAllowUnset");
                            d = function() {
                                a.i.X(b, e, f)
                            };
                            d();
                            f || e === a.i.p(b) ? setTimeout(d, 0) : a.k.t(a.a.ha, null, [b, "change"])
                        } else {
                            a.i.X(b, e)
                        }
                    }
                }
            };
            a.g.W.value = !0;
            a.d.visible = {
                update: function(b, c) {
                    var d = a.a.c(c()),
                        e = "none" != b.style.display;
                    d && !e ? b.style.display = "" : !d && e && (b.style.display = "none")
                }
            };
            (function(b) {
                a.d[b] = {
                    init: function(c, d, e, f, h) {
                        return a.d.event.init.call(this, c, function() {
                            var a = {};
                            a[b] = d();
                            return a
                        }, e, f, h)
                    }
                }
            })("click");
            a.C = function() {};
            a.C.prototype.renderTemplateSource = function() {
                throw Error("Override renderTemplateSource")
            };
            a.C.prototype.createJavaScriptEvaluatorBlock = function() {
                throw Error("Override createJavaScriptEvaluatorBlock")
            };
            a.C.prototype.makeTemplateSource = function(b, c) {
                if ("string" == typeof b) {
                    c = c || w;
                    var d = c.getElementById(b);
                    if (!d) {
                        throw Error("Cannot find template with ID " + b)
                    }
                    return new a.n.j(d)
                }
                if (1 == b.nodeType || 8 == b.nodeType) {
                    return new a.n.Z(b)
                }
                throw Error("Unknown template type: " + b)
            };
            a.C.prototype.renderTemplate = function(a, c, d, e) {
                a = this.makeTemplateSource(a, e);
                return this.renderTemplateSource(a, c, d)
            };
            a.C.prototype.isTemplateRewritten = function(a, c) {
                return !1 === this.allowTemplateRewriting ? !0 : this.makeTemplateSource(a, c).data("isRewritten")
            };
            a.C.prototype.rewriteTemplate = function(a, c, d) {
                a = this.makeTemplateSource(a, d);
                c = c(a.text());
                a.text(c);
                a.data("isRewritten", !0)
            };
            a.b("templateEngine", a.C);
            a.Za = function() {
                function b(b, c, d, g) {
                    b = a.g.Ra(b);
                    for (var k = a.g.aa, l = 0; l < b.length; l++) {
                        var n = b[l].key;
                        if (k.hasOwnProperty(n)) {
                            var r = k[n];
                            if ("function" === typeof r) {
                                if (n = r(b[l].value)) {
                                    throw Error(n)
                                }
                            } else {
                                if (!r) {
                                    throw Error("This template engine does not support the '" + n + "' binding within its templates")
                                }
                            }
                        }
                    }
                    d = "ko.__tr_ambtns(function($context,$element){return(function(){return{ " + a.g.qa(b, {
                        valueAccessors: !0
                    }) + " } })()},'" + d.toLowerCase() + "')";
                    return g.createJavaScriptEvaluatorBlock(d) + c
                }
                var c = /(<([a-z]+\d*)(?:\s+(?!data-bind\s*=\s*)[a-z0-9\-]+(?:=(?:\"[^\"]*\"|\'[^\']*\'))?)*\s+)data-bind\s*=\s*(["'])([\s\S]*?)\3/gi,
                    d = /\x3c!--\s*ko\b\s*([\s\S]*?)\s*--\x3e/g;
                return {
                    Ub: function(b, c, d) {
                        c.isTemplateRewritten(b, d) || c.rewriteTemplate(b, function(b) {
                            return a.Za.dc(b, c)
                        }, d)
                    },
                    dc: function(a, f) {
                        return a.replace(c, function(a, c, d, e, n) {
                            return b(n, c, d, f)
                        }).replace(d, function(a, c) {
                            return b(c, "\x3c!-- ko --\x3e", "#comment", f)
                        })
                    },
                    Mb: function(b, c) {
                        return a.w.Na(function(d, g) {
                            var k = d.nextSibling;
                            k && k.nodeName.toLowerCase() === c && a.xa(k, b, g)
                        })
                    }
                }
            }();
            a.b("__tr_ambtns", a.Za.Mb);
            (function() {
                a.n = {};
                a.n.j = function(a) {
                    this.j = a
                };
                a.n.j.prototype.text = function() {
                    var b = a.a.B(this.j),
                        b = "script" === b ? "text" : "textarea" === b ? "value" : "innerHTML";
                    if (0 == arguments.length) {
                        return this.j[b]
                    }
                    var c = arguments[0];
                    "innerHTML" === b ? a.a.Va(this.j, c) : this.j[b] = c
                };
                var b = a.a.f.L() + "_";
                a.n.j.prototype.data = function(c) {
                    if (1 === arguments.length) {
                        return a.a.f.get(this.j, b + c)
                    }
                    a.a.f.set(this.j, b + c, arguments[1])
                };
                var c = a.a.f.L();
                a.n.Z = function(a) {
                    this.j = a
                };
                a.n.Z.prototype = new a.n.j;
                a.n.Z.prototype.text = function() {
                    if (0 == arguments.length) {
                        var b = a.a.f.get(this.j, c) || {};
                        b.$a === p && b.Ba && (b.$a = b.Ba.innerHTML);
                        return b.$a
                    }
                    a.a.f.set(this.j, c, {
                        $a: arguments[0]
                    })
                };
                a.n.j.prototype.nodes = function() {
                    if (0 == arguments.length) {
                        return (a.a.f.get(this.j, c) || {}).Ba
                    }
                    a.a.f.set(this.j, c, {
                        Ba: arguments[0]
                    })
                };
                a.b("templateSources", a.n);
                a.b("templateSources.domElement", a.n.j);
                a.b("templateSources.anonymousTemplate", a.n.Z)
            })();
            (function() {
                function b(b, c, d) {
                    var e;
                    for (c = a.e.nextSibling(c); b && (e = b) !== c;) {
                        b = a.e.nextSibling(e), d(e, b)
                    }
                }

                function c(c, d) {
                    if (c.length) {
                        var e = c[0],
                            f = c[c.length - 1],
                            h = e.parentNode,
                            m = a.J.instance,
                            q = m.preprocessNode;
                        if (q) {
                            b(e, f, function(a, b) {
                                var c = a.previousSibling,
                                    d = q.call(m, a);
                                d && (a === e && (e = d[0] || b), a === f && (f = d[d.length - 1] || c))
                            });
                            c.length = 0;
                            if (!e) {
                                return
                            }
                            e === f ? c.push(e) : (c.push(e, f), a.a.ea(c, h))
                        }
                        b(e, f, function(b) {
                            1 !== b.nodeType && 8 !== b.nodeType || a.fb(d, b)
                        });
                        b(e, f, function(b) {
                            1 !== b.nodeType && 8 !== b.nodeType || a.w.Ib(b, [d])
                        });
                        a.a.ea(c, h)
                    }
                }

                function d(a) {
                    return a.nodeType ? a : 0 < a.length ? a[0] : null
                }

                function e(b, e, h, n, r) {
                    r = r || {};
                    var m = b && d(b),
                        m = m && m.ownerDocument,
                        q = r.templateEngine || f;
                    a.Za.Ub(h, q, m);
                    h = q.renderTemplate(h, n, r, m);
                    if ("number" != typeof h.length || 0 < h.length && "number" != typeof h[0].nodeType) {
                        throw Error("Template engine must return an array of DOM nodes")
                    }
                    m = !1;
                    switch (e) {
                        case "replaceChildren":
                            a.e.U(b, h);
                            m = !0;
                            break;
                        case "replaceNode":
                            a.a.Bb(b, h);
                            m = !0;
                            break;
                        case "ignoreTargetNode":
                            break;
                        default:
                            throw Error("Unknown renderMode: " + e)
                    }
                    m && (c(h, n), r.afterRender && a.k.t(r.afterRender, null, [h, n.$data]));
                    return h
                }
                var f;
                a.Wa = function(b) {
                    if (b != p && !(b instanceof a.C)) {
                        throw Error("templateEngine must inherit from ko.templateEngine")
                    }
                    f = b
                };
                a.Ta = function(b, c, h, n, r) {
                    h = h || {};
                    if ((h.templateEngine || f) == p) {
                        throw Error("Set a template engine before calling renderTemplate")
                    }
                    r = r || "replaceChildren";
                    if (n) {
                        var m = d(n);
                        return a.h(function() {
                            var f = c && c instanceof a.I ? c : new a.I(a.a.c(c)),
                                p = a.v(b) ? b() : "function" == typeof b ? b(f.$data, f) : b,
                                f = e(n, r, p, f, h);
                            "replaceNode" == r && (n = f, m = d(n))
                        }, null, {
                            Da: function() {
                                return !m || !a.a.Ea(m)
                            },
                            G: m && "replaceNode" == r ? m.parentNode : m
                        })
                    }
                    return a.w.Na(function(d) {
                        a.Ta(b, c, h, d, "replaceNode")
                    })
                };
                a.jc = function(b, d, f, h, r) {
                    function m(a, b) {
                        c(b, s);
                        f.afterRender && f.afterRender(b, a)
                    }

                    function q(a, c) {
                        s = r.createChildContext(a, f.as, function(a) {
                            a.$index = c
                        });
                        var d = "function" == typeof b ? b(a, s) : b;
                        return e(null, "ignoreTargetNode", d, s, f)
                    }
                    var s;
                    return a.h(function() {
                        var b = a.a.c(d) || [];
                        "undefined" == typeof b.length && (b = [b]);
                        b = a.a.la(b, function(b) {
                            return f.includeDestroyed || b === p || null === b || !a.a.c(b._destroy)
                        });
                        a.k.t(a.a.Ua, null, [h, b, q, f, m])
                    }, null, {
                        G: h
                    })
                };
                var h = a.a.f.L();
                a.d.template = {
                    init: function(b, c) {
                        var d = a.a.c(c());
                        "string" == typeof d || d.name ? a.e.da(b) : (d = a.e.childNodes(b), d = a.a.ec(d), (new a.n.Z(b)).nodes(d));
                        return {
                            controlsDescendantBindings: !0
                        }
                    },
                    update: function(b, c, d, e, f) {
                        var m = c(),
                            q;
                        c = a.a.c(m);
                        d = !0;
                        e = null;
                        "string" == typeof c ? c = {} : (m = c.name, "if" in c && (d = a.a.c(c["if"])), d && "ifnot" in c && (d = !a.a.c(c.ifnot)), q = a.a.c(c.data));
                        "foreach" in c ? e = a.jc(m || b, d && c.foreach || [], c, b, f) : d ? (f = "data" in c ? f.createChildContext(q, c.as) : f, e = a.Ta(m || b, f, c, b)) : a.e.da(b);
                        f = e;
                        (q = a.a.f.get(b, h)) && "function" == typeof q.F && q.F();
                        a.a.f.set(b, h, f && f.ga() ? f : p)
                    }
                };
                a.g.aa.template = function(b) {
                    b = a.g.Ra(b);
                    return 1 == b.length && b[0].unknown || a.g.bc(b, "name") ? null : "This template engine does not support anonymous templates nested within its templates"
                };
                a.e.Q.template = !0
            })();
            a.b("setTemplateEngine", a.Wa);
            a.b("renderTemplate", a.Ta);
            a.a.nb = function(a, c, d) {
                if (a.length && c.length) {
                    var e, f, h, g, k;
                    for (e = f = 0;
                         (!d || e < d) && (g = a[f]); ++f) {
                        for (h = 0; k = c[h]; ++h) {
                            if (g.value === k.value) {
                                g.moved = k.index;
                                k.moved = g.index;
                                c.splice(h, 1);
                                e = h = 0;
                                break
                            }
                        }
                        e += h
                    }
                }
            };
            a.a.Aa = function() {
                function b(b, d, e, f, h) {
                    var g = Math.min,
                        k = Math.max,
                        l = [],
                        n, p = b.length,
                        m, q = d.length,
                        s = q - p || 1,
                        t = p + q + 1,
                        u, w, y;
                    for (n = 0; n <= p; n++) {
                        for (w = u, l.push(u = []), y = g(q, n + s), m = k(0, n - 1); m <= y; m++) {
                            u[m] = m ? n ? b[n - 1] === d[m - 1] ? w[m - 1] : g(w[m] || t, u[m - 1] || t) + 1 : m + 1 : n + 1
                        }
                    }
                    g = [];
                    k = [];
                    s = [];
                    n = p;
                    for (m = q; n || m;) {
                        q = l[n][m] - 1, m && q === l[n][m - 1] ? k.push(g[g.length] = {
                            status: e,
                            value: d[--m],
                            index: m
                        }) : n && q === l[n - 1][m] ? s.push(g[g.length] = {
                            status: f,
                            value: b[--n],
                            index: n
                        }) : (--m, --n, h.sparse || g.push({
                            status: "retained",
                            value: d[m]
                        }))
                    }
                    a.a.nb(k, s, 10 * p);
                    return g.reverse()
                }
                return function(a, d, e) {
                    e = "boolean" === typeof e ? {
                        dontLimitMoves: e
                    } : e || {};
                    a = a || [];
                    d = d || [];
                    return a.length <= d.length ? b(a, d, "added", "deleted", e) : b(d, a, "deleted", "added", e)
                }
            }();
            a.b("utils.compareArrays", a.a.Aa);
            (function() {
                function b(b, c, f, h, g) {
                    var k = [],
                        l = a.h(function() {
                            var l = c(f, g, a.a.ea(k, b)) || [];
                            0 < k.length && (a.a.Bb(k, l), h && a.k.t(h, null, [f, l, g]));
                            k.length = 0;
                            a.a.$(k, l)
                        }, null, {
                            G: b,
                            Da: function() {
                                return !a.a.eb(k)
                            }
                        });
                    return {
                        S: k,
                        h: l.ga() ? l : p
                    }
                }
                var c = a.a.f.L();
                a.a.Ua = function(d, e, f, h, g) {
                    function k(b, c) {
                        v = r[c];
                        u !== c && (z[b] = v);
                        v.Ia(u++);
                        a.a.ea(v.S, d);
                        s.push(v);
                        y.push(v)
                    }

                    function l(b, c) {
                        if (b) {
                            for (var d = 0, e = c.length; d < e; d++) {
                                c[d] && a.a.r(c[d].S, function(a) {
                                    b(a, d, c[d].ka)
                                })
                            }
                        }
                    }
                    e = e || [];
                    h = h || {};
                    var n = a.a.f.get(d, c) === p,
                        r = a.a.f.get(d, c) || [],
                        m = a.a.ya(r, function(a) {
                            return a.ka
                        }),
                        q = a.a.Aa(m, e, h.dontLimitMoves),
                        s = [],
                        t = 0,
                        u = 0,
                        w = [],
                        y = [];
                    e = [];
                    for (var z = [], m = [], v, x = 0, A, C; A = q[x]; x++) {
                        switch (C = A.moved, A.status) {
                            case "deleted":
                                C === p && (v = r[t], v.h && v.h.F(), w.push.apply(w, a.a.ea(v.S, d)), h.beforeRemove && (e[x] = v, y.push(v)));
                                t++;
                                break;
                            case "retained":
                                k(x, t++);
                                break;
                            case "added":
                                C !== p ? k(x, C) : (v = {
                                    ka: A.value,
                                    Ia: a.m(u++)
                                }, s.push(v), y.push(v), n || (m[x] = v))
                        }
                    }
                    l(h.beforeMove, z);
                    a.a.r(w, h.beforeRemove ? a.M : a.removeNode);
                    for (var x = 0, n = a.e.firstChild(d), E; v = y[x]; x++) {
                        v.S || a.a.extend(v, b(d, f, v.ka, g, v.Ia));
                        for (t = 0; q = v.S[t]; n = q.nextSibling, E = q, t++) {
                            q !== n && a.e.rb(d, q, E)
                        }!v.Zb && g && (g(v.ka, v.S, v.Ia), v.Zb = !0)
                    }
                    l(h.beforeRemove, e);
                    l(h.afterMove, z);
                    l(h.afterAdd, m);
                    a.a.f.set(d, c, s)
                }
            })();
            a.b("utils.setDomNodeChildrenFromArrayMapping", a.a.Ua);
            a.K = function() {
                this.allowTemplateRewriting = !1
            };
            a.K.prototype = new a.C;
            a.K.prototype.renderTemplateSource = function(b) {
                var c = (9 > a.a.oa ? 0 : b.nodes) ? b.nodes() : null;
                if (c) {
                    return a.a.R(c.cloneNode(!0).childNodes)
                }
                b = b.text();
                return a.a.Qa(b)
            };
            a.K.Ja = new a.K;
            a.Wa(a.K.Ja);
            a.b("nativeTemplateEngine", a.K);
            (function() {
                a.La = function() {
                    var a = this.ac = function() {
                        if (!t || !t.tmpl) {
                            return 0
                        }
                        try {
                            if (0 <= t.tmpl.tag.tmpl.open.toString().indexOf("__")) {
                                return 2
                            }
                        } catch (a) {}
                        return 1
                    }();
                    this.renderTemplateSource = function(b, e, f) {
                        f = f || {};
                        if (2 > a) {
                            throw Error("Your version of jQuery.tmpl is too old. Please upgrade to jQuery.tmpl 1.0.0pre or later.")
                        }
                        var h = b.data("precompiled");
                        h || (h = b.text() || "", h = t.template(null, "{{ko_with $item.koBindingContext}}" + h + "{{/ko_with}}"), b.data("precompiled", h));
                        b = [e.$data];
                        e = t.extend({
                            koBindingContext: e
                        }, f.templateOptions);
                        e = t.tmpl(h, b, e);
                        e.appendTo(w.createElement("div"));
                        t.fragments = {};
                        return e
                    };
                    this.createJavaScriptEvaluatorBlock = function(a) {
                        return "{{ko_code ((function() { return " + a + " })()) }}"
                    };
                    this.addTemplate = function(a, b) {
                        w.write("<script type='text/html' id='" + a + "'>" + b + "\x3c/script>")
                    };
                    0 < a && (t.tmpl.tag.ko_code = {
                        open: "__.push($1 || '');"
                    }, t.tmpl.tag.ko_with = {
                        open: "with($1) {",
                        close: "} "
                    })
                };
                a.La.prototype = new a.C;
                var b = new a.La;
                0 < b.ac && a.Wa(b);
                a.b("jqueryTmplTemplateEngine", a.La)
            })()
        })
    })()
})(); /* jQuery Migrate v1.2.1 | (c) 2005, 2013 jQuery Foundation, Inc. and other contributors | jquery.org/license */
jQuery.migrateMute === void 0 && (jQuery.migrateMute = !0),
    function(G, af, X) {
        function ac(a) {
            var b = af.console;
            O[a] || (O[a] = !0, G.migrateWarnings.push(a), b && b.warn && !G.migrateMute && (b.warn("JQMIGRATE: " + a), G.migrateTrace && b.trace && b.trace()))
        }

        function q(f, b, c, d) {
            if (Object.defineProperty) {
                try {
                    return Object.defineProperty(f, b, {
                        configurable: !0,
                        enumerable: !0,
                        get: function() {
                            return ac(d), c
                        },
                        set: function(a) {
                            ac(d), c = a
                        }
                    }), X
                } catch (e) {}
            }
            G._definePropertyBroken = !0, f[b] = c
        }
        var O = {};
        G.migrateWarnings = [], !G.migrateMute && af.console && af.console.log && af.console.log("JQMIGRATE: Logging is active"), G.migrateTrace === X && (G.migrateTrace = !0), G.migrateReset = function() {
            O = {}, G.migrateWarnings.length = 0
        }, "BackCompat" === document.compatMode && ac("jQuery is not compatible with Quirks Mode");
        var Z = G("<input/>", {
                    size: 1
                }).attr("size") && G.attrFn,
            ad = G.attr,
            ah = G.attrHooks.value && G.attrHooks.value.get || function() {
                    return null
                }, D = G.attrHooks.value && G.attrHooks.value.set || function() {
                    return X
                }, U = /^(?:input|button)$/i,
            F = /^[238]$/,
            aa = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
            I = /^(?:checked|selected)$/i;
        q(G, "attrFn", Z || {}, "jQuery.attrFn is deprecated"), G.attr = function(h, b, f, j) {
            var d = b.toLowerCase(),
                e = h && h.nodeType;
            return j && (4 > ad.length && ac("jQuery.fn.attr( props, pass ) is deprecated"), h && !F.test(e) && (Z ? b in Z : G.isFunction(G.fn[b]))) ? G(h)[b](f) : ("type" === b && f !== X && U.test(h.nodeName) && h.parentNode && ac("Can't change the 'type' of an input or button in IE 6/7/8"), !G.attrHooks[d] && aa.test(d) && (G.attrHooks[d] = {
                get: function(l, k) {
                    var c, g = G.prop(l, k);
                    return g === !0 || "boolean" != typeof g && (c = l.getAttributeNode(k)) && c.nodeValue !== !1 ? k.toLowerCase() : X
                },
                set: function(k, g, i) {
                    var c;
                    return g === !1 ? G.removeAttr(k, i) : (c = G.propFix[i] || i, c in k && (k[c] = !0), k.setAttribute(i, i.toLowerCase())), i
                }
            }, I.test(d) && ac("jQuery.fn.attr('" + d + "') may use property instead of attribute")), ad.call(G, h, b, f))
        }, G.attrHooks.value = {
            get: function(a, c) {
                var b = (a.nodeName || "").toLowerCase();
                return "button" === b ? ah.apply(this, arguments) : ("input" !== b && "option" !== b && ac("jQuery.fn.attr('value') no longer gets properties"), c in a ? a.value : null)
            },
            set: function(c, d) {
                var b = (c.nodeName || "").toLowerCase();
                return "button" === b ? D.apply(this, arguments) : ("input" !== b && "option" !== b && ac("jQuery.fn.attr('value', val) no longer sets properties"), c.value = d, X)
            }
        };
        var J, K, ai = G.fn.init,
            V = G.parseJSON,
            al = /^([^<]*)(<[\w\W]+>)([^>]*)$/;
        G.fn.init = function(e, d, b) {
            var c;
            return e && "string" == typeof e && !G.isPlainObject(d) && (c = al.exec(G.trim(e))) && c[0] && ("<" !== e.charAt(0) && ac("$(html) HTML strings must start with '<' character"), c[3] && ac("$(html) HTML text after last tag is ignored"), "#" === c[0].charAt(0) && (ac("HTML string cannot start with a '#' character"), G.error("JQMIGRATE: Invalid selector string (XSS)")), d && d.context && (d = d.context), G.parseHTML) ? ai.call(this, G.parseHTML(c[2], d, !0), d, b) : ai.apply(this, arguments)
        }, G.fn.init.prototype = G.fn, G.parseJSON = function(a) {
            return a || null === a ? V.apply(this, arguments) : (ac("jQuery.parseJSON requires a valid JSON string"), null)
        }, G.uaMatch = function(a) {
            a = a.toLowerCase();
            var b = /(chrome)[ \/]([\w.]+)/.exec(a) || /(webkit)[ \/]([\w.]+)/.exec(a) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(a) || /(msie) ([\w.]+)/.exec(a) || 0 > a.indexOf("compatible") && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(a) || [];
            return {
                browser: b[1] || "",
                version: b[2] || "0"
            }
        }, G.browser || (J = G.uaMatch(navigator.userAgent), K = {}, J.browser && (K[J.browser] = !0, K.version = J.version), K.chrome ? K.webkit = !0 : K.webkit && (K.safari = !0), G.browser = K), q(G, "browser", G.browser, "jQuery.browser is deprecated"), G.sub = function() {
            function b(c, d) {
                return new b.fn.init(c, d)
            }
            G.extend(!0, b, this), b.superclass = this, b.fn = b.prototype = this(), b.fn.constructor = b, b.sub = this.sub, b.fn.init = function(d, c) {
                return c && c instanceof G && !(c instanceof b) && (c = b(c)), G.fn.init.call(this, d, c, a)
            }, b.fn.init.prototype = b.fn;
            var a = b(document);
            return ac("jQuery.sub() is deprecated"), b
        }, G.ajaxSetup({
            converters: {
                "text json": G.parseJSON
            }
        });
        var B = G.fn.data;
        G.fn.data = function(e) {
            var b, c, d = this[0];
            return !d || "events" !== e || 1 !== arguments.length || (b = G.data(d, e), c = G._data(d, e), b !== X && b !== c || c === X) ? B.apply(this, arguments) : (ac("Use of jQuery.fn.data('events') is deprecated"), c)
        };
        var P = /\/(java|ecma)script/i,
            aj = G.fn.andSelf || G.fn.addBack;
        G.fn.andSelf = function() {
            return ac("jQuery.fn.andSelf() replaced by jQuery.fn.addBack()"), aj.apply(this, arguments)
        }, G.clean || (G.clean = function(m, b, g, j) {
            b = b || document, b = !b.nodeType && b[0] || b, b = b.ownerDocument || b, ac("jQuery.clean() is deprecated");
            var k, n, e, h, f = [];
            if (G.merge(f, G.buildFragment(m, b).childNodes), g) {
                for (e = function(a) {
                    return !a.type || P.test(a.type) ? j ? j.push(a.parentNode ? a.parentNode.removeChild(a) : a) : g.appendChild(a) : X
                }, k = 0; null != (n = f[k]); k++) {
                    G.nodeName(n, "script") && e(n) || (g.appendChild(n), n.getElementsByTagName !== X && (h = G.grep(G.merge([], n.getElementsByTagName("script")), e), f.splice.apply(f, [k + 1, 0].concat(h)), k += h.length))
                }
            }
            return f
        });
        var ab = G.event.add,
            ak = G.event.remove,
            R = G.event.trigger,
            Y = G.fn.toggle,
            ag = G.fn.live,
            W = G.fn.die,
            ae = "ajaxStart|ajaxStop|ajaxSend|ajaxComplete|ajaxError|ajaxSuccess",
            E = RegExp("\\b(?:" + ae + ")\\b"),
            L = /(?:^|\s)hover(\.\S+|)\b/,
            z = function(a) {
                return "string" != typeof a || G.event.special.hover ? a : (L.test(a) && ac("'hover' pseudo-event is deprecated, use 'mouseenter mouseleave'"), a && a.replace(L, "mouseenter$1 mouseleave$1"))
            };
        G.event.props && "attrChange" !== G.event.props[0] && G.event.props.unshift("attrChange", "attrName", "relatedNode", "srcElement"), G.event.dispatch && q(G.event, "handle", G.event.dispatch, "jQuery.event.handle is undocumented and deprecated"), G.event.add = function(c, g, f, b, d) {
            c !== document && E.test(g) && ac("AJAX events should be attached to document: " + g), ab.call(this, c, z(g || ""), f, b, d)
        }, G.event.remove = function(c, g, d, f, b) {
            ak.call(this, c, z(g) || "", d, f, b)
        }, G.fn.error = function() {
            var a = Array.prototype.slice.call(arguments, 0);
            return ac("jQuery.fn.error() is deprecated"), a.splice(0, 0, "error"), arguments.length ? this.bind.apply(this, a) : (this.triggerHandler.apply(this, a), this)
        }, G.fn.toggle = function(g, d) {
            if (!G.isFunction(g) || !G.isFunction(d)) {
                return Y.apply(this, arguments)
            }
            ac("jQuery.fn.toggle(handler, handler...) is deprecated");
            var b = arguments,
                c = g.guid || G.guid++,
                e = 0,
                f = function(a) {
                    var h = (G._data(this, "lastToggle" + g.guid) || 0) % e;
                    return G._data(this, "lastToggle" + g.guid, h + 1), a.preventDefault(), b[h].apply(this, arguments) || !1
                };
            for (f.guid = c; b.length > e;) {
                b[e++].guid = c
            }
            return this.click(f)
        }, G.fn.live = function(d, c, b) {
            return ac("jQuery.fn.live() is deprecated"), ag ? ag.apply(this, arguments) : (G(this.context).on(d, this.selector, c, b), this)
        }, G.fn.die = function(b, a) {
            return ac("jQuery.fn.die() is deprecated"), W ? W.apply(this, arguments) : (G(this.context).off(b, this.selector || "**", a), this)
        }, G.event.trigger = function(c, f, d, b) {
            return d || E.test(c) || ac("Global events are undocumented and deprecated"), R.call(this, c, f, d || document, b)
        }, G.each(ae.split("|"), function(b, a) {
            G.event.special[a] = {
                setup: function() {
                    var c = this;
                    return c !== document && (G.event.add(document, a + "." + G.guid, function() {
                        G.event.trigger(a, null, c, !0)
                    }), G._data(this, a, G.guid++)), !1
                },
                teardown: function() {
                    return this !== document && G.event.remove(document, a + "." + G._data(this, a)), !1
                }
            }
        })
    }(jQuery, window);
var K2General = (function(c, a) {
    var b = false;
    var d = a({});
    c.TriggerScriptLoaded = function() {
        a(document).ready(function() {
            d.dequeue("asyncScripts");
            b = true
        })
    };
    c.RegisterToOnReady = function(e) {
        a(document).on("ready", e)
    };
    c.RegisterToScriptsLoaded = function(e) {
        if (!b) {
            d.queue("asyncScripts", function(f) {
                e();
                f()
            })
        } else {
            e()
        }
    };
    return c
}(K2General || {}, jQuery));
/*
 * MediaElement.js
 * HTML5 <video> and <audio> shim and player
 * http://mediaelementjs.com/
 *
 * Creates a JavaScript object that mimics HTML5 MediaElement API
 * for browsers that don't understand HTML5 or can't play the provided codec
 * Can play MP4 (H.264), Ogg, WebM, FLV, WMV, WMA, ACC, and MP3
 *
 * Copyright 2010-2012, John Dyer (http://j.hn)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 */
var mejs = mejs || {};
mejs.version = "2.10.1";
mejs.meIndex = 0;
mejs.plugins = {
    silverlight: [{
        version: [3, 0],
        types: ["video/mp4", "video/m4v", "video/mov", "video/wmv", "audio/wma", "audio/m4a", "audio/mp3", "audio/wav", "audio/mpeg"]
    }],
    flash: [{
        version: [9, 0, 124],
        types: ["video/mp4", "video/m4v", "video/mov", "video/flv", "video/rtmp", "video/x-flv", "audio/flv", "audio/x-flv", "audio/mp3", "audio/m4a", "audio/mpeg", "video/youtube", "video/x-youtube"]
    }],
    youtube: [{
        version: null,
        types: ["video/youtube", "video/x-youtube"]
    }],
    vimeo: [{
        version: null,
        types: ["video/vimeo", "video/x-vimeo"]
    }]
};
mejs.Utility = {
    encodeUrl: function(b) {
        return encodeURIComponent(b)
    },
    escapeHTML: function(b) {
        return b.toString().split("&").join("&amp;").split("<").join("&lt;").split('"').join("&quot;")
    },
    absolutizeUrl: function(c) {
        var d = document.createElement("div");
        d.innerHTML = '<a href="' + this.escapeHTML(c) + '">x</a>';
        return d.firstChild.href
    },
    getScriptPath: function(j) {
        for (var k = 0, m, n = "", o = "", q, p = document.getElementsByTagName("script"), r = p.length, s = j.length; k < r; k++) {
            q = p[k].src;
            for (m = 0; m < s; m++) {
                o = j[m];
                if (q.indexOf(o) > -1) {
                    n = q.substring(0, q.indexOf(o));
                    break
                }
            }
            if (n !== "") {
                break
            }
        }
        return n
    },
    secondsToTimeCode: function(h, j, k, l) {
        if (typeof k == "undefined") {
            k = false
        } else {
            if (typeof l == "undefined") {
                l = 25
            }
        }
        var m = Math.floor(h / 3600) % 24,
            o = Math.floor(h / 60) % 60,
            n = Math.floor(h % 60);
        h = Math.floor((h % 1 * l).toFixed(3));
        return (j || m > 0 ? (m < 10 ? "0" + m : m) + ":" : "") + (o < 10 ? "0" + o : o) + ":" + (n < 10 ? "0" + n : n) + (k ? ":" + (h < 10 ? "0" + h : h) : "")
    },
    timeCodeToSeconds: function(j, k, l, m) {
        if (typeof l == "undefined") {
            l = false
        } else {
            if (typeof m == "undefined") {
                m = 25
            }
        }
        j = j.split(":");
        k = parseInt(j[0], 10);
        var n = parseInt(j[1], 10),
            p = parseInt(j[2], 10),
            o = 0,
            q = 0;
        if (l) {
            o = parseInt(j[3]) / m
        }
        return q = k * 3600 + n * 60 + p + o
    },
    convertSMPTEtoSeconds: function(f) {
        if (typeof f != "string") {
            return false
        }
        f = f.replace(",", ".");
        var g = 0,
            h = f.indexOf(".") != -1 ? f.split(".")[1].length : 0,
            j = 1;
        f = f.split(":").reverse();
        for (var k = 0; k < f.length; k++) {
            j = 1;
            if (k > 0) {
                j = Math.pow(60, k)
            }
            g += Number(f[k]) * j
        }
        return Number(g.toFixed(h))
    },
    removeSwf: function(c) {
        var d = document.getElementById(c);
        if (d && d.nodeName == "OBJECT") {
            if (mejs.MediaFeatures.isIE) {
                d.style.display = "none";
                (function() {
                    d.readyState == 4 ? mejs.Utility.removeObjectInIE(c) : setTimeout(arguments.callee, 10)
                })()
            } else {
                d.parentNode.removeChild(d)
            }
        }
    },
    removeObjectInIE: function(c) {
        if (c = document.getElementById(c)) {
            for (var d in c) {
                if (typeof c[d] == "function") {
                    c[d] = null
                }
            }
            c.parentNode.removeChild(c)
        }
    }
};
mejs.PluginDetector = {
    hasPluginVersion: function(d, e) {
        var f = this.plugins[d];
        e[1] = e[1] || 0;
        e[2] = e[2] || 0;
        return f[0] > e[0] || f[0] == e[0] && f[1] > e[1] || f[0] == e[0] && f[1] == e[1] && f[2] >= e[2] ? true : false
    },
    nav: window.navigator,
    ua: window.navigator.userAgent.toLowerCase(),
    plugins: [],
    addPlugin: function(f, g, h, j, k) {
        this.plugins[f] = this.detectPlugin(g, h, j, k)
    },
    detectPlugin: function(h, j, k, l) {
        var m = [0, 0, 0],
            o;
        if (typeof this.nav.plugins != "undefined" && typeof this.nav.plugins[h] == "object") {
            if ((k = this.nav.plugins[h].description) && !(typeof this.nav.mimeTypes != "undefined" && this.nav.mimeTypes[j] && !this.nav.mimeTypes[j].enabledPlugin)) {
                m = k.replace(h, "").replace(/^\s+/, "").replace(/\sr/gi, ".").split(".");
                for (h = 0; h < m.length; h++) {
                    m[h] = parseInt(m[h].match(/\d+/), 10)
                }
            }
        } else {
            if (typeof window.ActiveXObject != "undefined") {
                try {
                    if (o = new ActiveXObject(k)) {
                        m = l(o)
                    }
                } catch (n) {}
            }
        }
        return m
    }
};
mejs.PluginDetector.addPlugin("flash", "Shockwave Flash", "application/x-shockwave-flash", "ShockwaveFlash.ShockwaveFlash", function(c) {
    var d = [];
    if (c = c.GetVariable("$version")) {
        c = c.split(" ")[1].split(",");
        d = [parseInt(c[0], 10), parseInt(c[1], 10), parseInt(c[2], 10)]
    }
    return d
});
mejs.PluginDetector.addPlugin("silverlight", "Silverlight Plug-In", "application/x-silverlight-2", "AgControl.AgControl", function(d) {
    var e = [0, 0, 0, 0],
        f = function(a, b, h, c) {
            for (; a.isVersionSupported(b[0] + "." + b[1] + "." + b[2] + "." + b[3]);) {
                b[h] += c
            }
            b[h] -= c
        };
    f(d, e, 0, 1);
    f(d, e, 1, 1);
    f(d, e, 2, 10000);
    f(d, e, 2, 1000);
    f(d, e, 2, 100);
    f(d, e, 2, 10);
    f(d, e, 2, 1);
    f(d, e, 3, 1);
    return e
});
mejs.MediaFeatures = {
    init: function() {
        var f = this,
            h = document,
            j = mejs.PluginDetector.nav,
            k = mejs.PluginDetector.ua.toLowerCase(),
            l, m = ["source", "track", "audio", "video"];
        f.isiPad = k.match(/ipad/i) !== null;
        f.isiPhone = k.match(/iphone/i) !== null;
        f.isiOS = f.isiPhone || f.isiPad;
        f.isAndroid = k.match(/android/i) !== null;
        f.isBustedAndroid = k.match(/android 2\.[12]/) !== null;
        f.isIE = j.appName.toLowerCase().indexOf("microsoft") != -1;
        f.isChrome = k.match(/chrome/gi) !== null;
        f.isFirefox = k.match(/firefox/gi) !== null;
        f.isWebkit = k.match(/webkit/gi) !== null;
        f.isGecko = k.match(/gecko/gi) !== null && !f.isWebkit;
        f.isOpera = k.match(/opera/gi) !== null;
        f.hasTouch = "ontouchstart" in window;
        f.svg = !! document.createElementNS && !! document.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGRect;
        for (j = 0; j < m.length; j++) {
            l = document.createElement(m[j])
        }
        f.supportsMediaTag = typeof l.canPlayType !== "undefined" || f.isBustedAndroid;
        f.hasSemiNativeFullScreen = typeof l.webkitEnterFullscreen !== "undefined";
        f.hasWebkitNativeFullScreen = typeof l.webkitRequestFullScreen !== "undefined";
        f.hasMozNativeFullScreen = typeof l.mozRequestFullScreen !== "undefined";
        f.hasTrueNativeFullScreen = f.hasWebkitNativeFullScreen || f.hasMozNativeFullScreen;
        f.nativeFullScreenEnabled = f.hasTrueNativeFullScreen;
        if (f.hasMozNativeFullScreen) {
            f.nativeFullScreenEnabled = l.mozFullScreenEnabled
        }
        if (this.isChrome) {
            f.hasSemiNativeFullScreen = false
        }
        if (f.hasTrueNativeFullScreen) {
            f.fullScreenEventName = f.hasWebkitNativeFullScreen ? "webkitfullscreenchange" : "mozfullscreenchange";
            f.isFullScreen = function() {
                if (l.mozRequestFullScreen) {
                    return h.mozFullScreen
                } else {
                    if (l.webkitRequestFullScreen) {
                        return h.webkitIsFullScreen
                    }
                }
            };
            f.requestFullScreen = function(a) {
                if (f.hasWebkitNativeFullScreen) {
                    a.webkitRequestFullScreen()
                } else {
                    f.hasMozNativeFullScreen && a.mozRequestFullScreen()
                }
            };
            f.cancelFullScreen = function() {
                if (f.hasWebkitNativeFullScreen) {
                    document.webkitCancelFullScreen()
                } else {
                    f.hasMozNativeFullScreen && document.mozCancelFullScreen()
                }
            }
        }
        if (f.hasSemiNativeFullScreen && k.match(/mac os x 10_5/i)) {
            f.hasNativeFullScreen = false;
            f.hasSemiNativeFullScreen = false
        }
    }
};
mejs.MediaFeatures.init();
mejs.HtmlMediaElement = {
    pluginType: "native",
    isFullScreen: false,
    setCurrentTime: function(b) {
        this.currentTime = b
    },
    setMuted: function(b) {
        this.muted = b
    },
    setVolume: function(b) {
        this.volume = b
    },
    stop: function() {
        this.pause()
    },
    setSrc: function(d) {
        for (var e = this.getElementsByTagName("source"); e.length > 0;) {
            this.removeChild(e[0])
        }
        if (typeof d == "string") {
            this.src = d
        } else {
            var f;
            for (e = 0; e < d.length; e++) {
                f = d[e];
                if (this.canPlayType(f.type)) {
                    this.src = f.src;
                    break
                }
            }
        }
    },
    setVideoSize: function(c, d) {
        this.width = c;
        this.height = d
    }
};
mejs.PluginMediaElement = function(d, e, f) {
    this.id = d;
    this.pluginType = e;
    this.src = f;
    this.events = {}
};
mejs.PluginMediaElement.prototype = {
    pluginElement: null,
    pluginType: "",
    isFullScreen: false,
    playbackRate: -1,
    defaultPlaybackRate: -1,
    seekable: [],
    played: [],
    paused: true,
    ended: false,
    seeking: false,
    duration: 0,
    error: null,
    tagName: "",
    muted: false,
    volume: 1,
    currentTime: 0,
    play: function() {
        if (this.pluginApi != null) {
            this.pluginType == "youtube" ? this.pluginApi.playVideo() : this.pluginApi.playMedia();
            this.paused = false
        }
    },
    load: function() {
        if (this.pluginApi != null) {
            this.pluginType != "youtube" && this.pluginApi.loadMedia();
            this.paused = false
        }
    },
    pause: function() {
        if (this.pluginApi != null) {
            this.pluginType == "youtube" ? this.pluginApi.pauseVideo() : this.pluginApi.pauseMedia();
            this.paused = true
        }
    },
    stop: function() {
        if (this.pluginApi != null) {
            this.pluginType == "youtube" ? this.pluginApi.stopVideo() : this.pluginApi.stopMedia();
            this.paused = true
        }
    },
    canPlayType: function(f) {
        var g, h, j, k = mejs.plugins[this.pluginType];
        for (g = 0; g < k.length; g++) {
            j = k[g];
            if (mejs.PluginDetector.hasPluginVersion(this.pluginType, j.version)) {
                for (h = 0; h < j.types.length; h++) {
                    if (f == j.types[h]) {
                        return true
                    }
                }
            }
        }
        return false
    },
    positionFullscreenButton: function(d, e, f) {
        this.pluginApi != null && this.pluginApi.positionFullscreenButton && this.pluginApi.positionFullscreenButton(d, e, f)
    },
    hideFullscreenButton: function() {
        this.pluginApi != null && this.pluginApi.hideFullscreenButton && this.pluginApi.hideFullscreenButton()
    },
    setSrc: function(d) {
        if (typeof d == "string") {
            this.pluginApi.setSrc(mejs.Utility.absolutizeUrl(d));
            this.src = mejs.Utility.absolutizeUrl(d)
        } else {
            var e, f;
            for (e = 0; e < d.length; e++) {
                f = d[e];
                if (this.canPlayType(f.type)) {
                    this.pluginApi.setSrc(mejs.Utility.absolutizeUrl(f.src));
                    this.src = mejs.Utility.absolutizeUrl(d);
                    break
                }
            }
        }
    },
    setCurrentTime: function(b) {
        if (this.pluginApi != null) {
            this.pluginType == "youtube" ? this.pluginApi.seekTo(b) : this.pluginApi.setCurrentTime(b);
            this.currentTime = b
        }
    },
    setVolume: function(b) {
        if (this.pluginApi != null) {
            this.pluginType == "youtube" ? this.pluginApi.setVolume(b * 100) : this.pluginApi.setVolume(b);
            this.volume = b
        }
    },
    setMuted: function(b) {
        if (this.pluginApi != null) {
            if (this.pluginType == "youtube") {
                b ? this.pluginApi.mute() : this.pluginApi.unMute();
                this.muted = b;
                this.dispatchEvent("volumechange")
            } else {
                this.pluginApi.setMuted(b)
            }
            this.muted = b
        }
    },
    setVideoSize: function(c, d) {
        if (this.pluginElement.style) {
            this.pluginElement.style.width = c + "px";
            this.pluginElement.style.height = d + "px"
        }
        this.pluginApi != null && this.pluginApi.setVideoSize && this.pluginApi.setVideoSize(c, d)
    },
    setFullscreen: function(b) {
        this.pluginApi != null && this.pluginApi.setFullscreen && this.pluginApi.setFullscreen(b)
    },
    enterFullScreen: function() {
        this.pluginApi != null && this.pluginApi.setFullscreen && this.setFullscreen(true)
    },
    exitFullScreen: function() {
        this.pluginApi != null && this.pluginApi.setFullscreen && this.setFullscreen(false)
    },
    addEventListener: function(c, d) {
        this.events[c] = this.events[c] || [];
        this.events[c].push(d)
    },
    removeEventListener: function(d, e) {
        if (!d) {
            this.events = {};
            return true
        }
        var f = this.events[d];
        if (!f) {
            return true
        }
        if (!e) {
            this.events[d] = [];
            return true
        }
        for (i = 0; i < f.length; i++) {
            if (f[i] === e) {
                this.events[d].splice(i, 1);
                return true
            }
        }
        return false
    },
    dispatchEvent: function(e) {
        var f, g, h = this.events[e];
        if (h) {
            g = Array.prototype.slice.call(arguments, 1);
            for (f = 0; f < h.length; f++) {
                h[f].apply(null, g)
            }
        }
    },
    attributes: {},
    hasAttribute: function(b) {
        return b in this.attributes
    },
    removeAttribute: function(b) {
        delete this.attributes[b]
    },
    getAttribute: function(b) {
        if (this.hasAttribute(b)) {
            return this.attributes[b]
        }
        return ""
    },
    setAttribute: function(c, d) {
        this.attributes[c] = d
    },
    remove: function() {
        mejs.Utility.removeSwf(this.pluginElement.id)
    }
};
mejs.MediaPluginBridge = {
    pluginMediaElements: {},
    htmlMediaElements: {},
    registerPluginElement: function(d, e, f) {
        this.pluginMediaElements[d] = e;
        this.htmlMediaElements[d] = f
    },
    initPlugin: function(d) {
        var e = this.pluginMediaElements[d],
            f = this.htmlMediaElements[d];
        if (e) {
            switch (e.pluginType) {
                case "flash":
                    e.pluginElement = e.pluginApi = document.getElementById(d);
                    break;
                case "silverlight":
                    e.pluginElement = document.getElementById(e.id);
                    e.pluginApi = e.pluginElement.Content.MediaElementJS
            }
            e.pluginApi != null && e.success && e.success(e, f)
        }
    },
    fireEvent: function(f, g, h) {
        var j, k;
        f = this.pluginMediaElements[f];
        g = {
            type: g,
            target: f
        };
        for (j in h) {
            f[j] = h[j];
            g[j] = h[j]
        }
        k = h.bufferedTime || 0;
        g.target.buffered = g.buffered = {
            start: function() {
                return 0
            },
            end: function() {
                return k
            },
            length: 1
        };
        f.dispatchEvent(g.type, g)
    }
};
mejs.MediaElementDefaults = {
    mode: "auto",
    plugins: ["flash", "silverlight", "youtube", "vimeo"],
    enablePluginDebug: false,
    type: "",
    pluginPath: mejs.Utility.getScriptPath(["mediaelement.js", "mediaelement.min.js", "mediaelement-and-player.js", "mediaelement-and-player.min.js"]),
    flashName: "flashmediaelement.swf",
    flashStreamer: "",
    enablePluginSmoothing: false,
    silverlightName: "silverlightmediaelement.xap",
    defaultVideoWidth: 480,
    defaultVideoHeight: 270,
    pluginWidth: -1,
    pluginHeight: -1,
    pluginVars: [],
    timerRate: 250,
    startVolume: 0.8,
    success: function() {},
    error: function() {}
};
mejs.MediaElement = function(c, d) {
    return mejs.HtmlMediaElementShim.create(c, d)
};
mejs.HtmlMediaElementShim = {
    create: function(m, n) {
        var o = mejs.MediaElementDefaults,
            p = typeof m == "string" ? document.getElementById(m) : m,
            q = p.tagName.toLowerCase(),
            s = q === "audio" || q === "video",
            r = s ? p.getAttribute("src") : p.getAttribute("href");
        q = p.getAttribute("poster");
        var t = p.getAttribute("autoplay"),
            w = p.getAttribute("preload"),
            u = p.getAttribute("controls"),
            v;
        for (v in n) {
            o[v] = n[v]
        }
        r = typeof r == "undefined" || r === null || r == "" ? null : r;
        q = typeof q == "undefined" || q === null ? "" : q;
        w = typeof w == "undefined" || w === null || w === "false" ? "none" : w;
        t = !(typeof t == "undefined" || t === null || t === "false");
        u = !(typeof u == "undefined" || u === null || u === "false");
        v = this.determinePlayback(p, o, mejs.MediaFeatures.supportsMediaTag, s, r);
        v.url = v.url !== null ? mejs.Utility.absolutizeUrl(v.url) : "";
        if (v.method == "native") {
            if (mejs.MediaFeatures.isBustedAndroid) {
                p.src = v.url;
                p.addEventListener("click", function() {
                    p.play()
                }, false)
            }
            return this.updateNative(v, o, t, w)
        } else {
            if (v.method !== "") {
                return this.createPlugin(v, o, q, t, w, u)
            } else {
                this.createErrorMessage(v, o, q);
                return this
            }
        }
    },
    determinePlayback: function(m, n, o, p, q) {
        var s = [],
            r, t, w, u = {
                method: "",
                url: "",
                htmlMediaElement: m,
                isVideo: m.tagName.toLowerCase() != "audio"
            }, v;
        if (typeof n.type != "undefined" && n.type !== "") {
            if (typeof n.type == "string") {
                s.push({
                    type: n.type,
                    url: q
                })
            } else {
                for (r = 0; r < n.type.length; r++) {
                    s.push({
                        type: n.type[r],
                        url: q
                    })
                }
            }
        } else {
            if (q !== null) {
                w = this.formatType(q, m.getAttribute("type"));
                s.push({
                    type: w,
                    url: q
                })
            } else {
                for (r = 0; r < m.childNodes.length; r++) {
                    t = m.childNodes[r];
                    if (t.nodeType == 1 && t.tagName.toLowerCase() == "source") {
                        q = t.getAttribute("src");
                        w = this.formatType(q, t.getAttribute("type"));
                        t = t.getAttribute("media");
                        if (!t || !window.matchMedia || window.matchMedia && window.matchMedia(t).matches) {
                            s.push({
                                type: w,
                                url: q
                            })
                        }
                    }
                }
            }
        } if (!p && s.length > 0 && s[0].url !== null && this.getTypeFromFile(s[0].url).indexOf("audio") > -1) {
            u.isVideo = false
        }
        if (mejs.MediaFeatures.isBustedAndroid) {
            m.canPlayType = function(a) {
                return a.match(/video\/(mp4|m4v)/gi) !== null ? "maybe" : ""
            }
        }
        if (o && (n.mode === "auto" || n.mode === "auto_plugin" || n.mode === "native")) {
            if (!p) {
                r = document.createElement(u.isVideo ? "video" : "audio");
                m.parentNode.insertBefore(r, m);
                m.style.display = "none";
                u.htmlMediaElement = m = r
            }
            for (r = 0; r < s.length; r++) {
                if (m.canPlayType(s[r].type).replace(/no/, "") !== "" || m.canPlayType(s[r].type.replace(/mp3/, "mpeg")).replace(/no/, "") !== "") {
                    u.method = "native";
                    u.url = s[r].url;
                    break
                }
            }
            if (u.method === "native") {
                if (u.url !== null) {
                    m.src = u.url
                }
                if (n.mode !== "auto_plugin") {
                    return u
                }
            }
        }
        if (n.mode === "auto" || n.mode === "auto_plugin" || n.mode === "shim") {
            for (r = 0; r < s.length; r++) {
                w = s[r].type;
                for (m = 0; m < n.plugins.length; m++) {
                    q = n.plugins[m];
                    t = mejs.plugins[q];
                    for (o = 0; o < t.length; o++) {
                        v = t[o];
                        if (v.version == null || mejs.PluginDetector.hasPluginVersion(q, v.version)) {
                            for (p = 0; p < v.types.length; p++) {
                                if (w == v.types[p]) {
                                    u.method = q;
                                    u.url = s[r].url;
                                    return u
                                }
                            }
                        }
                    }
                }
            }
        }
        if (n.mode === "auto_plugin" && u.method === "native") {
            return u
        }
        if (u.method === "" && s.length > 0) {
            u.url = s[0].url
        }
        return u
    },
    formatType: function(c, d) {
        return c && !d ? this.getTypeFromFile(c) : d && ~d.indexOf(";") ? d.substr(0, d.indexOf(";")) : d
    },
    getTypeFromFile: function(b) {
        b = b.split("?")[0];
        b = b.substring(b.lastIndexOf(".") + 1);
        return (/(mp4|m4v|ogg|ogv|webm|webmv|flv|wmv|mpeg|mov)/gi.test(b) ? "video" : "audio") + "/" + this.getTypeFromExtension(b)
    },
    getTypeFromExtension: function(b) {
        switch (b) {
            case "mp4":
            case "m4v":
                return "mp4";
            case "webm":
            case "webma":
            case "webmv":
                return "webm";
            case "ogg":
            case "oga":
            case "ogv":
                return "ogg";
            default:
                return b
        }
    },
    createErrorMessage: function(f, h, j) {
        var k = f.htmlMediaElement,
            l = document.createElement("div");
        l.className = "me-cannotplay";
        try {
            l.style.width = k.width + "px";
            l.style.height = k.height + "px"
        } catch (m) {}
        l.innerHTML = j !== "" ? '<a href="' + f.url + '"><img src="' + j + '" width="100%" height="100%" /></a>' : '<a href="' + f.url + '"><span>' + mejs.i18n.t("Download File") + "</span></a>";
        k.parentNode.insertBefore(l, k);
        k.style.display = "none";
        h.error(k)
    },
    createPlugin: function(o, p, q, r, s, u) {
        q = o.htmlMediaElement;
        var t = 1,
            v = 1,
            y = "me_" + o.method + "_" + mejs.meIndex++,
            w = new mejs.PluginMediaElement(y, o.method, o.url),
            x = document.createElement("div"),
            z;
        w.tagName = q.tagName;
        for (z = 0; z < q.attributes.length; z++) {
            var A = q.attributes[z];
            A.specified == true && w.setAttribute(A.name, A.value)
        }
        for (z = q.parentNode; z !== null && z.tagName.toLowerCase() != "body";) {
            if (z.parentNode.tagName.toLowerCase() == "p") {
                z.parentNode.parentNode.insertBefore(z, z.parentNode);
                break
            }
            z = z.parentNode
        }
        if (o.isVideo) {
            t = p.videoWidth > 0 ? p.videoWidth : q.getAttribute("width") !== null ? q.getAttribute("width") : p.defaultVideoWidth;
            v = p.videoHeight > 0 ? p.videoHeight : q.getAttribute("height") !== null ? q.getAttribute("height") : p.defaultVideoHeight;
            t = mejs.Utility.encodeUrl(t);
            v = mejs.Utility.encodeUrl(v)
        } else {
            if (p.enablePluginDebug) {
                t = 320;
                v = 240
            }
        }
        w.success = p.success;
        mejs.MediaPluginBridge.registerPluginElement(y, w, q);
        x.className = "me-plugin";
        x.id = y + "_container";
        o.isVideo ? q.parentNode.insertBefore(x, q) : document.body.insertBefore(x, document.body.childNodes[0]);
        r = ["id=" + y, "isvideo=" + (o.isVideo ? "true" : "false"), "autoplay=" + (r ? "true" : "false"), "preload=" + s, "width=" + t, "startvolume=" + p.startVolume, "timerrate=" + p.timerRate, "flashstreamer=" + p.flashStreamer, "height=" + v];
        if (o.url !== null) {
            o.method == "flash" ? r.push("file=" + mejs.Utility.encodeUrl(o.url)) : r.push("file=" + o.url)
        }
        p.enablePluginDebug && r.push("debug=true");
        p.enablePluginSmoothing && r.push("smoothing=true");
        u && r.push("controls=true");
        if (p.pluginVars) {
            r = r.concat(p.pluginVars)
        }
        switch (o.method) {
            case "silverlight":
                x.innerHTML = '<object data="data:application/x-silverlight-2," type="application/x-silverlight-2" id="' + y + '" name="' + y + '" width="' + t + '" height="' + v + '"><param name="initParams" value="' + r.join(",") + '" /><param name="windowless" value="true" /><param name="background" value="black" /><param name="minRuntimeVersion" value="3.0.0.0" /><param name="autoUpgrade" value="true" /><param name="source" value="' + p.pluginPath + p.silverlightName + '" /></object>';
                break;
            case "flash":
                if (mejs.MediaFeatures.isIE) {
                    o = document.createElement("div");
                    x.appendChild(o);
                    o.outerHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="//download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab" id="' + y + '" width="' + t + '" height="' + v + '"><param name="movie" value="' + p.pluginPath + p.flashName + "?x=" + new Date + '" /><param name="flashvars" value="' + r.join("&amp;") + '" /><param name="quality" value="high" /><param name="bgcolor" value="#000000" /><param name="wmode" value="transparent" /><param name="allowScriptAccess" value="always" /><param name="allowFullScreen" value="true" /></object>'
                } else {
                    x.innerHTML = '<embed id="' + y + '" name="' + y + '" play="true" loop="false" quality="high" bgcolor="#000000" wmode="transparent" allowScriptAccess="always" allowFullScreen="true" type="application/x-shockwave-flash" pluginspage="//www.macromedia.com/go/getflashplayer" src="' + p.pluginPath + p.flashName + '" flashvars="' + r.join("&") + '" width="' + t + '" height="' + v + '"></embed>'
                }
                break;
            case "youtube":
                p = o.url.substr(o.url.lastIndexOf("=") + 1);
                youtubeSettings = {
                    container: x,
                    containerId: x.id,
                    pluginMediaElement: w,
                    pluginId: y,
                    videoId: p,
                    height: v,
                    width: t
                };
                mejs.PluginDetector.hasPluginVersion("flash", [10, 0, 0]) ? mejs.YouTubeApi.createFlash(youtubeSettings) : mejs.YouTubeApi.enqueueIframe(youtubeSettings);
                break;
            case "vimeo":
                w.vimeoid = o.url.substr(o.url.lastIndexOf("/") + 1);
                x.innerHTML = '<iframe src="http://player.vimeo.com/video/' + w.vimeoid + '?portrait=0&byline=0&title=0" width="' + t + '" height="' + v + '" frameborder="0"></iframe>'
        }
        q.style.display = "none";
        return w
    },
    updateNative: function(e, f) {
        var g = e.htmlMediaElement,
            h;
        for (h in mejs.HtmlMediaElement) {
            g[h] = mejs.HtmlMediaElement[h]
        }
        f.success(g, g);
        return g
    }
};
mejs.YouTubeApi = {
    isIframeStarted: false,
    isIframeLoaded: false,
    loadIframeApi: function() {
        if (!this.isIframeStarted) {
            var c = document.createElement("script");
            c.src = "http://www.youtube.com/player_api";
            var d = document.getElementsByTagName("script")[0];
            d.parentNode.insertBefore(c, d);
            this.isIframeStarted = true
        }
    },
    iframeQueue: [],
    enqueueIframe: function(b) {
        if (this.isLoaded) {
            this.createIframe(b)
        } else {
            this.loadIframeApi();
            this.iframeQueue.push(b)
        }
    },
    createIframe: function(d) {
        var e = d.pluginMediaElement,
            f = new YT.Player(d.containerId, {
                height: d.height,
                width: d.width,
                videoId: d.videoId,
                playerVars: {
                    controls: 0
                },
                events: {
                    onReady: function() {
                        d.pluginMediaElement.pluginApi = f;
                        mejs.MediaPluginBridge.initPlugin(d.pluginId);
                        setInterval(function() {
                            mejs.YouTubeApi.createEvent(f, e, "timeupdate")
                        }, 250)
                    },
                    onStateChange: function(a) {
                        mejs.YouTubeApi.handleStateChange(a.data, f, e)
                    }
                }
            })
    },
    createEvent: function(e, f, g) {
        g = {
            type: g,
            target: f
        };
        if (e && e.getDuration) {
            f.currentTime = g.currentTime = e.getCurrentTime();
            f.duration = g.duration = e.getDuration();
            g.paused = f.paused;
            g.ended = f.ended;
            g.muted = e.isMuted();
            g.volume = e.getVolume() / 100;
            g.bytesTotal = e.getVideoBytesTotal();
            g.bufferedBytes = e.getVideoBytesLoaded();
            var h = g.bufferedBytes / g.bytesTotal * g.duration;
            g.target.buffered = g.buffered = {
                start: function() {
                    return 0
                },
                end: function() {
                    return h
                },
                length: 1
            }
        }
        f.dispatchEvent(g.type, g)
    },
    iFrameReady: function() {
        for (this.isIframeLoaded = this.isLoaded = true; this.iframeQueue.length > 0;) {
            this.createIframe(this.iframeQueue.pop())
        }
    },
    flashPlayers: {},
    createFlash: function(d) {
        this.flashPlayers[d.pluginId] = d;
        var e, f = "http://www.youtube.com/apiplayer?enablejsapi=1&amp;playerapiid=" + d.pluginId + "&amp;version=3&amp;autoplay=0&amp;controls=0&amp;modestbranding=1&loop=0";
        if (mejs.MediaFeatures.isIE) {
            e = document.createElement("div");
            d.container.appendChild(e);
            e.outerHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="//download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab" id="' + d.pluginId + '" width="' + d.width + '" height="' + d.height + '"><param name="movie" value="' + f + '" /><param name="wmode" value="transparent" /><param name="allowScriptAccess" value="always" /><param name="allowFullScreen" value="true" /></object>'
        } else {
            d.container.innerHTML = '<object type="application/x-shockwave-flash" id="' + d.pluginId + '" data="' + f + '" width="' + d.width + '" height="' + d.height + '" style="visibility: visible; "><param name="allowScriptAccess" value="always"><param name="wmode" value="transparent"></object>'
        }
    },
    flashReady: function(e) {
        var f = this.flashPlayers[e],
            g = document.getElementById(e),
            h = f.pluginMediaElement;
        h.pluginApi = h.pluginElement = g;
        mejs.MediaPluginBridge.initPlugin(e);
        g.cueVideoById(f.videoId);
        e = f.containerId + "_callback";
        window[e] = function(a) {
            mejs.YouTubeApi.handleStateChange(a, g, h)
        };
        g.addEventListener("onStateChange", e);
        setInterval(function() {
            mejs.YouTubeApi.createEvent(g, h, "timeupdate")
        }, 250)
    },
    handleStateChange: function(d, e, f) {
        switch (d) {
            case -1:
                f.paused = true;
                f.ended = true;
                mejs.YouTubeApi.createEvent(e, f, "loadedmetadata");
                break;
            case 0:
                f.paused = false;
                f.ended = true;
                mejs.YouTubeApi.createEvent(e, f, "ended");
                break;
            case 1:
                f.paused = false;
                f.ended = false;
                mejs.YouTubeApi.createEvent(e, f, "play");
                mejs.YouTubeApi.createEvent(e, f, "playing");
                break;
            case 2:
                f.paused = true;
                f.ended = false;
                mejs.YouTubeApi.createEvent(e, f, "pause");
                break;
            case 3:
                mejs.YouTubeApi.createEvent(e, f, "progress")
        }
    }
};

function onYouTubePlayerAPIReady() {
    mejs.YouTubeApi.iFrameReady()
}

function onYouTubePlayerReady(b) {
    mejs.YouTubeApi.flashReady(b)
}
window.mejs = mejs;
window.MediaElement = mejs.MediaElement;
(function(e, f, g) {
    var h = {
        locale: {
            strings: {}
        },
        methods: {}
    };
    h.locale.getLanguage = function() {
        return {
            language: navigator.language
        }
    };
    h.locale.INIT_LANGUAGE = h.locale.getLanguage();
    h.methods.checkPlain = function(a) {
        var c, b, d = {
            "&": "&amp;",
            '"': "&quot;",
            "<": "&lt;",
            ">": "&gt;"
        };
        a = String(a);
        for (c in d) {
            if (d.hasOwnProperty(c)) {
                b = RegExp(c, "g");
                a = a.replace(b, d[c])
            }
        }
        return a
    };
    h.methods.formatString = function(a, c) {
        for (var b in c) {
            switch (b.charAt(0)) {
                case "@":
                    c[b] = h.methods.checkPlain(c[b]);
                    break;
                case "!":
                    break;
                default:
                    c[b] = '<em class="placeholder">' + h.methods.checkPlain(c[b]) + "</em>"
            }
            a = a.replace(b, c[b])
        }
        return a
    };
    h.methods.t = function(a, c, b) {
        if (h.locale.strings && h.locale.strings[b.context] && h.locale.strings[b.context][a]) {
            a = h.locale.strings[b.context][a]
        }
        if (c) {
            a = h.methods.formatString(a, c)
        }
        return a
    };
    h.t = function(a, c, b) {
        if (typeof a === "string" && a.length > 0) {
            var d = h.locale.getLanguage();
            b = b || {
                context: d.language
            };
            return h.methods.t(a, c, b)
        } else {
            throw {
                name: "InvalidArgumentException",
                message: "First argument is either not a string or empty."
            }
        }
    };
    g.i18n = h
})(jQuery, document, mejs);
(function(b) {
    b.de = {
        Fullscreen: "Vollbild",
        "Go Fullscreen": "Vollbild an",
        "Turn off Fullscreen": "Vollbild aus",
        Close: "Schlie\u00dfen"
    }
})(mejs.i18n.locale.strings);
/*
 * MediaElementPlayer
 * http://mediaelementjs.com/
 *
 * Creates a controller bar for HTML5 <video> add <audio> tags
 * using jQuery and MediaElement.js (HTML5 Flash/Silverlight wrapper)
 *
 * Copyright 2010-2012, John Dyer (http://j.hn/)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 */
if (typeof jQuery != "undefined") {
    mejs.$ = jQuery
} else {
    if (typeof ender != "undefined") {
        mejs.$ = ender
    }
}(function(a) {
    mejs.MepDefaults = {
        poster: "",
        defaultVideoWidth: 480,
        defaultVideoHeight: 270,
        videoWidth: -1,
        videoHeight: -1,
        defaultAudioWidth: 400,
        defaultAudioHeight: 30,
        defaultSeekBackwardInterval: function(b) {
            return b.duration * 0.05
        },
        defaultSeekForwardInterval: function(b) {
            return b.duration * 0.05
        },
        audioWidth: -1,
        audioHeight: -1,
        startVolume: 0.8,
        loop: false,
        enableAutosize: true,
        alwaysShowHours: false,
        showTimecodeFrameCount: false,
        framesPerSecond: 25,
        autosizeProgress: true,
        alwaysShowControls: false,
        iPadUseNativeControls: false,
        iPhoneUseNativeControls: false,
        AndroidUseNativeControls: false,
        features: ["playpause", "current", "progress", "duration", "tracks", "volume", "fullscreen"],
        isVideo: true,
        enableKeyboard: true,
        pauseOtherPlayers: true,
        keyActions: [{
            keys: [32, 179],
            action: function(c, d) {
                d.paused || d.ended ? d.play() : d.pause()
            }
        }, {
            keys: [38],
            action: function(c, d) {
                d.setVolume(Math.min(d.volume + 0.1, 1))
            }
        }, {
            keys: [40],
            action: function(c, d) {
                d.setVolume(Math.max(d.volume - 0.1, 0))
            }
        }, {
            keys: [37, 227],
            action: function(d, e) {
                if (!isNaN(e.duration) && e.duration > 0) {
                    if (d.isVideo) {
                        d.showControls();
                        d.startControlsTimer()
                    }
                    var f = Math.max(e.currentTime - d.options.defaultSeekBackwardInterval(e), 0);
                    e.setCurrentTime(f)
                }
            }
        }, {
            keys: [39, 228],
            action: function(d, e) {
                if (!isNaN(e.duration) && e.duration > 0) {
                    if (d.isVideo) {
                        d.showControls();
                        d.startControlsTimer()
                    }
                    var f = Math.min(e.currentTime + d.options.defaultSeekForwardInterval(e), e.duration);
                    e.setCurrentTime(f)
                }
            }
        }, {
            keys: [70],
            action: function(b) {
                if (typeof b.enterFullScreen != "undefined") {
                    b.isFullScreen ? b.exitFullScreen() : b.enterFullScreen()
                }
            }
        }]
    };
    mejs.mepIndex = 0;
    mejs.players = [];
    mejs.MediaElementPlayer = function(c, d) {
        if (!(this instanceof mejs.MediaElementPlayer)) {
            return new mejs.MediaElementPlayer(c, d)
        }
        this.$media = this.$node = a(c);
        this.node = this.media = this.$media[0];
        if (typeof this.node.player != "undefined") {
            return this.node.player
        } else {
            this.node.player = this
        } if (typeof d == "undefined") {
            d = this.$node.data("mejsoptions")
        }
        this.options = a.extend({}, mejs.MepDefaults, d);
        mejs.players.push(this);
        this.init();
        return this
    };
    mejs.MediaElementPlayer.prototype = {
        hasFocus: false,
        controlsAreVisible: true,
        init: function() {
            var d = this,
                f = mejs.MediaFeatures,
                g = a.extend(true, {}, d.options, {
                    success: function(b, c) {
                        d.meReady(b, c)
                    },
                    error: function(b) {
                        d.handleError(b)
                    }
                }),
                h = d.media.tagName.toLowerCase();
            d.isDynamic = h !== "audio" && h !== "video";
            d.isVideo = d.isDynamic ? d.options.isVideo : h !== "audio" && d.options.isVideo;
            if (f.isiPad && d.options.iPadUseNativeControls || f.isiPhone && d.options.iPhoneUseNativeControls) {
                d.$media.attr("controls", "controls");
                if (f.isiPad && d.media.getAttribute("autoplay") !== null) {
                    d.media.load();
                    d.media.play()
                }
            } else {
                if (!(f.isAndroid && d.AndroidUseNativeControls)) {
                    d.$media.removeAttr("controls");
                    d.id = "mep_" + mejs.mepIndex++;
                    d.container = a('<div id="' + d.id + '" class="mejs-container ' + (mejs.MediaFeatures.svg ? "svg" : "no-svg") + '"><div class="mejs-inner"><div class="mejs-mediaelement"></div><div class="mejs-layers"></div><div class="mejs-controls"></div><div class="mejs-clear"></div></div></div>').addClass(d.$media[0].className).insertBefore(d.$media);
                    d.container.addClass((f.isAndroid ? "mejs-android " : "") + (f.isiOS ? "mejs-ios " : "") + (f.isiPad ? "mejs-ipad " : "") + (f.isiPhone ? "mejs-iphone " : "") + (d.isVideo ? "mejs-video " : "mejs-audio "));
                    if (f.isiOS) {
                        f = d.$media.clone();
                        d.container.find(".mejs-mediaelement").append(f);
                        d.$media.remove();
                        d.$node = d.$media = f;
                        d.node = d.media = f[0]
                    } else {
                        d.container.find(".mejs-mediaelement").append(d.$media)
                    }
                    d.controls = d.container.find(".mejs-controls");
                    d.layers = d.container.find(".mejs-layers");
                    f = d.isVideo ? "video" : "audio";
                    h = f.substring(0, 1).toUpperCase() + f.substring(1);
                    d.width = d.options[f + "Width"] > 0 || d.options[f + "Width"].toString().indexOf("%") > -1 ? d.options[f + "Width"] : d.media.style.width !== "" && d.media.style.width !== null ? d.media.style.width : d.media.getAttribute("width") !== null ? d.$media.attr("width") : d.options["default" + h + "Width"];
                    d.height = d.options[f + "Height"] > 0 || d.options[f + "Height"].toString().indexOf("%") > -1 ? d.options[f + "Height"] : d.media.style.height !== "" && d.media.style.height !== null ? d.media.style.height : d.$media[0].getAttribute("height") !== null ? d.$media.attr("height") : d.options["default" + h + "Height"];
                    d.setPlayerSize(d.width, d.height);
                    g.pluginWidth = d.height;
                    g.pluginHeight = d.width
                }
            }
            mejs.MediaElement(d.$media[0], g)
        },
        showControls: function(c) {
            var d = this;
            c = typeof c == "undefined" || c;
            if (!d.controlsAreVisible) {
                if (c) {
                    d.controls.css("visibility", "visible").stop(true, true).fadeIn(200, function() {
                        d.controlsAreVisible = true
                    });
                    d.container.find(".mejs-control").css("visibility", "visible").stop(true, true).fadeIn(200, function() {
                        d.controlsAreVisible = true
                    })
                } else {
                    d.controls.css("visibility", "visible").css("display", "block");
                    d.container.find(".mejs-control").css("visibility", "visible").css("display", "block");
                    d.controlsAreVisible = true
                }
                d.setControlsSize()
            }
        },
        hideControls: function(c) {
            var d = this;
            c = typeof c == "undefined" || c;
            if (d.controlsAreVisible) {
                if (c) {
                    d.controls.stop(true, true).fadeOut(200, function() {
                        a(this).css("visibility", "hidden").css("display", "block");
                        d.controlsAreVisible = false
                    });
                    d.container.find(".mejs-control").stop(true, true).fadeOut(200, function() {
                        a(this).css("visibility", "hidden").css("display", "block")
                    })
                } else {
                    d.controls.css("visibility", "hidden").css("display", "block");
                    d.container.find(".mejs-control").css("visibility", "hidden").css("display", "block");
                    d.controlsAreVisible = false
                }
            }
        },
        controlsTimer: null,
        startControlsTimer: function(c) {
            var d = this;
            c = typeof c != "undefined" ? c : 1500;
            d.killControlsTimer("start");
            d.controlsTimer = setTimeout(function() {
                d.hideControls();
                d.killControlsTimer("hide")
            }, c)
        },
        killControlsTimer: function() {
            if (this.controlsTimer !== null) {
                clearTimeout(this.controlsTimer);
                delete this.controlsTimer;
                this.controlsTimer = null
            }
        },
        controlsEnabled: true,
        disableControls: function() {
            this.killControlsTimer();
            this.hideControls(false);
            this.controlsEnabled = false
        },
        enableControls: function() {
            this.showControls(false);
            this.controlsEnabled = true
        },
        meReady: function(f, h) {
            var j = this,
                m = mejs.MediaFeatures,
                l = h.getAttribute("autoplay");
            l = !(typeof l == "undefined" || l === null || l === "false");
            var n;
            if (!j.created) {
                j.created = true;
                j.media = f;
                j.domNode = h;
                if (!(m.isAndroid && j.options.AndroidUseNativeControls) && !(m.isiPad && j.options.iPadUseNativeControls) && !(m.isiPhone && j.options.iPhoneUseNativeControls)) {
                    j.buildposter(j, j.controls, j.layers, j.media);
                    j.buildkeyboard(j, j.controls, j.layers, j.media);
                    j.buildoverlays(j, j.controls, j.layers, j.media);
                    j.findTracks();
                    for (n in j.options.features) {
                        m = j.options.features[n];
                        if (j["build" + m]) {
                            try {
                                j["build" + m](j, j.controls, j.layers, j.media)
                            } catch (o) {}
                        }
                    }
                    j.container.trigger("controlsready");
                    j.setPlayerSize(j.width, j.height);
                    j.setControlsSize();
                    if (j.isVideo) {
                        if (mejs.MediaFeatures.hasTouch) {
                            j.$media.bind("touchstart", function() {
                                if (j.controlsAreVisible) {
                                    j.hideControls(false)
                                } else {
                                    j.controlsEnabled && j.showControls(false)
                                }
                            })
                        } else {
                            j.media.addEventListener("click", function() {
                                j.media.paused ? j.media.play() : j.media.pause()
                            });
                            j.container.bind("mouseenter mouseover", function() {
                                if (j.controlsEnabled) {
                                    if (!j.options.alwaysShowControls) {
                                        j.killControlsTimer("enter");
                                        j.showControls();
                                        j.startControlsTimer(2500)
                                    }
                                }
                            }).bind("mousemove", function() {
                                if (j.controlsEnabled) {
                                    j.controlsAreVisible || j.showControls();
                                    j.options.alwaysShowControls || j.startControlsTimer(2500)
                                }
                            }).bind("mouseleave", function() {
                                j.controlsEnabled && !j.media.paused && !j.options.alwaysShowControls && j.startControlsTimer(1000)
                            })
                        }
                        l && !j.options.alwaysShowControls && j.hideControls();
                        j.options.enableAutosize && j.media.addEventListener("loadedmetadata", function(b) {
                            if (j.options.videoHeight <= 0 && j.domNode.getAttribute("height") === null && !isNaN(b.target.videoHeight)) {
                                j.setPlayerSize(b.target.videoWidth, b.target.videoHeight);
                                j.setControlsSize();
                                j.media.setVideoSize(b.target.videoWidth, b.target.videoHeight)
                            }
                        }, false)
                    }
                    f.addEventListener("play", function() {
                        for (var b = 0, d = mejs.players.length; b < d; b++) {
                            var c = mejs.players[b];
                            c.id != j.id && j.options.pauseOtherPlayers && !c.paused && !c.ended && c.pause();
                            c.hasFocus = false
                        }
                        j.hasFocus = true
                    }, false);
                    j.media.addEventListener("ended", function() {
                        try {
                            j.media.setCurrentTime(0)
                        } catch (b) {}
                        j.media.pause();
                        j.setProgressRail && j.setProgressRail();
                        j.setCurrentRail && j.setCurrentRail();
                        if (j.options.loop) {
                            j.media.play()
                        } else {
                            !j.options.alwaysShowControls && j.controlsEnabled && j.showControls()
                        }
                    }, false);
                    j.media.addEventListener("loadedmetadata", function() {
                        j.updateDuration && j.updateDuration();
                        j.updateCurrent && j.updateCurrent();
                        if (!j.isFullScreen) {
                            j.setPlayerSize(j.width, j.height);
                            j.setControlsSize()
                        }
                    }, false);
                    setTimeout(function() {
                        j.setPlayerSize(j.width, j.height);
                        j.setControlsSize()
                    }, 50);
                    a(window).resize(function() {
                        j.isFullScreen || mejs.MediaFeatures.hasTrueNativeFullScreen && document.webkitIsFullScreen || j.setPlayerSize(j.width, j.height);
                        j.setControlsSize()
                    });
                    j.media.pluginType == "youtube" && j.container.find(".mejs-overlay-play").hide()
                }
                if (l && f.pluginType == "native") {
                    f.load();
                    f.play()
                }
                if (j.options.success) {
                    typeof j.options.success == "string" ? window[j.options.success](j.media, j.domNode, j) : j.options.success(j.media, j.domNode, j)
                }
            }
        },
        handleError: function(b) {
            this.controls.hide();
            this.options.error && this.options.error(b)
        },
        setPlayerSize: function(f, g) {
            if (typeof f != "undefined") {
                this.width = f
            }
            if (typeof g != "undefined") {
                this.height = g
            }
            if (this.height.toString().indexOf("%") > 0 || this.$node.css("max-width") === "100%" || this.$node[0].currentStyle && this.$node[0].currentStyle.maxWidth === "100%") {
                var h = this.isVideo ? this.media.videoWidth && this.media.videoWidth > 0 ? this.media.videoWidth : this.options.defaultVideoWidth : this.options.defaultAudioWidth,
                    k = this.isVideo ? this.media.videoHeight && this.media.videoHeight > 0 ? this.media.videoHeight : this.options.defaultVideoHeight : this.options.defaultAudioHeight,
                    j = this.container.parent().closest(":visible").width();
                h = parseInt(j * k / h, 10);
                if (this.container.parent()[0].tagName.toLowerCase() === "body") {
                    j = a(window).width();
                    h = a(window).height()
                }
                if (h != 0) {
                    this.container.width(j).height(h);
                    this.$media.width("100%").height("100%");
                    this.container.find("object, embed, iframe").width("100%").height("100%");
                    this.isVideo && this.media.setVideoSize && this.media.setVideoSize(j, h);
                    this.layers.children(".mejs-layer").width("100%").height("100%")
                }
            } else {
                this.container.width(this.width).height(this.height);
                this.layers.children(".mejs-layer").width(this.width).height(this.height)
            }
        },
        setControlsSize: function() {
            var f = 0,
                g = 0,
                h = this.controls.find(".mejs-time-rail"),
                k = this.controls.find(".mejs-time-total");
            this.controls.find(".mejs-time-current");
            this.controls.find(".mejs-time-loaded");
            var j = h.siblings();
            if (this.options && !this.options.autosizeProgress) {
                g = parseInt(h.css("width"))
            }
            if (g === 0 || !g) {
                j.each(function() {
                    if (a(this).css("position") != "absolute") {
                        f += a(this).outerWidth(true)
                    }
                });
                g = this.controls.width() - f - (h.outerWidth(true) - h.width())
            }
            h.width(g);
            k.width(g - (k.outerWidth(true) - k.width()));
            this.setProgressRail && this.setProgressRail();
            this.setCurrentRail && this.setCurrentRail()
        },
        buildposter: function(f, g, h, k) {
            var j = a('<div class="mejs-poster mejs-layer"></div>').appendTo(h);
            g = f.$media.attr("poster");
            if (f.options.poster !== "") {
                g = f.options.poster
            }
            g !== "" && g != null ? this.setPoster(g) : j.hide();
            k.addEventListener("play", function() {
                j.hide()
            }, false)
        },
        setPoster: function(d) {
            var e = this.container.find(".mejs-poster"),
                f = e.find("img");
            if (f.length == 0) {
                f = a('<img width="100%" height="100%" />').appendTo(e)
            }
            f.attr("src", d)
        },
        buildoverlays: function(f, h, j, m) {
            if (f.isVideo) {
                var l = a('<div class="mejs-overlay mejs-layer"><div class="mejs-overlay-loading"><span></span></div></div>').hide().appendTo(j),
                    n = a('<div class="mejs-overlay mejs-layer"><div class="mejs-overlay-error"></div></div>').hide().appendTo(j),
                    o = a('<div class="mejs-overlay mejs-layer mejs-overlay-play"><div class="mejs-overlay-button"></div></div>').appendTo(j).click(function() {
                        m.paused ? m.play() : m.pause()
                    });
                m.addEventListener("play", function() {
                    o.hide();
                    l.hide();
                    h.find(".mejs-time-buffering").hide();
                    n.hide()
                }, false);
                m.addEventListener("playing", function() {
                    o.hide();
                    l.hide();
                    h.find(".mejs-time-buffering").hide();
                    n.hide()
                }, false);
                m.addEventListener("seeking", function() {
                    l.show();
                    h.find(".mejs-time-buffering").show()
                }, false);
                m.addEventListener("seeked", function() {
                    l.hide();
                    h.find(".mejs-time-buffering").hide()
                }, false);
                m.addEventListener("pause", function() {
                    mejs.MediaFeatures.isiPhone || o.show()
                }, false);
                m.addEventListener("waiting", function() {
                    l.show();
                    h.find(".mejs-time-buffering").show()
                }, false);
                m.addEventListener("loadeddata", function() {
                    l.show();
                    h.find(".mejs-time-buffering").show()
                }, false);
                m.addEventListener("canplay", function() {
                    l.hide();
                    h.find(".mejs-time-buffering").hide()
                }, false);
                m.addEventListener("error", function() {
                    l.hide();
                    h.find(".mejs-time-buffering").hide();
                    n.show();
                    n.find("mejs-overlay-error").html("Error loading this resource")
                }, false)
            }
        },
        buildkeyboard: function(d, f, g, h) {
            a(document).keydown(function(b) {
                if (d.hasFocus && d.options.enableKeyboard) {
                    for (var c = 0, j = d.options.keyActions.length; c < j; c++) {
                        for (var e = d.options.keyActions[c], m = 0, l = e.keys.length; m < l; m++) {
                            if (b.keyCode == e.keys[m]) {
                                b.preventDefault();
                                e.action(d, h, b.keyCode);
                                return false
                            }
                        }
                    }
                }
                return true
            });
            a(document).click(function(b) {
                if (a(b.target).closest(".mejs-container").length == 0) {
                    d.hasFocus = false
                }
            })
        },
        findTracks: function() {
            var c = this,
                d = c.$media.find("track");
            c.tracks = [];
            d.each(function(b, f) {
                f = a(f);
                c.tracks.push({
                    srclang: f.attr("srclang").toLowerCase(),
                    src: f.attr("src"),
                    kind: f.attr("kind"),
                    label: f.attr("label") || "",
                    entries: [],
                    isLoaded: false
                })
            })
        },
        changeSkin: function(b) {
            this.container[0].className = "mejs-container " + b;
            this.setPlayerSize(this.width, this.height);
            this.setControlsSize()
        },
        play: function() {
            this.media.play()
        },
        pause: function() {
            this.media.pause()
        },
        load: function() {
            this.media.load()
        },
        setMuted: function(b) {
            this.media.setMuted(b)
        },
        setCurrentTime: function(b) {
            this.media.setCurrentTime(b)
        },
        getCurrentTime: function() {
            return this.media.currentTime
        },
        setVolume: function(b) {
            this.media.setVolume(b)
        },
        getVolume: function() {
            return this.media.volume
        },
        setSrc: function(b) {
            this.media.setSrc(b)
        },
        remove: function() {
            if (this.media.pluginType === "flash") {
                this.media.remove()
            } else {
                this.media.pluginType === "native" && this.$media.prop("controls", true)
            }
            this.isDynamic || this.$node.insertBefore(this.container);
            this.container.remove()
        }
    };
    if (typeof jQuery != "undefined") {
        jQuery.fn.mediaelementplayer = function(b) {
            return this.each(function() {
                new mejs.MediaElementPlayer(this, b)
            })
        }
    }
    a(document).ready(function() {
        a(".mejs-player").mediaelementplayer()
    });
    window.MediaElementPlayer = mejs.MediaElementPlayer
})(mejs.$);
(function(a) {
    a.extend(mejs.MepDefaults, {
        playpauseText: "Play/Pause"
    });
    a.extend(MediaElementPlayer.prototype, {
        buildplaypause: function(f, g, h, k) {
            var j = a('<div class="mejs-button mejs-playpause-button mejs-play" ><button type="button" aria-controls="' + this.id + '" title="' + this.options.playpauseText + '"></button></div>').appendTo(g).click(function(b) {
                b.preventDefault();
                k.paused ? k.play() : k.pause();
                return false
            });
            k.addEventListener("play", function() {
                j.removeClass("mejs-play").addClass("mejs-pause")
            }, false);
            k.addEventListener("playing", function() {
                j.removeClass("mejs-play").addClass("mejs-pause")
            }, false);
            k.addEventListener("pause", function() {
                j.removeClass("mejs-pause").addClass("mejs-play")
            }, false);
            k.addEventListener("paused", function() {
                j.removeClass("mejs-pause").addClass("mejs-play")
            }, false)
        }
    })
})(mejs.$);
(function(a) {
    a.extend(mejs.MepDefaults, {
        stopText: "Stop"
    });
    a.extend(MediaElementPlayer.prototype, {
        buildstop: function(d, f, g, h) {
            a('<div class="mejs-button mejs-stop-button mejs-stop"><button type="button" aria-controls="' + this.id + '" title="' + this.options.stopText + '"></button></div>').appendTo(f).click(function() {
                h.paused || h.pause();
                if (h.currentTime > 0) {
                    h.setCurrentTime(0);
                    h.pause();
                    f.find(".mejs-time-current").width("0px");
                    f.find(".mejs-time-handle").css("left", "0px");
                    f.find(".mejs-time-float-current").html(mejs.Utility.secondsToTimeCode(0));
                    f.find(".mejs-currenttime").html(mejs.Utility.secondsToTimeCode(0));
                    g.find(".mejs-poster").show()
                }
            })
        }
    })
})(mejs.$);
(function(a) {
    a.extend(MediaElementPlayer.prototype, {
        buildprogress: function(f, j, l, q) {
            a('<div class="mejs-time-rail"><span class="mejs-time-total"><span class="mejs-time-buffering"></span><span class="mejs-time-loaded"></span><span class="mejs-time-current"></span><span class="mejs-time-handle"></span><span class="mejs-time-float"><span class="mejs-time-float-current">00:00</span><span class="mejs-time-float-corner"></span></span></span></div>').appendTo(j);
            j.find(".mejs-time-buffering").hide();
            var m = j.find(".mejs-time-total");
            l = j.find(".mejs-time-loaded");
            var r = j.find(".mejs-time-current"),
                t = j.find(".mejs-time-handle"),
                s = j.find(".mejs-time-float"),
                v = j.find(".mejs-time-float-current"),
                u = function(d) {
                    d = d.pageX;
                    var g = m.offset(),
                        b = m.outerWidth(true),
                        c = 0;
                    c = 0;
                    var e = d - g.left;
                    if (d > g.left && d <= b + g.left && q.duration) {
                        c = (d - g.left) / b;
                        c = c <= 0.02 ? 0 : c * q.duration;
                        w && q.setCurrentTime(c);
                        if (!mejs.MediaFeatures.hasTouch) {
                            s.css("left", e);
                            v.html(mejs.Utility.secondsToTimeCode(c));
                            s.show()
                        }
                    }
                }, w = false;
            m.bind("mousedown", function(b) {
                if (b.which === 1) {
                    w = true;
                    u(b);
                    a(document).bind("mousemove.dur", function(c) {
                        u(c)
                    }).bind("mouseup.dur", function() {
                        w = false;
                        s.hide();
                        a(document).unbind(".dur")
                    });
                    return false
                }
            }).bind("mouseenter", function() {
                a(document).bind("mousemove.dur", function(b) {
                    u(b)
                });
                mejs.MediaFeatures.hasTouch || s.show()
            }).bind("mouseleave", function() {
                if (!w) {
                    a(document).unbind(".dur");
                    s.hide()
                }
            });
            q.addEventListener("progress", function(b) {
                f.setProgressRail(b);
                f.setCurrentRail(b)
            }, false);
            q.addEventListener("timeupdate", function(b) {
                f.setProgressRail(b);
                f.setCurrentRail(b)
            }, false);
            this.loaded = l;
            this.total = m;
            this.current = r;
            this.handle = t
        },
        setProgressRail: function(d) {
            var e = d != undefined ? d.target : this.media,
                f = null;
            if (e && e.buffered && e.buffered.length > 0 && e.buffered.end && e.duration) {
                f = e.buffered.end(0) / e.duration
            } else {
                if (e && e.bytesTotal != undefined && e.bytesTotal > 0 && e.bufferedBytes != undefined) {
                    f = e.bufferedBytes / e.bytesTotal
                } else {
                    if (d && d.lengthComputable && d.total != 0) {
                        f = d.loaded / d.total
                    }
                }
            } if (f !== null) {
                f = Math.min(1, Math.max(0, f));
                this.loaded && this.total && this.loaded.width(this.total.width() * f)
            }
        },
        setCurrentRail: function() {
            if (this.media.currentTime != undefined && this.media.duration) {
                if (this.total && this.handle) {
                    var c = this.total.width() * this.media.currentTime / this.media.duration,
                        d = c - this.handle.outerWidth(true) / 2;
                    this.current.width(c);
                    this.handle.css("left", d)
                }
            }
        }
    })
})(mejs.$);
(function(a) {
    a.extend(mejs.MepDefaults, {
        duration: -1,
        timeAndDurationSeparator: " <span> | </span> "
    });
    a.extend(MediaElementPlayer.prototype, {
        buildcurrent: function(d, f, g, h) {
            a('<div class="mejs-time"><span class="mejs-currenttime">' + (d.options.alwaysShowHours ? "00:" : "") + (d.options.showTimecodeFrameCount ? "00:00:00" : "00:00") + "</span></div>").appendTo(f);
            this.currenttime = this.controls.find(".mejs-currenttime");
            h.addEventListener("timeupdate", function() {
                d.updateCurrent()
            }, false)
        },
        buildduration: function(d, f, g, h) {
            if (f.children().last().find(".mejs-currenttime").length > 0) {
                a(this.options.timeAndDurationSeparator + '<span class="mejs-duration">' + (this.options.duration > 0 ? mejs.Utility.secondsToTimeCode(this.options.duration, this.options.alwaysShowHours || this.media.duration > 3600, this.options.showTimecodeFrameCount, this.options.framesPerSecond || 25) : (d.options.alwaysShowHours ? "00:" : "") + (d.options.showTimecodeFrameCount ? "00:00:00" : "00:00")) + "</span>").appendTo(f.find(".mejs-time"))
            } else {
                f.find(".mejs-currenttime").parent().addClass("mejs-currenttime-container");
                a('<div class="mejs-time mejs-duration-container"><span class="mejs-duration">' + (this.options.duration > 0 ? mejs.Utility.secondsToTimeCode(this.options.duration, this.options.alwaysShowHours || this.media.duration > 3600, this.options.showTimecodeFrameCount, this.options.framesPerSecond || 25) : (d.options.alwaysShowHours ? "00:" : "") + (d.options.showTimecodeFrameCount ? "00:00:00" : "00:00")) + "</span></div>").appendTo(f)
            }
            this.durationD = this.controls.find(".mejs-duration");
            h.addEventListener("timeupdate", function() {
                d.updateDuration()
            }, false)
        },
        updateCurrent: function() {
            if (this.currenttime) {
                this.currenttime.html(mejs.Utility.secondsToTimeCode(this.media.currentTime, this.options.alwaysShowHours || this.media.duration > 3600, this.options.showTimecodeFrameCount, this.options.framesPerSecond || 25))
            }
        },
        updateDuration: function() {
            if (this.media.duration && this.durationD) {
                this.durationD.html(mejs.Utility.secondsToTimeCode(this.media.duration, this.options.alwaysShowHours, this.options.showTimecodeFrameCount, this.options.framesPerSecond || 25))
            }
        }
    })
})(mejs.$);
(function(a) {
    a.extend(mejs.MepDefaults, {
        muteText: "Mute Toggle",
        hideVolumeOnTouchDevices: true,
        audioVolume: "horizontal",
        videoVolume: "vertical"
    });
    a.extend(MediaElementPlayer.prototype, {
        buildvolume: function(f, j, m, s) {
            if (!(mejs.MediaFeatures.hasTouch && this.options.hideVolumeOnTouchDevices)) {
                var r = this.isVideo ? this.options.videoVolume : this.options.audioVolume,
                    t = r == "horizontal" ? a('<div class="mejs-button mejs-volume-button mejs-mute"><button type="button" aria-controls="' + this.id + '" title="' + this.options.muteText + '"></button></div><div class="mejs-horizontal-volume-slider"><div class="mejs-horizontal-volume-total"></div><div class="mejs-horizontal-volume-current"></div><div class="mejs-horizontal-volume-handle"></div></div>').appendTo(j) : a('<div class="mejs-button mejs-volume-button mejs-mute"><button type="button" aria-controls="' + this.id + '" title="' + this.options.muteText + '"></button><div class="mejs-volume-slider"><div class="mejs-volume-total"></div><div class="mejs-volume-current"></div><div class="mejs-volume-handle"></div></div></div>').appendTo(j),
                    w = this.container.find(".mejs-volume-slider, .mejs-horizontal-volume-slider"),
                    u = this.container.find(".mejs-volume-total, .mejs-horizontal-volume-total"),
                    z = this.container.find(".mejs-volume-current, .mejs-horizontal-volume-current"),
                    y = this.container.find(".mejs-volume-handle, .mejs-horizontal-volume-handle"),
                    A = function(b, c) {
                        if (!w.is(":visible") && typeof c == "undefined") {
                            w.show();
                            A(b, true);
                            w.hide()
                        } else {
                            b = Math.max(0, b);
                            b = Math.min(b, 1);
                            b == 0 ? t.removeClass("mejs-mute").addClass("mejs-unmute") : t.removeClass("mejs-unmute").addClass("mejs-mute");
                            if (r == "vertical") {
                                var d = u.height(),
                                    e = u.position(),
                                    g = d - d * b;
                                y.css("top", Math.round(e.top + g - y.height() / 2));
                                z.height(d - g);
                                z.css("top", e.top + g)
                            } else {
                                d = u.width();
                                e = u.position();
                                d = d * b;
                                y.css("left", Math.round(e.left + d - y.width() / 2));
                                z.width(Math.round(d))
                            }
                        }
                    }, x = function(b) {
                        var c = null,
                            d = u.offset();
                        if (r == "vertical") {
                            c = u.height();
                            parseInt(u.css("top").replace(/px/, ""), 10);
                            c = (c - (b.pageY - d.top)) / c;
                            if (d.top == 0 || d.left == 0) {
                                return
                            }
                        } else {
                            c = u.width();
                            c = (b.pageX - d.left) / c
                        }
                        c = Math.max(0, c);
                        c = Math.min(c, 1);
                        A(c);
                        c == 0 ? s.setMuted(true) : s.setMuted(false);
                        s.setVolume(c)
                    }, B = false,
                    v = false;
                t.hover(function() {
                    w.show();
                    v = true
                }, function() {
                    v = false;
                    !B && r == "vertical" && w.hide()
                });
                w.bind("mouseover", function() {
                    v = true
                }).bind("mousedown", function(b) {
                    x(b);
                    a(document).bind("mousemove.vol", function(c) {
                        x(c)
                    }).bind("mouseup.vol", function() {
                        B = false;
                        a(document).unbind(".vol");
                        !v && r == "vertical" && w.hide()
                    });
                    B = true;
                    return false
                });
                t.find("button").click(function() {
                    s.setMuted(!s.muted)
                });
                s.addEventListener("volumechange", function() {
                    if (!B) {
                        if (s.muted) {
                            A(0);
                            t.removeClass("mejs-mute").addClass("mejs-unmute")
                        } else {
                            A(s.volume);
                            t.removeClass("mejs-unmute").addClass("mejs-mute")
                        }
                    }
                }, false);
                if (this.container.is(":visible")) {
                    A(f.options.startVolume);
                    s.pluginType === "native" && s.setVolume(f.options.startVolume)
                }
            }
        }
    })
})(mejs.$);
(function(a) {
    a.extend(mejs.MepDefaults, {
        usePluginFullScreen: true,
        newWindowCallback: function() {
            return ""
        },
        fullscreenText: mejs.i18n.t("Fullscreen")
    });
    a.extend(MediaElementPlayer.prototype, {
        isFullScreen: false,
        isNativeFullScreen: false,
        docStyleOverflow: null,
        isInIframe: false,
        buildfullscreen: function(f, j, m, s) {
            if (f.isVideo) {
                f.isInIframe = window.location != window.parent.location;
                if (mejs.MediaFeatures.hasTrueNativeFullScreen) {
                    m = null;
                    m = mejs.MediaFeatures.hasMozNativeFullScreen ? a(document) : f.container;
                    m.bind(mejs.MediaFeatures.fullScreenEventName, function() {
                        if (mejs.MediaFeatures.isFullScreen()) {
                            f.isNativeFullScreen = true;
                            f.setControlsSize()
                        } else {
                            f.isNativeFullScreen = false;
                            f.exitFullScreen()
                        }
                    })
                }
                var r = this,
                    t = a('<div class="mejs-button mejs-fullscreen-button"><button type="button" aria-controls="' + r.id + '" title="' + r.options.fullscreenText + '"></button></div>').appendTo(j);
                if (r.media.pluginType === "native" || !r.options.usePluginFullScreen && !mejs.MediaFeatures.isFirefox) {
                    t.click(function() {
                        mejs.MediaFeatures.hasTrueNativeFullScreen && mejs.MediaFeatures.isFullScreen() || f.isFullScreen ? f.exitFullScreen() : f.enterFullScreen()
                    })
                } else {
                    var v = null;
                    if (function() {
                            var b = document.createElement("x"),
                                c = document.documentElement,
                                d = window.getComputedStyle;
                            if (!("pointerEvents" in b.style)) {
                                return false
                            }
                            b.style.pointerEvents = "auto";
                            b.style.pointerEvents = "x";
                            c.appendChild(b);
                            d = d && d(b, "").pointerEvents === "auto";
                            c.removeChild(b);
                            return !!d
                        }() && !mejs.MediaFeatures.isOpera) {
                        var u = false,
                            y = function() {
                                if (u) {
                                    x.hide();
                                    z.hide();
                                    w.hide();
                                    t.css("pointer-events", "");
                                    r.controls.css("pointer-events", "");
                                    u = false
                                }
                            }, x = a('<div class="mejs-fullscreen-hover" />').appendTo(r.container).mouseover(y),
                            z = a('<div class="mejs-fullscreen-hover"  />').appendTo(r.container).mouseover(y),
                            w = a('<div class="mejs-fullscreen-hover"  />').appendTo(r.container).mouseover(y),
                            A = function() {
                                var b = {
                                    position: "absolute",
                                    top: 0,
                                    left: 0
                                };
                                x.css(b);
                                z.css(b);
                                w.css(b);
                                x.width(r.container.width()).height(r.container.height() - r.controls.height());
                                b = t.offset().left - r.container.offset().left;
                                fullScreenBtnWidth = t.outerWidth(true);
                                z.width(b).height(r.controls.height()).css({
                                    top: r.container.height() - r.controls.height()
                                });
                                w.width(r.container.width() - b - fullScreenBtnWidth).height(r.controls.height()).css({
                                    top: r.container.height() - r.controls.height(),
                                    left: b + fullScreenBtnWidth
                                })
                            };
                        a(document).resize(function() {
                            A()
                        });
                        t.mouseover(function() {
                            if (!r.isFullScreen) {
                                var b = t.offset(),
                                    c = f.container.offset();
                                s.positionFullscreenButton(b.left - c.left, b.top - c.top, false);
                                t.css("pointer-events", "none");
                                r.controls.css("pointer-events", "none");
                                x.show();
                                w.show();
                                z.show();
                                A();
                                u = true
                            }
                        });
                        s.addEventListener("fullscreenchange", function() {
                            y()
                        })
                    } else {
                        t.mouseover(function() {
                            if (v !== null) {
                                clearTimeout(v);
                                delete v
                            }
                            var b = t.offset(),
                                c = f.container.offset();
                            s.positionFullscreenButton(b.left - c.left, b.top - c.top, true)
                        }).mouseout(function() {
                            if (v !== null) {
                                clearTimeout(v);
                                delete v
                            }
                            v = setTimeout(function() {
                                s.hideFullscreenButton()
                            }, 1500)
                        })
                    }
                }
                f.fullscreenBtn = t;
                a(document).bind("keydown", function(b) {
                    if ((mejs.MediaFeatures.hasTrueNativeFullScreen && mejs.MediaFeatures.isFullScreen() || r.isFullScreen) && b.keyCode == 27) {
                        f.exitFullScreen()
                    }
                })
            }
        },
        enterFullScreen: function() {
            var d = this;
            if (!(d.media.pluginType !== "native" && (mejs.MediaFeatures.isFirefox || d.options.usePluginFullScreen))) {
                docStyleOverflow = document.documentElement.style.overflow;
                document.documentElement.style.overflow = "hidden";
                normalHeight = d.container.height();
                normalWidth = d.container.width();
                if (d.media.pluginType === "native") {
                    if (mejs.MediaFeatures.hasTrueNativeFullScreen) {
                        mejs.MediaFeatures.requestFullScreen(d.container[0]);
                        d.isInIframe && setTimeout(function f() {
                            if (d.isNativeFullScreen) {
                                a(window).width() !== screen.width ? d.exitFullScreen() : setTimeout(f, 500)
                            }
                        }, 500)
                    } else {
                        if (mejs.MediaFeatures.hasSemiNativeFullScreen) {
                            d.media.webkitEnterFullscreen();
                            return
                        }
                    }
                }
                if (d.isInIframe) {
                    var e = d.options.newWindowCallback(this);
                    if (e !== "") {
                        if (mejs.MediaFeatures.hasTrueNativeFullScreen) {
                            setTimeout(function() {
                                if (!d.isNativeFullScreen) {
                                    d.pause();
                                    window.open(e, d.id, "top=0,left=0,width=" + screen.availWidth + ",height=" + screen.availHeight + ",resizable=yes,scrollbars=no,status=no,toolbar=no")
                                }
                            }, 250)
                        } else {
                            d.pause();
                            window.open(e, d.id, "top=0,left=0,width=" + screen.availWidth + ",height=" + screen.availHeight + ",resizable=yes,scrollbars=no,status=no,toolbar=no");
                            return
                        }
                    }
                }
                d.container.addClass("mejs-container-fullscreen").width("100%").height("100%");
                setTimeout(function() {
                    d.container.css({
                        width: "100%",
                        height: "100%"
                    });
                    d.setControlsSize()
                }, 500);
                if (d.pluginType === "native") {
                    d.$media.width("100%").height("100%")
                } else {
                    d.container.find("object, embed, iframe").width("100%").height("100%");
                    d.media.setVideoSize(a(window).width(), a(window).height())
                }
                d.layers.children("div").width("100%").height("100%");
                d.fullscreenBtn && d.fullscreenBtn.removeClass("mejs-fullscreen").addClass("mejs-unfullscreen");
                d.setControlsSize();
                d.isFullScreen = true
            }
        },
        exitFullScreen: function() {
            if (this.media.pluginType !== "native" && mejs.MediaFeatures.isFirefox) {
                this.media.setFullscreen(false)
            } else {
                if (mejs.MediaFeatures.hasTrueNativeFullScreen && (mejs.MediaFeatures.isFullScreen() || this.isFullScreen)) {
                    mejs.MediaFeatures.cancelFullScreen()
                }
                document.documentElement.style.overflow = docStyleOverflow;
                this.container.removeClass("mejs-container-fullscreen").width(normalWidth).height(normalHeight);
                if (this.pluginType === "native") {
                    this.$media.width(normalWidth).height(normalHeight)
                } else {
                    this.container.find("object embed").width(normalWidth).height(normalHeight);
                    this.media.setVideoSize(normalWidth, normalHeight)
                }
                this.layers.children("div").width(normalWidth).height(normalHeight);
                this.fullscreenBtn.removeClass("mejs-unfullscreen").addClass("mejs-fullscreen");
                this.setControlsSize();
                this.isFullScreen = false
            }
        }
    })
})(mejs.$);
(function(a) {
    a.extend(mejs.MepDefaults, {
        startLanguage: "",
        tracksText: "Captions/Subtitles"
    });
    a.extend(MediaElementPlayer.prototype, {
        hasChapters: false,
        buildtracks: function(f, g, h, k) {
            if (f.isVideo) {
                if (f.tracks.length != 0) {
                    var j;
                    f.chapters = a('<div class="mejs-chapters mejs-layer"></div>').prependTo(h).hide();
                    f.captions = a('<div class="mejs-captions-layer mejs-layer"><div class="mejs-captions-position"><span class="mejs-captions-text"></span></div></div>').prependTo(h).hide();
                    f.captionsText = f.captions.find(".mejs-captions-text");
                    f.captionsButton = a('<div class="mejs-button mejs-captions-button"><button type="button" aria-controls="' + this.id + '" title="' + this.options.tracksText + '"></button><div class="mejs-captions-selector"><ul><li><input type="radio" name="' + f.id + '_captions" id="' + f.id + '_captions_none" value="none" checked="checked" /><label for="' + f.id + '_captions_none">None</label></li></ul></div></div>').appendTo(g).hover(function() {
                        a(this).find(".mejs-captions-selector").css("visibility", "visible")
                    }, function() {
                        a(this).find(".mejs-captions-selector").css("visibility", "hidden")
                    }).delegate("input[type=radio]", "click", function() {
                        lang = this.value;
                        if (lang == "none") {
                            f.selectedTrack = null
                        } else {
                            for (j = 0; j < f.tracks.length; j++) {
                                if (f.tracks[j].srclang == lang) {
                                    f.selectedTrack = f.tracks[j];
                                    f.captions.attr("lang", f.selectedTrack.srclang);
                                    f.displayCaptions();
                                    break
                                }
                            }
                        }
                    });
                    f.options.alwaysShowControls ? f.container.find(".mejs-captions-position").addClass("mejs-captions-position-hover") : f.container.bind("mouseenter", function() {
                        f.container.find(".mejs-captions-position").addClass("mejs-captions-position-hover")
                    }).bind("mouseleave", function() {
                        k.paused || f.container.find(".mejs-captions-position").removeClass("mejs-captions-position-hover")
                    });
                    f.trackToLoad = -1;
                    f.selectedTrack = null;
                    f.isLoadingTrack = false;
                    for (j = 0; j < f.tracks.length; j++) {
                        f.tracks[j].kind == "subtitles" && f.addTrackButton(f.tracks[j].srclang, f.tracks[j].label)
                    }
                    f.loadNextTrack();
                    k.addEventListener("timeupdate", function() {
                        f.displayCaptions()
                    }, false);
                    k.addEventListener("loadedmetadata", function() {
                        f.displayChapters()
                    }, false);
                    f.container.hover(function() {
                        if (f.hasChapters) {
                            f.chapters.css("visibility", "visible");
                            f.chapters.fadeIn(200).height(f.chapters.find(".mejs-chapter").outerHeight())
                        }
                    }, function() {
                        f.hasChapters && !k.paused && f.chapters.fadeOut(200, function() {
                            a(this).css("visibility", "hidden");
                            a(this).css("display", "block")
                        })
                    });
                    f.node.getAttribute("autoplay") !== null && f.chapters.css("visibility", "hidden")
                }
            }
        },
        loadNextTrack: function() {
            this.trackToLoad++;
            if (this.trackToLoad < this.tracks.length) {
                this.isLoadingTrack = true;
                this.loadTrack(this.trackToLoad)
            } else {
                this.isLoadingTrack = false
            }
        },
        loadTrack: function(d) {
            var e = this,
                f = e.tracks[d];
            a.ajax({
                url: f.src,
                dataType: "text",
                success: function(b) {
                    f.entries = typeof b == "string" && /<tt\s+xml/ig.exec(b) ? mejs.TrackFormatParser.dfxp.parse(b) : mejs.TrackFormatParser.webvvt.parse(b);
                    f.isLoaded = true;
                    e.enableTrackButton(f.srclang, f.label);
                    e.loadNextTrack();
                    f.kind == "chapters" && e.media.addEventListener("play", function() {
                        e.media.duration > 0 && e.displayChapters(f)
                    }, false)
                },
                error: function() {
                    e.loadNextTrack()
                }
            })
        },
        enableTrackButton: function(c, d) {
            if (d === "") {
                d = mejs.language.codes[c] || c
            }
            this.captionsButton.find("input[value=" + c + "]").prop("disabled", false).siblings("label").html(d);
            this.options.startLanguage == c && a("#" + this.id + "_captions_" + c).click();
            this.adjustLanguageBox()
        },
        addTrackButton: function(c, d) {
            if (d === "") {
                d = mejs.language.codes[c] || c
            }
            this.captionsButton.find("ul").append(a('<li><input type="radio" name="' + this.id + '_captions" id="' + this.id + "_captions_" + c + '" value="' + c + '" disabled="disabled" /><label for="' + this.id + "_captions_" + c + '">' + d + " (loading)</label></li>"));
            this.adjustLanguageBox();
            this.container.find(".mejs-captions-translations option[value=" + c + "]").remove()
        },
        adjustLanguageBox: function() {
            this.captionsButton.find(".mejs-captions-selector").height(this.captionsButton.find(".mejs-captions-selector ul").outerHeight(true) + this.captionsButton.find(".mejs-captions-translations").outerHeight(true))
        },
        displayCaptions: function() {
            if (typeof this.tracks != "undefined") {
                var c, d = this.selectedTrack;
                if (d != null && d.isLoaded) {
                    for (c = 0; c < d.entries.times.length; c++) {
                        if (this.media.currentTime >= d.entries.times[c].start && this.media.currentTime <= d.entries.times[c].stop) {
                            this.captionsText.html(d.entries.text[c]);
                            this.captions.show().height(0);
                            return
                        }
                    }
                }
                this.captions.hide()
            }
        },
        displayChapters: function() {
            var b;
            for (b = 0; b < this.tracks.length; b++) {
                if (this.tracks[b].kind == "chapters" && this.tracks[b].isLoaded) {
                    this.drawChapters(this.tracks[b]);
                    this.hasChapters = true;
                    break
                }
            }
        },
        drawChapters: function(f) {
            var g = this,
                h, k, j = k = 0;
            g.chapters.empty();
            for (h = 0; h < f.entries.times.length; h++) {
                k = f.entries.times[h].stop - f.entries.times[h].start;
                k = Math.floor(k / g.media.duration * 100);
                if (k + j > 100 || h == f.entries.times.length - 1 && k + j < 100) {
                    k = 100 - j
                }
                g.chapters.append(a('<div class="mejs-chapter" rel="' + f.entries.times[h].start + '" style="left: ' + j.toString() + "%;width: " + k.toString() + '%;"><div class="mejs-chapter-block' + (h == f.entries.times.length - 1 ? " mejs-chapter-block-last" : "") + '"><span class="ch-title">' + f.entries.text[h] + '</span><span class="ch-time">' + mejs.Utility.secondsToTimeCode(f.entries.times[h].start) + "&ndash;" + mejs.Utility.secondsToTimeCode(f.entries.times[h].stop) + "</span></div></div>"));
                j += k
            }
            g.chapters.find("div.mejs-chapter").click(function() {
                g.media.setCurrentTime(parseFloat(a(this).attr("rel")));
                g.media.paused && g.media.play()
            });
            g.chapters.show()
        }
    });
    mejs.language = {
        codes: {
            af: "Afrikaans",
            sq: "Albanian",
            ar: "Arabic",
            be: "Belarusian",
            bg: "Bulgarian",
            ca: "Catalan",
            zh: "Chinese",
            "zh-cn": "Chinese Simplified",
            "zh-tw": "Chinese Traditional",
            hr: "Croatian",
            cs: "Czech",
            da: "Danish",
            nl: "Dutch",
            en: "English",
            et: "Estonian",
            tl: "Filipino",
            fi: "Finnish",
            fr: "French",
            gl: "Galician",
            de: "German",
            el: "Greek",
            ht: "Haitian Creole",
            iw: "Hebrew",
            hi: "Hindi",
            hu: "Hungarian",
            is: "Icelandic",
            id: "Indonesian",
            ga: "Irish",
            it: "Italian",
            ja: "Japanese",
            ko: "Korean",
            lv: "Latvian",
            lt: "Lithuanian",
            mk: "Macedonian",
            ms: "Malay",
            mt: "Maltese",
            no: "Norwegian",
            fa: "Persian",
            pl: "Polish",
            pt: "Portuguese",
            ro: "Romanian",
            ru: "Russian",
            sr: "Serbian",
            sk: "Slovak",
            sl: "Slovenian",
            es: "Spanish",
            sw: "Swahili",
            sv: "Swedish",
            tl: "Tagalog",
            th: "Thai",
            tr: "Turkish",
            uk: "Ukrainian",
            vi: "Vietnamese",
            cy: "Welsh",
            yi: "Yiddish"
        }
    };
    mejs.TrackFormatParser = {
        webvvt: {
            pattern_identifier: /^([a-zA-z]+-)?[0-9]+$/,
            pattern_timecode: /^([0-9]{2}:[0-9]{2}:[0-9]{2}([,.][0-9]{1,3})?) --\> ([0-9]{2}:[0-9]{2}:[0-9]{2}([,.][0-9]{3})?)(.*)$/,
            parse: function(f) {
                var g = 0;
                f = mejs.TrackFormatParser.split2(f, /\r?\n/);
                for (var h = {
                    text: [],
                    times: []
                }, k, j; g < f.length; g++) {
                    if (this.pattern_identifier.exec(f[g])) {
                        g++;
                        if ((k = this.pattern_timecode.exec(f[g])) && g < f.length) {
                            g++;
                            j = f[g];
                            for (g++; f[g] !== "" && g < f.length;) {
                                j = j + "\n" + f[g];
                                g++
                            }
                            j = a.trim(j).replace(/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig, "<a href='$1' target='_blank'>$1</a>");
                            h.text.push(j);
                            h.times.push({
                                start: mejs.Utility.convertSMPTEtoSeconds(k[1]) == 0 ? 0.2 : mejs.Utility.convertSMPTEtoSeconds(k[1]),
                                stop: mejs.Utility.convertSMPTEtoSeconds(k[3]),
                                settings: k[5]
                            })
                        }
                    }
                }
                return h
            }
        },
        dfxp: {
            parse: function(f) {
                f = a(f).filter("tt");
                var h = 0;
                h = f.children("div").eq(0);
                var j = h.find("p");
                h = f.find("#" + h.attr("style"));
                var m, l;
                f = {
                    text: [],
                    times: []
                };
                if (h.length) {
                    l = h.removeAttr("id").get(0).attributes;
                    if (l.length) {
                        m = {};
                        for (h = 0; h < l.length; h++) {
                            m[l[h].name.split(":")[1]] = l[h].value
                        }
                    }
                }
                for (h = 0; h < j.length; h++) {
                    var n;
                    l = {
                        start: null,
                        stop: null,
                        style: null
                    };
                    if (j.eq(h).attr("begin")) {
                        l.start = mejs.Utility.convertSMPTEtoSeconds(j.eq(h).attr("begin"))
                    }
                    if (!l.start && j.eq(h - 1).attr("end")) {
                        l.start = mejs.Utility.convertSMPTEtoSeconds(j.eq(h - 1).attr("end"))
                    }
                    if (j.eq(h).attr("end")) {
                        l.stop = mejs.Utility.convertSMPTEtoSeconds(j.eq(h).attr("end"))
                    }
                    if (!l.stop && j.eq(h + 1).attr("begin")) {
                        l.stop = mejs.Utility.convertSMPTEtoSeconds(j.eq(h + 1).attr("begin"))
                    }
                    if (m) {
                        n = "";
                        for (var o in m) {
                            n += o + ":" + m[o] + ";"
                        }
                    }
                    if (n) {
                        l.style = n
                    }
                    if (l.start == 0) {
                        l.start = 0.2
                    }
                    f.times.push(l);
                    l = a.trim(j.eq(h).html()).replace(/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig, "<a href='$1' target='_blank'>$1</a>");
                    f.text.push(l);
                    if (f.times.start == 0) {
                        f.times.start = 2
                    }
                }
                return f
            }
        },
        split2: function(c, d) {
            return c.split(d)
        }
    };
    if ("x\n\ny".split(/\n/gi).length != 3) {
        mejs.TrackFormatParser.split2 = function(f, g) {
            var h = [],
                k = "",
                j;
            for (j = 0; j < f.length; j++) {
                k += f.substring(j, j + 1);
                if (g.test(k)) {
                    h.push(k.replace(g, ""));
                    k = ""
                }
            }
            h.push(k);
            return h
        }
    }
})(mejs.$);
(function(a) {
    a.extend(mejs.MepDefaults, {
        contextMenuItems: [{
            render: function(b) {
                if (typeof b.enterFullScreen == "undefined") {
                    return null
                }
                return b.isFullScreen ? "Turn off Fullscreen" : "Go Fullscreen"
            },
            click: function(b) {
                b.isFullScreen ? b.exitFullScreen() : b.enterFullScreen()
            }
        }, {
            render: function(b) {
                return b.media.muted ? "Unmute" : "Mute"
            },
            click: function(b) {
                b.media.muted ? b.setMuted(false) : b.setMuted(true)
            }
        }, {
            isSeparator: true
        }, {
            render: function() {
                return "Download Video"
            },
            click: function(b) {
                window.location.href = b.media.currentSrc
            }
        }]
    });
    a.extend(MediaElementPlayer.prototype, {
        buildcontextmenu: function(b) {
            b.contextMenu = a('<div class="mejs-contextmenu"></div>').appendTo(a("body")).hide();
            b.container.bind("contextmenu", function(c) {
                if (b.isContextMenuEnabled) {
                    c.preventDefault();
                    b.renderContextMenu(c.clientX - 1, c.clientY - 1);
                    return false
                }
            });
            b.container.bind("click", function() {
                b.contextMenu.hide()
            });
            b.contextMenu.bind("mouseleave", function() {
                b.startContextMenuTimer()
            })
        },
        isContextMenuEnabled: true,
        enableContextMenu: function() {
            this.isContextMenuEnabled = true
        },
        disableContextMenu: function() {
            this.isContextMenuEnabled = false
        },
        contextMenuTimeout: null,
        startContextMenuTimer: function() {
            var b = this;
            b.killContextMenuTimer();
            b.contextMenuTimer = setTimeout(function() {
                b.hideContextMenu();
                b.killContextMenuTimer()
            }, 750)
        },
        killContextMenuTimer: function() {
            var b = this.contextMenuTimer;
            if (b != null) {
                clearTimeout(b);
                delete b
            }
        },
        hideContextMenu: function() {
            this.contextMenu.hide()
        },
        renderContextMenu: function(f, j) {
            for (var l = this, n = "", m = l.options.contextMenuItems, o = 0, q = m.length; o < q; o++) {
                if (m[o].isSeparator) {
                    n += '<div class="mejs-contextmenu-separator"></div>'
                } else {
                    var p = m[o].render(l);
                    if (p != null) {
                        n += '<div class="mejs-contextmenu-item" data-itemindex="' + o + '" id="element-' + Math.random() * 1000000 + '">' + p + "</div>"
                    }
                }
            }
            l.contextMenu.empty().append(a(n)).css({
                top: j,
                left: f
            }).show();
            l.contextMenu.find(".mejs-contextmenu-item").each(function() {
                var c = a(this),
                    b = parseInt(c.data("itemindex"), 10),
                    d = l.options.contextMenuItems[b];
                typeof d.show != "undefined" && d.show(c, l);
                c.click(function() {
                    typeof d.click != "undefined" && d.click(l);
                    l.contextMenu.hide()
                })
            });
            setTimeout(function() {
                l.killControlsTimer("rev3")
            }, 100)
        }
    })
})(mejs.$);
(function(a) {
    a.extend(mejs.MepDefaults, {
        postrollCloseText: mejs.i18n.t("Close")
    });
    a.extend(MediaElementPlayer.prototype, {
        buildpostroll: function(d, f, g) {
            var h = this.container.find('link[rel="postroll"]').attr("href");
            if (typeof h !== "undefined") {
                d.postroll = a('<div class="mejs-postroll-layer mejs-layer"><a class="mejs-postroll-close" onclick="$(this).parent().hide();return false;">' + this.options.postrollCloseText + '</a><div class="mejs-postroll-layer-content"></div></div>').prependTo(g).hide();
                this.media.addEventListener("ended", function() {
                    a.ajax({
                        dataType: "html",
                        url: h,
                        success: function(b) {
                            g.find(".mejs-postroll-layer-content").html(b)
                        }
                    });
                    d.postroll.show()
                }, false)
            }
        }
    })
})(mejs.$);
var Skoda = Skoda || {};
Skoda.K2 = Skoda.K2 || {};
(function(a) {
    a.initMediaPlayer = function(c, b) {
        $(c + " video.videoplayerMain,audio").mediaelementplayer({
            startVolume: 1,
            alwaysShowControls: true,
            enableAutosize: true,
            features: ["playpause", "progress", "current", "duration", "tracks", "volume", "fullscreen"],
            pluginPath: "/_layouts/Skoda.SharePoint/scripts/",
            flashName: "flashmediaelement.swf",
            success: function(f, e) {
                a.rotation();
                var d = (f.id) ? jQuery("#" + f.id) : jQuery(f);
                d.parents(".mejs-container").find(".mejs-volume-current").css("top", "8px");
                d.parents(".mejs-container").find(".mejs-volume-handle").css("top", "5px");
                f.addEventListener("loadeddata", function() {}, false);
                f.addEventListener("loadedmetadata", function() {
                    if (f.duration == Infinity) {
                        $(none).parent().parent().find(".mejs-time span").slice(1, 3).css("visibility", "hidden");
                        $(e).parent().parent().find(".mejs-time-float").css("visibility", "hidden")
                    }
                }, false);
                f.addEventListener("canplay", function() {}, false);
                f.addEventListener("seeked", function() {}, false);
                f.addEventListener("ended", function() {
                    f.load()
                }, false);
                if (b == "True") {
                    f.play()
                }
            }
        })
    };
    a.rotation = function() {
        $(".mejs-overlay-loading span").rotate({
            angle: 0,
            animateTo: 360,
            callback: a.rotation,
            easing: function(i, h, e, f, g) {
                return f * (h / g) + e
            }
        })
    }
})(Skoda.K2);
(function(a) {
    var e = "k2oembed",
        b = {};
    var d = function(h, i, g) {
        this.name = h;
        this.urlSchemes = i;
        this.apiEndpoint = g
    };
    var f = [new d("youtube", ["youtube\\.com/watch.+v=[\\w-]+&?", "youtu\\.be/[\\w-]+", "youtube.com/embed"], null), new d("wistia", [/https?:\/\/(.+)?(wistia.com|wi.st)\/.*/], "//fast.wistia.com/oembed"), new d("vimeo", ["www.vimeo.com/groups/.*/videos/.*", "www.vimeo.com/.*", "vimeo.com/groups/.*/videos/.*", "vimeo.com/.*"], "//vimeo.com/api/oembed.json")];
    var c = function(g, h) {
        this.$container = g;
        this.options = a.extend({}, b, h);
        this.init()
    };
    c.prototype = {
        init: function() {
            var i = this.getProviderFromUrl(this.options.mediaUrl);
            if (typeof(i) === "undefined" || i == null) {
                return
            }
            switch (i.name) {
                case "youtube":
                    var h = this.options.mediaUrl,
                        j = h.replace(/.*(?:v\=|be\/|embed\/)([\w\-]+)&?.*/, "http://www.youtube.com/embed/$1?wmode=transparent");
                    this.$container.html(this.getYoutubeHtml(j));
                    this.setThumbnail(this.getYoutubeThumbnailUrlFromVideoId(this.getYoutubeVideoIdFromVideoUrl(h)));
                    break;
                case "wistia":
                    this.embedDataFromProvider(i);
                    break;
                case "vimeo":
                    var l = this.options.mediaUrl;
                    var g = l.match(/https?:\/\/(?:www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/);
                    var k = (g.length > 3) ? g[3] : "";
                    this.$container.html(this.getVimeoHtml(k));
                    this.setThumbnail(this.getVimeoThumbnailUrlFromVideoId(k));
                    break;
                default:
                    break
            }
        },
        getVimeoThumbnailUrlFromVideoId: function(g) {
            if (!g) {
                return ""
            }
            var h = "";
            a.ajax({
                url: "//vimeo.com/api/v2/video/" + g + ".json",
                context: this,
                dataType: "json",
                async: false,
                success: function(i) {
                    h = i[0].thumbnail_medium
                }
            });
            return h
        },
        getVimeoHtml: function(h) {
            var g = '<iframe src="//player.vimeo.com/video/' + h;
            if (this.options.autoplay) {
                g = g + '?autoplay=1"'
            } else {
                g = g + '"'
            } if (this.options.width) {
                g = g + ' width="' + this.options.width + '"'
            }
            if (this.options.height) {
                g = g + ' height="' + this.options.height + '"'
            }
            g = g + 'frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
            return g
        },
        getYoutubeHtml: function(h) {
            var g = '<iframe id="youtubePlayer" src="' + h;
            g = g + "&enablejsapi=1&playerapiid=normalplayer";
            if (this.options.autoplay) {
                g = g + '&autoplay=1"'
            } else {
                g = g + '"'
            } if (this.options.width) {
                g = g + ' width="' + this.options.width + '"'
            }
            if (this.options.height) {
                g = g + ' height="' + this.options.height + '"'
            }
            g = g + " allowfullscreen></iframe>";
            return g
        },
        getYoutubeVideoIdFromVideoUrl: function(i) {
            if (!i) {
                return null
            }
            var h = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
            var g = i.match(h);
            if (g && g[2].length > 0) {
                return g[2]
            } else {
                return null
            }
        },
        getYoutubeThumbnailUrlFromVideoId: function(g) {
            if (!g) {
                return ""
            }
            return "http://img.youtube.com/vi/" + g + "/hqdefault.jpg"
        },
        embedHtmlToContainer: function(g) {
            this.$container.html(g.html);
            this.setThumbnail(g.thumbnail_url)
        },
        setThumbnail: function(i) {
            if (i && i.length > 0) {
                var h = this.$container.closest("div[data-k2oembed-group-id]").attr("data-k2oembed-group-id");
                if (h) {
                    var g = a("img[data-k2oembed-group-id='" + h + "']");
                    if (g.length > 0) {
                        g.attr("src", i)
                    }
                }
            }
        },
        getProviderFromUrl: function(m) {
            for (var g = 0; g < f.length; g++) {
                var k = f[g];
                for (var h = 0; h < k.urlSchemes.length; h++) {
                    var l = k.urlSchemes[h];
                    if (m.match(l)) {
                        return k
                    }
                }
            }
            return null
        },
        embedDataFromProvider: function(g) {
            a.ajax({
                url: this.getUrlFromOptions(g.apiEndpoint, g.name),
                context: this,
                dataType: "json",
                success: this.embedHtmlToContainer
            })
        },
        getUrlFromOptions: function(i, h) {
            var j = i + "?url=" + this.options.mediaUrl,
                g = this.options.height;
            if (this.options.width) {
                j = j + "&width=" + this.options.width
            }
            if (g) {
                j = j + "&height=" + g
            }
            if (typeof(this.options.autoplay) !== "undefined" && this.options.autoplay) {
                if (h == "wistia") {
                    j = j + "&autoPlay=true"
                } else {
                    j = j + "&autoplay=1"
                }
            }
            return j
        }
    };
    a.fn.k2oembed = function(g) {
        return this.each(function() {
            var h = a(this),
                i;
            if (!h.is("div")) {
                throw new Error("jquery.k2oembed: Unsupported type of element. Use <div/> instead.")
            }
            if (!g || !g.mediaUrl) {
                i = h.attr("data-url");
                if (!i) {
                    console.log("jquery.k2oembed: URL of the media must be specified in data-url attribute of the element or in options parameter.");
                    return
                } else {
                    g = {
                        mediaUrl: i
                    }
                }
            }
            if (!g.height) {
                g.height = h.attr("data-height")
            }
            if (!g.width) {
                g.width = h.attr("data-width")
            }
            if (typeof(g.autoplay) === "undefined") {
                g.autoplay = h.attr("data-autoplay")
            }
            if (!h.data("plugin_" + e)) {
                h.data("plugin_" + e, new c(h, g))
            }
        })
    }
}(jQuery));
/*
 * Autolinker.js
 * 0.12.2
 *
 * Copyright(c) 2014 Gregory Jacobs <greg@greg-jacobs.com>
 * MIT Licensed. http://www.opensource.org/licenses/mit-license.php
 *
 * https://github.com/gregjacobs/Autolinker.js
 */
! function(c, d) {
    "function" == typeof define && define.amd ? define(d) : "undefined" != typeof exports ? module.exports = d() : c.Autolinker = d()
}(this, function() {
    var b = function(a) {
        b.Util.assign(this, a)
    };
    return b.prototype = {
        constructor: b,
        urls: !0,
        email: !0,
        twitter: !0,
        newWindow: !0,
        stripPrefix: !0,
        className: "",
        htmlCharacterEntitiesRegex: /(&nbsp;|&#160;|&lt;|&#60;|&gt;|&#62;)/gi,
        matcherRegex: function() {
            var h = /(^|[^\w])@(\w{1,15})/,
                i = /(?:[\-;:&=\+\$,\w\.]+@)/,
                j = /(?:[A-Za-z]{3,9}:(?:\/\/)?)/,
                k = /(?:www\.)/,
                l = /[A-Za-z0-9\.\-]*[A-Za-z0-9\-]/,
                m = /\.(?:international|construction|contractors|enterprises|photography|productions|foundation|immobilien|industries|management|properties|technology|christmas|community|directory|education|equipment|institute|marketing|solutions|vacations|bargains|boutique|builders|catering|cleaning|clothing|computer|democrat|diamonds|graphics|holdings|lighting|partners|plumbing|supplies|training|ventures|academy|careers|company|cruises|domains|exposed|flights|florist|gallery|guitars|holiday|kitchen|neustar|okinawa|recipes|rentals|reviews|shiksha|singles|support|systems|agency|berlin|camera|center|coffee|condos|dating|estate|events|expert|futbol|kaufen|luxury|maison|monash|museum|nagoya|photos|repair|report|social|supply|tattoo|tienda|travel|viajes|villas|vision|voting|voyage|actor|build|cards|cheap|codes|dance|email|glass|house|mango|ninja|parts|photo|shoes|solar|today|tokyo|tools|watch|works|aero|arpa|asia|best|bike|blue|buzz|camp|club|cool|coop|farm|fish|gift|guru|info|jobs|kiwi|kred|land|limo|link|menu|mobi|moda|name|pics|pink|post|qpon|rich|ruhr|sexy|tips|vote|voto|wang|wien|wiki|zone|bar|bid|biz|cab|cat|ceo|com|edu|gov|int|kim|mil|net|onl|org|pro|pub|red|tel|uno|wed|xxx|xyz|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cw|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|za|zm|zw)\b/,
                n = /(?:[\-A-Za-z0-9+&@#\/%?=~_()|!:,.;]*[\-A-Za-z0-9+&@#\/%=~_()|])?/;
            return new RegExp(["(", h.source, ")", "|", "(", i.source, l.source, m.source, ")", "|", "(", "(?:", "(?:", j.source, l.source, ")", "|", "(?:", "(.?//)?", k.source, l.source, ")", "|", "(?:", "(.?//)?", l.source, m.source, ")", ")", n.source, ")"].join(""), "gi")
        }(),
        invalidProtocolRelMatchRegex: /^[\w]\/\//,
        charBeforeProtocolRelMatchRegex: /^(.)?\/\//,
        link: function(a) {
            var h = this,
                i = this.getHtmlParser(),
                j = this.htmlCharacterEntitiesRegex,
                k = 0,
                l = [];
            return i.parse(a, {
                processHtmlNode: function(d, e, f) {
                    "a" === e && (f ? k = Math.max(k - 1, 0) : k++), l.push(d)
                },
                processTextNode: function(c) {
                    if (0 === k) {
                        for (var e = b.Util.splitAndCapture(c, j), f = 0, g = e.length; g > f; f++) {
                            var m = e[f],
                                n = h.processTextNode(m);
                            l.push(n)
                        }
                    } else {
                        l.push(c)
                    }
                }
            }), l.join("")
        },
        getHtmlParser: function() {
            var a = this.htmlParser;
            return a || (a = this.htmlParser = new b.HtmlParser), a
        },
        getTagBuilder: function() {
            var a = this.tagBuilder;
            return a || (a = this.tagBuilder = new b.AnchorTagBuilder({
                newWindow: this.newWindow,
                truncate: this.truncate,
                className: this.className
            })), a
        },
        processTextNode: function(a) {
            var e = this,
                f = this.charBeforeProtocolRelMatchRegex;
            return a.replace(this.matcherRegex, function(c, d, w, x, y, z, A, B) {
                var C, D = d,
                    E = w,
                    F = x,
                    G = y,
                    H = z,
                    I = A || B,
                    J = "",
                    K = "";
                if (!e.isValidMatch(D, G, H, I)) {
                    return c
                }
                if (e.matchHasUnbalancedClosingParen(c) && (c = c.substr(0, c.length - 1), K = ")"), G) {
                    C = new b.match.Email({
                        matchedText: c,
                        email: G
                    })
                } else {
                    if (D) {
                        E && (J = E, c = c.slice(1)), C = new b.match.Twitter({
                            matchedText: c,
                            twitterHandle: F
                        })
                    } else {
                        if (I) {
                            var L = I.match(f)[1] || "";
                            L && (J = L, c = c.slice(1))
                        }
                        C = new b.match.Url({
                            matchedText: c,
                            url: c,
                            protocolRelativeMatch: I,
                            stripPrefix: e.stripPrefix
                        })
                    }
                }
                var M = e.createMatchReturnVal(C, c);
                return J + M + K
            })
        },
        isValidMatch: function(e, f, g, h) {
            return e && !this.twitter || f && !this.email || g && !this.urls || g && -1 === g.indexOf(".") || g && /^[A-Za-z]{3,9}:/.test(g) && !/:.*?[A-Za-z]/.test(g) || h && this.invalidProtocolRelMatchRegex.test(h) ? !1 : !0
        },
        matchHasUnbalancedClosingParen: function(g) {
            var h = g.charAt(g.length - 1);
            if (")" === h) {
                var i = g.match(/\(/g),
                    j = g.match(/\)/g),
                    k = i && i.length || 0,
                    l = j && j.length || 0;
                if (l > k) {
                    return !0
                }
            }
            return !1
        },
        createMatchReturnVal: function(a, g) {
            var h;
            if (this.replaceFn && (h = this.replaceFn.call(this, this, a)), "string" == typeof h) {
                return h
            }
            if (h === !1) {
                return g
            }
            if (h instanceof b.HtmlTag) {
                return h.toString()
            }
            var i = this.getTagBuilder(),
                j = i.build(a);
            return j.toString()
        }
    }, b.link = function(a, e) {
        var f = new b(e);
        return f.link(a)
    }, b.match = {}, b.Util = {
        abstractMethod: function() {
            throw "abstract"
        },
        assign: function(d, e) {
            for (var f in e) {
                e.hasOwnProperty(f) && (d[f] = e[f])
            }
            return d
        },
        extend: function(a, h) {
            var i = a.prototype,
                j = function() {};
            j.prototype = i;
            var k;
            k = h.hasOwnProperty("constructor") ? h.constructor : function() {
                i.constructor.apply(this, arguments)
            };
            var l = k.prototype = new j;
            return l.constructor = k, l.superclass = i, delete h.constructor, b.Util.assign(l, h), k
        },
        ellipsis: function(d, e, f) {
            return d.length > e && (f = null == f ? ".." : f, d = d.substring(0, e - f.length) + f), d
        },
        indexOf: function(e, f) {
            if (Array.prototype.indexOf) {
                return e.indexOf(f)
            }
            for (var g = 0, h = e.length; h > g; g++) {
                if (e[g] === f) {
                    return g
                }
            }
            return -1
        },
        splitAndCapture: function(f, g) {
            if (!g.global) {
                throw new Error("`splitRegex` must have the 'g' flag set")
            }
            for (var h, i = [], j = 0; h = g.exec(f);) {
                i.push(f.substring(j, h.index)), i.push(h[0]), j = h.index + h[0].length
            }
            return i.push(f.substring(j)), i
        }
    }, b.HtmlParser = b.Util.extend(Object, {
        htmlRegex: function() {
            var e = /[0-9a-zA-Z:]+/,
                f = /[^\s\0"'>\/=\x01-\x1F\x7F]+/,
                g = /(?:".*?"|'.*?'|[^'"=<>`\s]+)/,
                h = f.source + "(?:\\s*=\\s*" + g.source + ")?";
            return new RegExp(["<(?:!|(/))?", "(" + e.source + ")", "(?:", "\\s+", "(?:", h, "|", g.source + ")", ")*", "\\s*/?", ">"].join(""), "g")
        }(),
        parse: function(m, n) {
            n = n || {};
            for (var o, p = n.processHtmlNode || function() {}, q = n.processTextNode || function() {}, r = this.htmlRegex, s = 0; null !== (o = r.exec(m));) {
                var t = o[0],
                    u = o[2],
                    v = !! o[1],
                    w = m.substring(s, o.index);
                w && q(w), p(t, u, v), s = o.index + t.length
            }
            if (s < m.length) {
                var x = m.substring(s);
                x && q(x)
            }
        }
    }), b.HtmlTag = b.Util.extend(Object, {
        whitespaceRegex: /\s+/,
        constructor: function(a) {
            b.Util.assign(this, a), this.innerHtml = this.innerHtml || this.innerHTML
        },
        setTagName: function(c) {
            return this.tagName = c, this
        },
        getTagName: function() {
            return this.tagName || ""
        },
        setAttr: function(d, e) {
            var f = this.getAttrs();
            return f[d] = e, this
        },
        getAttr: function(c) {
            return this.getAttrs()[c]
        },
        setAttrs: function(a) {
            var d = this.getAttrs();
            return b.Util.assign(d, a), this
        },
        getAttrs: function() {
            return this.attrs || (this.attrs = {})
        },
        setClass: function(c) {
            return this.setAttr("class", c)
        },
        addClass: function(a) {
            for (var i, j = this.getClass(), k = this.whitespaceRegex, l = b.Util.indexOf, m = j ? j.split(k) : [], n = a.split(k); i = n.shift();) {
                -1 === l(m, i) && m.push(i)
            }
            return this.getAttrs()["class"] = m.join(" "), this
        },
        removeClass: function(a) {
            for (var j, k = this.getClass(), l = this.whitespaceRegex, m = b.Util.indexOf, n = k ? k.split(l) : [], o = a.split(l); n.length && (j = o.shift());) {
                var p = m(n, j); - 1 !== p && n.splice(p, 1)
            }
            return this.getAttrs()["class"] = n.join(" "), this
        },
        getClass: function() {
            return this.getAttrs()["class"] || ""
        },
        hasClass: function(c) {
            return -1 !== (" " + this.getClass() + " ").indexOf(" " + c + " ")
        },
        setInnerHtml: function(c) {
            return this.innerHtml = c, this
        },
        getInnerHtml: function() {
            return this.innerHtml || ""
        },
        toString: function() {
            var c = this.getTagName(),
                d = this.buildAttrsStr();
            return d = d ? " " + d : "", ["<", c, d, ">", this.getInnerHtml(), "</", c, ">"].join("")
        },
        buildAttrsStr: function() {
            if (!this.attrs) {
                return ""
            }
            var d = this.getAttrs(),
                e = [];
            for (var f in d) {
                d.hasOwnProperty(f) && e.push(f + '="' + d[f] + '"')
            }
            return e.join(" ")
        }
    }), b.AnchorTagBuilder = b.Util.extend(Object, {
        constructor: function(a) {
            b.Util.assign(this, a)
        },
        build: function(a) {
            var d = new b.HtmlTag({
                tagName: "a",
                attrs: this.createAttrs(a.getType(), a.getAnchorHref()),
                innerHtml: this.processAnchorText(a.getAnchorText())
            });
            return d
        },
        createAttrs: function(e, f) {
            var g = {
                href: f
            }, h = this.createCssClass(e);
            return h && (g["class"] = h), this.newWindow && (g.target = "_blank"), g
        },
        createCssClass: function(c) {
            var d = this.className;
            return d ? d + " " + d + "-" + c : ""
        },
        processAnchorText: function(c) {
            return c = this.doTruncate(c)
        },
        doTruncate: function(a) {
            return b.Util.ellipsis(a, this.truncate || Number.POSITIVE_INFINITY)
        }
    }), b.match.Match = b.Util.extend(Object, {
        constructor: function(a) {
            b.Util.assign(this, a)
        },
        getType: b.Util.abstractMethod,
        getMatchedText: function() {
            return this.matchedText
        },
        getAnchorHref: b.Util.abstractMethod,
        getAnchorText: b.Util.abstractMethod
    }), b.match.Email = b.Util.extend(b.match.Match, {
        getType: function() {
            return "email"
        },
        getEmail: function() {
            return this.email
        },
        getAnchorHref: function() {
            return "mailto:" + this.email
        },
        getAnchorText: function() {
            return this.email
        }
    }), b.match.Twitter = b.Util.extend(b.match.Match, {
        getType: function() {
            return "twitter"
        },
        getTwitterHandle: function() {
            return this.twitterHandle
        },
        getAnchorHref: function() {
            return "https://twitter.com/" + this.twitterHandle
        },
        getAnchorText: function() {
            return "@" + this.twitterHandle
        }
    }), b.match.Url = b.Util.extend(b.match.Match, {
        urlPrefixRegex: /^(https?:\/\/)?(www\.)?/i,
        protocolRelativeRegex: /^\/\//,
        checkForProtocolRegex: /^[A-Za-z]{3,9}:/,
        getType: function() {
            return "url"
        },
        getUrl: function() {
            var c = this.url;
            return this.protocolRelativeMatch || this.checkForProtocolRegex.test(c) || (c = this.url = "http://" + c), c
        },
        getAnchorHref: function() {
            var c = this.getUrl();
            return c.replace(/&amp;/g, "&")
        },
        getAnchorText: function() {
            var c = this.getUrl();
            return this.protocolRelativeMatch && (c = this.stripProtocolRelativePrefix(c)), this.stripPrefix && (c = this.stripUrlPrefix(c)), c = this.removeTrailingSlash(c)
        },
        stripUrlPrefix: function(c) {
            return c.replace(this.urlPrefixRegex, "")
        },
        stripProtocolRelativePrefix: function(c) {
            return c.replace(this.protocolRelativeRegex, "")
        },
        removeTrailingSlash: function(c) {
            return "/" === c.charAt(c.length - 1) && (c = c.slice(0, -1)), c
        }
    }), b
});
var StockCarsList;
(function(f) {
    var a = function d(r, s, t, i, q, g, j, m, h, p, o, w, n, l, k, v, u) {
        this.pageCount = r;
        this.priceFrom = s;
        this.priceTo = t;
        this.dealerDistrict = i;
        this.modelType = q;
        this.bodyType = g;
        this.fuel = j;
        this.isAction = m;
        this.colorGroups = h;
        this.minPower = p;
        this.maxConsumption = o;
        this.transmission = w;
        this.isAwd = n;
        this.indexFrom = l;
        this.holdIndex = k;
        this.sortOption = v;
        this.sortAsc = u
    };
    f.Filter = a;
    var b = {
        Nothing: 0,
        Model: 1,
        ModelBody: 2,
        Price: 3,
        Action: 4,
        Power: 5,
        Consumption: 6,
        Fuel: 7,
        Transmission: 8,
        Awd: 9,
        Color: 10,
        Sort: 11
    };
    var c = function e(j, k, h, i, u, t, n, o, r, l, q, m, p, s) {
        this.stockcarslistData = null;
        this.handlerUrl = j;
        this.importerBid = k;
        this.cultureName = h;
        this.dealerId = i;
        this.subsidiariesOption = u;
        this.nameSource = t;
        var g = "";
        if (n == "False") {
            $(".stockcarslistfilterDealerDistrict").hide()
        }
        if (o == "False") {
            $(".stockcarslistfilterModel").hide()
        }
        if (r == "False") {
            $(".stockcarslistfilterPrice").hide()
        }
        if (l == "False") {
            $(".stockcarslistfilterColor").hide();
            this.isColorFilterDisplayed = false;
            g = ""
        } else {
            this.isColorFilterDisplayed = true;
            g = "*"
        } if (q == "False") {
            $(".stockcarslistfilterPower").hide()
        }
        if (m == "False") {
            $(".stockcarslistfilterConsumption").hide()
        }
        if (p == "False") {
            $(".stockcarslistfilterOther").hide()
        }
        if (n == "False" && o == "False" && r == "False" && l == "False" && q == "False" && m == "False" && p == "False") {
            $(".stockcarslistFilterContainer").hide()
        }
        this.clearState();
        this.filter = new a(10, -10000000, 4294967295, "", s != "" ? s : "", "", "", false, g, 0, 4294967295, "", false, 0, false, "m", true)
    };
    c.prototype = {
        init: function() {
            var g = this;
            $.address.init(function(h) {}).internalChange(function(h) {}).externalChange(function(h) {
                if (h.parameters && h.parameterNames.length > 0) {
                    g.setFilterFromAddress(h);
                    g.loadCarsList(true, false, false)
                } else {
                    g.loadCarsList(true, true, false)
                }
            })
        },
        clearFilter: function() {
            this.filter.pageCount = 10;
            this.filter.indexFrom = 0;
            this.filter.priceFrom = 0;
            this.filter.priceTo = 4294967295;
            this.filter.dealerDistrict = "";
            this.filter.modelType = "";
            this.filter.bodyType = "";
            if (this.isColorFilterDisplayed) {
                this.filter.colorGroups = ""
            } else {
                this.filter.colorGroups = ""
            }
            this.filter.minPower = 0;
            this.filter.maxConsumption = 4294967295;
            this.filter.transmission = "";
            this.filter.isAwd = false;
            this.filter.fuel = "";
            this.filter.isAction = false;
            this.filter.holdIndex = false;
            this.filter.sortOption = "m";
            this.filter.sortAsc = true;
            this.loadCarsList(false, false, false)
        },
        clearState: function() {
            this.settingValuesProgress = false;
            this.isInitialized = false;
            this.isPriceMinChanged = false;
            this.isPriceMaxChanged = false;
            this.isPowerChanged = false;
            this.isConsumptionChanged = false
        },
        initFilterFromData: function() {
            if (this.stockcarslistData == null) {
                return
            }
            this.filter.pageCount = 10;
            this.filter.indexFrom = 0;
            this.filter.priceFrom = this.stockcarslistData.Filter.Price.MinPrice - 0;
            this.filter.priceTo = this.stockcarslistData.Filter.Price.MaxPrice - 0;
            this.filter.dealerDistrict = "";
            if (this.filter.modelType == "") {
                this.filter.modelType = ""
            }
            this.filter.bodyType = "";
            if (this.isColorFilterDisplayed) {
                this.filter.colorGroups = this.getSelectedColorGroupsIds()
            } else {
                this.filter.colorGroups = ""
            }
            this.filter.minPower = this.stockcarslistData.Filter.MinPower - 0;
            this.filter.maxConsumption = this.stockcarslistData.Filter.MaxConsumption - 0;
            this.filter.transmission = "";
            this.filter.isAwd = false;
            this.filter.fuel = "";
            this.filter.isAction = false;
            this.filter.holdIndex = false;
            this.filter.sortOption = "m";
            this.filter.sortAsc = true
        },
        setFilterFromAddress: function(g) {
            this.filter.pageCount = g.parameters.pagCnt - 0;
            this.filter.indexFrom = g.parameters.idxFr - 0;
            this.filter.priceFrom = g.parameters.prFr - 0;
            this.filter.priceTo = g.parameters.prTo - 0;
            this.filter.dealerDistrict = g.parameters.district;
            this.filter.modelType = g.parameters.modelT;
            this.filter.bodyType = g.parameters.bodyT;
            this.filter.colorGroups = g.parameters.colGrs;
            this.filter.minPower = g.parameters.minPow - 0;
            this.filter.maxConsumption = g.parameters.maxCon - 0;
            this.filter.transmission = g.parameters.tran;
            this.filter.isAwd = g.parameters.isAwd == "true" ? true : false;
            this.filter.fuel = g.parameters.fuel;
            this.filter.isAction = g.parameters.isAct == "true" ? true : false;
            this.filter.holdIndex = g.parameters.holdIdx == "true" ? true : false;
            this.filter.sortOption = g.parameters.sort;
            this.filter.sortAsc = g.parameters.asc == "true" ? true : false
        },
        setAddressFromFilter: function() {
            if (this.settingValuesProgress) {
                return
            }
            var g = "/filter/";
            g = g + "?idxFr=" + this.filter.indexFrom;
            g = g + "&pagCnt=" + this.filter.pageCount;
            g = g + "&prFr=" + this.filter.priceFrom;
            g = g + "&prTo=" + this.filter.priceTo;
            g = g + "&district=" + this.filter.dealerDistrict;
            g = g + "&modelT=" + this.filter.modelType;
            g = g + "&bodyT=" + this.filter.bodyType;
            g = g + "&colGrs=" + this.filter.colorGroups;
            g = g + "&minPow=" + this.filter.minPower;
            g = g + "&maxCon=" + this.filter.maxConsumption;
            g = g + "&tran=" + this.filter.transmission;
            g = g + "&isAwd=" + this.filter.isAwd;
            g = g + "&fuel=" + this.filter.fuel;
            g = g + "&isAct=" + this.filter.isAction;
            g = g + "&holdIdx=" + this.filter.holdIndex;
            g = g + "&sort=" + this.filter.sortOption;
            g = g + "&asc=" + this.filter.sortAsc;
            $.address.value(g)
        },
        setControlValues: function() {
            this.setPriceSliderValue();
            this.setFilterDealerDistrictsValue();
            this.setFilterModelGroupsValue();
            this.setFilterBodyTypesValue();
            this.setFilterFuelTypesValue();
            this.setFilterTransmissionValue();
            this.setActionModelFilterValue();
            this.setColorGroups();
            this.setConsumptionSliderValue();
            this.setPowerSliderValue();
            this.setAwdCheckBoxValue();
            this.setSortingDropDownValue();
            this.setSortingLabelValue()
        },
        loadCarsList: function(g, h, i) {
            $(".stockcarslistLoadingImage").show();
            var k = this.handlerUrl;
            k = k || "//skodainterfacesv2.cloudapp.net/services/stockcarslistdatasource.ashx";
            if (!i) {
                this.filter.indexFrom = 0
            }
            k = k + "?idxFr=" + this.filter.indexFrom + "&pagCnt=" + this.filter.pageCount;
            k = k + "&prFr=" + (this.isPriceMinChanged ? this.filter.priceFrom : 0);
            k = k + "&prTo=" + (this.isPriceMaxChanged ? this.filter.priceTo : 4294967295);
            k = k + "&district=" + this.filter.dealerDistrict;
            k = k + "&modelT=" + this.filter.modelType;
            k = k + "&bodyT=" + this.filter.bodyType;
            k = k + "&colGrs=" + this.filter.colorGroups;
            k = k + "&minPow=" + (this.isPowerChanged ? this.filter.minPower : 0);
            k = k + "&maxCon=" + (this.isConsumptionChanged ? this.filter.maxConsumption : 4294967295);
            k = k + "&tran=" + this.filter.transmission;
            k = k + "&isAwd=" + this.filter.isAwd;
            k = k + "&fuel=" + this.filter.fuel;
            k = k + "&isAct=" + this.filter.isAction;
            k = k + "&sort=" + this.filter.sortOption;
            k = k + "&asc=" + this.filter.sortAsc;
            k = k + "&bid=" + this.importerBid;
            k = k + "&culture=" + this.cultureName;
            k = k + "&dealerId=" + this.dealerId;
            k = k + "&subs=" + this.subsidiariesOption;
            k = k + "&nameSrc=" + this.nameSource;
            var j = this;
            $.ajax({
                url: k,
                async: true,
                cache: false,
                timeout: 30000,
                dataType: "json",
                type: "GET",
                context: document.body,
                error: function(m, l) {
                    $(".stockcarslistLoadingImage").hide();
                    $(".stockcarslistPaging").hide();
                    j.showNoCarsMessage()
                }
            }).done(function(l) {
                if (l) {
                    if (l == null || l.Cars == null) {
                        $(".stockcarslistPaging").hide();
                        j.showNoCarsMessage();
                        return
                    }
                    if (l.Cars.length < 1) {
                        $(".stockcarslistPaging").hide();
                        j.showNoCarsMessage()
                    } else {
                        $(".stockcarslistPaging").show();
                        j.hideNoCarsMessage()
                    }
                    j.stockcarslistData = l;
                    if (g) {
                        j.initStockCarsList();
                        if (h) {
                            j.initFilterFromData()
                        }
                    }
                    j.fillStockCarsList();
                    j.settingValuesProgress = true;
                    j.setControlValues();
                    j.setPaging();
                    j.setAddressFromFilter();
                    j.settingValuesProgress = false
                }
                $(".stockcarslistLoadingImage").hide()
            })
        },
        fillStockCarsList: function() {
            var i = this;
            var j = "#stockcarslistItemTemplate";
            var g = $("#stockcarslistItems");
            g.empty();
            var h = $(j);
            $.each(this.stockcarslistData.Cars, function(l, k) {
                i.removeCurrencySymbols(k);
                h.tmpl(k).appendTo(g)
            });
            $("#stockcarslistAllArea .stockcarslistItemInfoRowValueEquipment").bind({
                mouseenter: function() {
                    var l = $(this).closest(".stockcarslistItem");
                    i.fillEquipment(l);
                    var m = $(this);
                    var k = m.parent().find(".stockcarslistItemInfoRowLayer");
                    k.css("display", "block")
                },
                mouseleave: function() {
                    var l = $(this);
                    var k = l.parent().find(".stockcarslistItemInfoRowLayer");
                    k.hide()
                }
            });
            this.refreshListCurrencySymbol()
        },
        removeCurrencySymbols: function(g) {
            g.FinalPrice = this.removeCurrencySymbol(g.FinalPrice);
            g.CustomerPrice = this.removeCurrencySymbol(g.CustomerPrice)
        },
        removeCurrencySymbol: function(i) {
            if (i == null || i == "") {
                return ""
            }
            var h = /[\d\s.,]+/;
            var g = i.match(h);
            if (g == null) {
                return ""
            }
            return g[0]
        },
        refreshListCurrencySymbol: function() {
            var g = $("#webUrl").val();
            $(".currency-green").currency({
                webUrl: g,
                symbolName: "green"
            });
            $(".currency-black").currency({
                webUrl: g,
                symbolName: "black"
            })
        },
        initStockCarsList: function() {
            this.fillStockCarsList();
            this.fillFilterBodyTypes();
            this.fillFilterDealerDistricts();
            this.fillFilterModelGroups();
            this.fillFuelTypesDropDown();
            this.fillTransmissionDropDown();
            this.fillColorGroups();
            this.defineActionModelFilterEvents();
            this.setPriceSlider();
            this.setPowerSlider();
            this.setConsumptionSlider();
            this.setAwdCheckBox();
            this.bindPagerButtonsEvents();
            this.setClearFilterButtonEvents();
            this.fillSortingDropDown();
            this.setSortOrderButtonEvents();
            this.isInitialized = true
        },
        setSlice: function(g, h) {
            $("#stockcarslistItems > div").removeClass("hidePaging");
            $("#stockcarslistItems > div:visible").addClass("hidePaging").slice(g, h).removeClass("hidePaging")
        },
        bindPagerButtonsEvents: function() {
            var h = this;
            var g = $("#stockcarslistAllArea .stockcarslistPagingArrowLeft");
            g.unbind("click");
            g.click(function() {
                if (h.filter.indexFrom > 0) {
                    h.filter.indexFrom -= h.filter.pageCount;
                    h.loadCarsList(false, false, true)
                }
                return false
            });
            var i = $("#stockcarslistAllArea .stockcarslistPagingArrowRight");
            i.unbind("click");
            i.click(function() {
                var j = h.stockcarslistData.AllCarsCount;
                if ((h.filter.indexFrom + h.filter.pageCount) < j) {
                    h.filter.indexFrom += h.filter.pageCount;
                    h.loadCarsList(false, false, true)
                }
                return false
            })
        },
        setPaging: function() {
            if (this.stockcarslistData != null && this.stockcarslistData.Cars != null && this.stockcarslistData.Cars.length > 0) {
                var h = this.stockcarslistData.AllCarsCount;
                $("#stockcarslistAllArea .stockcarslistPagingCount span").text(h);
                var i = this.filter.indexFrom;
                var j = this.filter.indexFrom + this.filter.pageCount;
                if (j > h) {
                    j = h
                }
                var g = $("#stockcarslistAllArea .stockcarslistPagingItems");
                g.text((i + 1) + " - " + j)
            } else {
                $("#stockcarslistAllArea .stockcarslistPagingCount span").text(0);
                $("#stockcarslistAllArea .stockcarslistPagingItems").text(0)
            }
        },
        fillEquipment: function(h) {
            var g = h.find(".stockcarslistItemInfo > .stockcarslistItemInfoRow > span.stockcarslistItemInfoRowValueEquipment > span.stockcarslistItemInfoRowLayer > ul.stockcarslistItemExtraEquipment");
            var i = h.find(".stockcarslistItemInfo > .stockcarslistItemInfoRow > span.stockcarslistItemInfoRowValueEquipment > span.stockcarslistItemInfoRowLayer > ul.stockcarslistItemStandardEquipment");
            if (g.has("li").length && i.has("li").length) {
                return
            }
            var k = h.attr("class").split(" ")[4].substring(3);
            var l = "http://skodainterfacesv2.cloudapp.net/Services/DataService.svc/StockCars(" + k + ")?$format=json&$callback=?&$expand=ExtraEquipmentItems,StandardEquipmentItems";
            var j;
            $.ajax({
                url: l,
                async: false,
                timeout: 10000,
                crossDomain: true,
                dataType: "jsonp",
                context: document.body,
                error: function(n, m) {
                    console.log("error retreiving equipments")
                }
            }).done(function(m) {
                j = m;
                g.empty();
                var p = [];
                if (j.ExtraEquipmentItems != null && j.ExtraEquipmentItems.length > 0) {
                    for (var n = 0; n < 10 && n < j.ExtraEquipmentItems.length; n++) {
                        var o = j.ExtraEquipmentItems[n];
                        p.push("<li>" + o.Name + "</li>")
                    }
                }
                var q = p.join("");
                g.append(q);
                i.empty();
                p = [];
                if (j.StandardEquipmentItems != null && j.StandardEquipmentItems.length > 0) {
                    for (n = 0; n < 10 && n < j.StandardEquipmentItems.length; n++) {
                        o = j.StandardEquipmentItems[n];
                        p.push("<li>" + o.Name + "</li>")
                    }
                }
                q = p.join("");
                i.append(q)
            })
        },
        setPriceSliderValue: function() {
            var i, h, g, l, j, k;
            g = this.filter.priceFrom;
            l = this.filter.priceTo;
            i = this.stockcarslistData.Filter.Price.MinPrice;
            h = this.stockcarslistData.Filter.Price.MaxPrice;
            if (i > g || !this.isPriceMinChanged) {
                g = this.stockcarslistData.Filter.Price.MinPrice
            }
            if (h < l || !this.isPriceMaxChanged) {
                l = this.stockcarslistData.Filter.Price.MaxPrice
            }
            $("#sliderBudget").slider({
                min: i,
                max: h,
                values: [g, l]
            });
            j = this.roundNumberBudget(g);
            k = this.roundNumberBudget(l);
            $("#labelBudgetFrom").text(accounting.formatNumber(j));
            $("#labelBudgetTo").text(accounting.formatNumber(k));
            $("#labelBudgetMin").text(accounting.formatNumber(i));
            $("#labelBudgetMax").text(accounting.formatNumber(h));
            this.refreshFilterCurrencySymbol()
        },
        refreshFilterCurrencySymbol: function() {
            this.refreshFilterRangeCurrencySymbol();
            this.refreshFilterMaxRangeCurrencySymbol()
        },
        refreshFilterMaxRangeCurrencySymbol: function() {
            var g = $("#webUrl").val();
            $("#labelBudgetMin").currency({
                webUrl: g,
                symbolName: "gray-9f"
            });
            $("#labelBudgetMax").currency({
                webUrl: g,
                symbolName: "gray-9f"
            })
        },
        filterModelsByPrice: function(g, h) {
            this.filter.priceFrom = g;
            this.filter.priceTo = h;
            if (!this.settingValuesProgress) {
                this.loadCarsList(false, false, false)
            }
        },
        setPriceSlider: function() {
            var l = this;
            var j = 0;
            var h = 10000000;
            if (l.stockcarslistData.Filter != null) {
                j = l.stockcarslistData.Filter.Price.MinPrice;
                h = l.stockcarslistData.Filter.Price.MaxPrice;
                var g = "%s%v";
                switch (l.stockcarslistData.Filter.Price.CurrencyFormatPattern) {
                    case 0:
                        g = "%s%v";
                        break;
                    case 1:
                        g = "%v%s";
                        break;
                    case 2:
                        g = "%s %v";
                        break;
                    case 3:
                        g = "%v %s";
                        break
                }
                accounting.settings = {
                    currency: {
                        symbol: l.stockcarslistData.Filter.Price.CurrencySymbol,
                        format: g,
                        precision: 0,
                        decimal: ".",
                        thousand: l.stockcarslistData.Filter.Price.CurrencyGroupSeparator
                    },
                    number: {
                        precision: 0,
                        decimal: ".",
                        thousand: l.stockcarslistData.Filter.Price.CurrencyGroupSeparator
                    }
                }
            }
            var k = l.filter.priceFrom;
            var i = l.filter.priceTo;
            if (k < j) {
                k = j
            }
            if (i > h) {
                i = h
            }
            var m = 1;
            if (h > 0) {
                m = Math.floor((h - j) / 4000) * 100;
                if (m > 1000 && m < 5000) {
                    m = 1000
                } else {
                    if (m > 5000 && m < 40000) {
                        m = 10000
                    } else {
                        if (m > 40000) {
                            m = 50000
                        }
                    }
                }
            }
            $("#sliderBudget").slider({
                range: true,
                min: j,
                max: h,
                values: [k, i],
                step: m,
                slide: function(n, q) {
                    var r = q.values[0];
                    var s = q.values[1];
                    var o = l.roundNumberBudget(r);
                    var p = l.roundNumberBudget(s);
                    $("#labelBudgetFrom").text(accounting.formatNumber(o));
                    $("#labelBudgetTo").text(accounting.formatNumber(p));
                    l.refreshFilterRangeCurrencySymbol()
                },
                change: function(n, s) {
                    var t = s.values[0];
                    var u = s.values[1];
                    var q = l.roundNumberBudget(t);
                    var r = l.roundNumberBudget(u);
                    var p = $("#sliderBudget").slider("option", "min");
                    var o = $("#sliderBudget").slider("option", "max");
                    l.isPriceMinChanged = l.settingValuesProgress ? l.isPriceMinChanged : t != p;
                    l.isPriceMaxChanged = l.settingValuesProgress ? l.isPriceMaxChanged : u != o;
                    l.filterModelsByPrice(q, r)
                }
            });
            $("#labelBudgetMin").text(accounting.formatNumber(j));
            $("#labelBudgetMax").text(accounting.formatNumber(h));
            $("#labelBudgetFrom").text(accounting.formatNumber(k));
            $("#labelBudgetTo").text(accounting.formatNumber(i));
            this.refreshFilterCurrencySymbol()
        },
        refreshFilterRangeCurrencySymbol: function() {
            var g = $("#webUrl").val();
            $("#labelBudgetFrom").currency({
                webUrl: g,
                symbolName: "gray-9f"
            });
            $("#labelBudgetTo").currency({
                webUrl: g,
                symbolName: "gray-9f"
            })
        },
        roundNumberBudget: function(g) {
            var h = Math.round(g);
            return h
        },
        setFilterDealerDistrictsValue: function() {
            var h = $("#stockcarslistfilterAllArea .stockcarslistfilterMainDealerDistrictSelect");
            $("#stockcarslistfilterAllArea .stockcarslistfilterMainDealerDistrictSelect option").not('[value="All"]').remove();
            if (this.stockcarslistData.Filter.DealerDistricts != null) {
                var g = $("#stockcarslistfilterAllArea .stockcarslistfilterMainDealerDistrictLabel");
                if (this.stockcarslistData.Filter.DealerDistricts.length === 1) {
                    g.text(this.stockcarslistData.Filter.DealerDistricts[0].Name);
                    g.parent().show();
                    h.hide()
                } else {
                    h.show();
                    g.parent().hide()
                } if (this.stockcarslistData.Filter.DealerDistrictsAll.length === 1) {
                    g.parent().addClass("stockcarslistFilterSelectSubstituteDisabled")
                } else {
                    g.parent().removeClass("stockcarslistFilterSelectSubstituteDisabled")
                }
                $.each(this.stockcarslistData.Filter.DealerDistricts, function(j, i) {
                    $("#stockcarslistTemplateDealerGroups").tmpl(i).appendTo(h)
                });
                if (this.filter.dealerDistrict == "") {
                    h.val("All")
                } else {
                    h.val(this.filter.dealerDistrict)
                }
            }
        },
        fillFilterDealerDistricts: function() {
            if (this.stockcarslistData.Filter.DealerDistricts == null) {
                return
            }
            var g = $("#stockcarslistfilterAllArea .stockcarslistfilterMainDealerDistrictSelect");
            $.each(this.stockcarslistData.Filter.DealerDistricts, function(i, h) {
                $("#stockcarslistTemplateDealerGroups").tmpl(h).appendTo(g)
            });
            this.defineFilterDealerDistrictsEvents()
        },
        defineFilterDealerDistrictsEvents: function() {
            var h = $("#stockcarslistfilterAllArea .stockcarslistfilterMainDealerDistrictSelect");
            var i = this;
            var g = $("#stockcarslistfilterAllArea .stockcarslistfilterMainDealerDistrictLabel");
            g.next().click(function() {
                i.filter.dealerDistrict = "";
                if (!i.settingValuesProgress) {
                    i.loadCarsList(false, false, false)
                }
            });
            h.bind("change", function() {
                var j = $(this);
                var k = j.val();
                if (k == "All") {
                    i.filter.dealerDistrict = ""
                } else {
                    i.filter.dealerDistrict = $("#stockcarslistfilterAllArea .stockcarslistfilterMainDealerDistrictSelect option:selected").val()
                } if (!i.settingValuesProgress) {
                    i.loadCarsList(false, false, false)
                }
            })
        },
        setFilterModelGroupsValue: function() {
            var i = this;
            var h = $("#stockcarslistfilterAllArea .stockcarslistfilterMainModelGroupSelect");
            $("#stockcarslistfilterAllArea .stockcarslistfilterMainModelGroupSelect option").not('[value="All"]').remove();
            if (this.stockcarslistData.Filter.ModelGroups != null) {
                var g = $("#stockcarslistfilterAllArea .stockcarslistfilterMainModelGroupLabel");
                if (this.stockcarslistData.Filter.ModelGroups.length === 1) {
                    g.text(this.stockcarslistData.Filter.ModelGroups[0].Name);
                    g.parent().show();
                    h.hide()
                } else {
                    h.show();
                    g.parent().hide()
                } if (this.stockcarslistData.Filter.ModelGroupsAll.length === 1) {
                    g.parent().addClass("stockcarslistFilterSelectSubstituteDisabled")
                } else {
                    g.parent().removeClass("stockcarslistFilterSelectSubstituteDisabled")
                }
                $.each(this.stockcarslistData.Filter.ModelGroups, function(j, k) {
                    $("#stockcarslistTemplateModelGroups").tmpl(k).appendTo(h)
                });
                if (this.filter.modelType == "") {
                    h.val("All")
                } else {
                    h.val(this.filter.modelType)
                }
            }
        },
        fillFilterModelGroups: function() {
            if (this.stockcarslistData.Filter.ModelGroups == null) {
                return
            }
            var g = $("#stockcarslistfilterAllArea .stockcarslistfilterMainModelGroupSelect");
            $.each(this.stockcarslistData.Filter.ModelGroups, function(h, i) {
                $("#stockcarslistTemplateModelGroups").tmpl(i).appendTo(g)
            });
            this.defineFilterModelGroupsEvents()
        },
        defineFilterModelGroupsEvents: function() {
            var h = $("#stockcarslistfilterAllArea .stockcarslistfilterMainModelGroupSelect");
            var i = this;
            var g = $("#stockcarslistfilterAllArea .stockcarslistfilterMainModelGroupLabel");
            g.next().click(function() {
                i.filter.modelType = "";
                if (!i.settingValuesProgress) {
                    i.loadCarsList(false, false, false)
                }
            });
            h.bind("change", function() {
                var j = $(this);
                var k = j.val();
                if (k == "All") {
                    i.filter.modelType = ""
                } else {
                    i.filter.modelType = $("#stockcarslistfilterAllArea .stockcarslistfilterMainModelGroupSelect option:selected").val()
                } if (!i.settingValuesProgress) {
                    i.loadCarsList(false, false, false)
                }
            })
        },
        fillFilterBodyTypesByModelGroup: function() {
            var i = $("#stockcarslistfilterAllArea .stockcarslistfilterMainModelGroupSelect option:selected").val();
            var g = this;
            $.each($("#stockcarslistfilterAllArea .stockcarslistfilterMainBodyTypeSelect option"), function(k, j) {
                if (i != "All" && $(j).val() != "All" && $(j).val().substr(0, 2) != i) {
                    $(g.filter.bodyType).hide();
                    $(g.filter.bodyType).attr("disabled", "disabled")
                } else {
                    $(g.filter.bodyType).show();
                    $(g.filter.bodyType).removeAttr("disabled")
                }
            });
            var h = $("#stockcarslistfilterAllArea .stockcarslistfilterMainBodyTypeSelect option:selected").val();
            if (h && h.length > 0 && h.substr(0, 2) != i && h != "All") {
                this.filter.bodyType = ""
            }
        },
        setFilterBodyTypesValue: function() {
            var g = $("#stockcarslistfilterAllArea .stockcarslistfilterMainBodyTypeSelect");
            $("#stockcarslistfilterAllArea .stockcarslistfilterMainBodyTypeSelect option").not('[value="All"]').remove();
            this.fillFilterBodyTypesValues();
            if (this.filter.bodyType == "") {
                g.val("All")
            } else {
                g.val(this.filter.bodyType)
            }
        },
        isBodyEnabledForModelType: function(g, n) {
            if (n == "All") {
                return true
            }
            if (this.stockcarslistData != null) {
                var m = this.stockcarslistData.Filter.ModelGroups;
                for (var h = 0; h < m.length; h++) {
                    var l = m[h].ModelBodies;
                    if (n == m[h].Value) {
                        for (var k = 0; k < l.length; k++) {
                            if (g == l[k].ID) {
                                return true
                            }
                        }
                    }
                }
            }
            return false
        },
        fillFilterBodyTypesValues: function() {
            if (this.stockcarslistData.Filter.BodyTypes == null) {
                return
            }
            var h = $("#stockcarslistfilterAllArea .stockcarslistfilterMainBodyTypeSelect");
            var i = this;
            var g = $("#stockcarslistfilterAllArea .stockcarslistfilterMainBodyTypeLabel");
            if (this.stockcarslistData.Filter.BodyTypes.length === 1) {
                g.text(this.stockcarslistData.Filter.BodyTypes[0].Title);
                g.parent().show();
                h.hide()
            } else {
                g.parent().hide();
                h.show()
            } if (this.stockcarslistData.Filter.BodyTypesAll.length === 1) {
                g.parent().addClass("stockcarslistFilterSelectSubstituteDisabled")
            } else {
                g.parent().removeClass("stockcarslistFilterSelectSubstituteDisabled")
            }
            $.each(this.stockcarslistData.Filter.BodyTypes, function(k, j) {
                var l = $("#stockcarslistfilterAllArea .stockcarslistfilterMainModelGroupSelect option:selected").val();
                if (i.isBodyEnabledForModelType(j.Code, l)) {
                    $("#stockcarslistTemplateBodyTypes").tmpl(j).appendTo(h)
                } else {
                    $("#stockcarslistTemplateBodyTypes").tmpl(j).appendTo(h).attr("disabled", "disabled")
                }
            })
        },
        fillFilterBodyTypes: function() {
            this.fillFilterBodyTypesValues();
            this.defineFilterBodyTypesEvents()
        },
        defineFilterBodyTypesEvents: function() {
            var h = $("#stockcarslistfilterAllArea .stockcarslistfilterMainBodyTypeSelect");
            var i = this;
            var g = $("#stockcarslistfilterAllArea .stockcarslistfilterMainBodyTypeLabel");
            g.next().click(function() {
                i.filter.bodyType = "";
                if (!i.settingValuesProgress) {
                    i.loadCarsList(false, false, false)
                }
            });
            h.bind("change", function() {
                var j = $(this);
                var k = j.val();
                if (k == "All") {
                    i.filter.bodyType = ""
                } else {
                    i.filter.bodyType = k
                } if (!i.settingValuesProgress) {
                    i.loadCarsList(false, false, false)
                }
            })
        },
        setFilterFuelTypesValue: function() {
            var h = $("#stockcarslistfilterAllArea .stockcarslistfilterMainFuelTypeSelect");
            if (this.stockcarslistData.Filter.FuelTypes != null) {
                var g = $("#stockcarslistfilterAllArea .stockcarslistfilterMainFuelTypeLabel");
                if (this.stockcarslistData.Filter.FuelTypes.length === 1) {
                    h.hide();
                    g.text(this.stockcarslistData.Filter.FuelTypes[0].Name);
                    g.parent().show()
                } else {
                    g.parent().hide();
                    h.show()
                } if (this.stockcarslistData.Filter.FuelTypesAll.length === 1) {
                    g.parent().addClass("stockcarslistFilterSelectSubstituteDisabled")
                } else {
                    g.parent().removeClass("stockcarslistFilterSelectSubstituteDisabled")
                }
                $("#stockcarslistfilterAllArea .stockcarslistfilterMainFuelTypeSelect option").not('[value=""]').remove();
                $.each(this.stockcarslistData.Filter.FuelTypes, function(j, i) {
                    $("#stockcarslistTemplateFuelTypes").tmpl(i).appendTo(h)
                })
            }
            h.val(this.filter.fuel)
        },
        fillFuelTypesDropDown: function() {
            var h = $("#stockcarslistfilterAllArea .stockcarslistfilterMainFuelTypeSelect"),
                g = $("#stockcarslistfilterAllArea .stockcarslistfilterMainFuelTypeLabel"),
                i = this;
            $.each(this.stockcarslistData.Filter.FuelTypes, function(k, j) {
                $("#stockcarslistTemplateFuelTypes").tmpl(j).appendTo(h)
            });
            $("#stockcarslistfilterAllArea .stockcarslistfilterMainFuelTypeLabel").next().click(function() {
                i.filter.fuel = "";
                if (!i.settingValuesProgress) {
                    i.loadCarsList(false, false, false)
                }
            });
            h.bind("change", function() {
                var j = $(this);
                var k = j.val();
                i.filter.fuel = k;
                if (!i.settingValuesProgress) {
                    i.loadCarsList(false, false, false)
                }
            })
        },
        setFilterTransmissionValue: function() {
            var h = $("#stockcarslistfilterAllArea .stockcarslistfilterTransmissionSelect");
            if (this.stockcarslistData.Filter.Transmissions != null) {
                $("#stockcarslistfilterAllArea .stockcarslistfilterTransmissionSelect option").not('[value=""]').remove();
                var g = $("#stockcarslistfilterAllArea .stockcarslistfilterMainTransmissionTypeLabel");
                if (this.stockcarslistData.Filter.Transmissions.length === 1) {
                    h.hide();
                    g.text(this.stockcarslistData.Filter.Transmissions[0].Name);
                    g.parent().show()
                } else {
                    g.parent().hide();
                    h.show()
                } if (this.stockcarslistData.Filter.TransmissionsAll.length === 1) {
                    g.parent().addClass("stockcarslistFilterSelectSubstituteDisabled")
                } else {
                    g.parent().removeClass("stockcarslistFilterSelectSubstituteDisabled")
                }
                $.each(this.stockcarslistData.Filter.Transmissions, function(i, j) {
                    $("#stockcarslistTemplateTransmission").tmpl(j).appendTo(h)
                })
            }
            h.val(this.filter.transmission)
        },
        fillTransmissionDropDown: function() {
            if (this.stockcarslistData.Filter.Transmissions == null) {
                return
            }
            var h = $("#stockcarslistfilterAllArea .stockcarslistfilterTransmissionSelect"),
                g = $("#stockcarslistfilterAllArea .stockcarslistfilterMainTransmissionTypeLabel");
            $.each(this.stockcarslistData.Filter.Transmissions, function(j, k) {
                $("#stockcarslistTemplateTransmission").tmpl(k).appendTo(h)
            });
            var i = this;
            g.next().click(function() {
                i.filter.transmission = "";
                if (!i.settingValuesProgress) {
                    i.loadCarsList(false, false, false)
                }
            });
            h.bind("change", function() {
                var j = $(this);
                var k = j.val();
                if (k == "") {
                    i.filter.transmission = ""
                } else {
                    i.filter.transmission = k
                } if (!i.settingValuesProgress) {
                    i.loadCarsList(false, false, false)
                }
            })
        },
        setAwdCheckBoxValue: function() {
            var g = $("#stockcarslistfilterAllArea .stockcarslistfilterMainAwd input");
            g.attr("checked", this.filter.isAwd);
            if (!this.stockcarslistData.Filter.IsAwdEnabled) {
                $(".stockcarslistfilterMainAwd").hide();
                g.attr("disabled", "disabled")
            } else {
                $(".stockcarslistfilterMainAwd").show();
                g.removeAttr("disabled")
            }
        },
        setAwdCheckBox: function() {
            var h = this;
            var g = $("#stockcarslistfilterAllArea .stockcarslistfilterMainAwd input");
            g.bind("change", function() {
                var i = $(this);
                var j = i.is(":checked");
                h.filter.isAwd = j;
                if (!h.settingValuesProgress) {
                    h.loadCarsList(false, false, false)
                }
            });
            $("#stockcarslistfilterAllArea .stockcarslistfilterMainAwd label").bind("click", function() {
                var j = $(this);
                var i = j.parent().find("input");
                var k = i.is(":checked");
                i.attr("checked", !k);
                h.filter.isAwd = !k;
                if (!h.settingValuesProgress) {
                    h.loadCarsList(false, false, false)
                }
            })
        },
        setPowerSliderValue: function() {
            var i = this;
            var h = i.stockcarslistData.Filter.MinPower;
            var g = i.stockcarslistData.Filter.MaxPower;
            if (i.filter.minPower < h || !i.isPowerChanged) {
                i.filter.minPower = h
            }
            if (i.filter.minPower > g) {
                i.filter.minPower = g
            }
            $("#sliderPower").slider({
                range: "max",
                min: h,
                max: g,
                value: i.filter.minPower
            });
            $("#labelDesiredMinPower").text(i.filter.minPower);
            $("#labelPowerMin").text(h);
            $("#labelPowerMax").text(g)
        },
        setPowerSlider: function() {
            var h = 0;
            var g = 500;
            if (this.stockcarslistData.Filter != null) {
                h = this.stockcarslistData.Filter.MinPower;
                g = this.stockcarslistData.Filter.MaxPower
            }
            var i = this;
            $("#sliderPower").slider({
                range: "max",
                min: h,
                max: g,
                value: h,
                step: 1,
                slide: function(j, k) {
                    $("#labelDesiredMinPower").text(k.value)
                },
                change: function(k, m) {
                    var j = m.value;
                    var l = $("#sliderPower").slider("option", "min");
                    i.isPowerChanged = i.settingValuesProgress ? i.isPowerChanged : l < j;
                    i.filterCarsByPower(j)
                }
            });
            $("#labelPowerMin").text(h);
            $("#labelPowerMax").text(g);
            $("#labelDesiredMinPower").text($("#sliderPower").slider("value"))
        },
        filterCarsByPower: function(g) {
            this.filter.minPower = g;
            if (!this.settingValuesProgress) {
                this.loadCarsList(false, false, false)
            }
        },
        setConsumptionSliderValue: function() {
            var i = this;
            var h = i.stockcarslistData.Filter.MinConsumption;
            var g = i.stockcarslistData.Filter.MaxConsumption;
            if (i.filter.maxConsumption > g || !i.isConsumptionChanged) {
                i.filter.maxConsumption = g
            }
            if (i.filter.maxConsumption < h) {
                i.filter.maxConsumption = h
            }
            $("#sliderConsumption").slider({
                range: "min",
                min: h,
                max: g,
                value: i.filter.maxConsumption
            });
            $("#labelMaxDesiredConsumption").text(i.filter.maxConsumption);
            $("#labelConsumptionMin").text(h);
            $("#labelConsumptionMax").text(g)
        },
        setConsumptionSlider: function() {
            var h = 0;
            var g = 500;
            if (this.stockcarslistData.Filter != null) {
                h = this.stockcarslistData.Filter.MinConsumption;
                g = this.stockcarslistData.Filter.MaxConsumption
            }
            var i = this;
            $("#sliderConsumption").slider({
                range: "min",
                min: h,
                max: g,
                value: g,
                step: 0.2,
                slide: function(j, k) {
                    $("#labelMaxDesiredConsumption").text(k.value)
                },
                change: function(k, m) {
                    var j = m.value;
                    var l = $("#sliderConsumption").slider("option", "max");
                    i.isConsumptionChanged = i.settingValuesProgress ? i.isConsumptionChanged : j < l;
                    i.filterCarsByConsumption(j)
                }
            });
            $("#labelConsumptionMin").text(h);
            $("#labelConsumptionMax").text(g);
            $("#labelDesiredMaxConsumption").text($("#sliderConsumption").slider("value"))
        },
        filterCarsByConsumption: function(g) {
            this.filter.maxConsumption = g;
            if (!this.settingValuesProgress) {
                this.loadCarsList(false, false, false)
            }
        },
        setActionModelFilterValue: function() {
            var g = $("#stockcarslistfilterAllArea .stockcarslistfilterMainAction input");
            g.attr("checked", this.filter.isAction);
            if (!this.stockcarslistData.Filter.IsActionEnabled) {
                $(".stockcarslistfilterMainAction").hide();
                g.attr("disabled", "disabled")
            } else {
                $(".stockcarslistfilterMainAction").show();
                g.removeAttr("disabled")
            }
        },
        defineActionModelFilterEvents: function() {
            var g = $("#stockcarslistfilterAllArea .stockcarslistfilterMainAction input");
            var i = this;
            g.bind("change", function() {
                var j = $(this);
                var k = j.is(":checked");
                i.filter.isAction = k;
                if (!i.settingValuesProgress) {
                    i.loadCarsList(false, false, false)
                }
            });
            var h = $("#stockcarslistfilterAllArea .stockcarslistfilterMainAction label");
            h.bind("click", function() {
                var k = $(this);
                var j = k.parent().find("input");
                var l = j.is(":checked");
                j.attr("checked", !l);
                i.filter.isAction = !l;
                if (!i.settingValuesProgress) {
                    i.loadCarsList(false, false, false)
                }
            })
        },
        setColorGroups: function() {
            if (this.filter.colorGroups == null) {
                return
            }
            $("#stockcarslistfilterAllArea .stockcarslistColorGroupsItems").empty();
            this.fillColorGroups();
            var g = this.filter.colorGroups.split(",");
            var h = this;
            $("#stockcarslistfilterAllArea .stockcarslistColorGroupsItems .stockcarslistColorGroup").each(function() {
                var i = $(this).attr("class").split(" ")[1].substr(26);
                if (($.inArray(i, g) > -1) || h.filter.colorGroups == "*") {
                    $(this).find(".stockcarslistColorGroupCheck").show();
                    $(this).addClass("selectedColorGroup")
                } else {
                    $(this).find(".stockcarslistColorGroupCheck").hide();
                    $(this).removeClass("selectedColorGroup")
                }
            })
        },
        fillColorGroups: function() {
            if (this.stockcarslistData.Filter.ColorGroups == null || this.stockcarslistData.Filter.ColorGroups.length < 1) {
                return
            }
            var g = this;
            $.each(this.stockcarslistData.Filter.ColorGroups, function(i, h) {
                if (h.HexCode != null) {
                    $("#stockcarslistTemplateColorGroups").tmpl(h).appendTo($("#stockcarslistfilterAllArea .stockcarslistColorGroupsItems")).find(".stockcarslistColorGroupBox").css("background-color", h.HexCode);
                    $("#stockcarslistfilterAllArea .stockcarslistColorGroupsItems .stockcarsListColorGroupId_" + h.Id).click(function() {
                        if (!g.isColorFilterDisplayed) {
                            return
                        }
                        var j = $(this).find(".stockcarslistColorGroupCheck");
                        if ($(this).hasClass("selectedColorGroup")) {
                            j.hide();
                            $(this).removeClass("selectedColorGroup")
                        } else {
                            j.show();
                            $(this).addClass("selectedColorGroup")
                        }
                        g.filter.colorGroups = g.getSelectedColorGroupsIds();
                        if (!g.settingValuesProgress) {
                            g.loadCarsList(false, false, false)
                        }
                    })
                }
            })
        },
        getSelectedColorGroupsIds: function() {
            var g = "";
            $("#stockcarslistfilterAllArea .stockcarslistColorGroupsItems .selectedColorGroup").each(function() {
                g = g + $(this).attr("class").split(" ")[1].substr(26) + ","
            });
            if (g == "") {
                return ""
            }
            return g
        },
        setClearFilterButtonEvents: function() {
            var g = this;
            $(".stockcarslistfilterClear").click(function() {
                g.clearFilter()
            })
        },
        setSortingDropDownValue: function() {
            var g = $(".stockcarslistNavigationSorting .select");
            g.val(this.filter.sortOption)
        },
        setSortingLabelValue: function() {
            if (this.filter.sortAsc == true) {
                $(".stockcarslistNavigationSortOrder .stockcarslistNavigationAsc").show();
                $(".stockcarslistNavigationSortOrder .stockcarslistNavigationDesc").hide();
                $(".stockcarslistNavigationArrow").removeClass("stockcarslistNavigationArrowDown");
                $(".stockcarslistNavigationArrow").addClass("stockcarslistNavigationArrowUp")
            } else {
                $(".stockcarslistNavigationSortOrder .stockcarslistNavigationAsc").hide();
                $(".stockcarslistNavigationSortOrder .stockcarslistNavigationDesc").show();
                $(".stockcarslistNavigationArrow").removeClass("stockcarslistNavigationArrowUp");
                $(".stockcarslistNavigationArrow").addClass("stockcarslistNavigationArrowDown")
            }
        },
        fillSortingDropDown: function() {
            var g = this;
            $(".stockcarslistNavigationSorting .select").bind("change", function() {
                var h = $(this);
                var i = h.val();
                g.filter.sortOption = i;
                if (!g.settingValuesProgress) {
                    g.loadCarsList(false, false, false)
                }
            })
        },
        setSortOrderButtonEvents: function() {
            var g = this;
            $(".stockcarslistNavigationSortOrder .stockcarslistNavigationDesc").hide();
            $(".stockcarslistNavigationSortOrder").click(function() {
                if ($(".stockcarslistNavigationSortOrder .stockcarslistNavigationAsc").is(":visible")) {
                    $(".stockcarslistNavigationSortOrder .stockcarslistNavigationAsc").hide();
                    $(".stockcarslistNavigationSortOrder .stockcarslistNavigationDesc").show();
                    $(".stockcarslistNavigationArrow").removeClass("stockcarslistNavigationArrowUp");
                    $(".stockcarslistNavigationArrow").addClass("stockcarslistNavigationArrowDown");
                    g.filter.sortAsc = false;
                    g.loadCarsList(false, false, false)
                } else {
                    $(".stockcarslistNavigationSortOrder .stockcarslistNavigationAsc").show();
                    $(".stockcarslistNavigationSortOrder .stockcarslistNavigationDesc").hide();
                    $(".stockcarslistNavigationArrow").removeClass("stockcarslistNavigationArrowDown");
                    $(".stockcarslistNavigationArrow").addClass("stockcarslistNavigationArrowUp");
                    g.filter.sortAsc = true;
                    g.loadCarsList(false, false, false)
                }
            })
        },
        showNoCarsMessage: function() {
            $(".stockcarslistNoItems").show()
        },
        hideNoCarsMessage: function() {
            $(".stockcarslistNoItems").hide()
        }
    };
    f.List = c
})(StockCarsList || (StockCarsList = {}));
(function(a) {
    a.address = function() {
        var aF = function(d) {
                d = a.extend(a.Event(d), function() {
                    for (var e = {}, g = a.address.parameterNames(), h = 0, i = g.length; h < i; h++) {
                        e[g[h]] = a.address.parameter(g[h])
                    }
                    return {
                        value: a.address.value(),
                        path: a.address.path(),
                        pathNames: a.address.pathNames(),
                        parameterNames: g,
                        parameters: e,
                        queryString: a.address.queryString()
                    }
                }.call(a.address));
                a(a.address).trigger(d);
                return d
            }, aG = function(d) {
                return Array.prototype.slice.call(d)
            }, aj = function() {
                a().bind.apply(a(a.address), Array.prototype.slice.call(arguments));
                return a.address
            }, ad = function() {
                a().unbind.apply(a(a.address), Array.prototype.slice.call(arguments));
                return a.address
            }, ac = function() {
                return aS.pushState && R.state !== ag
            }, aK = function() {
                return ("/" + am.pathname.replace(new RegExp(R.state), "") + am.search + (ax() ? "#" + ax() : "")).replace(aI, "/")
            }, ax = function() {
                var d = am.href.indexOf("#");
                return d != -1 ? aH(am.href.substr(d + 1), at) : ""
            }, aJ = function() {
                return ac() ? aK() : ax()
            }, aM = function() {
                return "javascript"
            }, aC = function(d) {
                d = d.toString();
                return (R.strict && d.substr(0, 1) != "/" ? "/" : "") + d
            }, aH = function(d, e) {
                if (R.crawlable && e) {
                    return (d !== "" ? "!" : "") + d
                }
                return d.replace(/^\!/, "")
            }, aL = function(d, e) {
                return parseInt(d.css(e), 10)
            }, ak = function() {
                if (!aP) {
                    var d = aJ();
                    if (decodeURI(ab) != decodeURI(d)) {
                        if (aN && c < 7) {
                            am.reload()
                        } else {
                            aN && !ae && R.history && aE(ay, 50);
                            _old = ab;
                            ab = d;
                            ah(at)
                        }
                    }
                }
            }, ah = function(d) {
                var e = aF(aO);
                d = aF(d ? aQ : aR);
                aE(af, 10);
                if (e.isDefaultPrevented() || d.isDefaultPrevented()) {
                    ai()
                }
            }, ai = function() {
                ab = _old;
                if (ac()) {
                    aS.popState({}, "", R.state.replace(/\/$/, "") + (ab === "" ? "/" : ab))
                } else {
                    aP = az;
                    if (m) {
                        if (R.history) {
                            am.hash = "#" + aH(ab, az)
                        } else {
                            am.replace("#" + aH(ab, az))
                        }
                    } else {
                        if (ab != aJ()) {
                            if (R.history) {
                                am.hash = "#" + aH(ab, az)
                            } else {
                                am.replace("#" + aH(ab, az))
                            }
                        }
                    }
                    aN && !ae && R.history && aE(ay, 50);
                    if (m) {
                        aE(function() {
                            aP = at
                        }, 1)
                    } else {
                        aP = at
                    }
                }
            }, af = function() {
                if (R.tracker !== "null" && R.tracker !== an) {
                    var d = a.isFunction(R.tracker) ? R.tracker : ap[R.tracker],
                        e = (am.pathname + am.search + (a.address && !ac() ? a.address.value() : "")).replace(/\/\//, "/").replace(/^\/$/, "");
                    if (a.isFunction(d)) {
                        d(e)
                    } else {
                        if (a.isFunction(ap.urchinTracker)) {
                            ap.urchinTracker(e)
                        } else {
                            if (ap.pageTracker !== ag && a.isFunction(ap.pageTracker._trackPageview)) {
                                ap.pageTracker._trackPageview(e)
                            } else {
                                ap._gaq !== ag && a.isFunction(ap._gaq.push) && ap._gaq.push(["_trackPageview", decodeURI(e)])
                            }
                        }
                    }
                }
            }, ay = function() {
                var d = aM() + ":" + at + ";document.open();document.writeln('<html><head><title>" + aB.title.replace(/\'/g, "\\'") + "</title><script>var " + y + ' = "' + encodeURIComponent(aJ()).replace(/\'/g, "\\'") + (aB.domain != am.hostname ? '";document.domain="' + aB.domain : "") + "\";</script></head></html>');document.close();";
                if (c < 7) {
                    aw.src = d
                } else {
                    aw.contentWindow.location.replace(d)
                }
            }, b = function() {
                if (aq && aT != -1) {
                    var d, e, g = aq.substr(aT + 1).split("&");
                    for (d = 0; d < g.length; d++) {
                        e = g[d].split("=");
                        if (/^(autoUpdate|crawlable|history|strict|wrap)$/.test(e[0])) {
                            R[e[0]] = isNaN(e[1]) ? /^(true|yes)$/i.test(e[1]) : parseInt(e[1], 10) !== 0
                        }
                        if (/^(state|tracker)$/.test(e[0])) {
                            R[e[0]] = e[1]
                        }
                    }
                    aq = an
                }
                _old = ab;
                ab = aJ()
            }, p = function() {
                if (!f) {
                    f = az;
                    b();
                    var d = function() {
                        al.call(this);
                        ao.call(this)
                    }, e = a("body").ajaxComplete(d);
                    d();
                    if (R.wrap) {
                        a("body > *").wrapAll('<div style="padding:' + (aL(e, "marginTop") + aL(e, "paddingTop")) + "px " + (aL(e, "marginRight") + aL(e, "paddingRight")) + "px " + (aL(e, "marginBottom") + aL(e, "paddingBottom")) + "px " + (aL(e, "marginLeft") + aL(e, "paddingLeft")) + 'px;" />').parent().wrap('<div id="' + y + '" style="height:100%;overflow:auto;position:relative;' + (m && !window.statusbar.visible ? "resize:both;" : "") + '" />');
                        a("html, body").css({
                            height: "100%",
                            margin: 0,
                            padding: 0,
                            overflow: "hidden"
                        });
                        m && a('<style type="text/css" />').appendTo("head").text("#" + y + "::-webkit-resizer { background-color: #fff; }")
                    }
                    if (aN && !ae) {
                        d = aB.getElementsByTagName("frameset")[0];
                        aw = aB.createElement((d ? "" : "i") + "frame");
                        aw.src = aM() + ":" + at;
                        if (d) {
                            d.insertAdjacentElement("beforeEnd", aw);
                            d[d.cols ? "cols" : "rows"] += ",0";
                            aw.noResize = az;
                            aw.frameBorder = aw.frameSpacing = 0
                        } else {
                            aw.style.display = "none";
                            aw.style.width = aw.style.height = 0;
                            aw.tabIndex = -1;
                            aB.body.insertAdjacentElement("afterBegin", aw)
                        }
                        aE(function() {
                            a(aw).bind("load", function() {
                                var g = aw.contentWindow;
                                _old = ab;
                                ab = g[y] !== ag ? g[y] : "";
                                if (ab != aJ()) {
                                    ah(at);
                                    am.hash = aH(ab, az)
                                }
                            });
                            aw.contentWindow[y] === ag && ay()
                        }, 50)
                    }
                    aE(function() {
                        aF("init");
                        ah(at)
                    }, 1);
                    if (!ac()) {
                        if (aN && c > 7 || !aN && ae) {
                            if (ap.addEventListener) {
                                ap.addEventListener(au, ak, at)
                            } else {
                                ap.attachEvent && ap.attachEvent("on" + au, ak)
                            }
                        } else {
                            ar(ak, 50)
                        }
                    }
                    "state" in window.history && a(window).trigger("popstate")
                }
            }, al = function() {
                var d, e = a("a"),
                    g = e.size(),
                    h = -1,
                    i = function() {
                        if (++h != g) {
                            d = a(e.get(h));
                            d.is('[rel*="address:"]') && d.address('[rel*="address:"]');
                            aE(i, 1)
                        }
                    };
                aE(i, 1)
            }, ao = function() {
                if (R.crawlable) {
                    var d = am.pathname.replace(/\/$/, "");
                    a("body").html().indexOf("_escaped_fragment_") != -1 && a('a[href]:not([href^=http]), a[href*="' + document.domain + '"]').each(function() {
                        var e = a(this).attr("href").replace(/^http:/, "").replace(new RegExp(d + "/?$"), "");
                        if (e === "" || e.indexOf("_escaped_fragment_") != -1) {
                            a(this).attr("href", "#" + encodeURI(decodeURIComponent(e.replace(/\/(.*)\?_escaped_fragment_=(.*)$/, "!$2"))))
                        }
                    })
                }
            }, ag, an = null,
            y = "jQueryAddress",
            au = "hashchange",
            aO = "change",
            aQ = "internalChange",
            aR = "externalChange",
            az = true,
            at = false,
            R = {
                autoUpdate: az,
                crawlable: at,
                history: az,
                strict: az,
                wrap: at
            }, S = a.browser,
            c = parseFloat(S.version),
            aN = !a.support.opacity,
            m = S.webkit || S.safari,
            ap = function() {
                try {
                    return top.document !== ag && top.document.title !== ag ? top : window
                } catch (d) {
                    return window
                }
            }(),
            aB = ap.document,
            aS = ap.history,
            am = ap.location,
            ar = setInterval,
            aE = setTimeout,
            aI = /\/{2,9}/g;
        S = navigator.userAgent;
        var ae = "on" + au in ap,
            aw, aq = a("script:last").attr("src"),
            aT = aq ? aq.indexOf("?") : -1,
            aD = aB.title,
            aP = at,
            f = at,
            Q = az,
            aA = at,
            ab = aJ();
        _old = ab;
        if (aN) {
            c = parseFloat(S.substr(S.indexOf("MSIE") + 4));
            if (aB.documentMode && aB.documentMode != c) {
                c = aB.documentMode != 8 ? 7 : 8
            }
            var aa = aB.onpropertychange;
            aB.onpropertychange = function() {
                aa && aa.call(aB);
                if (aB.title != aD && aB.title.indexOf("#" + aJ()) != -1) {
                    aB.title = aD
                }
            }
        }
        if (aS.navigationMode) {
            aS.navigationMode = "compatible"
        }
        if (document.readyState == "complete") {
            var av = setInterval(function() {
                if (a.address) {
                    p();
                    clearInterval(av)
                }
            }, 50)
        } else {
            b();
            a(p)
        }
        a(window).bind("popstate", function() {
            if (decodeURI(ab) != decodeURI(aJ())) {
                _old = ab;
                ab = aJ();
                ah(at)
            }
        }).bind("unload", function() {
            if (ap.removeEventListener) {
                ap.removeEventListener(au, ak, at)
            } else {
                ap.detachEvent && ap.detachEvent("on" + au, ak)
            }
        });
        return {
            bind: function() {
                return aj.apply(this, aG(arguments))
            },
            unbind: function() {
                return ad.apply(this, aG(arguments))
            },
            init: function() {
                return aj.apply(this, ["init"].concat(aG(arguments)))
            },
            change: function() {
                return aj.apply(this, [aO].concat(aG(arguments)))
            },
            internalChange: function() {
                return aj.apply(this, [aQ].concat(aG(arguments)))
            },
            externalChange: function() {
                return aj.apply(this, [aR].concat(aG(arguments)))
            },
            baseURL: function() {
                var d = am.href;
                if (d.indexOf("#") != -1) {
                    d = d.substr(0, d.indexOf("#"))
                }
                if (/\/$/.test(d)) {
                    d = d.substr(0, d.length - 1)
                }
                return d
            },
            autoUpdate: function(d) {
                if (d !== ag) {
                    R.autoUpdate = d;
                    return this
                }
                return R.autoUpdate
            },
            crawlable: function(d) {
                if (d !== ag) {
                    R.crawlable = d;
                    return this
                }
                return R.crawlable
            },
            history: function(d) {
                if (d !== ag) {
                    R.history = d;
                    return this
                }
                return R.history
            },
            state: function(d) {
                if (d !== ag) {
                    R.state = d;
                    var e = aK();
                    if (R.state !== ag) {
                        if (aS.pushState) {
                            e.substr(0, 3) == "/#/" && am.replace(R.state.replace(/^\/$/, "") + e.substr(2))
                        } else {
                            e != "/" && e.replace(/^\/#/, "") != ax() && aE(function() {
                                am.replace(R.state.replace(/^\/$/, "") + "/#" + e)
                            }, 1)
                        }
                    }
                    return this
                }
                return R.state
            },
            strict: function(d) {
                if (d !== ag) {
                    R.strict = d;
                    return this
                }
                return R.strict
            },
            tracker: function(d) {
                if (d !== ag) {
                    R.tracker = d;
                    return this
                }
                return R.tracker
            },
            wrap: function(d) {
                if (d !== ag) {
                    R.wrap = d;
                    return this
                }
                return R.wrap
            },
            update: function() {
                aA = az;
                this.value(ab);
                aA = at;
                return this
            },
            title: function(d) {
                if (d !== ag) {
                    aE(function() {
                        aD = aB.title = d;
                        if (Q && aw && aw.contentWindow && aw.contentWindow.document) {
                            aw.contentWindow.document.title = d;
                            Q = at
                        }
                    }, 50);
                    return this
                }
                return aB.title
            },
            value: function(d) {
                if (d !== ag) {
                    d = aC(d);
                    if (d == "/") {
                        d = ""
                    }
                    if (ab == d && !aA) {
                        return
                    }
                    _old = ab;
                    ab = d;
                    if (R.autoUpdate || aA) {
                        ah(az);
                        if (ac()) {
                            aS[R.history ? "pushState" : "replaceState"]({}, "", R.state.replace(/\/$/, "") + (ab === "" ? "/" : ab))
                        } else {
                            aP = az;
                            if (m) {
                                if (R.history) {
                                    am.hash = "#" + aH(ab, az)
                                } else {
                                    am.replace("#" + aH(ab, az))
                                }
                            } else {
                                if (ab != aJ()) {
                                    if (R.history) {
                                        am.hash = "#" + aH(ab, az)
                                    } else {
                                        am.replace("#" + aH(ab, az))
                                    }
                                }
                            }
                            aN && !ae && R.history && aE(ay, 50);
                            if (m) {
                                aE(function() {
                                    aP = at
                                }, 1)
                            } else {
                                aP = at
                            }
                        }
                    }
                    return this
                }
                return aC(ab)
            },
            path: function(d) {
                if (d !== ag) {
                    var e = this.queryString(),
                        g = this.hash();
                    this.value(d + (e ? "?" + e : "") + (g ? "#" + g : ""));
                    return this
                }
                return aC(ab).split("#")[0].split("?")[0]
            },
            pathNames: function() {
                var d = this.path(),
                    e = d.replace(aI, "/").split("/");
                if (d.substr(0, 1) == "/" || d.length === 0) {
                    e.splice(0, 1)
                }
                d.substr(d.length - 1, 1) == "/" && e.splice(e.length - 1, 1);
                return e
            },
            queryString: function(d) {
                if (d !== ag) {
                    var e = this.hash();
                    this.value(this.path() + (d ? "?" + d : "") + (e ? "#" + e : ""));
                    return this
                }
                d = ab.split("?");
                return d.slice(1, d.length).join("?").split("#")[0]
            },
            parameter: function(d, e, g) {
                var h, i;
                if (e !== ag) {
                    var j = this.parameterNames();
                    i = [];
                    e = e === ag || e === an ? "" : e.toString();
                    for (h = 0; h < j.length; h++) {
                        var k = j[h],
                            n = this.parameter(k);
                        if (typeof n == "string") {
                            n = [n]
                        }
                        if (k == d) {
                            n = e === an || e === "" ? [] : g ? n.concat([e]) : [e]
                        }
                        for (var l = 0; l < n.length; l++) {
                            i.push(k + "=" + n[l])
                        }
                    }
                    a.inArray(d, j) == -1 && e !== an && e !== "" && i.push(d + "=" + e);
                    this.queryString(i.join("&"));
                    return this
                }
                if (e = this.queryString()) {
                    g = [];
                    i = e.split("&");
                    for (h = 0; h < i.length; h++) {
                        e = i[h].split("=");
                        e[0] == d && g.push(e.slice(1).join("="))
                    }
                    if (g.length !== 0) {
                        return g.length != 1 ? g : g[0]
                    }
                }
            },
            parameterNames: function() {
                var d = this.queryString(),
                    e = [];
                if (d && d.indexOf("=") != -1) {
                    d = d.split("&");
                    for (var g = 0; g < d.length; g++) {
                        var h = d[g].split("=")[0];
                        a.inArray(h, e) == -1 && e.push(h)
                    }
                }
                return e
            },
            hash: function(d) {
                if (d !== ag) {
                    this.value(ab.split("#")[0] + (d ? "#" + d : ""));
                    return this
                }
                d = ab.split("#");
                return d.slice(1, d.length).join("#")
            }
        }
    }();
    a.fn.address = function(b) {
        var c;
        if (typeof b == "string") {
            c = b;
            b = undefined
        }
        a(this).attr("address") || a(c ? c : this).live("click", function(d) {
            if (d.shiftKey || d.ctrlKey || d.metaKey || d.which == 2) {
                return true
            }
            if (a(this).is("a")) {
                d.preventDefault();
                d = b ? b.call(this) : /address:/.test(a(this).attr("rel")) ? a(this).attr("rel").split("address:")[1].split(" ")[0] : a.address.state() !== undefined && !/^\/?$/.test(a.address.state()) ? a(this).attr("href").replace(new RegExp("^(.*" + a.address.state() + "|\\.)"), "") : a(this).attr("href").replace(/^(#\!?|\.)/, "");
                a.address.value(d)
            }
        }).live("submit", function(d) {
            if (a(this).is("form")) {
                d.preventDefault();
                d = a(this).attr("action");
                d = b ? b.call(this) : (d.indexOf("?") != -1 ? d.replace(/&$/, "") : d + "?") + a(this).serialize();
                a.address.value(d)
            }
        }).attr("address", true);
        return this
    }
})(jQuery);