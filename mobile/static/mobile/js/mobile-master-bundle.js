/* jQuery v1.9.1 | (c) 2005, 2012 jQuery Foundation, Inc. | jquery.org/license
 //@ sourceMappingURL=jquery.min.map
 */
(function(e, t) {
    var n, r, i = typeof t,
        o = e.document,
        a = e.location,
        s = e.jQuery,
        u = e.$,
        l = {}, c = [],
        p = "1.9.1",
        f = c.concat,
        d = c.push,
        h = c.slice,
        g = c.indexOf,
        m = l.toString,
        y = l.hasOwnProperty,
        v = p.trim,
        b = function(e, t) {
            return new b.fn.init(e, t, r)
        }, x = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        w = /\S+/g,
        T = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        N = /^(?:(<[\w\W]+>)[^>]*|#([\w-]*))$/,
        C = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
        k = /^[\],:{}\s]*$/,
        E = /(?:^|:|,)(?:\s*\[)+/g,
        S = /\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,
        A = /"[^"\\\r\n]*"|true|false|null|-?(?:\d+\.|)\d+(?:[eE][+-]?\d+|)/g,
        j = /^-ms-/,
        D = /-([\da-z])/gi,
        L = function(e, t) {
            return t.toUpperCase()
        }, H = function(e) {
            (o.addEventListener || "load" === e.type || "complete" === o.readyState) && (q(), b.ready())
        }, q = function() {
            o.addEventListener ? (o.removeEventListener("DOMContentLoaded", H, !1), e.removeEventListener("load", H, !1)) : (o.detachEvent("onreadystatechange", H), e.detachEvent("onload", H))
        };
    b.fn = b.prototype = {
        jquery: p,
        constructor: b,
        init: function(e, n, r) {
            var i, a;
            if (!e) {
                return this
            }
            if ("string" == typeof e) {
                if (i = "<" === e.charAt(0) && ">" === e.charAt(e.length - 1) && e.length >= 3 ? [null, e, null] : N.exec(e), !i || !i[1] && n) {
                    return !n || n.jquery ? (n || r).find(e) : this.constructor(n).find(e)
                }
                if (i[1]) {
                    if (n = n instanceof b ? n[0] : n, b.merge(this, b.parseHTML(i[1], n && n.nodeType ? n.ownerDocument || n : o, !0)), C.test(i[1]) && b.isPlainObject(n)) {
                        for (i in n) {
                            b.isFunction(this[i]) ? this[i](n[i]) : this.attr(i, n[i])
                        }
                    }
                    return this
                }
                if (a = o.getElementById(i[2]), a && a.parentNode) {
                    if (a.id !== i[2]) {
                        return r.find(e)
                    }
                    this.length = 1, this[0] = a
                }
                return this.context = o, this.selector = e, this
            }
            return e.nodeType ? (this.context = this[0] = e, this.length = 1, this) : b.isFunction(e) ? r.ready(e) : (e.selector !== t && (this.selector = e.selector, this.context = e.context), b.makeArray(e, this))
        },
        selector: "",
        length: 0,
        size: function() {
            return this.length
        },
        toArray: function() {
            return h.call(this)
        },
        get: function(e) {
            return null == e ? this.toArray() : 0 > e ? this[this.length + e] : this[e]
        },
        pushStack: function(e) {
            var t = b.merge(this.constructor(), e);
            return t.prevObject = this, t.context = this.context, t
        },
        each: function(e, t) {
            return b.each(this, e, t)
        },
        ready: function(e) {
            return b.ready.promise().done(e), this
        },
        slice: function() {
            return this.pushStack(h.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(e) {
            var t = this.length,
                n = +e + (0 > e ? t : 0);
            return this.pushStack(n >= 0 && t > n ? [this[n]] : [])
        },
        map: function(e) {
            return this.pushStack(b.map(this, function(t, n) {
                return e.call(t, n, t)
            }))
        },
        end: function() {
            return this.prevObject || this.constructor(null)
        },
        push: d,
        sort: [].sort,
        splice: [].splice
    }, b.fn.init.prototype = b.fn, b.extend = b.fn.extend = function() {
        var e, n, r, i, o, a, s = arguments[0] || {}, u = 1,
            l = arguments.length,
            c = !1;
        for ("boolean" == typeof s && (c = s, s = arguments[1] || {}, u = 2), "object" == typeof s || b.isFunction(s) || (s = {}), l === u && (s = this, --u); l > u; u++) {
            if (null != (o = arguments[u])) {
                for (i in o) {
                    e = s[i], r = o[i], s !== r && (c && r && (b.isPlainObject(r) || (n = b.isArray(r))) ? (n ? (n = !1, a = e && b.isArray(e) ? e : []) : a = e && b.isPlainObject(e) ? e : {}, s[i] = b.extend(c, a, r)) : r !== t && (s[i] = r))
                }
            }
        }
        return s
    }, b.extend({
        noConflict: function(t) {
            return e.$ === b && (e.$ = u), t && e.jQuery === b && (e.jQuery = s), b
        },
        isReady: !1,
        readyWait: 1,
        holdReady: function(e) {
            e ? b.readyWait++ : b.ready(!0)
        },
        ready: function(e) {
            if (e === !0 ? !--b.readyWait : !b.isReady) {
                if (!o.body) {
                    return setTimeout(b.ready)
                }
                b.isReady = !0, e !== !0 && --b.readyWait > 0 || (n.resolveWith(o, [b]), b.fn.trigger && b(o).trigger("ready").off("ready"))
            }
        },
        isFunction: function(e) {
            return "function" === b.type(e)
        },
        isArray: Array.isArray || function(e) {
            return "array" === b.type(e)
        },
        isWindow: function(e) {
            return null != e && e == e.window
        },
        isNumeric: function(e) {
            return !isNaN(parseFloat(e)) && isFinite(e)
        },
        type: function(e) {
            return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? l[m.call(e)] || "object" : typeof e
        },
        isPlainObject: function(e) {
            if (!e || "object" !== b.type(e) || e.nodeType || b.isWindow(e)) {
                return !1
            }
            try {
                if (e.constructor && !y.call(e, "constructor") && !y.call(e.constructor.prototype, "isPrototypeOf")) {
                    return !1
                }
            } catch (n) {
                return !1
            }
            var r;
            for (r in e) {}
            return r === t || y.call(e, r)
        },
        isEmptyObject: function(e) {
            var t;
            for (t in e) {
                return !1
            }
            return !0
        },
        error: function(e) {
            throw Error(e)
        },
        parseHTML: function(e, t, n) {
            if (!e || "string" != typeof e) {
                return null
            }
            "boolean" == typeof t && (n = t, t = !1), t = t || o;
            var r = C.exec(e),
                i = !n && [];
            return r ? [t.createElement(r[1])] : (r = b.buildFragment([e], t, i), i && b(i).remove(), b.merge([], r.childNodes))
        },
        parseJSON: function(n) {
            return e.JSON && e.JSON.parse ? e.JSON.parse(n) : null === n ? n : "string" == typeof n && (n = b.trim(n), n && k.test(n.replace(S, "@").replace(A, "]").replace(E, ""))) ? Function("return " + n)() : (b.error("Invalid JSON: " + n), t)
        },
        parseXML: function(n) {
            var r, i;
            if (!n || "string" != typeof n) {
                return null
            }
            try {
                e.DOMParser ? (i = new DOMParser, r = i.parseFromString(n, "text/xml")) : (r = new ActiveXObject("Microsoft.XMLDOM"), r.async = "false", r.loadXML(n))
            } catch (o) {
                r = t
            }
            return r && r.documentElement && !r.getElementsByTagName("parsererror").length || b.error("Invalid XML: " + n), r
        },
        noop: function() {},
        globalEval: function(t) {
            t && b.trim(t) && (e.execScript || function(t) {
                e.eval.call(e, t)
            })(t)
        },
        camelCase: function(e) {
            return e.replace(j, "ms-").replace(D, L)
        },
        nodeName: function(e, t) {
            return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
        },
        each: function(e, t, n) {
            var r, i = 0,
                o = e.length,
                a = M(e);
            if (n) {
                if (a) {
                    for (; o > i; i++) {
                        if (r = t.apply(e[i], n), r === !1) {
                            break
                        }
                    }
                } else {
                    for (i in e) {
                        if (r = t.apply(e[i], n), r === !1) {
                            break
                        }
                    }
                }
            } else {
                if (a) {
                    for (; o > i; i++) {
                        if (r = t.call(e[i], i, e[i]), r === !1) {
                            break
                        }
                    }
                } else {
                    for (i in e) {
                        if (r = t.call(e[i], i, e[i]), r === !1) {
                            break
                        }
                    }
                }
            }
            return e
        },
        trim: v && !v.call("\ufeff\u00a0") ? function(e) {
            return null == e ? "" : v.call(e)
        } : function(e) {
            return null == e ? "" : (e + "").replace(T, "")
        },
        makeArray: function(e, t) {
            var n = t || [];
            return null != e && (M(Object(e)) ? b.merge(n, "string" == typeof e ? [e] : e) : d.call(n, e)), n
        },
        inArray: function(e, t, n) {
            var r;
            if (t) {
                if (g) {
                    return g.call(t, e, n)
                }
                for (r = t.length, n = n ? 0 > n ? Math.max(0, r + n) : n : 0; r > n; n++) {
                    if (n in t && t[n] === e) {
                        return n
                    }
                }
            }
            return -1
        },
        merge: function(e, n) {
            var r = n.length,
                i = e.length,
                o = 0;
            if ("number" == typeof r) {
                for (; r > o; o++) {
                    e[i++] = n[o]
                }
            } else {
                while (n[o] !== t) {
                    e[i++] = n[o++]
                }
            }
            return e.length = i, e
        },
        grep: function(e, t, n) {
            var r, i = [],
                o = 0,
                a = e.length;
            for (n = !! n; a > o; o++) {
                r = !! t(e[o], o), n !== r && i.push(e[o])
            }
            return i
        },
        map: function(e, t, n) {
            var r, i = 0,
                o = e.length,
                a = M(e),
                s = [];
            if (a) {
                for (; o > i; i++) {
                    r = t(e[i], i, n), null != r && (s[s.length] = r)
                }
            } else {
                for (i in e) {
                    r = t(e[i], i, n), null != r && (s[s.length] = r)
                }
            }
            return f.apply([], s)
        },
        guid: 1,
        proxy: function(e, n) {
            var r, i, o;
            return "string" == typeof n && (o = e[n], n = e, e = o), b.isFunction(e) ? (r = h.call(arguments, 2), i = function() {
                return e.apply(n || this, r.concat(h.call(arguments)))
            }, i.guid = e.guid = e.guid || b.guid++, i) : t
        },
        access: function(e, n, r, i, o, a, s) {
            var u = 0,
                l = e.length,
                c = null == r;
            if ("object" === b.type(r)) {
                o = !0;
                for (u in r) {
                    b.access(e, n, u, r[u], !0, a, s)
                }
            } else {
                if (i !== t && (o = !0, b.isFunction(i) || (s = !0), c && (s ? (n.call(e, i), n = null) : (c = n, n = function(e, t, n) {
                        return c.call(b(e), n)
                    })), n)) {
                    for (; l > u; u++) {
                        n(e[u], r, s ? i : i.call(e[u], u, n(e[u], r)))
                    }
                }
            }
            return o ? e : c ? n.call(e) : l ? n(e[0], r) : a
        },
        now: function() {
            return (new Date).getTime()
        }
    }), b.ready.promise = function(t) {
        if (!n) {
            if (n = b.Deferred(), "complete" === o.readyState) {
                setTimeout(b.ready)
            } else {
                if (o.addEventListener) {
                    o.addEventListener("DOMContentLoaded", H, !1), e.addEventListener("load", H, !1)
                } else {
                    o.attachEvent("onreadystatechange", H), e.attachEvent("onload", H);
                    var r = !1;
                    try {
                        r = null == e.frameElement && o.documentElement
                    } catch (i) {}
                    r && r.doScroll && function a() {
                        if (!b.isReady) {
                            try {
                                r.doScroll("left")
                            } catch (e) {
                                return setTimeout(a, 50)
                            }
                            q(), b.ready()
                        }
                    }()
                }
            }
        }
        return n.promise(t)
    }, b.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(e, t) {
        l["[object " + t + "]"] = t.toLowerCase()
    });

    function M(e) {
        var t = e.length,
            n = b.type(e);
        return b.isWindow(e) ? !1 : 1 === e.nodeType && t ? !0 : "array" === n || "function" !== n && (0 === t || "number" == typeof t && t > 0 && t - 1 in e)
    }
    r = b(o);
    var _ = {};

    function F(e) {
        var t = _[e] = {};
        return b.each(e.match(w) || [], function(e, n) {
            t[n] = !0
        }), t
    }
    b.Callbacks = function(e) {
        e = "string" == typeof e ? _[e] || F(e) : b.extend({}, e);
        var n, r, i, o, a, s, u = [],
            l = !e.once && [],
            c = function(t) {
                for (r = e.memory && t, i = !0, a = s || 0, s = 0, o = u.length, n = !0; u && o > a; a++) {
                    if (u[a].apply(t[0], t[1]) === !1 && e.stopOnFalse) {
                        r = !1;
                        break
                    }
                }
                n = !1, u && (l ? l.length && c(l.shift()) : r ? u = [] : p.disable())
            }, p = {
                add: function() {
                    if (u) {
                        var t = u.length;
                        (function i(t) {
                            b.each(t, function(t, n) {
                                var r = b.type(n);
                                "function" === r ? e.unique && p.has(n) || u.push(n) : n && n.length && "string" !== r && i(n)
                            })
                        })(arguments), n ? o = u.length : r && (s = t, c(r))
                    }
                    return this
                },
                remove: function() {
                    return u && b.each(arguments, function(e, t) {
                        var r;
                        while ((r = b.inArray(t, u, r)) > -1) {
                            u.splice(r, 1), n && (o >= r && o--, a >= r && a--)
                        }
                    }), this
                },
                has: function(e) {
                    return e ? b.inArray(e, u) > -1 : !(!u || !u.length)
                },
                empty: function() {
                    return u = [], this
                },
                disable: function() {
                    return u = l = r = t, this
                },
                disabled: function() {
                    return !u
                },
                lock: function() {
                    return l = t, r || p.disable(), this
                },
                locked: function() {
                    return !l
                },
                fireWith: function(e, t) {
                    return t = t || [], t = [e, t.slice ? t.slice() : t], !u || i && !l || (n ? l.push(t) : c(t)), this
                },
                fire: function() {
                    return p.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!i
                }
            };
        return p
    }, b.extend({
        Deferred: function(e) {
            var t = [
                    ["resolve", "done", b.Callbacks("once memory"), "resolved"],
                    ["reject", "fail", b.Callbacks("once memory"), "rejected"],
                    ["notify", "progress", b.Callbacks("memory")]
                ],
                n = "pending",
                r = {
                    state: function() {
                        return n
                    },
                    always: function() {
                        return i.done(arguments).fail(arguments), this
                    },
                    then: function() {
                        var e = arguments;
                        return b.Deferred(function(n) {
                            b.each(t, function(t, o) {
                                var a = o[0],
                                    s = b.isFunction(e[t]) && e[t];
                                i[o[1]](function() {
                                    var e = s && s.apply(this, arguments);
                                    e && b.isFunction(e.promise) ? e.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[a + "With"](this === r ? n.promise() : this, s ? [e] : arguments)
                                })
                            }), e = null
                        }).promise()
                    },
                    promise: function(e) {
                        return null != e ? b.extend(e, r) : r
                    }
                }, i = {};
            return r.pipe = r.then, b.each(t, function(e, o) {
                var a = o[2],
                    s = o[3];
                r[o[1]] = a.add, s && a.add(function() {
                    n = s
                }, t[1 ^ e][2].disable, t[2][2].lock), i[o[0]] = function() {
                    return i[o[0] + "With"](this === i ? r : this, arguments), this
                }, i[o[0] + "With"] = a.fireWith
            }), r.promise(i), e && e.call(i, i), i
        },
        when: function(e) {
            var t = 0,
                n = h.call(arguments),
                r = n.length,
                i = 1 !== r || e && b.isFunction(e.promise) ? r : 0,
                o = 1 === i ? e : b.Deferred(),
                a = function(e, t, n) {
                    return function(r) {
                        t[e] = this, n[e] = arguments.length > 1 ? h.call(arguments) : r, n === s ? o.notifyWith(t, n) : --i || o.resolveWith(t, n)
                    }
                }, s, u, l;
            if (r > 1) {
                for (s = Array(r), u = Array(r), l = Array(r); r > t; t++) {
                    n[t] && b.isFunction(n[t].promise) ? n[t].promise().done(a(t, l, n)).fail(o.reject).progress(a(t, u, s)) : --i
                }
            }
            return i || o.resolveWith(l, n), o.promise()
        }
    }), b.support = function() {
        var t, n, r, a, s, u, l, c, p, f, d = o.createElement("div");
        if (d.setAttribute("className", "t"), d.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", n = d.getElementsByTagName("*"), r = d.getElementsByTagName("a")[0], !n || !r || !n.length) {
            return {}
        }
        s = o.createElement("select"), l = s.appendChild(o.createElement("option")), a = d.getElementsByTagName("input")[0], r.style.cssText = "top:1px;float:left;opacity:.5", t = {
            getSetAttribute: "t" !== d.className,
            leadingWhitespace: 3 === d.firstChild.nodeType,
            tbody: !d.getElementsByTagName("tbody").length,
            htmlSerialize: !! d.getElementsByTagName("link").length,
            style: /top/.test(r.getAttribute("style")),
            hrefNormalized: "/a" === r.getAttribute("href"),
            opacity: /^0.5/.test(r.style.opacity),
            cssFloat: !! r.style.cssFloat,
            checkOn: !! a.value,
            optSelected: l.selected,
            enctype: !! o.createElement("form").enctype,
            html5Clone: "<:nav></:nav>" !== o.createElement("nav").cloneNode(!0).outerHTML,
            boxModel: "CSS1Compat" === o.compatMode,
            deleteExpando: !0,
            noCloneEvent: !0,
            inlineBlockNeedsLayout: !1,
            shrinkWrapBlocks: !1,
            reliableMarginRight: !0,
            boxSizingReliable: !0,
            pixelPosition: !1
        }, a.checked = !0, t.noCloneChecked = a.cloneNode(!0).checked, s.disabled = !0, t.optDisabled = !l.disabled;
        try {
            delete d.test
        } catch (h) {
            t.deleteExpando = !1
        }
        a = o.createElement("input"), a.setAttribute("value", ""), t.input = "" === a.getAttribute("value"), a.value = "t", a.setAttribute("type", "radio"), t.radioValue = "t" === a.value, a.setAttribute("checked", "t"), a.setAttribute("name", "t"), u = o.createDocumentFragment(), u.appendChild(a), t.appendChecked = a.checked, t.checkClone = u.cloneNode(!0).cloneNode(!0).lastChild.checked, d.attachEvent && (d.attachEvent("onclick", function() {
            t.noCloneEvent = !1
        }), d.cloneNode(!0).click());
        for (f in {
            submit: !0,
            change: !0,
            focusin: !0
        }) {
            d.setAttribute(c = "on" + f, "t"), t[f + "Bubbles"] = c in e || d.attributes[c].expando === !1
        }
        return d.style.backgroundClip = "content-box", d.cloneNode(!0).style.backgroundClip = "", t.clearCloneStyle = "content-box" === d.style.backgroundClip, b(function() {
            var n, r, a, s = "padding:0;margin:0;border:0;display:block;box-sizing:content-box;-moz-box-sizing:content-box;-webkit-box-sizing:content-box;",
                u = o.getElementsByTagName("body")[0];
            u && (n = o.createElement("div"), n.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px", u.appendChild(n).appendChild(d), d.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", a = d.getElementsByTagName("td"), a[0].style.cssText = "padding:0;margin:0;border:0;display:none", p = 0 === a[0].offsetHeight, a[0].style.display = "", a[1].style.display = "none", t.reliableHiddenOffsets = p && 0 === a[0].offsetHeight, d.innerHTML = "", d.style.cssText = "box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;", t.boxSizing = 4 === d.offsetWidth, t.doesNotIncludeMarginInBodyOffset = 1 !== u.offsetTop, e.getComputedStyle && (t.pixelPosition = "1%" !== (e.getComputedStyle(d, null) || {}).top, t.boxSizingReliable = "4px" === (e.getComputedStyle(d, null) || {
                width: "4px"
            }).width, r = d.appendChild(o.createElement("div")), r.style.cssText = d.style.cssText = s, r.style.marginRight = r.style.width = "0", d.style.width = "1px", t.reliableMarginRight = !parseFloat((e.getComputedStyle(r, null) || {}).marginRight)), typeof d.style.zoom !== i && (d.innerHTML = "", d.style.cssText = s + "width:1px;padding:1px;display:inline;zoom:1", t.inlineBlockNeedsLayout = 3 === d.offsetWidth, d.style.display = "block", d.innerHTML = "<div></div>", d.firstChild.style.width = "5px", t.shrinkWrapBlocks = 3 !== d.offsetWidth, t.inlineBlockNeedsLayout && (u.style.zoom = 1)), u.removeChild(n), n = d = a = r = null)
        }), n = s = u = l = r = a = null, t
    }();
    var O = /(?:\{[\s\S]*\}|\[[\s\S]*\])$/,
        B = /([A-Z])/g;

    function P(e, n, r, i) {
        if (b.acceptData(e)) {
            var o, a, s = b.expando,
                u = "string" == typeof n,
                l = e.nodeType,
                p = l ? b.cache : e,
                f = l ? e[s] : e[s] && s;
            if (f && p[f] && (i || p[f].data) || !u || r !== t) {
                return f || (l ? e[s] = f = c.pop() || b.guid++ : f = s), p[f] || (p[f] = {}, l || (p[f].toJSON = b.noop)), ("object" == typeof n || "function" == typeof n) && (i ? p[f] = b.extend(p[f], n) : p[f].data = b.extend(p[f].data, n)), o = p[f], i || (o.data || (o.data = {}), o = o.data), r !== t && (o[b.camelCase(n)] = r), u ? (a = o[n], null == a && (a = o[b.camelCase(n)])) : a = o, a
            }
        }
    }

    function R(e, t, n) {
        if (b.acceptData(e)) {
            var r, i, o, a = e.nodeType,
                s = a ? b.cache : e,
                u = a ? e[b.expando] : b.expando;
            if (s[u]) {
                if (t && (o = n ? s[u] : s[u].data)) {
                    b.isArray(t) ? t = t.concat(b.map(t, b.camelCase)) : t in o ? t = [t] : (t = b.camelCase(t), t = t in o ? [t] : t.split(" "));
                    for (r = 0, i = t.length; i > r; r++) {
                        delete o[t[r]]
                    }
                    if (!(n ? $ : b.isEmptyObject)(o)) {
                        return
                    }
                }(n || (delete s[u].data, $(s[u]))) && (a ? b.cleanData([e], !0) : b.support.deleteExpando || s != s.window ? delete s[u] : s[u] = null)
            }
        }
    }
    b.extend({
        cache: {},
        expando: "jQuery" + (p + Math.random()).replace(/\D/g, ""),
        noData: {
            embed: !0,
            object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
            applet: !0
        },
        hasData: function(e) {
            return e = e.nodeType ? b.cache[e[b.expando]] : e[b.expando], !! e && !$(e)
        },
        data: function(e, t, n) {
            return P(e, t, n)
        },
        removeData: function(e, t) {
            return R(e, t)
        },
        _data: function(e, t, n) {
            return P(e, t, n, !0)
        },
        _removeData: function(e, t) {
            return R(e, t, !0)
        },
        acceptData: function(e) {
            if (e.nodeType && 1 !== e.nodeType && 9 !== e.nodeType) {
                return !1
            }
            var t = e.nodeName && b.noData[e.nodeName.toLowerCase()];
            return !t || t !== !0 && e.getAttribute("classid") === t
        }
    }), b.fn.extend({
        data: function(e, n) {
            var r, i, o = this[0],
                a = 0,
                s = null;
            if (e === t) {
                if (this.length && (s = b.data(o), 1 === o.nodeType && !b._data(o, "parsedAttrs"))) {
                    for (r = o.attributes; r.length > a; a++) {
                        i = r[a].name, i.indexOf("data-") || (i = b.camelCase(i.slice(5)), W(o, i, s[i]))
                    }
                    b._data(o, "parsedAttrs", !0)
                }
                return s
            }
            return "object" == typeof e ? this.each(function() {
                b.data(this, e)
            }) : b.access(this, function(n) {
                return n === t ? o ? W(o, e, b.data(o, e)) : null : (this.each(function() {
                    b.data(this, e, n)
                }), t)
            }, null, n, arguments.length > 1, null, !0)
        },
        removeData: function(e) {
            return this.each(function() {
                b.removeData(this, e)
            })
        }
    });

    function W(e, n, r) {
        if (r === t && 1 === e.nodeType) {
            var i = "data-" + n.replace(B, "-$1").toLowerCase();
            if (r = e.getAttribute(i), "string" == typeof r) {
                try {
                    r = "true" === r ? !0 : "false" === r ? !1 : "null" === r ? null : +r + "" === r ? +r : O.test(r) ? b.parseJSON(r) : r
                } catch (o) {}
                b.data(e, n, r)
            } else {
                r = t
            }
        }
        return r
    }

    function $(e) {
        var t;
        for (t in e) {
            if (("data" !== t || !b.isEmptyObject(e[t])) && "toJSON" !== t) {
                return !1
            }
        }
        return !0
    }
    b.extend({
        queue: function(e, n, r) {
            var i;
            return e ? (n = (n || "fx") + "queue", i = b._data(e, n), r && (!i || b.isArray(r) ? i = b._data(e, n, b.makeArray(r)) : i.push(r)), i || []) : t
        },
        dequeue: function(e, t) {
            t = t || "fx";
            var n = b.queue(e, t),
                r = n.length,
                i = n.shift(),
                o = b._queueHooks(e, t),
                a = function() {
                    b.dequeue(e, t)
                };
            "inprogress" === i && (i = n.shift(), r--), o.cur = i, i && ("fx" === t && n.unshift("inprogress"), delete o.stop, i.call(e, a, o)), !r && o && o.empty.fire()
        },
        _queueHooks: function(e, t) {
            var n = t + "queueHooks";
            return b._data(e, n) || b._data(e, n, {
                    empty: b.Callbacks("once memory").add(function() {
                        b._removeData(e, t + "queue"), b._removeData(e, n)
                    })
                })
        }
    }), b.fn.extend({
        queue: function(e, n) {
            var r = 2;
            return "string" != typeof e && (n = e, e = "fx", r--), r > arguments.length ? b.queue(this[0], e) : n === t ? this : this.each(function() {
                var t = b.queue(this, e, n);
                b._queueHooks(this, e), "fx" === e && "inprogress" !== t[0] && b.dequeue(this, e)
            })
        },
        dequeue: function(e) {
            return this.each(function() {
                b.dequeue(this, e)
            })
        },
        delay: function(e, t) {
            return e = b.fx ? b.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function(t, n) {
                var r = setTimeout(t, e);
                n.stop = function() {
                    clearTimeout(r)
                }
            })
        },
        clearQueue: function(e) {
            return this.queue(e || "fx", [])
        },
        promise: function(e, n) {
            var r, i = 1,
                o = b.Deferred(),
                a = this,
                s = this.length,
                u = function() {
                    --i || o.resolveWith(a, [a])
                };
            "string" != typeof e && (n = e, e = t), e = e || "fx";
            while (s--) {
                r = b._data(a[s], e + "queueHooks"), r && r.empty && (i++, r.empty.add(u))
            }
            return u(), o.promise(n)
        }
    });
    var I, z, X = /[\t\r\n]/g,
        U = /\r/g,
        V = /^(?:input|select|textarea|button|object)$/i,
        Y = /^(?:a|area)$/i,
        J = /^(?:checked|selected|autofocus|autoplay|async|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped)$/i,
        G = /^(?:checked|selected)$/i,
        Q = b.support.getSetAttribute,
        K = b.support.input;
    b.fn.extend({
        attr: function(e, t) {
            return b.access(this, b.attr, e, t, arguments.length > 1)
        },
        removeAttr: function(e) {
            return this.each(function() {
                b.removeAttr(this, e)
            })
        },
        prop: function(e, t) {
            return b.access(this, b.prop, e, t, arguments.length > 1)
        },
        removeProp: function(e) {
            return e = b.propFix[e] || e, this.each(function() {
                try {
                    this[e] = t, delete this[e]
                } catch (n) {}
            })
        },
        addClass: function(e) {
            var t, n, r, i, o, a = 0,
                s = this.length,
                u = "string" == typeof e && e;
            if (b.isFunction(e)) {
                return this.each(function(t) {
                    b(this).addClass(e.call(this, t, this.className))
                })
            }
            if (u) {
                for (t = (e || "").match(w) || []; s > a; a++) {
                    if (n = this[a], r = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(X, " ") : " ")) {
                        o = 0;
                        while (i = t[o++]) {
                            0 > r.indexOf(" " + i + " ") && (r += i + " ")
                        }
                        n.className = b.trim(r)
                    }
                }
            }
            return this
        },
        removeClass: function(e) {
            var t, n, r, i, o, a = 0,
                s = this.length,
                u = 0 === arguments.length || "string" == typeof e && e;
            if (b.isFunction(e)) {
                return this.each(function(t) {
                    b(this).removeClass(e.call(this, t, this.className))
                })
            }
            if (u) {
                for (t = (e || "").match(w) || []; s > a; a++) {
                    if (n = this[a], r = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(X, " ") : "")) {
                        o = 0;
                        while (i = t[o++]) {
                            while (r.indexOf(" " + i + " ") >= 0) {
                                r = r.replace(" " + i + " ", " ")
                            }
                        }
                        n.className = e ? b.trim(r) : ""
                    }
                }
            }
            return this
        },
        toggleClass: function(e, t) {
            var n = typeof e,
                r = "boolean" == typeof t;
            return b.isFunction(e) ? this.each(function(n) {
                b(this).toggleClass(e.call(this, n, this.className, t), t)
            }) : this.each(function() {
                if ("string" === n) {
                    var o, a = 0,
                        s = b(this),
                        u = t,
                        l = e.match(w) || [];
                    while (o = l[a++]) {
                        u = r ? u : !s.hasClass(o), s[u ? "addClass" : "removeClass"](o)
                    }
                } else {
                    (n === i || "boolean" === n) && (this.className && b._data(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : b._data(this, "__className__") || "")
                }
            })
        },
        hasClass: function(e) {
            var t = " " + e + " ",
                n = 0,
                r = this.length;
            for (; r > n; n++) {
                if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(X, " ").indexOf(t) >= 0) {
                    return !0
                }
            }
            return !1
        },
        val: function(e) {
            var n, r, i, o = this[0];
            if (arguments.length) {
                return i = b.isFunction(e), this.each(function(n) {
                    var o, a = b(this);
                    1 === this.nodeType && (o = i ? e.call(this, n, a.val()) : e, null == o ? o = "" : "number" == typeof o ? o += "" : b.isArray(o) && (o = b.map(o, function(e) {
                        return null == e ? "" : e + ""
                    })), r = b.valHooks[this.type] || b.valHooks[this.nodeName.toLowerCase()], r && "set" in r && r.set(this, o, "value") !== t || (this.value = o))
                })
            }
            if (o) {
                return r = b.valHooks[o.type] || b.valHooks[o.nodeName.toLowerCase()], r && "get" in r && (n = r.get(o, "value")) !== t ? n : (n = o.value, "string" == typeof n ? n.replace(U, "") : null == n ? "" : n)
            }
        }
    }), b.extend({
        valHooks: {
            option: {
                get: function(e) {
                    var t = e.attributes.value;
                    return !t || t.specified ? e.value : e.text
                }
            },
            select: {
                get: function(e) {
                    var t, n, r = e.options,
                        i = e.selectedIndex,
                        o = "select-one" === e.type || 0 > i,
                        a = o ? null : [],
                        s = o ? i + 1 : r.length,
                        u = 0 > i ? s : o ? i : 0;
                    for (; s > u; u++) {
                        if (n = r[u], !(!n.selected && u !== i || (b.support.optDisabled ? n.disabled : null !== n.getAttribute("disabled")) || n.parentNode.disabled && b.nodeName(n.parentNode, "optgroup"))) {
                            if (t = b(n).val(), o) {
                                return t
                            }
                            a.push(t)
                        }
                    }
                    return a
                },
                set: function(e, t) {
                    var n = b.makeArray(t);
                    return b(e).find("option").each(function() {
                        this.selected = b.inArray(b(this).val(), n) >= 0
                    }), n.length || (e.selectedIndex = -1), n
                }
            }
        },
        attr: function(e, n, r) {
            var o, a, s, u = e.nodeType;
            if (e && 3 !== u && 8 !== u && 2 !== u) {
                return typeof e.getAttribute === i ? b.prop(e, n, r) : (a = 1 !== u || !b.isXMLDoc(e), a && (n = n.toLowerCase(), o = b.attrHooks[n] || (J.test(n) ? z : I)), r === t ? o && a && "get" in o && null !== (s = o.get(e, n)) ? s : (typeof e.getAttribute !== i && (s = e.getAttribute(n)), null == s ? t : s) : null !== r ? o && a && "set" in o && (s = o.set(e, r, n)) !== t ? s : (e.setAttribute(n, r + ""), r) : (b.removeAttr(e, n), t))
            }
        },
        removeAttr: function(e, t) {
            var n, r, i = 0,
                o = t && t.match(w);
            if (o && 1 === e.nodeType) {
                while (n = o[i++]) {
                    r = b.propFix[n] || n, J.test(n) ? !Q && G.test(n) ? e[b.camelCase("default-" + n)] = e[r] = !1 : e[r] = !1 : b.attr(e, n, ""), e.removeAttribute(Q ? n : r)
                }
            }
        },
        attrHooks: {
            type: {
                set: function(e, t) {
                    if (!b.support.radioValue && "radio" === t && b.nodeName(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t
                    }
                }
            }
        },
        propFix: {
            tabindex: "tabIndex",
            readonly: "readOnly",
            "for": "htmlFor",
            "class": "className",
            maxlength: "maxLength",
            cellspacing: "cellSpacing",
            cellpadding: "cellPadding",
            rowspan: "rowSpan",
            colspan: "colSpan",
            usemap: "useMap",
            frameborder: "frameBorder",
            contenteditable: "contentEditable"
        },
        prop: function(e, n, r) {
            var i, o, a, s = e.nodeType;
            if (e && 3 !== s && 8 !== s && 2 !== s) {
                return a = 1 !== s || !b.isXMLDoc(e), a && (n = b.propFix[n] || n, o = b.propHooks[n]), r !== t ? o && "set" in o && (i = o.set(e, r, n)) !== t ? i : e[n] = r : o && "get" in o && null !== (i = o.get(e, n)) ? i : e[n]
            }
        },
        propHooks: {
            tabIndex: {
                get: function(e) {
                    var n = e.getAttributeNode("tabindex");
                    return n && n.specified ? parseInt(n.value, 10) : V.test(e.nodeName) || Y.test(e.nodeName) && e.href ? 0 : t
                }
            }
        }
    }), z = {
        get: function(e, n) {
            var r = b.prop(e, n),
                i = "boolean" == typeof r && e.getAttribute(n),
                o = "boolean" == typeof r ? K && Q ? null != i : G.test(n) ? e[b.camelCase("default-" + n)] : !! i : e.getAttributeNode(n);
            return o && o.value !== !1 ? n.toLowerCase() : t
        },
        set: function(e, t, n) {
            return t === !1 ? b.removeAttr(e, n) : K && Q || !G.test(n) ? e.setAttribute(!Q && b.propFix[n] || n, n) : e[b.camelCase("default-" + n)] = e[n] = !0, n
        }
    }, K && Q || (b.attrHooks.value = {
        get: function(e, n) {
            var r = e.getAttributeNode(n);
            return b.nodeName(e, "input") ? e.defaultValue : r && r.specified ? r.value : t
        },
        set: function(e, n, r) {
            return b.nodeName(e, "input") ? (e.defaultValue = n, t) : I && I.set(e, n, r)
        }
    }), Q || (I = b.valHooks.button = {
        get: function(e, n) {
            var r = e.getAttributeNode(n);
            return r && ("id" === n || "name" === n || "coords" === n ? "" !== r.value : r.specified) ? r.value : t
        },
        set: function(e, n, r) {
            var i = e.getAttributeNode(r);
            return i || e.setAttributeNode(i = e.ownerDocument.createAttribute(r)), i.value = n += "", "value" === r || n === e.getAttribute(r) ? n : t
        }
    }, b.attrHooks.contenteditable = {
        get: I.get,
        set: function(e, t, n) {
            I.set(e, "" === t ? !1 : t, n)
        }
    }, b.each(["width", "height"], function(e, n) {
        b.attrHooks[n] = b.extend(b.attrHooks[n], {
            set: function(e, r) {
                return "" === r ? (e.setAttribute(n, "auto"), r) : t
            }
        })
    })), b.support.hrefNormalized || (b.each(["href", "src", "width", "height"], function(e, n) {
        b.attrHooks[n] = b.extend(b.attrHooks[n], {
            get: function(e) {
                var r = e.getAttribute(n, 2);
                return null == r ? t : r
            }
        })
    }), b.each(["href", "src"], function(e, t) {
        b.propHooks[t] = {
            get: function(e) {
                return e.getAttribute(t, 4)
            }
        }
    })), b.support.style || (b.attrHooks.style = {
        get: function(e) {
            return e.style.cssText || t
        },
        set: function(e, t) {
            return e.style.cssText = t + ""
        }
    }), b.support.optSelected || (b.propHooks.selected = b.extend(b.propHooks.selected, {
        get: function(e) {
            var t = e.parentNode;
            return t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex), null
        }
    })), b.support.enctype || (b.propFix.enctype = "encoding"), b.support.checkOn || b.each(["radio", "checkbox"], function() {
        b.valHooks[this] = {
            get: function(e) {
                return null === e.getAttribute("value") ? "on" : e.value
            }
        }
    }), b.each(["radio", "checkbox"], function() {
        b.valHooks[this] = b.extend(b.valHooks[this], {
            set: function(e, n) {
                return b.isArray(n) ? e.checked = b.inArray(b(e).val(), n) >= 0 : t
            }
        })
    });
    var Z = /^(?:input|select|textarea)$/i,
        et = /^key/,
        tt = /^(?:mouse|contextmenu)|click/,
        nt = /^(?:focusinfocus|focusoutblur)$/,
        rt = /^([^.]*)(?:\.(.+)|)$/;

    function it() {
        return !0
    }

    function ot() {
        return !1
    }
    b.event = {
        global: {},
        add: function(e, n, r, o, a) {
            var s, u, l, c, p, f, d, h, g, m, y, v = b._data(e);
            if (v) {
                r.handler && (c = r, r = c.handler, a = c.selector), r.guid || (r.guid = b.guid++), (u = v.events) || (u = v.events = {}), (f = v.handle) || (f = v.handle = function(e) {
                    return typeof b === i || e && b.event.triggered === e.type ? t : b.event.dispatch.apply(f.elem, arguments)
                }, f.elem = e), n = (n || "").match(w) || [""], l = n.length;
                while (l--) {
                    s = rt.exec(n[l]) || [], g = y = s[1], m = (s[2] || "").split(".").sort(), p = b.event.special[g] || {}, g = (a ? p.delegateType : p.bindType) || g, p = b.event.special[g] || {}, d = b.extend({
                        type: g,
                        origType: y,
                        data: o,
                        handler: r,
                        guid: r.guid,
                        selector: a,
                        needsContext: a && b.expr.match.needsContext.test(a),
                        namespace: m.join(".")
                    }, c), (h = u[g]) || (h = u[g] = [], h.delegateCount = 0, p.setup && p.setup.call(e, o, m, f) !== !1 || (e.addEventListener ? e.addEventListener(g, f, !1) : e.attachEvent && e.attachEvent("on" + g, f))), p.add && (p.add.call(e, d), d.handler.guid || (d.handler.guid = r.guid)), a ? h.splice(h.delegateCount++, 0, d) : h.push(d), b.event.global[g] = !0
                }
                e = null
            }
        },
        remove: function(e, t, n, r, i) {
            var o, a, s, u, l, c, p, f, d, h, g, m = b.hasData(e) && b._data(e);
            if (m && (c = m.events)) {
                t = (t || "").match(w) || [""], l = t.length;
                while (l--) {
                    if (s = rt.exec(t[l]) || [], d = g = s[1], h = (s[2] || "").split(".").sort(), d) {
                        p = b.event.special[d] || {}, d = (r ? p.delegateType : p.bindType) || d, f = c[d] || [], s = s[2] && RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), u = o = f.length;
                        while (o--) {
                            a = f[o], !i && g !== a.origType || n && n.guid !== a.guid || s && !s.test(a.namespace) || r && r !== a.selector && ("**" !== r || !a.selector) || (f.splice(o, 1), a.selector && f.delegateCount--, p.remove && p.remove.call(e, a))
                        }
                        u && !f.length && (p.teardown && p.teardown.call(e, h, m.handle) !== !1 || b.removeEvent(e, d, m.handle), delete c[d])
                    } else {
                        for (d in c) {
                            b.event.remove(e, d + t[l], n, r, !0)
                        }
                    }
                }
                b.isEmptyObject(c) && (delete m.handle, b._removeData(e, "events"))
            }
        },
        trigger: function(n, r, i, a) {
            var s, u, l, c, p, f, d, h = [i || o],
                g = y.call(n, "type") ? n.type : n,
                m = y.call(n, "namespace") ? n.namespace.split(".") : [];
            if (l = f = i = i || o, 3 !== i.nodeType && 8 !== i.nodeType && !nt.test(g + b.event.triggered) && (g.indexOf(".") >= 0 && (m = g.split("."), g = m.shift(), m.sort()), u = 0 > g.indexOf(":") && "on" + g, n = n[b.expando] ? n : new b.Event(g, "object" == typeof n && n), n.isTrigger = !0, n.namespace = m.join("."), n.namespace_re = n.namespace ? RegExp("(^|\\.)" + m.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, n.result = t, n.target || (n.target = i), r = null == r ? [n] : b.makeArray(r, [n]), p = b.event.special[g] || {}, a || !p.trigger || p.trigger.apply(i, r) !== !1)) {
                if (!a && !p.noBubble && !b.isWindow(i)) {
                    for (c = p.delegateType || g, nt.test(c + g) || (l = l.parentNode); l; l = l.parentNode) {
                        h.push(l), f = l
                    }
                    f === (i.ownerDocument || o) && h.push(f.defaultView || f.parentWindow || e)
                }
                d = 0;
                while ((l = h[d++]) && !n.isPropagationStopped()) {
                    n.type = d > 1 ? c : p.bindType || g, s = (b._data(l, "events") || {})[n.type] && b._data(l, "handle"), s && s.apply(l, r), s = u && l[u], s && b.acceptData(l) && s.apply && s.apply(l, r) === !1 && n.preventDefault()
                }
                if (n.type = g, !(a || n.isDefaultPrevented() || p._default && p._default.apply(i.ownerDocument, r) !== !1 || "click" === g && b.nodeName(i, "a") || !b.acceptData(i) || !u || !i[g] || b.isWindow(i))) {
                    f = i[u], f && (i[u] = null), b.event.triggered = g;
                    try {
                        i[g]()
                    } catch (v) {}
                    b.event.triggered = t, f && (i[u] = f)
                }
                return n.result
            }
        },
        dispatch: function(e) {
            e = b.event.fix(e);
            var n, r, i, o, a, s = [],
                u = h.call(arguments),
                l = (b._data(this, "events") || {})[e.type] || [],
                c = b.event.special[e.type] || {};
            if (u[0] = e, e.delegateTarget = this, !c.preDispatch || c.preDispatch.call(this, e) !== !1) {
                s = b.event.handlers.call(this, e, l), n = 0;
                while ((o = s[n++]) && !e.isPropagationStopped()) {
                    e.currentTarget = o.elem, a = 0;
                    while ((i = o.handlers[a++]) && !e.isImmediatePropagationStopped()) {
                        (!e.namespace_re || e.namespace_re.test(i.namespace)) && (e.handleObj = i, e.data = i.data, r = ((b.event.special[i.origType] || {}).handle || i.handler).apply(o.elem, u), r !== t && (e.result = r) === !1 && (e.preventDefault(), e.stopPropagation()))
                    }
                }
                return c.postDispatch && c.postDispatch.call(this, e), e.result
            }
        },
        handlers: function(e, n) {
            var r, i, o, a, s = [],
                u = n.delegateCount,
                l = e.target;
            if (u && l.nodeType && (!e.button || "click" !== e.type)) {
                for (; l != this; l = l.parentNode || this) {
                    if (1 === l.nodeType && (l.disabled !== !0 || "click" !== e.type)) {
                        for (o = [], a = 0; u > a; a++) {
                            i = n[a], r = i.selector + " ", o[r] === t && (o[r] = i.needsContext ? b(r, this).index(l) >= 0 : b.find(r, this, null, [l]).length), o[r] && o.push(i)
                        }
                        o.length && s.push({
                            elem: l,
                            handlers: o
                        })
                    }
                }
            }
            return n.length > u && s.push({
                elem: this,
                handlers: n.slice(u)
            }), s
        },
        fix: function(e) {
            if (e[b.expando]) {
                return e
            }
            var t, n, r, i = e.type,
                a = e,
                s = this.fixHooks[i];
            s || (this.fixHooks[i] = s = tt.test(i) ? this.mouseHooks : et.test(i) ? this.keyHooks : {}), r = s.props ? this.props.concat(s.props) : this.props, e = new b.Event(a), t = r.length;
            while (t--) {
                n = r[t], e[n] = a[n]
            }
            return e.target || (e.target = a.srcElement || o), 3 === e.target.nodeType && (e.target = e.target.parentNode), e.metaKey = !! e.metaKey, s.filter ? s.filter(e, a) : e
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(e, t) {
                return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(e, n) {
                var r, i, a, s = n.button,
                    u = n.fromElement;
                return null == e.pageX && null != n.clientX && (i = e.target.ownerDocument || o, a = i.documentElement, r = i.body, e.pageX = n.clientX + (a && a.scrollLeft || r && r.scrollLeft || 0) - (a && a.clientLeft || r && r.clientLeft || 0), e.pageY = n.clientY + (a && a.scrollTop || r && r.scrollTop || 0) - (a && a.clientTop || r && r.clientTop || 0)), !e.relatedTarget && u && (e.relatedTarget = u === e.target ? n.toElement : u), e.which || s === t || (e.which = 1 & s ? 1 : 2 & s ? 3 : 4 & s ? 2 : 0), e
            }
        },
        special: {
            load: {
                noBubble: !0
            },
            click: {
                trigger: function() {
                    return b.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : t
                }
            },
            focus: {
                trigger: function() {
                    if (this !== o.activeElement && this.focus) {
                        try {
                            return this.focus(), !1
                        } catch (e) {}
                    }
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    return this === o.activeElement && this.blur ? (this.blur(), !1) : t
                },
                delegateType: "focusout"
            },
            beforeunload: {
                postDispatch: function(e) {
                    e.result !== t && (e.originalEvent.returnValue = e.result)
                }
            }
        },
        simulate: function(e, t, n, r) {
            var i = b.extend(new b.Event, n, {
                type: e,
                isSimulated: !0,
                originalEvent: {}
            });
            r ? b.event.trigger(i, null, t) : b.event.dispatch.call(t, i), i.isDefaultPrevented() && n.preventDefault()
        }
    }, b.removeEvent = o.removeEventListener ? function(e, t, n) {
        e.removeEventListener && e.removeEventListener(t, n, !1)
    } : function(e, t, n) {
        var r = "on" + t;
        e.detachEvent && (typeof e[r] === i && (e[r] = null), e.detachEvent(r, n))
    }, b.Event = function(e, n) {
        return this instanceof b.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || e.returnValue === !1 || e.getPreventDefault && e.getPreventDefault() ? it : ot) : this.type = e, n && b.extend(this, n), this.timeStamp = e && e.timeStamp || b.now(), this[b.expando] = !0, t) : new b.Event(e, n)
    }, b.Event.prototype = {
        isDefaultPrevented: ot,
        isPropagationStopped: ot,
        isImmediatePropagationStopped: ot,
        preventDefault: function() {
            var e = this.originalEvent;
            this.isDefaultPrevented = it, e && (e.preventDefault ? e.preventDefault() : e.returnValue = !1)
        },
        stopPropagation: function() {
            var e = this.originalEvent;
            this.isPropagationStopped = it, e && (e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0)
        },
        stopImmediatePropagation: function() {
            this.isImmediatePropagationStopped = it, this.stopPropagation()
        }
    }, b.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout"
    }, function(e, t) {
        b.event.special[e] = {
            delegateType: t,
            bindType: t,
            handle: function(e) {
                var n, r = this,
                    i = e.relatedTarget,
                    o = e.handleObj;
                return (!i || i !== r && !b.contains(r, i)) && (e.type = o.origType, n = o.handler.apply(this, arguments), e.type = t), n
            }
        }
    }), b.support.submitBubbles || (b.event.special.submit = {
        setup: function() {
            return b.nodeName(this, "form") ? !1 : (b.event.add(this, "click._submit keypress._submit", function(e) {
                var n = e.target,
                    r = b.nodeName(n, "input") || b.nodeName(n, "button") ? n.form : t;
                r && !b._data(r, "submitBubbles") && (b.event.add(r, "submit._submit", function(e) {
                    e._submit_bubble = !0
                }), b._data(r, "submitBubbles", !0))
            }), t)
        },
        postDispatch: function(e) {
            e._submit_bubble && (delete e._submit_bubble, this.parentNode && !e.isTrigger && b.event.simulate("submit", this.parentNode, e, !0))
        },
        teardown: function() {
            return b.nodeName(this, "form") ? !1 : (b.event.remove(this, "._submit"), t)
        }
    }), b.support.changeBubbles || (b.event.special.change = {
        setup: function() {
            return Z.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (b.event.add(this, "propertychange._change", function(e) {
                "checked" === e.originalEvent.propertyName && (this._just_changed = !0)
            }), b.event.add(this, "click._change", function(e) {
                this._just_changed && !e.isTrigger && (this._just_changed = !1), b.event.simulate("change", this, e, !0)
            })), !1) : (b.event.add(this, "beforeactivate._change", function(e) {
                var t = e.target;
                Z.test(t.nodeName) && !b._data(t, "changeBubbles") && (b.event.add(t, "change._change", function(e) {
                    !this.parentNode || e.isSimulated || e.isTrigger || b.event.simulate("change", this.parentNode, e, !0)
                }), b._data(t, "changeBubbles", !0))
            }), t)
        },
        handle: function(e) {
            var n = e.target;
            return this !== n || e.isSimulated || e.isTrigger || "radio" !== n.type && "checkbox" !== n.type ? e.handleObj.handler.apply(this, arguments) : t
        },
        teardown: function() {
            return b.event.remove(this, "._change"), !Z.test(this.nodeName)
        }
    }), b.support.focusinBubbles || b.each({
        focus: "focusin",
        blur: "focusout"
    }, function(e, t) {
        var n = 0,
            r = function(e) {
                b.event.simulate(t, e.target, b.event.fix(e), !0)
            };
        b.event.special[t] = {
            setup: function() {
                0 === n++ && o.addEventListener(e, r, !0)
            },
            teardown: function() {
                0 === --n && o.removeEventListener(e, r, !0)
            }
        }
    }), b.fn.extend({
        on: function(e, n, r, i, o) {
            var a, s;
            if ("object" == typeof e) {
                "string" != typeof n && (r = r || n, n = t);
                for (a in e) {
                    this.on(a, n, r, e[a], o)
                }
                return this
            }
            if (null == r && null == i ? (i = n, r = n = t) : null == i && ("string" == typeof n ? (i = r, r = t) : (i = r, r = n, n = t)), i === !1) {
                i = ot
            } else {
                if (!i) {
                    return this
                }
            }
            return 1 === o && (s = i, i = function(e) {
                return b().off(e), s.apply(this, arguments)
            }, i.guid = s.guid || (s.guid = b.guid++)), this.each(function() {
                b.event.add(this, e, i, r, n)
            })
        },
        one: function(e, t, n, r) {
            return this.on(e, t, n, r, 1)
        },
        off: function(e, n, r) {
            var i, o;
            if (e && e.preventDefault && e.handleObj) {
                return i = e.handleObj, b(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this
            }
            if ("object" == typeof e) {
                for (o in e) {
                    this.off(o, n, e[o])
                }
                return this
            }
            return (n === !1 || "function" == typeof n) && (r = n, n = t), r === !1 && (r = ot), this.each(function() {
                b.event.remove(this, e, r, n)
            })
        },
        bind: function(e, t, n) {
            return this.on(e, null, t, n)
        },
        unbind: function(e, t) {
            return this.off(e, null, t)
        },
        delegate: function(e, t, n, r) {
            return this.on(t, e, n, r)
        },
        undelegate: function(e, t, n) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
        },
        trigger: function(e, t) {
            return this.each(function() {
                b.event.trigger(e, t, this)
            })
        },
        triggerHandler: function(e, n) {
            var r = this[0];
            return r ? b.event.trigger(e, n, r, !0) : t
        }
    }),
        function(e, t) {
            var n, r, i, o, a, s, u, l, c, p, f, d, h, g, m, y, v, x = "sizzle" + -new Date,
                w = e.document,
                T = {}, N = 0,
                C = 0,
                k = it(),
                E = it(),
                S = it(),
                A = typeof t,
                j = 1 << 31,
                D = [],
                L = D.pop,
                H = D.push,
                q = D.slice,
                M = D.indexOf || function(e) {
                        var t = 0,
                            n = this.length;
                        for (; n > t; t++) {
                            if (this[t] === e) {
                                return t
                            }
                        }
                        return -1
                    }, _ = "[\\x20\\t\\r\\n\\f]",
                F = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
                O = F.replace("w", "w#"),
                B = "([*^$|!~]?=)",
                P = "\\[" + _ + "*(" + F + ")" + _ + "*(?:" + B + _ + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + O + ")|)|)" + _ + "*\\]",
                R = ":(" + F + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + P.replace(3, 8) + ")*)|.*)\\)|)",
                W = RegExp("^" + _ + "+|((?:^|[^\\\\])(?:\\\\.)*)" + _ + "+$", "g"),
                $ = RegExp("^" + _ + "*," + _ + "*"),
                I = RegExp("^" + _ + "*([\\x20\\t\\r\\n\\f>+~])" + _ + "*"),
                z = RegExp(R),
                X = RegExp("^" + O + "$"),
                U = {
                    ID: RegExp("^#(" + F + ")"),
                    CLASS: RegExp("^\\.(" + F + ")"),
                    NAME: RegExp("^\\[name=['\"]?(" + F + ")['\"]?\\]"),
                    TAG: RegExp("^(" + F.replace("w", "w*") + ")"),
                    ATTR: RegExp("^" + P),
                    PSEUDO: RegExp("^" + R),
                    CHILD: RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + _ + "*(even|odd|(([+-]|)(\\d*)n|)" + _ + "*(?:([+-]|)" + _ + "*(\\d+)|))" + _ + "*\\)|)", "i"),
                    needsContext: RegExp("^" + _ + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + _ + "*((?:-\\d)?\\d*)" + _ + "*\\)|)(?=[^-]|$)", "i")
                }, V = /[\x20\t\r\n\f]*[+~]/,
                Y = /^[^{]+\{\s*\[native code/,
                J = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
                G = /^(?:input|select|textarea|button)$/i,
                Q = /^h\d$/i,
                K = /'|\\/g,
                Z = /\=[\x20\t\r\n\f]*([^'"\]]*)[\x20\t\r\n\f]*\]/g,
                et = /\\([\da-fA-F]{1,6}[\x20\t\r\n\f]?|.)/g,
                tt = function(e, t) {
                    var n = "0x" + t - 65536;
                    return n !== n ? t : 0 > n ? String.fromCharCode(n + 65536) : String.fromCharCode(55296 | n >> 10, 56320 | 1023 & n)
                };
            try {
                q.call(w.documentElement.childNodes, 0)[0].nodeType
            } catch (nt) {
                q = function(e) {
                    var t, n = [];
                    while (t = this[e++]) {
                        n.push(t)
                    }
                    return n
                }
            }

            function rt(e) {
                return Y.test(e + "")
            }

            function it() {
                var e, t = [];
                return e = function(n, r) {
                    return t.push(n += " ") > i.cacheLength && delete e[t.shift()], e[n] = r
                }
            }

            function ot(e) {
                return e[x] = !0, e
            }

            function at(e) {
                var t = p.createElement("div");
                try {
                    return e(t)
                } catch (n) {
                    return !1
                } finally {
                    t = null
                }
            }

            function st(e, t, n, r) {
                var i, o, a, s, u, l, f, g, m, v;
                if ((t ? t.ownerDocument || t : w) !== p && c(t), t = t || p, n = n || [], !e || "string" != typeof e) {
                    return n
                }
                if (1 !== (s = t.nodeType) && 9 !== s) {
                    return []
                }
                if (!d && !r) {
                    if (i = J.exec(e)) {
                        if (a = i[1]) {
                            if (9 === s) {
                                if (o = t.getElementById(a), !o || !o.parentNode) {
                                    return n
                                }
                                if (o.id === a) {
                                    return n.push(o), n
                                }
                            } else {
                                if (t.ownerDocument && (o = t.ownerDocument.getElementById(a)) && y(t, o) && o.id === a) {
                                    return n.push(o), n
                                }
                            }
                        } else {
                            if (i[2]) {
                                return H.apply(n, q.call(t.getElementsByTagName(e), 0)), n
                            }
                            if ((a = i[3]) && T.getByClassName && t.getElementsByClassName) {
                                return H.apply(n, q.call(t.getElementsByClassName(a), 0)), n
                            }
                        }
                    }
                    if (T.qsa && !h.test(e)) {
                        if (f = !0, g = x, m = t, v = 9 === s && e, 1 === s && "object" !== t.nodeName.toLowerCase()) {
                            l = ft(e), (f = t.getAttribute("id")) ? g = f.replace(K, "\\$&") : t.setAttribute("id", g), g = "[id='" + g + "'] ", u = l.length;
                            while (u--) {
                                l[u] = g + dt(l[u])
                            }
                            m = V.test(e) && t.parentNode || t, v = l.join(",")
                        }
                        if (v) {
                            try {
                                return H.apply(n, q.call(m.querySelectorAll(v), 0)), n
                            } catch (b) {} finally {
                                f || t.removeAttribute("id")
                            }
                        }
                    }
                }
                return wt(e.replace(W, "$1"), t, n, r)
            }
            a = st.isXML = function(e) {
                var t = e && (e.ownerDocument || e).documentElement;
                return t ? "HTML" !== t.nodeName : !1
            }, c = st.setDocument = function(e) {
                var n = e ? e.ownerDocument || e : w;
                return n !== p && 9 === n.nodeType && n.documentElement ? (p = n, f = n.documentElement, d = a(n), T.tagNameNoComments = at(function(e) {
                    return e.appendChild(n.createComment("")), !e.getElementsByTagName("*").length
                }), T.attributes = at(function(e) {
                    e.innerHTML = "<select></select>";
                    var t = typeof e.lastChild.getAttribute("multiple");
                    return "boolean" !== t && "string" !== t
                }), T.getByClassName = at(function(e) {
                    return e.innerHTML = "<div class='hidden e'></div><div class='hidden'></div>", e.getElementsByClassName && e.getElementsByClassName("e").length ? (e.lastChild.className = "e", 2 === e.getElementsByClassName("e").length) : !1
                }), T.getByName = at(function(e) {
                    e.id = x + 0, e.innerHTML = "<a name='" + x + "'></a><div name='" + x + "'></div>", f.insertBefore(e, f.firstChild);
                    var t = n.getElementsByName && n.getElementsByName(x).length === 2 + n.getElementsByName(x + 0).length;
                    return T.getIdNotName = !n.getElementById(x), f.removeChild(e), t
                }), i.attrHandle = at(function(e) {
                    return e.innerHTML = "<a href='#'></a>", e.firstChild && typeof e.firstChild.getAttribute !== A && "#" === e.firstChild.getAttribute("href")
                }) ? {} : {
                    href: function(e) {
                        return e.getAttribute("href", 2)
                    },
                    type: function(e) {
                        return e.getAttribute("type")
                    }
                }, T.getIdNotName ? (i.find.ID = function(e, t) {
                    if (typeof t.getElementById !== A && !d) {
                        var n = t.getElementById(e);
                        return n && n.parentNode ? [n] : []
                    }
                }, i.filter.ID = function(e) {
                    var t = e.replace(et, tt);
                    return function(e) {
                        return e.getAttribute("id") === t
                    }
                }) : (i.find.ID = function(e, n) {
                    if (typeof n.getElementById !== A && !d) {
                        var r = n.getElementById(e);
                        return r ? r.id === e || typeof r.getAttributeNode !== A && r.getAttributeNode("id").value === e ? [r] : t : []
                    }
                }, i.filter.ID = function(e) {
                    var t = e.replace(et, tt);
                    return function(e) {
                        var n = typeof e.getAttributeNode !== A && e.getAttributeNode("id");
                        return n && n.value === t
                    }
                }), i.find.TAG = T.tagNameNoComments ? function(e, n) {
                    return typeof n.getElementsByTagName !== A ? n.getElementsByTagName(e) : t
                } : function(e, t) {
                    var n, r = [],
                        i = 0,
                        o = t.getElementsByTagName(e);
                    if ("*" === e) {
                        while (n = o[i++]) {
                            1 === n.nodeType && r.push(n)
                        }
                        return r
                    }
                    return o
                }, i.find.NAME = T.getByName && function(e, n) {
                    return typeof n.getElementsByName !== A ? n.getElementsByName(name) : t
                }, i.find.CLASS = T.getByClassName && function(e, n) {
                    return typeof n.getElementsByClassName === A || d ? t : n.getElementsByClassName(e)
                }, g = [], h = [":focus"], (T.qsa = rt(n.querySelectorAll)) && (at(function(e) {
                    e.innerHTML = "<select><option selected=''></option></select>", e.querySelectorAll("[selected]").length || h.push("\\[" + _ + "*(?:checked|disabled|ismap|multiple|readonly|selected|value)"), e.querySelectorAll(":checked").length || h.push(":checked")
                }), at(function(e) {
                    e.innerHTML = "<input type='hidden' i=''/>", e.querySelectorAll("[i^='']").length && h.push("[*^$]=" + _ + "*(?:\"\"|'')"), e.querySelectorAll(":enabled").length || h.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), h.push(",.*:")
                })), (T.matchesSelector = rt(m = f.matchesSelector || f.mozMatchesSelector || f.webkitMatchesSelector || f.oMatchesSelector || f.msMatchesSelector)) && at(function(e) {
                    T.disconnectedMatch = m.call(e, "div"), m.call(e, "[s!='']:x"), g.push("!=", R)
                }), h = RegExp(h.join("|")), g = RegExp(g.join("|")), y = rt(f.contains) || f.compareDocumentPosition ? function(e, t) {
                    var n = 9 === e.nodeType ? e.documentElement : e,
                        r = t && t.parentNode;
                    return e === r || !(!r || 1 !== r.nodeType || !(n.contains ? n.contains(r) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(r)))
                } : function(e, t) {
                    if (t) {
                        while (t = t.parentNode) {
                            if (t === e) {
                                return !0
                            }
                        }
                    }
                    return !1
                }, v = f.compareDocumentPosition ? function(e, t) {
                    var r;
                    return e === t ? (u = !0, 0) : (r = t.compareDocumentPosition && e.compareDocumentPosition && e.compareDocumentPosition(t)) ? 1 & r || e.parentNode && 11 === e.parentNode.nodeType ? e === n || y(w, e) ? -1 : t === n || y(w, t) ? 1 : 0 : 4 & r ? -1 : 1 : e.compareDocumentPosition ? -1 : 1
                } : function(e, t) {
                    var r, i = 0,
                        o = e.parentNode,
                        a = t.parentNode,
                        s = [e],
                        l = [t];
                    if (e === t) {
                        return u = !0, 0
                    }
                    if (!o || !a) {
                        return e === n ? -1 : t === n ? 1 : o ? -1 : a ? 1 : 0
                    }
                    if (o === a) {
                        return ut(e, t)
                    }
                    r = e;
                    while (r = r.parentNode) {
                        s.unshift(r)
                    }
                    r = t;
                    while (r = r.parentNode) {
                        l.unshift(r)
                    }
                    while (s[i] === l[i]) {
                        i++
                    }
                    return i ? ut(s[i], l[i]) : s[i] === w ? -1 : l[i] === w ? 1 : 0
                }, u = !1, [0, 0].sort(v), T.detectDuplicates = u, p) : p
            }, st.matches = function(e, t) {
                return st(e, null, null, t)
            }, st.matchesSelector = function(e, t) {
                if ((e.ownerDocument || e) !== p && c(e), t = t.replace(Z, "='$1']"), !(!T.matchesSelector || d || g && g.test(t) || h.test(t))) {
                    try {
                        var n = m.call(e, t);
                        if (n || T.disconnectedMatch || e.document && 11 !== e.document.nodeType) {
                            return n
                        }
                    } catch (r) {}
                }
                return st(t, p, null, [e]).length > 0
            }, st.contains = function(e, t) {
                return (e.ownerDocument || e) !== p && c(e), y(e, t)
            }, st.attr = function(e, t) {
                var n;
                return (e.ownerDocument || e) !== p && c(e), d || (t = t.toLowerCase()), (n = i.attrHandle[t]) ? n(e) : d || T.attributes ? e.getAttribute(t) : ((n = e.getAttributeNode(t)) || e.getAttribute(t)) && e[t] === !0 ? t : n && n.specified ? n.value : null
            }, st.error = function(e) {
                throw Error("Syntax error, unrecognized expression: " + e)
            }, st.uniqueSort = function(e) {
                var t, n = [],
                    r = 1,
                    i = 0;
                if (u = !T.detectDuplicates, e.sort(v), u) {
                    for (; t = e[r]; r++) {
                        t === e[r - 1] && (i = n.push(r))
                    }
                    while (i--) {
                        e.splice(n[i], 1)
                    }
                }
                return e
            };

            function ut(e, t) {
                var n = t && e,
                    r = n && (~t.sourceIndex || j) - (~e.sourceIndex || j);
                if (r) {
                    return r
                }
                if (n) {
                    while (n = n.nextSibling) {
                        if (n === t) {
                            return -1
                        }
                    }
                }
                return e ? 1 : -1
            }

            function lt(e) {
                return function(t) {
                    var n = t.nodeName.toLowerCase();
                    return "input" === n && t.type === e
                }
            }

            function ct(e) {
                return function(t) {
                    var n = t.nodeName.toLowerCase();
                    return ("input" === n || "button" === n) && t.type === e
                }
            }

            function pt(e) {
                return ot(function(t) {
                    return t = +t, ot(function(n, r) {
                        var i, o = e([], n.length, t),
                            a = o.length;
                        while (a--) {
                            n[i = o[a]] && (n[i] = !(r[i] = n[i]))
                        }
                    })
                })
            }
            o = st.getText = function(e) {
                var t, n = "",
                    r = 0,
                    i = e.nodeType;
                if (i) {
                    if (1 === i || 9 === i || 11 === i) {
                        if ("string" == typeof e.textContent) {
                            return e.textContent
                        }
                        for (e = e.firstChild; e; e = e.nextSibling) {
                            n += o(e)
                        }
                    } else {
                        if (3 === i || 4 === i) {
                            return e.nodeValue
                        }
                    }
                } else {
                    for (; t = e[r]; r++) {
                        n += o(t)
                    }
                }
                return n
            }, i = st.selectors = {
                cacheLength: 50,
                createPseudo: ot,
                match: U,
                find: {},
                relative: {
                    ">": {
                        dir: "parentNode",
                        first: !0
                    },
                    " ": {
                        dir: "parentNode"
                    },
                    "+": {
                        dir: "previousSibling",
                        first: !0
                    },
                    "~": {
                        dir: "previousSibling"
                    }
                },
                preFilter: {
                    ATTR: function(e) {
                        return e[1] = e[1].replace(et, tt), e[3] = (e[4] || e[5] || "").replace(et, tt), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                    },
                    CHILD: function(e) {
                        return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || st.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && st.error(e[0]), e
                    },
                    PSEUDO: function(e) {
                        var t, n = !e[5] && e[2];
                        return U.CHILD.test(e[0]) ? null : (e[4] ? e[2] = e[4] : n && z.test(n) && (t = ft(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                    }
                },
                filter: {
                    TAG: function(e) {
                        return "*" === e ? function() {
                            return !0
                        } : (e = e.replace(et, tt).toLowerCase(), function(t) {
                            return t.nodeName && t.nodeName.toLowerCase() === e
                        })
                    },
                    CLASS: function(e) {
                        var t = k[e + " "];
                        return t || (t = RegExp("(^|" + _ + ")" + e + "(" + _ + "|$)")) && k(e, function(e) {
                                return t.test(e.className || typeof e.getAttribute !== A && e.getAttribute("class") || "")
                            })
                    },
                    ATTR: function(e, t, n) {
                        return function(r) {
                            var i = st.attr(r, e);
                            return null == i ? "!=" === t : t ? (i += "", "=" === t ? i === n : "!=" === t ? i !== n : "^=" === t ? n && 0 === i.indexOf(n) : "*=" === t ? n && i.indexOf(n) > -1 : "$=" === t ? n && i.slice(-n.length) === n : "~=" === t ? (" " + i + " ").indexOf(n) > -1 : "|=" === t ? i === n || i.slice(0, n.length + 1) === n + "-" : !1) : !0
                        }
                    },
                    CHILD: function(e, t, n, r, i) {
                        var o = "nth" !== e.slice(0, 3),
                            a = "last" !== e.slice(-4),
                            s = "of-type" === t;
                        return 1 === r && 0 === i ? function(e) {
                            return !!e.parentNode
                        } : function(t, n, u) {
                            var l, c, p, f, d, h, g = o !== a ? "nextSibling" : "previousSibling",
                                m = t.parentNode,
                                y = s && t.nodeName.toLowerCase(),
                                v = !u && !s;
                            if (m) {
                                if (o) {
                                    while (g) {
                                        p = t;
                                        while (p = p[g]) {
                                            if (s ? p.nodeName.toLowerCase() === y : 1 === p.nodeType) {
                                                return !1
                                            }
                                        }
                                        h = g = "only" === e && !h && "nextSibling"
                                    }
                                    return !0
                                }
                                if (h = [a ? m.firstChild : m.lastChild], a && v) {
                                    c = m[x] || (m[x] = {}), l = c[e] || [], d = l[0] === N && l[1], f = l[0] === N && l[2], p = d && m.childNodes[d];
                                    while (p = ++d && p && p[g] || (f = d = 0) || h.pop()) {
                                        if (1 === p.nodeType && ++f && p === t) {
                                            c[e] = [N, d, f];
                                            break
                                        }
                                    }
                                } else {
                                    if (v && (l = (t[x] || (t[x] = {}))[e]) && l[0] === N) {
                                        f = l[1]
                                    } else {
                                        while (p = ++d && p && p[g] || (f = d = 0) || h.pop()) {
                                            if ((s ? p.nodeName.toLowerCase() === y : 1 === p.nodeType) && ++f && (v && ((p[x] || (p[x] = {}))[e] = [N, f]), p === t)) {
                                                break
                                            }
                                        }
                                    }
                                }
                                return f -= i, f === r || 0 === f % r && f / r >= 0
                            }
                        }
                    },
                    PSEUDO: function(e, t) {
                        var n, r = i.pseudos[e] || i.setFilters[e.toLowerCase()] || st.error("unsupported pseudo: " + e);
                        return r[x] ? r(t) : r.length > 1 ? (n = [e, e, "", t], i.setFilters.hasOwnProperty(e.toLowerCase()) ? ot(function(e, n) {
                            var i, o = r(e, t),
                                a = o.length;
                            while (a--) {
                                i = M.call(e, o[a]), e[i] = !(n[i] = o[a])
                            }
                        }) : function(e) {
                            return r(e, 0, n)
                        }) : r
                    }
                },
                pseudos: {
                    not: ot(function(e) {
                        var t = [],
                            n = [],
                            r = s(e.replace(W, "$1"));
                        return r[x] ? ot(function(e, t, n, i) {
                            var o, a = r(e, null, i, []),
                                s = e.length;
                            while (s--) {
                                (o = a[s]) && (e[s] = !(t[s] = o))
                            }
                        }) : function(e, i, o) {
                            return t[0] = e, r(t, null, o, n), !n.pop()
                        }
                    }),
                    has: ot(function(e) {
                        return function(t) {
                            return st(e, t).length > 0
                        }
                    }),
                    contains: ot(function(e) {
                        return function(t) {
                            return (t.textContent || t.innerText || o(t)).indexOf(e) > -1
                        }
                    }),
                    lang: ot(function(e) {
                        return X.test(e || "") || st.error("unsupported lang: " + e), e = e.replace(et, tt).toLowerCase(),
                            function(t) {
                                var n;
                                do {
                                    if (n = d ? t.getAttribute("xml:lang") || t.getAttribute("lang") : t.lang) {
                                        return n = n.toLowerCase(), n === e || 0 === n.indexOf(e + "-")
                                    }
                                } while ((t = t.parentNode) && 1 === t.nodeType);
                                return !1
                            }
                    }),
                    target: function(t) {
                        var n = e.location && e.location.hash;
                        return n && n.slice(1) === t.id
                    },
                    root: function(e) {
                        return e === f
                    },
                    focus: function(e) {
                        return e === p.activeElement && (!p.hasFocus || p.hasFocus()) && !! (e.type || e.href || ~e.tabIndex)
                    },
                    enabled: function(e) {
                        return e.disabled === !1
                    },
                    disabled: function(e) {
                        return e.disabled === !0
                    },
                    checked: function(e) {
                        var t = e.nodeName.toLowerCase();
                        return "input" === t && !! e.checked || "option" === t && !! e.selected
                    },
                    selected: function(e) {
                        return e.parentNode && e.parentNode.selectedIndex, e.selected === !0
                    },
                    empty: function(e) {
                        for (e = e.firstChild; e; e = e.nextSibling) {
                            if (e.nodeName > "@" || 3 === e.nodeType || 4 === e.nodeType) {
                                return !1
                            }
                        }
                        return !0
                    },
                    parent: function(e) {
                        return !i.pseudos.empty(e)
                    },
                    header: function(e) {
                        return Q.test(e.nodeName)
                    },
                    input: function(e) {
                        return G.test(e.nodeName)
                    },
                    button: function(e) {
                        var t = e.nodeName.toLowerCase();
                        return "input" === t && "button" === e.type || "button" === t
                    },
                    text: function(e) {
                        var t;
                        return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || t.toLowerCase() === e.type)
                    },
                    first: pt(function() {
                        return [0]
                    }),
                    last: pt(function(e, t) {
                        return [t - 1]
                    }),
                    eq: pt(function(e, t, n) {
                        return [0 > n ? n + t : n]
                    }),
                    even: pt(function(e, t) {
                        var n = 0;
                        for (; t > n; n += 2) {
                            e.push(n)
                        }
                        return e
                    }),
                    odd: pt(function(e, t) {
                        var n = 1;
                        for (; t > n; n += 2) {
                            e.push(n)
                        }
                        return e
                    }),
                    lt: pt(function(e, t, n) {
                        var r = 0 > n ? n + t : n;
                        for (; --r >= 0;) {
                            e.push(r)
                        }
                        return e
                    }),
                    gt: pt(function(e, t, n) {
                        var r = 0 > n ? n + t : n;
                        for (; t > ++r;) {
                            e.push(r)
                        }
                        return e
                    })
                }
            };
            for (n in {
                radio: !0,
                checkbox: !0,
                file: !0,
                password: !0,
                image: !0
            }) {
                i.pseudos[n] = lt(n)
            }
            for (n in {
                submit: !0,
                reset: !0
            }) {
                i.pseudos[n] = ct(n)
            }

            function ft(e, t) {
                var n, r, o, a, s, u, l, c = E[e + " "];
                if (c) {
                    return t ? 0 : c.slice(0)
                }
                s = e, u = [], l = i.preFilter;
                while (s) {
                    (!n || (r = $.exec(s))) && (r && (s = s.slice(r[0].length) || s), u.push(o = [])), n = !1, (r = I.exec(s)) && (n = r.shift(), o.push({
                        value: n,
                        type: r[0].replace(W, " ")
                    }), s = s.slice(n.length));
                    for (a in i.filter) {
                        !(r = U[a].exec(s)) || l[a] && !(r = l[a](r)) || (n = r.shift(), o.push({
                            value: n,
                            type: a,
                            matches: r
                        }), s = s.slice(n.length))
                    }
                    if (!n) {
                        break
                    }
                }
                return t ? s.length : s ? st.error(e) : E(e, u).slice(0)
            }

            function dt(e) {
                var t = 0,
                    n = e.length,
                    r = "";
                for (; n > t; t++) {
                    r += e[t].value
                }
                return r
            }

            function ht(e, t, n) {
                var i = t.dir,
                    o = n && "parentNode" === i,
                    a = C++;
                return t.first ? function(t, n, r) {
                    while (t = t[i]) {
                        if (1 === t.nodeType || o) {
                            return e(t, n, r)
                        }
                    }
                } : function(t, n, s) {
                    var u, l, c, p = N + " " + a;
                    if (s) {
                        while (t = t[i]) {
                            if ((1 === t.nodeType || o) && e(t, n, s)) {
                                return !0
                            }
                        }
                    } else {
                        while (t = t[i]) {
                            if (1 === t.nodeType || o) {
                                if (c = t[x] || (t[x] = {}), (l = c[i]) && l[0] === p) {
                                    if ((u = l[1]) === !0 || u === r) {
                                        return u === !0
                                    }
                                } else {
                                    if (l = c[i] = [p], l[1] = e(t, n, s) || r, l[1] === !0) {
                                        return !0
                                    }
                                }
                            }
                        }
                    }
                }
            }

            function gt(e) {
                return e.length > 1 ? function(t, n, r) {
                    var i = e.length;
                    while (i--) {
                        if (!e[i](t, n, r)) {
                            return !1
                        }
                    }
                    return !0
                } : e[0]
            }

            function mt(e, t, n, r, i) {
                var o, a = [],
                    s = 0,
                    u = e.length,
                    l = null != t;
                for (; u > s; s++) {
                    (o = e[s]) && (!n || n(o, r, i)) && (a.push(o), l && t.push(s))
                }
                return a
            }

            function yt(e, t, n, r, i, o) {
                return r && !r[x] && (r = yt(r)), i && !i[x] && (i = yt(i, o)), ot(function(o, a, s, u) {
                    var l, c, p, f = [],
                        d = [],
                        h = a.length,
                        g = o || xt(t || "*", s.nodeType ? [s] : s, []),
                        m = !e || !o && t ? g : mt(g, f, e, s, u),
                        y = n ? i || (o ? e : h || r) ? [] : a : m;
                    if (n && n(m, y, s, u), r) {
                        l = mt(y, d), r(l, [], s, u), c = l.length;
                        while (c--) {
                            (p = l[c]) && (y[d[c]] = !(m[d[c]] = p))
                        }
                    }
                    if (o) {
                        if (i || e) {
                            if (i) {
                                l = [], c = y.length;
                                while (c--) {
                                    (p = y[c]) && l.push(m[c] = p)
                                }
                                i(null, y = [], l, u)
                            }
                            c = y.length;
                            while (c--) {
                                (p = y[c]) && (l = i ? M.call(o, p) : f[c]) > -1 && (o[l] = !(a[l] = p))
                            }
                        }
                    } else {
                        y = mt(y === a ? y.splice(h, y.length) : y), i ? i(null, a, y, u) : H.apply(a, y)
                    }
                })
            }

            function vt(e) {
                var t, n, r, o = e.length,
                    a = i.relative[e[0].type],
                    s = a || i.relative[" "],
                    u = a ? 1 : 0,
                    c = ht(function(e) {
                        return e === t
                    }, s, !0),
                    p = ht(function(e) {
                        return M.call(t, e) > -1
                    }, s, !0),
                    f = [
                        function(e, n, r) {
                            return !a && (r || n !== l) || ((t = n).nodeType ? c(e, n, r) : p(e, n, r))
                        }
                    ];
                for (; o > u; u++) {
                    if (n = i.relative[e[u].type]) {
                        f = [ht(gt(f), n)]
                    } else {
                        if (n = i.filter[e[u].type].apply(null, e[u].matches), n[x]) {
                            for (r = ++u; o > r; r++) {
                                if (i.relative[e[r].type]) {
                                    break
                                }
                            }
                            return yt(u > 1 && gt(f), u > 1 && dt(e.slice(0, u - 1)).replace(W, "$1"), n, r > u && vt(e.slice(u, r)), o > r && vt(e = e.slice(r)), o > r && dt(e))
                        }
                        f.push(n)
                    }
                }
                return gt(f)
            }

            function bt(e, t) {
                var n = 0,
                    o = t.length > 0,
                    a = e.length > 0,
                    s = function(s, u, c, f, d) {
                        var h, g, m, y = [],
                            v = 0,
                            b = "0",
                            x = s && [],
                            w = null != d,
                            T = l,
                            C = s || a && i.find.TAG("*", d && u.parentNode || u),
                            k = N += null == T ? 1 : Math.random() || 0.1;
                        for (w && (l = u !== p && u, r = n); null != (h = C[b]); b++) {
                            if (a && h) {
                                g = 0;
                                while (m = e[g++]) {
                                    if (m(h, u, c)) {
                                        f.push(h);
                                        break
                                    }
                                }
                                w && (N = k, r = ++n)
                            }
                            o && ((h = !m && h) && v--, s && x.push(h))
                        }
                        if (v += b, o && b !== v) {
                            g = 0;
                            while (m = t[g++]) {
                                m(x, y, u, c)
                            }
                            if (s) {
                                if (v > 0) {
                                    while (b--) {
                                        x[b] || y[b] || (y[b] = L.call(f))
                                    }
                                }
                                y = mt(y)
                            }
                            H.apply(f, y), w && !s && y.length > 0 && v + t.length > 1 && st.uniqueSort(f)
                        }
                        return w && (N = k, l = T), x
                    };
                return o ? ot(s) : s
            }
            s = st.compile = function(e, t) {
                var n, r = [],
                    i = [],
                    o = S[e + " "];
                if (!o) {
                    t || (t = ft(e)), n = t.length;
                    while (n--) {
                        o = vt(t[n]), o[x] ? r.push(o) : i.push(o)
                    }
                    o = S(e, bt(i, r))
                }
                return o
            };

            function xt(e, t, n) {
                var r = 0,
                    i = t.length;
                for (; i > r; r++) {
                    st(e, t[r], n)
                }
                return n
            }

            function wt(e, t, n, r) {
                var o, a, u, l, c, p = ft(e);
                if (!r && 1 === p.length) {
                    if (a = p[0] = p[0].slice(0), a.length > 2 && "ID" === (u = a[0]).type && 9 === t.nodeType && !d && i.relative[a[1].type]) {
                        if (t = i.find.ID(u.matches[0].replace(et, tt), t)[0], !t) {
                            return n
                        }
                        e = e.slice(a.shift().value.length)
                    }
                    o = U.needsContext.test(e) ? 0 : a.length;
                    while (o--) {
                        if (u = a[o], i.relative[l = u.type]) {
                            break
                        }
                        if ((c = i.find[l]) && (r = c(u.matches[0].replace(et, tt), V.test(a[0].type) && t.parentNode || t))) {
                            if (a.splice(o, 1), e = r.length && dt(a), !e) {
                                return H.apply(n, q.call(r, 0)), n
                            }
                            break
                        }
                    }
                }
                return s(e, p)(r, t, d, n, V.test(e)), n
            }
            i.pseudos.nth = i.pseudos.eq;

            function Tt() {}
            i.filters = Tt.prototype = i.pseudos, i.setFilters = new Tt, c(), st.attr = b.attr, b.find = st, b.expr = st.selectors, b.expr[":"] = b.expr.pseudos, b.unique = st.uniqueSort, b.text = st.getText, b.isXMLDoc = st.isXML, b.contains = st.contains
        }(e);
    var at = /Until$/,
        st = /^(?:parents|prev(?:Until|All))/,
        ut = /^.[^:#\[\.,]*$/,
        lt = b.expr.match.needsContext,
        ct = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    b.fn.extend({
        find: function(e) {
            var t, n, r, i = this.length;
            if ("string" != typeof e) {
                return r = this, this.pushStack(b(e).filter(function() {
                    for (t = 0; i > t; t++) {
                        if (b.contains(r[t], this)) {
                            return !0
                        }
                    }
                }))
            }
            for (n = [], t = 0; i > t; t++) {
                b.find(e, this[t], n)
            }
            return n = this.pushStack(i > 1 ? b.unique(n) : n), n.selector = (this.selector ? this.selector + " " : "") + e, n
        },
        has: function(e) {
            var t, n = b(e, this),
                r = n.length;
            return this.filter(function() {
                for (t = 0; r > t; t++) {
                    if (b.contains(this, n[t])) {
                        return !0
                    }
                }
            })
        },
        not: function(e) {
            return this.pushStack(ft(this, e, !1))
        },
        filter: function(e) {
            return this.pushStack(ft(this, e, !0))
        },
        is: function(e) {
            return !!e && ("string" == typeof e ? lt.test(e) ? b(e, this.context).index(this[0]) >= 0 : b.filter(e, this).length > 0 : this.filter(e).length > 0)
        },
        closest: function(e, t) {
            var n, r = 0,
                i = this.length,
                o = [],
                a = lt.test(e) || "string" != typeof e ? b(e, t || this.context) : 0;
            for (; i > r; r++) {
                n = this[r];
                while (n && n.ownerDocument && n !== t && 11 !== n.nodeType) {
                    if (a ? a.index(n) > -1 : b.find.matchesSelector(n, e)) {
                        o.push(n);
                        break
                    }
                    n = n.parentNode
                }
            }
            return this.pushStack(o.length > 1 ? b.unique(o) : o)
        },
        index: function(e) {
            return e ? "string" == typeof e ? b.inArray(this[0], b(e)) : b.inArray(e.jquery ? e[0] : e, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(e, t) {
            var n = "string" == typeof e ? b(e, t) : b.makeArray(e && e.nodeType ? [e] : e),
                r = b.merge(this.get(), n);
            return this.pushStack(b.unique(r))
        },
        addBack: function(e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }), b.fn.andSelf = b.fn.addBack;

    function pt(e, t) {
        do {
            e = e[t]
        } while (e && 1 !== e.nodeType);
        return e
    }
    b.each({
        parent: function(e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        },
        parents: function(e) {
            return b.dir(e, "parentNode")
        },
        parentsUntil: function(e, t, n) {
            return b.dir(e, "parentNode", n)
        },
        next: function(e) {
            return pt(e, "nextSibling")
        },
        prev: function(e) {
            return pt(e, "previousSibling")
        },
        nextAll: function(e) {
            return b.dir(e, "nextSibling")
        },
        prevAll: function(e) {
            return b.dir(e, "previousSibling")
        },
        nextUntil: function(e, t, n) {
            return b.dir(e, "nextSibling", n)
        },
        prevUntil: function(e, t, n) {
            return b.dir(e, "previousSibling", n)
        },
        siblings: function(e) {
            return b.sibling((e.parentNode || {}).firstChild, e)
        },
        children: function(e) {
            return b.sibling(e.firstChild)
        },
        contents: function(e) {
            return b.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : b.merge([], e.childNodes)
        }
    }, function(e, t) {
        b.fn[e] = function(n, r) {
            var i = b.map(this, t, n);
            return at.test(e) || (r = n), r && "string" == typeof r && (i = b.filter(r, i)), i = this.length > 1 && !ct[e] ? b.unique(i) : i, this.length > 1 && st.test(e) && (i = i.reverse()), this.pushStack(i)
        }
    }), b.extend({
        filter: function(e, t, n) {
            return n && (e = ":not(" + e + ")"), 1 === t.length ? b.find.matchesSelector(t[0], e) ? [t[0]] : [] : b.find.matches(e, t)
        },
        dir: function(e, n, r) {
            var i = [],
                o = e[n];
            while (o && 9 !== o.nodeType && (r === t || 1 !== o.nodeType || !b(o).is(r))) {
                1 === o.nodeType && i.push(o), o = o[n]
            }
            return i
        },
        sibling: function(e, t) {
            var n = [];
            for (; e; e = e.nextSibling) {
                1 === e.nodeType && e !== t && n.push(e)
            }
            return n
        }
    });

    function ft(e, t, n) {
        if (t = t || 0, b.isFunction(t)) {
            return b.grep(e, function(e, r) {
                var i = !! t.call(e, r, e);
                return i === n
            })
        }
        if (t.nodeType) {
            return b.grep(e, function(e) {
                return e === t === n
            })
        }
        if ("string" == typeof t) {
            var r = b.grep(e, function(e) {
                return 1 === e.nodeType
            });
            if (ut.test(t)) {
                return b.filter(t, r, !n)
            }
            t = b.filter(t, r)
        }
        return b.grep(e, function(e) {
            return b.inArray(e, t) >= 0 === n
        })
    }

    function dt(e) {
        var t = ht.split("|"),
            n = e.createDocumentFragment();
        if (n.createElement) {
            while (t.length) {
                n.createElement(t.pop())
            }
        }
        return n
    }
    var ht = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
        gt = / jQuery\d+="(?:null|\d+)"/g,
        mt = RegExp("<(?:" + ht + ")[\\s/>]", "i"),
        yt = /^\s+/,
        vt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
        bt = /<([\w:]+)/,
        xt = /<tbody/i,
        wt = /<|&#?\w+;/,
        Tt = /<(?:script|style|link)/i,
        Nt = /^(?:checkbox|radio)$/i,
        Ct = /checked\s*(?:[^=]|=\s*.checked.)/i,
        kt = /^$|\/(?:java|ecma)script/i,
        Et = /^true\/(.*)/,
        St = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
        At = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            legend: [1, "<fieldset>", "</fieldset>"],
            area: [1, "<map>", "</map>"],
            param: [1, "<object>", "</object>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: b.support.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
        }, jt = dt(o),
        Dt = jt.appendChild(o.createElement("div"));
    At.optgroup = At.option, At.tbody = At.tfoot = At.colgroup = At.caption = At.thead, At.th = At.td, b.fn.extend({
        text: function(e) {
            return b.access(this, function(e) {
                return e === t ? b.text(this) : this.empty().append((this[0] && this[0].ownerDocument || o).createTextNode(e))
            }, null, e, arguments.length)
        },
        wrapAll: function(e) {
            if (b.isFunction(e)) {
                return this.each(function(t) {
                    b(this).wrapAll(e.call(this, t))
                })
            }
            if (this[0]) {
                var t = b(e, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                    var e = this;
                    while (e.firstChild && 1 === e.firstChild.nodeType) {
                        e = e.firstChild
                    }
                    return e
                }).append(this)
            }
            return this
        },
        wrapInner: function(e) {
            return b.isFunction(e) ? this.each(function(t) {
                b(this).wrapInner(e.call(this, t))
            }) : this.each(function() {
                var t = b(this),
                    n = t.contents();
                n.length ? n.wrapAll(e) : t.append(e)
            })
        },
        wrap: function(e) {
            var t = b.isFunction(e);
            return this.each(function(n) {
                b(this).wrapAll(t ? e.call(this, n) : e)
            })
        },
        unwrap: function() {
            return this.parent().each(function() {
                b.nodeName(this, "body") || b(this).replaceWith(this.childNodes)
            }).end()
        },
        append: function() {
            return this.domManip(arguments, !0, function(e) {
                (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) && this.appendChild(e)
            })
        },
        prepend: function() {
            return this.domManip(arguments, !0, function(e) {
                (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) && this.insertBefore(e, this.firstChild)
            })
        },
        before: function() {
            return this.domManip(arguments, !1, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        },
        after: function() {
            return this.domManip(arguments, !1, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        },
        remove: function(e, t) {
            var n, r = 0;
            for (; null != (n = this[r]); r++) {
                (!e || b.filter(e, [n]).length > 0) && (t || 1 !== n.nodeType || b.cleanData(Ot(n)), n.parentNode && (t && b.contains(n.ownerDocument, n) && Mt(Ot(n, "script")), n.parentNode.removeChild(n)))
            }
            return this
        },
        empty: function() {
            var e, t = 0;
            for (; null != (e = this[t]); t++) {
                1 === e.nodeType && b.cleanData(Ot(e, !1));
                while (e.firstChild) {
                    e.removeChild(e.firstChild)
                }
                e.options && b.nodeName(e, "select") && (e.options.length = 0)
            }
            return this
        },
        clone: function(e, t) {
            return e = null == e ? !1 : e, t = null == t ? e : t, this.map(function() {
                return b.clone(this, e, t)
            })
        },
        html: function(e) {
            return b.access(this, function(e) {
                var n = this[0] || {}, r = 0,
                    i = this.length;
                if (e === t) {
                    return 1 === n.nodeType ? n.innerHTML.replace(gt, "") : t
                }
                if (!("string" != typeof e || Tt.test(e) || !b.support.htmlSerialize && mt.test(e) || !b.support.leadingWhitespace && yt.test(e) || At[(bt.exec(e) || ["", ""])[1].toLowerCase()])) {
                    e = e.replace(vt, "<$1></$2>");
                    try {
                        for (; i > r; r++) {
                            n = this[r] || {}, 1 === n.nodeType && (b.cleanData(Ot(n, !1)), n.innerHTML = e)
                        }
                        n = 0
                    } catch (o) {}
                }
                n && this.empty().append(e)
            }, null, e, arguments.length)
        },
        replaceWith: function(e) {
            var t = b.isFunction(e);
            return t || "string" == typeof e || (e = b(e).not(this).detach()), this.domManip([e], !0, function(e) {
                var t = this.nextSibling,
                    n = this.parentNode;
                n && (b(this).remove(), n.insertBefore(e, t))
            })
        },
        detach: function(e) {
            return this.remove(e, !0)
        },
        domManip: function(e, n, r) {
            e = f.apply([], e);
            var i, o, a, s, u, l, c = 0,
                p = this.length,
                d = this,
                h = p - 1,
                g = e[0],
                m = b.isFunction(g);
            if (m || !(1 >= p || "string" != typeof g || b.support.checkClone) && Ct.test(g)) {
                return this.each(function(i) {
                    var o = d.eq(i);
                    m && (e[0] = g.call(this, i, n ? o.html() : t)), o.domManip(e, n, r)
                })
            }
            if (p && (l = b.buildFragment(e, this[0].ownerDocument, !1, this), i = l.firstChild, 1 === l.childNodes.length && (l = i), i)) {
                for (n = n && b.nodeName(i, "tr"), s = b.map(Ot(l, "script"), Ht), a = s.length; p > c; c++) {
                    o = l, c !== h && (o = b.clone(o, !0, !0), a && b.merge(s, Ot(o, "script"))), r.call(n && b.nodeName(this[c], "table") ? Lt(this[c], "tbody") : this[c], o, c)
                }
                if (a) {
                    for (u = s[s.length - 1].ownerDocument, b.map(s, qt), c = 0; a > c; c++) {
                        o = s[c], kt.test(o.type || "") && !b._data(o, "globalEval") && b.contains(u, o) && (o.src ? b.ajax({
                            url: o.src,
                            type: "GET",
                            dataType: "script",
                            async: !1,
                            global: !1,
                            "throws": !0
                        }) : b.globalEval((o.text || o.textContent || o.innerHTML || "").replace(St, "")))
                    }
                }
                l = i = null
            }
            return this
        }
    });

    function Lt(e, t) {
        return e.getElementsByTagName(t)[0] || e.appendChild(e.ownerDocument.createElement(t))
    }

    function Ht(e) {
        var t = e.getAttributeNode("type");
        return e.type = (t && t.specified) + "/" + e.type, e
    }

    function qt(e) {
        var t = Et.exec(e.type);
        return t ? e.type = t[1] : e.removeAttribute("type"), e
    }

    function Mt(e, t) {
        var n, r = 0;
        for (; null != (n = e[r]); r++) {
            b._data(n, "globalEval", !t || b._data(t[r], "globalEval"))
        }
    }

    function _t(e, t) {
        if (1 === t.nodeType && b.hasData(e)) {
            var n, r, i, o = b._data(e),
                a = b._data(t, o),
                s = o.events;
            if (s) {
                delete a.handle, a.events = {};
                for (n in s) {
                    for (r = 0, i = s[n].length; i > r; r++) {
                        b.event.add(t, n, s[n][r])
                    }
                }
            }
            a.data && (a.data = b.extend({}, a.data))
        }
    }

    function Ft(e, t) {
        var n, r, i;
        if (1 === t.nodeType) {
            if (n = t.nodeName.toLowerCase(), !b.support.noCloneEvent && t[b.expando]) {
                i = b._data(t);
                for (r in i.events) {
                    b.removeEvent(t, r, i.handle)
                }
                t.removeAttribute(b.expando)
            }
            "script" === n && t.text !== e.text ? (Ht(t).text = e.text, qt(t)) : "object" === n ? (t.parentNode && (t.outerHTML = e.outerHTML), b.support.html5Clone && e.innerHTML && !b.trim(t.innerHTML) && (t.innerHTML = e.innerHTML)) : "input" === n && Nt.test(e.type) ? (t.defaultChecked = t.checked = e.checked, t.value !== e.value && (t.value = e.value)) : "option" === n ? t.defaultSelected = t.selected = e.defaultSelected : ("input" === n || "textarea" === n) && (t.defaultValue = e.defaultValue)
        }
    }
    b.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(e, t) {
        b.fn[e] = function(e) {
            var n, r = 0,
                i = [],
                o = b(e),
                a = o.length - 1;
            for (; a >= r; r++) {
                n = r === a ? this : this.clone(!0), b(o[r])[t](n), d.apply(i, n.get())
            }
            return this.pushStack(i)
        }
    });

    function Ot(e, n) {
        var r, o, a = 0,
            s = typeof e.getElementsByTagName !== i ? e.getElementsByTagName(n || "*") : typeof e.querySelectorAll !== i ? e.querySelectorAll(n || "*") : t;
        if (!s) {
            for (s = [], r = e.childNodes || e; null != (o = r[a]); a++) {
                !n || b.nodeName(o, n) ? s.push(o) : b.merge(s, Ot(o, n))
            }
        }
        return n === t || n && b.nodeName(e, n) ? b.merge([e], s) : s
    }

    function Bt(e) {
        Nt.test(e.type) && (e.defaultChecked = e.checked)
    }
    b.extend({
        clone: function(e, t, n) {
            var r, i, o, a, s, u = b.contains(e.ownerDocument, e);
            if (b.support.html5Clone || b.isXMLDoc(e) || !mt.test("<" + e.nodeName + ">") ? o = e.cloneNode(!0) : (Dt.innerHTML = e.outerHTML, Dt.removeChild(o = Dt.firstChild)), !(b.support.noCloneEvent && b.support.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || b.isXMLDoc(e))) {
                for (r = Ot(o), s = Ot(e), a = 0; null != (i = s[a]); ++a) {
                    r[a] && Ft(i, r[a])
                }
            }
            if (t) {
                if (n) {
                    for (s = s || Ot(e), r = r || Ot(o), a = 0; null != (i = s[a]); a++) {
                        _t(i, r[a])
                    }
                } else {
                    _t(e, o)
                }
            }
            return r = Ot(o, "script"), r.length > 0 && Mt(r, !u && Ot(e, "script")), r = s = i = null, o
        },
        buildFragment: function(e, t, n, r) {
            var i, o, a, s, u, l, c, p = e.length,
                f = dt(t),
                d = [],
                h = 0;
            for (; p > h; h++) {
                if (o = e[h], o || 0 === o) {
                    if ("object" === b.type(o)) {
                        b.merge(d, o.nodeType ? [o] : o)
                    } else {
                        if (wt.test(o)) {
                            s = s || f.appendChild(t.createElement("div")), u = (bt.exec(o) || ["", ""])[1].toLowerCase(), c = At[u] || At._default, s.innerHTML = c[1] + o.replace(vt, "<$1></$2>") + c[2], i = c[0];
                            while (i--) {
                                s = s.lastChild
                            }
                            if (!b.support.leadingWhitespace && yt.test(o) && d.push(t.createTextNode(yt.exec(o)[0])), !b.support.tbody) {
                                o = "table" !== u || xt.test(o) ? "<table>" !== c[1] || xt.test(o) ? 0 : s : s.firstChild, i = o && o.childNodes.length;
                                while (i--) {
                                    b.nodeName(l = o.childNodes[i], "tbody") && !l.childNodes.length && o.removeChild(l)
                                }
                            }
                            b.merge(d, s.childNodes), s.textContent = "";
                            while (s.firstChild) {
                                s.removeChild(s.firstChild)
                            }
                            s = f.lastChild
                        } else {
                            d.push(t.createTextNode(o))
                        }
                    }
                }
            }
            s && f.removeChild(s), b.support.appendChecked || b.grep(Ot(d, "input"), Bt), h = 0;
            while (o = d[h++]) {
                if ((!r || -1 === b.inArray(o, r)) && (a = b.contains(o.ownerDocument, o), s = Ot(f.appendChild(o), "script"), a && Mt(s), n)) {
                    i = 0;
                    while (o = s[i++]) {
                        kt.test(o.type || "") && n.push(o)
                    }
                }
            }
            return s = null, f
        },
        cleanData: function(e, t) {
            var n, r, o, a, s = 0,
                u = b.expando,
                l = b.cache,
                p = b.support.deleteExpando,
                f = b.event.special;
            for (; null != (n = e[s]); s++) {
                if ((t || b.acceptData(n)) && (o = n[u], a = o && l[o])) {
                    if (a.events) {
                        for (r in a.events) {
                            f[r] ? b.event.remove(n, r) : b.removeEvent(n, r, a.handle)
                        }
                    }
                    l[o] && (delete l[o], p ? delete n[u] : typeof n.removeAttribute !== i ? n.removeAttribute(u) : n[u] = null, c.push(o))
                }
            }
        }
    });
    var Pt, Rt, Wt, $t = /alpha\([^)]*\)/i,
        It = /opacity\s*=\s*([^)]*)/,
        zt = /^(top|right|bottom|left)$/,
        Xt = /^(none|table(?!-c[ea]).+)/,
        Ut = /^margin/,
        Vt = RegExp("^(" + x + ")(.*)$", "i"),
        Yt = RegExp("^(" + x + ")(?!px)[a-z%]+$", "i"),
        Jt = RegExp("^([+-])=(" + x + ")", "i"),
        Gt = {
            BODY: "block"
        }, Qt = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        }, Kt = {
            letterSpacing: 0,
            fontWeight: 400
        }, Zt = ["Top", "Right", "Bottom", "Left"],
        en = ["Webkit", "O", "Moz", "ms"];

    function tn(e, t) {
        if (t in e) {
            return t
        }
        var n = t.charAt(0).toUpperCase() + t.slice(1),
            r = t,
            i = en.length;
        while (i--) {
            if (t = en[i] + n, t in e) {
                return t
            }
        }
        return r
    }

    function nn(e, t) {
        return e = t || e, "none" === b.css(e, "display") || !b.contains(e.ownerDocument, e)
    }

    function rn(e, t) {
        var n, r, i, o = [],
            a = 0,
            s = e.length;
        for (; s > a; a++) {
            r = e[a], r.style && (o[a] = b._data(r, "olddisplay"), n = r.style.display, t ? (o[a] || "none" !== n || (r.style.display = ""), "" === r.style.display && nn(r) && (o[a] = b._data(r, "olddisplay", un(r.nodeName)))) : o[a] || (i = nn(r), (n && "none" !== n || !i) && b._data(r, "olddisplay", i ? n : b.css(r, "display"))))
        }
        for (a = 0; s > a; a++) {
            r = e[a], r.style && (t && "none" !== r.style.display && "" !== r.style.display || (r.style.display = t ? o[a] || "" : "none"))
        }
        return e
    }
    b.fn.extend({
        css: function(e, n) {
            return b.access(this, function(e, n, r) {
                var i, o, a = {}, s = 0;
                if (b.isArray(n)) {
                    for (o = Rt(e), i = n.length; i > s; s++) {
                        a[n[s]] = b.css(e, n[s], !1, o)
                    }
                    return a
                }
                return r !== t ? b.style(e, n, r) : b.css(e, n)
            }, e, n, arguments.length > 1)
        },
        show: function() {
            return rn(this, !0)
        },
        hide: function() {
            return rn(this)
        },
        toggle: function(e) {
            var t = "boolean" == typeof e;
            return this.each(function() {
                (t ? e : nn(this)) ? b(this).show() : b(this).hide()
            })
        }
    }), b.extend({
        cssHooks: {
            opacity: {
                get: function(e, t) {
                    if (t) {
                        var n = Wt(e, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": b.support.cssFloat ? "cssFloat" : "styleFloat"
        },
        style: function(e, n, r, i) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var o, a, s, u = b.camelCase(n),
                    l = e.style;
                if (n = b.cssProps[u] || (b.cssProps[u] = tn(l, u)), s = b.cssHooks[n] || b.cssHooks[u], r === t) {
                    return s && "get" in s && (o = s.get(e, !1, i)) !== t ? o : l[n]
                }
                if (a = typeof r, "string" === a && (o = Jt.exec(r)) && (r = (o[1] + 1) * o[2] + parseFloat(b.css(e, n)), a = "number"), !(null == r || "number" === a && isNaN(r) || ("number" !== a || b.cssNumber[u] || (r += "px"), b.support.clearCloneStyle || "" !== r || 0 !== n.indexOf("background") || (l[n] = "inherit"), s && "set" in s && (r = s.set(e, r, i)) === t))) {
                    try {
                        l[n] = r
                    } catch (c) {}
                }
            }
        },
        css: function(e, n, r, i) {
            var o, a, s, u = b.camelCase(n);
            return n = b.cssProps[u] || (b.cssProps[u] = tn(e.style, u)), s = b.cssHooks[n] || b.cssHooks[u], s && "get" in s && (a = s.get(e, !0, r)), a === t && (a = Wt(e, n, i)), "normal" === a && n in Kt && (a = Kt[n]), "" === r || r ? (o = parseFloat(a), r === !0 || b.isNumeric(o) ? o || 0 : a) : a
        },
        swap: function(e, t, n, r) {
            var i, o, a = {};
            for (o in t) {
                a[o] = e.style[o], e.style[o] = t[o]
            }
            i = n.apply(e, r || []);
            for (o in t) {
                e.style[o] = a[o]
            }
            return i
        }
    }), e.getComputedStyle ? (Rt = function(t) {
        return e.getComputedStyle(t, null)
    }, Wt = function(e, n, r) {
        var i, o, a, s = r || Rt(e),
            u = s ? s.getPropertyValue(n) || s[n] : t,
            l = e.style;
        return s && ("" !== u || b.contains(e.ownerDocument, e) || (u = b.style(e, n)), Yt.test(u) && Ut.test(n) && (i = l.width, o = l.minWidth, a = l.maxWidth, l.minWidth = l.maxWidth = l.width = u, u = s.width, l.width = i, l.minWidth = o, l.maxWidth = a)), u
    }) : o.documentElement.currentStyle && (Rt = function(e) {
        return e.currentStyle
    }, Wt = function(e, n, r) {
        var i, o, a, s = r || Rt(e),
            u = s ? s[n] : t,
            l = e.style;
        return null == u && l && l[n] && (u = l[n]), Yt.test(u) && !zt.test(n) && (i = l.left, o = e.runtimeStyle, a = o && o.left, a && (o.left = e.currentStyle.left), l.left = "fontSize" === n ? "1em" : u, u = l.pixelLeft + "px", l.left = i, a && (o.left = a)), "" === u ? "auto" : u
    });

    function on(e, t, n) {
        var r = Vt.exec(t);
        return r ? Math.max(0, r[1] - (n || 0)) + (r[2] || "px") : t
    }

    function an(e, t, n, r, i) {
        var o = n === (r ? "border" : "content") ? 4 : "width" === t ? 1 : 0,
            a = 0;
        for (; 4 > o; o += 2) {
            "margin" === n && (a += b.css(e, n + Zt[o], !0, i)), r ? ("content" === n && (a -= b.css(e, "padding" + Zt[o], !0, i)), "margin" !== n && (a -= b.css(e, "border" + Zt[o] + "Width", !0, i))) : (a += b.css(e, "padding" + Zt[o], !0, i), "padding" !== n && (a += b.css(e, "border" + Zt[o] + "Width", !0, i)))
        }
        return a
    }

    function sn(e, t, n) {
        var r = !0,
            i = "width" === t ? e.offsetWidth : e.offsetHeight,
            o = Rt(e),
            a = b.support.boxSizing && "border-box" === b.css(e, "boxSizing", !1, o);
        if (0 >= i || null == i) {
            if (i = Wt(e, t, o), (0 > i || null == i) && (i = e.style[t]), Yt.test(i)) {
                return i
            }
            r = a && (b.support.boxSizingReliable || i === e.style[t]), i = parseFloat(i) || 0
        }
        return i + an(e, t, n || (a ? "border" : "content"), r, o) + "px"
    }

    function un(e) {
        var t = o,
            n = Gt[e];
        return n || (n = ln(e, t), "none" !== n && n || (Pt = (Pt || b("<iframe frameborder='0' width='0' height='0'/>").css("cssText", "display:block !important")).appendTo(t.documentElement), t = (Pt[0].contentWindow || Pt[0].contentDocument).document, t.write("<!doctype html><html><body>"), t.close(), n = ln(e, t), Pt.detach()), Gt[e] = n), n
    }

    function ln(e, t) {
        var n = b(t.createElement(e)).appendTo(t.body),
            r = b.css(n[0], "display");
        return n.remove(), r
    }
    b.each(["height", "width"], function(e, n) {
        b.cssHooks[n] = {
            get: function(e, r, i) {
                return r ? 0 === e.offsetWidth && Xt.test(b.css(e, "display")) ? b.swap(e, Qt, function() {
                    return sn(e, n, i)
                }) : sn(e, n, i) : t
            },
            set: function(e, t, r) {
                var i = r && Rt(e);
                return on(e, t, r ? an(e, n, r, b.support.boxSizing && "border-box" === b.css(e, "boxSizing", !1, i), i) : 0)
            }
        }
    }), b.support.opacity || (b.cssHooks.opacity = {
        get: function(e, t) {
            return It.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? 0.01 * parseFloat(RegExp.$1) + "" : t ? "1" : ""
        },
        set: function(e, t) {
            var n = e.style,
                r = e.currentStyle,
                i = b.isNumeric(t) ? "alpha(opacity=" + 100 * t + ")" : "",
                o = r && r.filter || n.filter || "";
            n.zoom = 1, (t >= 1 || "" === t) && "" === b.trim(o.replace($t, "")) && n.removeAttribute && (n.removeAttribute("filter"), "" === t || r && !r.filter) || (n.filter = $t.test(o) ? o.replace($t, i) : o + " " + i)
        }
    }), b(function() {
        b.support.reliableMarginRight || (b.cssHooks.marginRight = {
            get: function(e, n) {
                return n ? b.swap(e, {
                    display: "inline-block"
                }, Wt, [e, "marginRight"]) : t
            }
        }), !b.support.pixelPosition && b.fn.position && b.each(["top", "left"], function(e, n) {
            b.cssHooks[n] = {
                get: function(e, r) {
                    return r ? (r = Wt(e, n), Yt.test(r) ? b(e).position()[n] + "px" : r) : t
                }
            }
        })
    }), b.expr && b.expr.filters && (b.expr.filters.hidden = function(e) {
        return 0 >= e.offsetWidth && 0 >= e.offsetHeight || !b.support.reliableHiddenOffsets && "none" === (e.style && e.style.display || b.css(e, "display"))
    }, b.expr.filters.visible = function(e) {
        return !b.expr.filters.hidden(e)
    }), b.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(e, t) {
        b.cssHooks[e + t] = {
            expand: function(n) {
                var r = 0,
                    i = {}, o = "string" == typeof n ? n.split(" ") : [n];
                for (; 4 > r; r++) {
                    i[e + Zt[r] + t] = o[r] || o[r - 2] || o[0]
                }
                return i
            }
        }, Ut.test(e) || (b.cssHooks[e + t].set = on)
    });
    var cn = /%20/g,
        pn = /\[\]$/,
        fn = /\r?\n/g,
        dn = /^(?:submit|button|image|reset|file)$/i,
        hn = /^(?:input|select|textarea|keygen)/i;
    b.fn.extend({
        serialize: function() {
            return b.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var e = b.prop(this, "elements");
                return e ? b.makeArray(e) : this
            }).filter(function() {
                var e = this.type;
                return this.name && !b(this).is(":disabled") && hn.test(this.nodeName) && !dn.test(e) && (this.checked || !Nt.test(e))
            }).map(function(e, t) {
                var n = b(this).val();
                return null == n ? null : b.isArray(n) ? b.map(n, function(e) {
                    return {
                        name: t.name,
                        value: e.replace(fn, "\r\n")
                    }
                }) : {
                    name: t.name,
                    value: n.replace(fn, "\r\n")
                }
            }).get()
        }
    }), b.param = function(e, n) {
        var r, i = [],
            o = function(e, t) {
                t = b.isFunction(t) ? t() : null == t ? "" : t, i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
            };
        if (n === t && (n = b.ajaxSettings && b.ajaxSettings.traditional), b.isArray(e) || e.jquery && !b.isPlainObject(e)) {
            b.each(e, function() {
                o(this.name, this.value)
            })
        } else {
            for (r in e) {
                gn(r, e[r], n, o)
            }
        }
        return i.join("&").replace(cn, "+")
    };

    function gn(e, t, n, r) {
        var i;
        if (b.isArray(t)) {
            b.each(t, function(t, i) {
                n || pn.test(e) ? r(e, i) : gn(e + "[" + ("object" == typeof i ? t : "") + "]", i, n, r)
            })
        } else {
            if (n || "object" !== b.type(t)) {
                r(e, t)
            } else {
                for (i in t) {
                    gn(e + "[" + i + "]", t[i], n, r)
                }
            }
        }
    }
    b.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(e, t) {
        b.fn[t] = function(e, n) {
            return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
        }
    }), b.fn.hover = function(e, t) {
        return this.mouseenter(e).mouseleave(t || e)
    };
    var mn, yn, vn = b.now(),
        bn = /\?/,
        xn = /#.*$/,
        wn = /([?&])_=[^&]*/,
        Tn = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
        Nn = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        Cn = /^(?:GET|HEAD)$/,
        kn = /^\/\//,
        En = /^([\w.+-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,
        Sn = b.fn.load,
        An = {}, jn = {}, Dn = "*/".concat("*");
    try {
        yn = a.href
    } catch (Ln) {
        yn = o.createElement("a"), yn.href = "", yn = yn.href
    }
    mn = En.exec(yn.toLowerCase()) || [];

    function Hn(e) {
        return function(t, n) {
            "string" != typeof t && (n = t, t = "*");
            var r, i = 0,
                o = t.toLowerCase().match(w) || [];
            if (b.isFunction(n)) {
                while (r = o[i++]) {
                    "+" === r[0] ? (r = r.slice(1) || "*", (e[r] = e[r] || []).unshift(n)) : (e[r] = e[r] || []).push(n)
                }
            }
        }
    }

    function qn(e, n, r, i) {
        var o = {}, a = e === jn;

        function s(u) {
            var l;
            return o[u] = !0, b.each(e[u] || [], function(e, u) {
                var c = u(n, r, i);
                return "string" != typeof c || a || o[c] ? a ? !(l = c) : t : (n.dataTypes.unshift(c), s(c), !1)
            }), l
        }
        return s(n.dataTypes[0]) || !o["*"] && s("*")
    }

    function Mn(e, n) {
        var r, i, o = b.ajaxSettings.flatOptions || {};
        for (i in n) {
            n[i] !== t && ((o[i] ? e : r || (r = {}))[i] = n[i])
        }
        return r && b.extend(!0, e, r), e
    }
    b.fn.load = function(e, n, r) {
        if ("string" != typeof e && Sn) {
            return Sn.apply(this, arguments)
        }
        var i, o, a, s = this,
            u = e.indexOf(" ");
        return u >= 0 && (i = e.slice(u, e.length), e = e.slice(0, u)), b.isFunction(n) ? (r = n, n = t) : n && "object" == typeof n && (a = "POST"), s.length > 0 && b.ajax({
            url: e,
            type: a,
            dataType: "html",
            data: n
        }).done(function(e) {
            o = arguments, s.html(i ? b("<div>").append(b.parseHTML(e)).find(i) : e)
        }).complete(r && function(e, t) {
            s.each(r, o || [e.responseText, t, e])
        }), this
    }, b.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
        b.fn[t] = function(e) {
            return this.on(t, e)
        }
    }), b.each(["get", "post"], function(e, n) {
        b[n] = function(e, r, i, o) {
            return b.isFunction(r) && (o = o || i, i = r, r = t), b.ajax({
                url: e,
                type: n,
                dataType: o,
                data: r,
                success: i
            })
        }
    }), b.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: yn,
            type: "GET",
            isLocal: Nn.test(mn[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Dn,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText"
            },
            converters: {
                "* text": e.String,
                "text html": !0,
                "text json": b.parseJSON,
                "text xml": b.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(e, t) {
            return t ? Mn(Mn(e, b.ajaxSettings), t) : Mn(b.ajaxSettings, e)
        },
        ajaxPrefilter: Hn(An),
        ajaxTransport: Hn(jn),
        ajax: function(e, n) {
            "object" == typeof e && (n = e, e = t), n = n || {};
            var r, i, o, a, s, u, l, c, p = b.ajaxSetup({}, n),
                f = p.context || p,
                d = p.context && (f.nodeType || f.jquery) ? b(f) : b.event,
                h = b.Deferred(),
                g = b.Callbacks("once memory"),
                m = p.statusCode || {}, y = {}, v = {}, x = 0,
                T = "canceled",
                N = {
                    readyState: 0,
                    getResponseHeader: function(e) {
                        var t;
                        if (2 === x) {
                            if (!c) {
                                c = {};
                                while (t = Tn.exec(a)) {
                                    c[t[1].toLowerCase()] = t[2]
                                }
                            }
                            t = c[e.toLowerCase()]
                        }
                        return null == t ? null : t
                    },
                    getAllResponseHeaders: function() {
                        return 2 === x ? a : null
                    },
                    setRequestHeader: function(e, t) {
                        var n = e.toLowerCase();
                        return x || (e = v[n] = v[n] || e, y[e] = t), this
                    },
                    overrideMimeType: function(e) {
                        return x || (p.mimeType = e), this
                    },
                    statusCode: function(e) {
                        var t;
                        if (e) {
                            if (2 > x) {
                                for (t in e) {
                                    m[t] = [m[t], e[t]]
                                }
                            } else {
                                N.always(e[N.status])
                            }
                        }
                        return this
                    },
                    abort: function(e) {
                        var t = e || T;
                        return l && l.abort(t), k(0, t), this
                    }
                };
            if (h.promise(N).complete = g.add, N.success = N.done, N.error = N.fail, p.url = ((e || p.url || yn) + "").replace(xn, "").replace(kn, mn[1] + "//"), p.type = n.method || n.type || p.method || p.type, p.dataTypes = b.trim(p.dataType || "*").toLowerCase().match(w) || [""], null == p.crossDomain && (r = En.exec(p.url.toLowerCase()), p.crossDomain = !(!r || r[1] === mn[1] && r[2] === mn[2] && (r[3] || ("http:" === r[1] ? 80 : 443)) == (mn[3] || ("http:" === mn[1] ? 80 : 443)))), p.data && p.processData && "string" != typeof p.data && (p.data = b.param(p.data, p.traditional)), qn(An, p, n, N), 2 === x) {
                return N
            }
            u = p.global, u && 0 === b.active++ && b.event.trigger("ajaxStart"), p.type = p.type.toUpperCase(), p.hasContent = !Cn.test(p.type), o = p.url, p.hasContent || (p.data && (o = p.url += (bn.test(o) ? "&" : "?") + p.data, delete p.data), p.cache === !1 && (p.url = wn.test(o) ? o.replace(wn, "$1_=" + vn++) : o + (bn.test(o) ? "&" : "?") + "_=" + vn++)), p.ifModified && (b.lastModified[o] && N.setRequestHeader("If-Modified-Since", b.lastModified[o]), b.etag[o] && N.setRequestHeader("If-None-Match", b.etag[o])), (p.data && p.hasContent && p.contentType !== !1 || n.contentType) && N.setRequestHeader("Content-Type", p.contentType), N.setRequestHeader("Accept", p.dataTypes[0] && p.accepts[p.dataTypes[0]] ? p.accepts[p.dataTypes[0]] + ("*" !== p.dataTypes[0] ? ", " + Dn + "; q=0.01" : "") : p.accepts["*"]);
            for (i in p.headers) {
                N.setRequestHeader(i, p.headers[i])
            }
            if (p.beforeSend && (p.beforeSend.call(f, N, p) === !1 || 2 === x)) {
                return N.abort()
            }
            T = "abort";
            for (i in {
                success: 1,
                error: 1,
                complete: 1
            }) {
                N[i](p[i])
            }
            if (l = qn(jn, p, n, N)) {
                N.readyState = 1, u && d.trigger("ajaxSend", [N, p]), p.async && p.timeout > 0 && (s = setTimeout(function() {
                    N.abort("timeout")
                }, p.timeout));
                try {
                    x = 1, l.send(y, k)
                } catch (C) {
                    if (!(2 > x)) {
                        throw C
                    }
                    k(-1, C)
                }
            } else {
                k(-1, "No Transport")
            }

            function k(e, n, r, i) {
                var c, y, v, w, T, C = n;
                2 !== x && (x = 2, s && clearTimeout(s), l = t, a = i || "", N.readyState = e > 0 ? 4 : 0, r && (w = _n(p, N, r)), e >= 200 && 300 > e || 304 === e ? (p.ifModified && (T = N.getResponseHeader("Last-Modified"), T && (b.lastModified[o] = T), T = N.getResponseHeader("etag"), T && (b.etag[o] = T)), 204 === e ? (c = !0, C = "nocontent") : 304 === e ? (c = !0, C = "notmodified") : (c = Fn(p, w), C = c.state, y = c.data, v = c.error, c = !v)) : (v = C, (e || !C) && (C = "error", 0 > e && (e = 0))), N.status = e, N.statusText = (n || C) + "", c ? h.resolveWith(f, [y, C, N]) : h.rejectWith(f, [N, C, v]), N.statusCode(m), m = t, u && d.trigger(c ? "ajaxSuccess" : "ajaxError", [N, p, c ? y : v]), g.fireWith(f, [N, C]), u && (d.trigger("ajaxComplete", [N, p]), --b.active || b.event.trigger("ajaxStop")))
            }
            return N
        },
        getScript: function(e, n) {
            return b.get(e, t, n, "script")
        },
        getJSON: function(e, t, n) {
            return b.get(e, t, n, "json")
        }
    });

    function _n(e, n, r) {
        var i, o, a, s, u = e.contents,
            l = e.dataTypes,
            c = e.responseFields;
        for (s in c) {
            s in r && (n[c[s]] = r[s])
        }
        while ("*" === l[0]) {
            l.shift(), o === t && (o = e.mimeType || n.getResponseHeader("Content-Type"))
        }
        if (o) {
            for (s in u) {
                if (u[s] && u[s].test(o)) {
                    l.unshift(s);
                    break
                }
            }
        }
        if (l[0] in r) {
            a = l[0]
        } else {
            for (s in r) {
                if (!l[0] || e.converters[s + " " + l[0]]) {
                    a = s;
                    break
                }
                i || (i = s)
            }
            a = a || i
        }
        return a ? (a !== l[0] && l.unshift(a), r[a]) : t
    }

    function Fn(e, t) {
        var n, r, i, o, a = {}, s = 0,
            u = e.dataTypes.slice(),
            l = u[0];
        if (e.dataFilter && (t = e.dataFilter(t, e.dataType)), u[1]) {
            for (i in e.converters) {
                a[i.toLowerCase()] = e.converters[i]
            }
        }
        for (; r = u[++s];) {
            if ("*" !== r) {
                if ("*" !== l && l !== r) {
                    if (i = a[l + " " + r] || a["* " + r], !i) {
                        for (n in a) {
                            if (o = n.split(" "), o[1] === r && (i = a[l + " " + o[0]] || a["* " + o[0]])) {
                                i === !0 ? i = a[n] : a[n] !== !0 && (r = o[0], u.splice(s--, 0, r));
                                break
                            }
                        }
                    }
                    if (i !== !0) {
                        if (i && e["throws"]) {
                            t = i(t)
                        } else {
                            try {
                                t = i(t)
                            } catch (c) {
                                return {
                                    state: "parsererror",
                                    error: i ? c : "No conversion from " + l + " to " + r
                                }
                            }
                        }
                    }
                }
                l = r
            }
        }
        return {
            state: "success",
            data: t
        }
    }
    b.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function(e) {
                return b.globalEval(e), e
            }
        }
    }), b.ajaxPrefilter("script", function(e) {
        e.cache === t && (e.cache = !1), e.crossDomain && (e.type = "GET", e.global = !1)
    }), b.ajaxTransport("script", function(e) {
        if (e.crossDomain) {
            var n, r = o.head || b("head")[0] || o.documentElement;
            return {
                send: function(t, i) {
                    n = o.createElement("script"), n.async = !0, e.scriptCharset && (n.charset = e.scriptCharset), n.src = e.url, n.onload = n.onreadystatechange = function(e, t) {
                        (t || !n.readyState || /loaded|complete/.test(n.readyState)) && (n.onload = n.onreadystatechange = null, n.parentNode && n.parentNode.removeChild(n), n = null, t || i(200, "success"))
                    }, r.insertBefore(n, r.firstChild)
                },
                abort: function() {
                    n && n.onload(t, !0)
                }
            }
        }
    });
    var On = [],
        Bn = /(=)\?(?=&|$)|\?\?/;
    b.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var e = On.pop() || b.expando + "_" + vn++;
            return this[e] = !0, e
        }
    }), b.ajaxPrefilter("json jsonp", function(n, r, i) {
        var o, a, s, u = n.jsonp !== !1 && (Bn.test(n.url) ? "url" : "string" == typeof n.data && !(n.contentType || "").indexOf("application/x-www-form-urlencoded") && Bn.test(n.data) && "data");
        return u || "jsonp" === n.dataTypes[0] ? (o = n.jsonpCallback = b.isFunction(n.jsonpCallback) ? n.jsonpCallback() : n.jsonpCallback, u ? n[u] = n[u].replace(Bn, "$1" + o) : n.jsonp !== !1 && (n.url += (bn.test(n.url) ? "&" : "?") + n.jsonp + "=" + o), n.converters["script json"] = function() {
            return s || b.error(o + " was not called"), s[0]
        }, n.dataTypes[0] = "json", a = e[o], e[o] = function() {
            s = arguments
        }, i.always(function() {
            e[o] = a, n[o] && (n.jsonpCallback = r.jsonpCallback, On.push(o)), s && b.isFunction(a) && a(s[0]), s = a = t
        }), "script") : t
    });
    var Pn, Rn, Wn = 0,
        $n = e.ActiveXObject && function() {
                var e;
                for (e in Pn) {
                    Pn[e](t, !0)
                }
            };

    function In() {
        try {
            return new e.XMLHttpRequest
        } catch (t) {}
    }

    function zn() {
        try {
            return new e.ActiveXObject("Microsoft.XMLHTTP")
        } catch (t) {}
    }
    b.ajaxSettings.xhr = e.ActiveXObject ? function() {
        return !this.isLocal && In() || zn()
    } : In, Rn = b.ajaxSettings.xhr(), b.support.cors = !! Rn && "withCredentials" in Rn, Rn = b.support.ajax = !! Rn, Rn && b.ajaxTransport(function(n) {
        if (!n.crossDomain || b.support.cors) {
            var r;
            return {
                send: function(i, o) {
                    var a, s, u = n.xhr();
                    if (n.username ? u.open(n.type, n.url, n.async, n.username, n.password) : u.open(n.type, n.url, n.async), n.xhrFields) {
                        for (s in n.xhrFields) {
                            u[s] = n.xhrFields[s]
                        }
                    }
                    n.mimeType && u.overrideMimeType && u.overrideMimeType(n.mimeType), n.crossDomain || i["X-Requested-With"] || (i["X-Requested-With"] = "XMLHttpRequest");
                    try {
                        for (s in i) {
                            u.setRequestHeader(s, i[s])
                        }
                    } catch (l) {}
                    u.send(n.hasContent && n.data || null), r = function(e, i) {
                        var s, l, c, p;
                        try {
                            if (r && (i || 4 === u.readyState)) {
                                if (r = t, a && (u.onreadystatechange = b.noop, $n && delete Pn[a]), i) {
                                    4 !== u.readyState && u.abort()
                                } else {
                                    p = {}, s = u.status, l = u.getAllResponseHeaders(), "string" == typeof u.responseText && (p.text = u.responseText);
                                    try {
                                        c = u.statusText
                                    } catch (f) {
                                        c = ""
                                    }
                                    s || !n.isLocal || n.crossDomain ? 1223 === s && (s = 204) : s = p.text ? 200 : 404
                                }
                            }
                        } catch (d) {
                            i || o(-1, d)
                        }
                        p && o(s, c, p, l)
                    }, n.async ? 4 === u.readyState ? setTimeout(r) : (a = ++Wn, $n && (Pn || (Pn = {}, b(e).unload($n)), Pn[a] = r), u.onreadystatechange = r) : r()
                },
                abort: function() {
                    r && r(t, !0)
                }
            }
        }
    });
    var Xn, Un, Vn = /^(?:toggle|show|hide)$/,
        Yn = RegExp("^(?:([+-])=|)(" + x + ")([a-z%]*)$", "i"),
        Jn = /queueHooks$/,
        Gn = [nr],
        Qn = {
            "*": [
                function(e, t) {
                    var n, r, i = this.createTween(e, t),
                        o = Yn.exec(t),
                        a = i.cur(),
                        s = +a || 0,
                        u = 1,
                        l = 20;
                    if (o) {
                        if (n = +o[2], r = o[3] || (b.cssNumber[e] ? "" : "px"), "px" !== r && s) {
                            s = b.css(i.elem, e, !0) || n || 1;
                            do {
                                u = u || ".5", s /= u, b.style(i.elem, e, s + r)
                            } while (u !== (u = i.cur() / a) && 1 !== u && --l)
                        }
                        i.unit = r, i.start = s, i.end = o[1] ? s + (o[1] + 1) * n : n
                    }
                    return i
                }
            ]
        };

    function Kn() {
        return setTimeout(function() {
            Xn = t
        }), Xn = b.now()
    }

    function Zn(e, t) {
        b.each(t, function(t, n) {
            var r = (Qn[t] || []).concat(Qn["*"]),
                i = 0,
                o = r.length;
            for (; o > i; i++) {
                if (r[i].call(e, t, n)) {
                    return
                }
            }
        })
    }

    function er(e, t, n) {
        var r, i, o = 0,
            a = Gn.length,
            s = b.Deferred().always(function() {
                delete u.elem
            }),
            u = function() {
                if (i) {
                    return !1
                }
                var t = Xn || Kn(),
                    n = Math.max(0, l.startTime + l.duration - t),
                    r = n / l.duration || 0,
                    o = 1 - r,
                    a = 0,
                    u = l.tweens.length;
                for (; u > a; a++) {
                    l.tweens[a].run(o)
                }
                return s.notifyWith(e, [l, o, n]), 1 > o && u ? n : (s.resolveWith(e, [l]), !1)
            }, l = s.promise({
                elem: e,
                props: b.extend({}, t),
                opts: b.extend(!0, {
                    specialEasing: {}
                }, n),
                originalProperties: t,
                originalOptions: n,
                startTime: Xn || Kn(),
                duration: n.duration,
                tweens: [],
                createTween: function(t, n) {
                    var r = b.Tween(e, l.opts, t, n, l.opts.specialEasing[t] || l.opts.easing);
                    return l.tweens.push(r), r
                },
                stop: function(t) {
                    var n = 0,
                        r = t ? l.tweens.length : 0;
                    if (i) {
                        return this
                    }
                    for (i = !0; r > n; n++) {
                        l.tweens[n].run(1)
                    }
                    return t ? s.resolveWith(e, [l, t]) : s.rejectWith(e, [l, t]), this
                }
            }),
            c = l.props;
        for (tr(c, l.opts.specialEasing); a > o; o++) {
            if (r = Gn[o].call(l, e, c, l.opts)) {
                return r
            }
        }
        return Zn(l, c), b.isFunction(l.opts.start) && l.opts.start.call(e, l), b.fx.timer(b.extend(u, {
            elem: e,
            anim: l,
            queue: l.opts.queue
        })), l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always)
    }

    function tr(e, t) {
        var n, r, i, o, a;
        for (i in e) {
            if (r = b.camelCase(i), o = t[r], n = e[i], b.isArray(n) && (o = n[1], n = e[i] = n[0]), i !== r && (e[r] = n, delete e[i]), a = b.cssHooks[r], a && "expand" in a) {
                n = a.expand(n), delete e[r];
                for (i in n) {
                    i in e || (e[i] = n[i], t[i] = o)
                }
            } else {
                t[r] = o
            }
        }
    }
    b.Animation = b.extend(er, {
        tweener: function(e, t) {
            b.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" ");
            var n, r = 0,
                i = e.length;
            for (; i > r; r++) {
                n = e[r], Qn[n] = Qn[n] || [], Qn[n].unshift(t)
            }
        },
        prefilter: function(e, t) {
            t ? Gn.unshift(e) : Gn.push(e)
        }
    });

    function nr(e, t, n) {
        var r, i, o, a, s, u, l, c, p, f = this,
            d = e.style,
            h = {}, g = [],
            m = e.nodeType && nn(e);
        n.queue || (c = b._queueHooks(e, "fx"), null == c.unqueued && (c.unqueued = 0, p = c.empty.fire, c.empty.fire = function() {
            c.unqueued || p()
        }), c.unqueued++, f.always(function() {
            f.always(function() {
                c.unqueued--, b.queue(e, "fx").length || c.empty.fire()
            })
        })), 1 === e.nodeType && ("height" in t || "width" in t) && (n.overflow = [d.overflow, d.overflowX, d.overflowY], "inline" === b.css(e, "display") && "none" === b.css(e, "float") && (b.support.inlineBlockNeedsLayout && "inline" !== un(e.nodeName) ? d.zoom = 1 : d.display = "inline-block")), n.overflow && (d.overflow = "hidden", b.support.shrinkWrapBlocks || f.always(function() {
            d.overflow = n.overflow[0], d.overflowX = n.overflow[1], d.overflowY = n.overflow[2]
        }));
        for (i in t) {
            if (a = t[i], Vn.exec(a)) {
                if (delete t[i], u = u || "toggle" === a, a === (m ? "hide" : "show")) {
                    continue
                }
                g.push(i)
            }
        }
        if (o = g.length) {
            s = b._data(e, "fxshow") || b._data(e, "fxshow", {}), "hidden" in s && (m = s.hidden), u && (s.hidden = !m), m ? b(e).show() : f.done(function() {
                b(e).hide()
            }), f.done(function() {
                var t;
                b._removeData(e, "fxshow");
                for (t in h) {
                    b.style(e, t, h[t])
                }
            });
            for (i = 0; o > i; i++) {
                r = g[i], l = f.createTween(r, m ? s[r] : 0), h[r] = s[r] || b.style(e, r), r in s || (s[r] = l.start, m && (l.end = l.start, l.start = "width" === r || "height" === r ? 1 : 0))
            }
        }
    }

    function rr(e, t, n, r, i) {
        return new rr.prototype.init(e, t, n, r, i)
    }
    b.Tween = rr, rr.prototype = {
        constructor: rr,
        init: function(e, t, n, r, i, o) {
            this.elem = e, this.prop = n, this.easing = i || "swing", this.options = t, this.start = this.now = this.cur(), this.end = r, this.unit = o || (b.cssNumber[n] ? "" : "px")
        },
        cur: function() {
            var e = rr.propHooks[this.prop];
            return e && e.get ? e.get(this) : rr.propHooks._default.get(this)
        },
        run: function(e) {
            var t, n = rr.propHooks[this.prop];
            return this.pos = t = this.options.duration ? b.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : rr.propHooks._default.set(this), this
        }
    }, rr.prototype.init.prototype = rr.prototype, rr.propHooks = {
        _default: {
            get: function(e) {
                var t;
                return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = b.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0) : e.elem[e.prop]
            },
            set: function(e) {
                b.fx.step[e.prop] ? b.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[b.cssProps[e.prop]] || b.cssHooks[e.prop]) ? b.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now
            }
        }
    }, rr.propHooks.scrollTop = rr.propHooks.scrollLeft = {
        set: function(e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, b.each(["toggle", "show", "hide"], function(e, t) {
        var n = b.fn[t];
        b.fn[t] = function(e, r, i) {
            return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(ir(t, !0), e, r, i)
        }
    }), b.fn.extend({
        fadeTo: function(e, t, n, r) {
            return this.filter(nn).css("opacity", 0).show().end().animate({
                opacity: t
            }, e, n, r)
        },
        animate: function(e, t, n, r) {
            var i = b.isEmptyObject(e),
                o = b.speed(t, n, r),
                a = function() {
                    var t = er(this, b.extend({}, e), o);
                    a.finish = function() {
                        t.stop(!0)
                    }, (i || b._data(this, "finish")) && t.stop(!0)
                };
            return a.finish = a, i || o.queue === !1 ? this.each(a) : this.queue(o.queue, a)
        },
        stop: function(e, n, r) {
            var i = function(e) {
                var t = e.stop;
                delete e.stop, t(r)
            };
            return "string" != typeof e && (r = n, n = e, e = t), n && e !== !1 && this.queue(e || "fx", []), this.each(function() {
                var t = !0,
                    n = null != e && e + "queueHooks",
                    o = b.timers,
                    a = b._data(this);
                if (n) {
                    a[n] && a[n].stop && i(a[n])
                } else {
                    for (n in a) {
                        a[n] && a[n].stop && Jn.test(n) && i(a[n])
                    }
                }
                for (n = o.length; n--;) {
                    o[n].elem !== this || null != e && o[n].queue !== e || (o[n].anim.stop(r), t = !1, o.splice(n, 1))
                }(t || !r) && b.dequeue(this, e)
            })
        },
        finish: function(e) {
            return e !== !1 && (e = e || "fx"), this.each(function() {
                var t, n = b._data(this),
                    r = n[e + "queue"],
                    i = n[e + "queueHooks"],
                    o = b.timers,
                    a = r ? r.length : 0;
                for (n.finish = !0, b.queue(this, e, []), i && i.cur && i.cur.finish && i.cur.finish.call(this), t = o.length; t--;) {
                    o[t].elem === this && o[t].queue === e && (o[t].anim.stop(!0), o.splice(t, 1))
                }
                for (t = 0; a > t; t++) {
                    r[t] && r[t].finish && r[t].finish.call(this)
                }
                delete n.finish
            })
        }
    });

    function ir(e, t) {
        var n, r = {
            height: e
        }, i = 0;
        for (t = t ? 1 : 0; 4 > i; i += 2 - t) {
            n = Zt[i], r["margin" + n] = r["padding" + n] = e
        }
        return t && (r.opacity = r.width = e), r
    }
    b.each({
        slideDown: ir("show"),
        slideUp: ir("hide"),
        slideToggle: ir("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(e, t) {
        b.fn[e] = function(e, n, r) {
            return this.animate(t, e, n, r)
        }
    }), b.speed = function(e, t, n) {
        var r = e && "object" == typeof e ? b.extend({}, e) : {
            complete: n || !n && t || b.isFunction(e) && e,
            duration: e,
            easing: n && t || t && !b.isFunction(t) && t
        };
        return r.duration = b.fx.off ? 0 : "number" == typeof r.duration ? r.duration : r.duration in b.fx.speeds ? b.fx.speeds[r.duration] : b.fx.speeds._default, (null == r.queue || r.queue === !0) && (r.queue = "fx"), r.old = r.complete, r.complete = function() {
            b.isFunction(r.old) && r.old.call(this), r.queue && b.dequeue(this, r.queue)
        }, r
    }, b.easing = {
        linear: function(e) {
            return e
        },
        swing: function(e) {
            return 0.5 - Math.cos(e * Math.PI) / 2
        }
    }, b.timers = [], b.fx = rr.prototype.init, b.fx.tick = function() {
        var e, n = b.timers,
            r = 0;
        for (Xn = b.now(); n.length > r; r++) {
            e = n[r], e() || n[r] !== e || n.splice(r--, 1)
        }
        n.length || b.fx.stop(), Xn = t
    }, b.fx.timer = function(e) {
        e() && b.timers.push(e) && b.fx.start()
    }, b.fx.interval = 13, b.fx.start = function() {
        Un || (Un = setInterval(b.fx.tick, b.fx.interval))
    }, b.fx.stop = function() {
        clearInterval(Un), Un = null
    }, b.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    }, b.fx.step = {}, b.expr && b.expr.filters && (b.expr.filters.animated = function(e) {
        return b.grep(b.timers, function(t) {
            return e === t.elem
        }).length
    }), b.fn.offset = function(e) {
        if (arguments.length) {
            return e === t ? this : this.each(function(t) {
                b.offset.setOffset(this, e, t)
            })
        }
        var n, r, o = {
                top: 0,
                left: 0
            }, a = this[0],
            s = a && a.ownerDocument;
        if (s) {
            return n = s.documentElement, b.contains(n, a) ? (typeof a.getBoundingClientRect !== i && (o = a.getBoundingClientRect()), r = or(s), {
                top: o.top + (r.pageYOffset || n.scrollTop) - (n.clientTop || 0),
                left: o.left + (r.pageXOffset || n.scrollLeft) - (n.clientLeft || 0)
            }) : o
        }
    }, b.offset = {
        setOffset: function(e, t, n) {
            var r = b.css(e, "position");
            "static" === r && (e.style.position = "relative");
            var i = b(e),
                o = i.offset(),
                a = b.css(e, "top"),
                s = b.css(e, "left"),
                u = ("absolute" === r || "fixed" === r) && b.inArray("auto", [a, s]) > -1,
                l = {}, c = {}, p, f;
            u ? (c = i.position(), p = c.top, f = c.left) : (p = parseFloat(a) || 0, f = parseFloat(s) || 0), b.isFunction(t) && (t = t.call(e, n, o)), null != t.top && (l.top = t.top - o.top + p), null != t.left && (l.left = t.left - o.left + f), "using" in t ? t.using.call(e, l) : i.css(l)
        }
    }, b.fn.extend({
        position: function() {
            if (this[0]) {
                var e, t, n = {
                    top: 0,
                    left: 0
                }, r = this[0];
                return "fixed" === b.css(r, "position") ? t = r.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), b.nodeName(e[0], "html") || (n = e.offset()), n.top += b.css(e[0], "borderTopWidth", !0), n.left += b.css(e[0], "borderLeftWidth", !0)), {
                    top: t.top - n.top - b.css(r, "marginTop", !0),
                    left: t.left - n.left - b.css(r, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                var e = this.offsetParent || o.documentElement;
                while (e && !b.nodeName(e, "html") && "static" === b.css(e, "position")) {
                    e = e.offsetParent
                }
                return e || o.documentElement
            })
        }
    }), b.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(e, n) {
        var r = /Y/.test(n);
        b.fn[e] = function(i) {
            return b.access(this, function(e, i, o) {
                var a = or(e);
                return o === t ? a ? n in a ? a[n] : a.document.documentElement[i] : e[i] : (a ? a.scrollTo(r ? b(a).scrollLeft() : o, r ? o : b(a).scrollTop()) : e[i] = o, t)
            }, e, i, arguments.length, null)
        }
    });

    function or(e) {
        return b.isWindow(e) ? e : 9 === e.nodeType ? e.defaultView || e.parentWindow : !1
    }
    b.each({
        Height: "height",
        Width: "width"
    }, function(e, n) {
        b.each({
            padding: "inner" + e,
            content: n,
            "": "outer" + e
        }, function(r, i) {
            b.fn[i] = function(i, o) {
                var a = arguments.length && (r || "boolean" != typeof i),
                    s = r || (i === !0 || o === !0 ? "margin" : "border");
                return b.access(this, function(n, r, i) {
                    var o;
                    return b.isWindow(n) ? n.document.documentElement["client" + e] : 9 === n.nodeType ? (o = n.documentElement, Math.max(n.body["scroll" + e], o["scroll" + e], n.body["offset" + e], o["offset" + e], o["client" + e])) : i === t ? b.css(n, r, s) : b.style(n, r, i, s)
                }, n, a ? i : t, a, null)
            }
        })
    }), e.jQuery = e.$ = b, "function" == typeof define && define.amd && define.amd.jQuery && define("jquery", [], function() {
        return b
    })
})(window);

function setCookie(d, h, b, e, a, f) {
    var g = new Date();
    g.setTime(g.getTime());
    if (b) {
        b = b * 1000 * 60 * 60 * 24
    }
    var c = new Date(g.getTime() + (b));
    document.cookie = d + "=" + escape(h) + ((b) ? ";expires=" + c.toGMTString() : "") + ((e) ? ";path=" + e : "") + ((a) ? ";domain=" + a : "") + ((f) ? ";secure" : "")
}
jQuery(function(a, j) {
    var c = a(document);
    var e = a(window);
    var b = a(document.body);
    var d = a("html");
    a("a[rel=external]", b).attr("target", "_blank");
    var i = (d.attr("class") == "rtl");
    a(".hmLayer", b).parent().bind("click", function(p) {
        var m = a(this);
        var k = m.find(".hmLayer");
        var l = m.find("a:first-child");
        var o = true;
        if (l.attr("title") == "Models") {
            var s = navigator.userAgent;
            if (s.indexOf("Android") >= 0) {
                var n = parseFloat(s.slice(s.indexOf("Android") + 8));
                if (n < 3) {
                    var q = window.location.href.toLowerCase().indexOf("mobile");
                    if (q >= 0) {
                        o = false;
                        var r = window.location.href.substring(0, q + 6);
                        window.location = r + "/models"
                    }
                }
            }
        }
        if (o) {
            if (k.is(":visible")) {
                g()
            } else {
                g();
                l.addClass("selected");
                k.show();
                p.preventDefault()
            }
        }
    });
    a(".hmlClose span", b).bind("click", function(k) {
        k.stopPropagation();
        g()
    });

    function g() {
        var k = a(".hMenu");
        var l = k.find(".hmLayer");
        var m = k.find("a");
        l.hide();
        m.removeClass("selected")
    }
    a(".modelpagefBox", b).parent().find("span:first").bind("click", function() {
        var l = a(this);
        var m = l.parent();
        var k = m.find(".modelpagefBox");
        if (k.is(":visible")) {
            h()
        } else {
            h();
            l.addClass("open");
            k.show();
            if (a("> div.rowExterior", k).length > 0) {
                var n = new SwipeCarouselItems(a("#colorsContainer"), "> div.swipe-wrap", "> div.swipe-wrap > div", "release");
                n.init()
            }
        }
    });

    function h() {
        var l = a(".modelpageFeatures");
        var m = l.find(".modelpagefBox");
        var k = l.find("span");
        m.hide();
        k.removeClass("open")
    }
    a(".modelpagefBox > a.hide", b).bind("click", function(k) {
        a(this).parent().parent().find("span:first").trigger("click");
        k.preventDefault()
    });
    a(".detailbodyfBox", b).parent().find("span:first").bind("click", function() {
        var l = a(this);
        var m = l.parent();
        var k = m.find(".detailbodyfBox");
        if (k.is(":visible")) {
            f()
        } else {
            f();
            l.addClass("open");
            k.show()
        }
    });

    function f() {
        var l = a(".detailBody");
        var m = l.find(".detailbodyfBox");
        var k = l.find("span");
        m.hide();
        k.removeClass("open")
    }
    a(".detailbodyfBox > a.hide", b).bind("click", function(k) {
        a(this).parent().parent().find("span:first").trigger("click");
        k.preventDefault()
    })
});
(function() {
    var a = 0;
    var b = ["ms", "moz", "webkit", "o"];
    for (var c = 0; c < b.length && !window.requestAnimationFrame; ++c) {
        window.requestAnimationFrame = window[b[c] + "RequestAnimationFrame"];
        window.cancelAnimationFrame = window[b[c] + "CancelAnimationFrame"] || window[b[c] + "CancelRequestAnimationFrame"]
    }
    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = function(d, f) {
            var e = new Date().getTime();
            var h = Math.max(0, 16 - (e - a));
            var g = window.setTimeout(function() {
                d(e + h)
            }, h);
            a = e + h;
            return g
        }
    }
    if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = function(d) {
            clearTimeout(d)
        }
    }
}());

function SwipeCarouselItems(a, n, g, e) {
    var k = this;
    var b = $(n, a);
    var j = $(g, a);
    var i = 0;
    var h = j.length;
    var c = 0;
    var d = "release";
    if (e == "") {
        d = "swipeleft swiperight dragleft dragright release"
    }
    this.init = function() {
        m();
        $(window).on("load resize orientationchange", function() {
            m()
        })
    };

    function m() {
        if (a[0].clientWidth == 0) {
            return
        }
        i = a.width();
        j.each(function() {
            $(this).width(i)
        });
        b.width(i * h)
    }
    this.showPane = function(o, p) {
        if (!p) {
            o = Math.max(0, Math.min(o, h - 1))
        } else {
            if (o > h - 1) {
                o = 0
            } else {
                if (o < 0) {
                    o = h - 1
                }
            }
        }
        c = o;
        var q = -((100 / h) * c);
        l(q, true);
        a.trigger("changeposition")
    };

    function l(p, o) {
        b.removeClass("animate");
        if (o) {
            b.addClass("animate")
        }
        if (Modernizr.csstransforms3d) {
            b.css("transform", "translate3d(" + p + "%,0,0) scale3d(1,1,1)")
        } else {
            if (Modernizr.csstransforms) {
                b.css("transform", "translate(" + p + "%,0)")
            } else {
                var q = ((i * h) / 100) * p;
                b.css("left", q + "px")
            }
        }
    }
    this.next = function() {
        return this.showPane(c + 1, true)
    };
    this.prev = function() {
        return this.showPane(c - 1, true)
    };
    this.position = function() {
        return c
    };

    function f(p) {
        switch (p.type) {
            case "dragright":
            case "dragleft":
                var q = -(100 / h) * c;
                var o = ((100 / i) * p.gesture.deltaX) / h;
                if ((c == 0 && p.gesture.direction == Hammer.DIRECTION_RIGHT) || (c == h - 1 && p.gesture.direction == Hammer.DIRECTION_LEFT)) {
                    o *= 0.4
                }
                l(o + q);
                break;
            case "swipeleft":
                k.next();
                p.gesture.stopDetect();
                break;
            case "swiperight":
                k.prev();
                p.gesture.stopDetect();
                break;
            case "release":
                if (Math.abs(p.gesture.deltaX) > i / 2) {
                    if (p.gesture.direction == "right") {
                        k.prev()
                    } else {
                        k.next()
                    }
                } else {
                    if (p.gesture.deltaX != 0) {
                        k.showPane(c, true)
                    }
                }
                break;
            default:
                break
        }
    }
    a.hammer({
        drag_lock_to_axis: true,
        drag_block_horizontal: true,
        drag_block_vertical: false,
        prevent_default: false,
        prevent_mouseevents: false,
        stop_browser_behavior: {
            touchAction: "pan-y"
        }
    }).on(d, function(o) {
        if (o.gesture.direction != Hammer.DIRECTION_RIGHT && o.gesture.direction != Hammer.DIRECTION_LEFT) {
            o.gesture.stopDetect();
            return
        }
        o.preventDefault();
        o.stopPropagation();
        f(o)
    })
}

function openNewWindow(a) {
    var b = window.open(a, "screenX=100");
    if (!b) {
        window.location = a
    } else {
        b.onload = function() {
            setTimeout(function() {
                if (b.screenX === 0) {
                    window.location = a
                }
            }, 0)
        }
    }
}
var K2Mobile;
(function(i) {
    var o;
    var n;
    var g;
    var h = false;
    var a;
    var q;

    function j() {
        var r = navigator.userAgent;
        if (r.indexOf("(iPhone;") >= 0) {
            h = true
        }
        g = ko.observable(0);
        o = ko.observable(0);
        n = ko.observable(0);
        a = ko.computed(function() {
            var u = $("div.header").height();
            var z = $("#suiteBar");
            var A = z.length > 0 ? z.height() : 0;
            var x = $("#s4-ribbonrow");
            var y = x.length > 0 ? x.height() : 0;
            var v = $("#notificationArea");
            var w = v.length > 0 ? v.height() : 0;
            var s = $("#DeltaPageStatusBar");
            var t = s.length > 0 ? s.height() : 0;
            return n() - u - A - y - w - t
        });
        i.heightOffset = g;
        i.screenHeight = n;
        i.screenWidth = o;
        i.stickyMenuHeight = q;
        i.bodyHeight = a;
        p();
        $(window).on("load resize orientationchange scroll", function() {
            l()
        })
    }
    i.OnInit = j;

    function k() {
        ko.applyBindings(i, $("div.header").get(0))
    }
    i.OnLoad = k;

    function l() {
        setTimeout(p, 100)
    }
    i.OnScreenResize = l;

    function p() {
        var u = f();
        var s = e();
        var r = d();
        var t = $("div.header").height;
        o(u);
        n(s + r);
        g(r);
        q = t
    }

    function b() {
        return a()
    }
    i.GetBodyHeight = b;

    function f() {
        var r = null;
        if (window.screen != null) {
            r = window.screen.availWidth
        }
        if (window.innerWidth != null) {
            r = window.innerWidth
        }
        if (document.body != null) {
            r = document.body.clientWidth
        }
        return r
    }
    i.GetScreenWidth = f;

    function e() {
        var r = null;
        if (window.screen != null) {
            r = window.screen.availHeight
        }
        if (window.innerHeight != null) {
            r = window.innerHeight
        }
        if (document.body != null) {
            r = document.body.clientHeight
        }
        return r
    }
    i.GetScreenHeight = e;

    function d() {
        if (!h) {
            return 0
        }
        return 60
    }

    function c() {
        if (/iP(hone|od|ad)/.test(navigator.platform)) {
            var r = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
            return [parseInt(r[1], 10), parseInt(r[2], 10), parseInt(r[3] || 0, 10)]
        }
        return -1
    }
    i.GetiOsVersion = c;

    function m(r) {
        var s = window.open(r, "screenX=100");
        if (!s) {
            window.location = r
        } else {
            s.onload = function() {
                setTimeout(function() {
                    if (s.screenX === 0) {
                        window.location = r
                    }
                }, 0)
            }
        }
    }
    i.OpenNewWindow = m
})(K2Mobile || (K2Mobile = {}));
$(window).on("load", function() {
    K2Mobile.OnInit();
    K2Mobile.OnLoad()
});

function getCurrentCountryCode() {
    if (google.loader.ClientLocation) {
        return google.loader.ClientLocation.address.country_code
    } else {
        try {
            var a = geoip_country_code();
            if (a != null) {
                return a
            } else {
                return null
            }
        } catch (b) {
            return null
        }
    }
}

function getCountryData(a, c) {
    var b = $(c).data(a);
    return b
}

function getCurrentCountryData(c) {
    var a = getCurrentCountryCode();
    if (a == null) {
        return null
    } else {
        var b = getCountryData(a, c);
        return b
    }
}
if (!String.prototype.format) {
    String.prototype.format = function() {
        var a = arguments;
        return this.replace(/{(\d+)}/g, function(b, c) {
            return typeof a[c] != "undefined" ? a[c] : b
        })
    }
}; /* iScroll v5.1.1 ~ (c) 2008-2014 Matteo Spinelli ~ http://cubiq.org/license */
(function(h, b, e) {
    var f = h.requestAnimationFrame || h.webkitRequestAnimationFrame || h.mozRequestAnimationFrame || h.oRequestAnimationFrame || h.msRequestAnimationFrame || function(i) {
            h.setTimeout(i, 1000 / 60)
        };
    var g = (function() {
        var n = {};
        var i = b.createElement("div").style;
        var l = (function() {
            var r = ["t", "webkitT", "MozT", "msT", "OT"],
                q, o = 0,
                p = r.length;
            for (; o < p; o++) {
                q = r[o] + "ransform";
                if (q in i) {
                    return r[o].substr(0, r[o].length - 1)
                }
            }
            return false
        })();

        function j(o) {
            if (l === false) {
                return false
            }
            if (l === "") {
                return o
            }
            return l + o.charAt(0).toUpperCase() + o.substr(1)
        }
        n.getTime = Date.now || function m() {
            return new Date().getTime()
        };
        n.extend = function(q, p) {
            for (var o in p) {
                q[o] = p[o]
            }
        };
        n.addEvent = function(p, r, q, o) {
            p.addEventListener(r, q, !! o)
        };
        n.removeEvent = function(p, r, q, o) {
            p.removeEventListener(r, q, !! o)
        };
        n.momentum = function(o, v, w, t, x, p) {
            var r = o - v,
                u = e.abs(r) / w,
                q, s;
            p = p === undefined ? 0.0006 : p;
            q = o + (u * u) / (2 * p) * (r < 0 ? -1 : 1);
            s = u / p;
            if (q < t) {
                q = x ? t - (x / 2.5 * (u / 8)) : t;
                r = e.abs(q - o);
                s = r / u
            } else {
                if (q > 0) {
                    q = x ? x / 2.5 * (u / 8) : 0;
                    r = e.abs(o) + q;
                    s = r / u
                }
            }
            return {
                destination: e.round(q),
                duration: s
            }
        };
        var k = j("transform");
        n.extend(n, {
            hasTransform: k !== false,
            hasPerspective: j("perspective") in i,
            hasTouch: "ontouchstart" in h,
            hasPointer: navigator.msPointerEnabled,
            hasTransition: j("transition") in i
        });
        n.isBadAndroid = /Android /.test(h.navigator.appVersion) && !(/Chrome\/\d/.test(h.navigator.appVersion));
        n.extend(n.style = {}, {
            transform: k,
            transitionTimingFunction: j("transitionTimingFunction"),
            transitionDuration: j("transitionDuration"),
            transitionDelay: j("transitionDelay"),
            transformOrigin: j("transformOrigin")
        });
        n.hasClass = function(p, o) {
            var q = new RegExp("(^|\\s)" + o + "(\\s|$)");
            return q.test(p.className)
        };
        n.addClass = function(p, o) {
            if (n.hasClass(p, o)) {
                return
            }
            var q = p.className.split(" ");
            q.push(o);
            p.className = q.join(" ")
        };
        n.removeClass = function(p, o) {
            if (!n.hasClass(p, o)) {
                return
            }
            var q = new RegExp("(^|\\s)" + o + "(\\s|$)", "g");
            p.className = p.className.replace(q, " ")
        };
        n.offset = function(o) {
            var p = -o.offsetLeft,
                q = -o.offsetTop;
            while (o = o.offsetParent) {
                p -= o.offsetLeft;
                q -= o.offsetTop
            }
            return {
                left: p,
                top: q
            }
        };
        n.preventDefaultException = function(o, p) {
            for (var q in p) {
                if (p[q].test(o[q])) {
                    return true
                }
            }
            return false
        };
        n.extend(n.eventType = {}, {
            touchstart: 1,
            touchmove: 1,
            touchend: 1,
            mousedown: 2,
            mousemove: 2,
            mouseup: 2,
            MSPointerDown: 3,
            MSPointerMove: 3,
            MSPointerUp: 3
        });
        n.extend(n.ease = {}, {
            quadratic: {
                style: "cubic-bezier(0.25, 0.46, 0.45, 0.94)",
                fn: function(o) {
                    return o * (2 - o)
                }
            },
            circular: {
                style: "cubic-bezier(0.1, 0.57, 0.1, 1)",
                fn: function(o) {
                    return e.sqrt(1 - (--o * o))
                }
            },
            back: {
                style: "cubic-bezier(0.175, 0.885, 0.32, 1.275)",
                fn: function(p) {
                    var o = 4;
                    return (p = p - 1) * p * ((o + 1) * p + o) + 1
                }
            },
            bounce: {
                style: "",
                fn: function(o) {
                    if ((o /= 1) < (1 / 2.75)) {
                        return 7.5625 * o * o
                    } else {
                        if (o < (2 / 2.75)) {
                            return 7.5625 * (o -= (1.5 / 2.75)) * o + 0.75
                        } else {
                            if (o < (2.5 / 2.75)) {
                                return 7.5625 * (o -= (2.25 / 2.75)) * o + 0.9375
                            } else {
                                return 7.5625 * (o -= (2.625 / 2.75)) * o + 0.984375
                            }
                        }
                    }
                }
            },
            elastic: {
                style: "",
                fn: function(q) {
                    var p = 0.22,
                        o = 0.4;
                    if (q === 0) {
                        return 0
                    }
                    if (q == 1) {
                        return 1
                    }
                    return (o * e.pow(2, -10 * q) * e.sin((q - p / 4) * (2 * e.PI) / p) + 1)
                }
            }
        });
        n.tap = function(o, q) {
            var p = b.createEvent("Event");
            p.initEvent(q, true, true);
            p.pageX = o.pageX;
            p.pageY = o.pageY;
            o.target.dispatchEvent(p)
        };
        n.click = function(o) {
            var q = o.target,
                p;
            if (!(/(SELECT|INPUT|TEXTAREA)/i).test(q.tagName)) {
                p = b.createEvent("MouseEvents");
                p.initMouseEvent("click", true, true, o.view, 1, q.screenX, q.screenY, q.clientX, q.clientY, o.ctrlKey, o.altKey, o.shiftKey, o.metaKey, 0, null);
                p._constructed = true;
                q.dispatchEvent(p)
            }
        };
        return n
    })();

    function d(j, l) {
        this.wrapper = typeof j == "string" ? b.querySelector(j) : j;
        this.scroller = this.wrapper.children[0];
        this.scrollerStyle = this.scroller.style;
        this.options = {
            resizeScrollbars: true,
            mouseWheelSpeed: 20,
            snapThreshold: 0.334,
            startX: 0,
            startY: 0,
            scrollY: true,
            directionLockThreshold: 5,
            momentum: true,
            bounce: true,
            bounceTime: 600,
            bounceEasing: "",
            preventDefault: true,
            preventDefaultException: {
                tagName: /^(INPUT|TEXTAREA|BUTTON|SELECT)$/
            },
            HWCompositing: true,
            useTransition: true,
            useTransform: true
        };
        for (var k in l) {
            this.options[k] = l[k]
        }
        this.translateZ = this.options.HWCompositing && g.hasPerspective ? " translateZ(0)" : "";
        this.options.useTransition = g.hasTransition && this.options.useTransition;
        this.options.useTransform = g.hasTransform && this.options.useTransform;
        this.options.eventPassthrough = this.options.eventPassthrough === true ? "vertical" : this.options.eventPassthrough;
        this.options.preventDefault = !this.options.eventPassthrough && this.options.preventDefault;
        this.options.scrollY = this.options.eventPassthrough == "vertical" ? false : this.options.scrollY;
        this.options.scrollX = this.options.eventPassthrough == "horizontal" ? false : this.options.scrollX;
        this.options.freeScroll = this.options.freeScroll && !this.options.eventPassthrough;
        this.options.directionLockThreshold = this.options.eventPassthrough ? 0 : this.options.directionLockThreshold;
        this.options.bounceEasing = typeof this.options.bounceEasing == "string" ? g.ease[this.options.bounceEasing] || g.ease.circular : this.options.bounceEasing;
        this.options.resizePolling = this.options.resizePolling === undefined ? 60 : this.options.resizePolling;
        if (this.options.tap === true) {
            this.options.tap = "tap"
        }
        if (this.options.shrinkScrollbars == "scale") {
            this.options.useTransition = false
        }
        this.options.invertWheelDirection = this.options.invertWheelDirection ? -1 : 1;
        this.x = 0;
        this.y = 0;
        this.directionX = 0;
        this.directionY = 0;
        this._events = {};
        this._init();
        this.refresh();
        this.scrollTo(this.options.startX, this.options.startY);
        this.enable()
    }
    d.prototype = {
        version: "5.1.1",
        _init: function() {
            this._initEvents();
            if (this.options.scrollbars || this.options.indicators) {
                this._initIndicators()
            }
            if (this.options.mouseWheel) {
                this._initWheel()
            }
            if (this.options.snap) {
                this._initSnap()
            }
            if (this.options.keyBindings) {
                this._initKeys()
            }
        },
        destroy: function() {
            this._initEvents(true);
            this._execEvent("destroy")
        },
        _transitionEnd: function(i) {
            if (i.target != this.scroller || !this.isInTransition) {
                return
            }
            this._transitionTime();
            if (!this.resetPosition(this.options.bounceTime)) {
                this.isInTransition = false;
                this._execEvent("scrollEnd")
            }
        },
        _start: function(i) {
            if (g.eventType[i.type] != 1) {
                if (i.button !== 0) {
                    return
                }
            }
            if (!this.enabled || (this.initiated && g.eventType[i.type] !== this.initiated)) {
                return
            }
            if (this.options.preventDefault && !g.isBadAndroid && !g.preventDefaultException(i.target, this.options.preventDefaultException)) {
                i.preventDefault()
            }
            var j = i.touches ? i.touches[0] : i,
                k;
            this.initiated = g.eventType[i.type];
            this.moved = false;
            this.distX = 0;
            this.distY = 0;
            this.directionX = 0;
            this.directionY = 0;
            this.directionLocked = 0;
            this._transitionTime();
            this.startTime = g.getTime();
            if (this.options.useTransition && this.isInTransition) {
                this.isInTransition = false;
                k = this.getComputedPosition();
                this._translate(e.round(k.x), e.round(k.y));
                this._execEvent("scrollEnd")
            } else {
                if (!this.options.useTransition && this.isAnimating) {
                    this.isAnimating = false;
                    this._execEvent("scrollEnd")
                }
            }
            this.startX = this.x;
            this.startY = this.y;
            this.absStartX = this.x;
            this.absStartY = this.y;
            this.pointX = j.pageX;
            this.pointY = j.pageY;
            this._execEvent("beforeScrollStart")
        },
        _move: function(m) {
            if (!this.enabled || g.eventType[m.type] !== this.initiated) {
                return
            }
            if (this.options.preventDefault) {
                m.preventDefault()
            }
            var p = m.touches ? m.touches[0] : m,
                k = p.pageX - this.pointX,
                l = p.pageY - this.pointY,
                q = g.getTime(),
                n, o, i, j;
            this.pointX = p.pageX;
            this.pointY = p.pageY;
            this.distX += k;
            this.distY += l;
            i = e.abs(this.distX);
            j = e.abs(this.distY);
            if (q - this.endTime > 300 && (i < 10 && j < 10)) {
                return
            }
            if (!this.directionLocked && !this.options.freeScroll) {
                if (i > j + this.options.directionLockThreshold) {
                    this.directionLocked = "h"
                } else {
                    if (j >= i + this.options.directionLockThreshold) {
                        this.directionLocked = "v"
                    } else {
                        this.directionLocked = "n"
                    }
                }
            }
            if (this.directionLocked == "h") {
                if (this.options.eventPassthrough == "vertical") {
                    m.preventDefault()
                } else {
                    if (this.options.eventPassthrough == "horizontal") {
                        this.initiated = false;
                        return
                    }
                }
                l = 0
            } else {
                if (this.directionLocked == "v") {
                    if (this.options.eventPassthrough == "horizontal") {
                        m.preventDefault()
                    } else {
                        if (this.options.eventPassthrough == "vertical") {
                            this.initiated = false;
                            return
                        }
                    }
                    k = 0
                }
            }
            k = this.hasHorizontalScroll ? k : 0;
            l = this.hasVerticalScroll ? l : 0;
            n = this.x + k;
            o = this.y + l;
            if (n > 0 || n < this.maxScrollX) {
                n = this.options.bounce ? this.x + k / 3 : n > 0 ? 0 : this.maxScrollX
            }
            if (o > 0 || o < this.maxScrollY) {
                o = this.options.bounce ? this.y + l / 3 : o > 0 ? 0 : this.maxScrollY
            }
            this.directionX = k > 0 ? -1 : k < 0 ? 1 : 0;
            this.directionY = l > 0 ? -1 : l < 0 ? 1 : 0;
            if (!this.moved) {
                this._execEvent("scrollStart")
            }
            this.moved = true;
            this._translate(n, o);
            if (q - this.startTime > 300) {
                this.startTime = q;
                this.startX = this.x;
                this.startY = this.y
            }
        },
        _end: function(l) {
            if (!this.enabled || g.eventType[l.type] !== this.initiated) {
                return
            }
            if (this.options.preventDefault && !g.preventDefaultException(l.target, this.options.preventDefaultException)) {
                l.preventDefault()
            }
            var r = l.changedTouches ? l.changedTouches[0] : l,
                n, o, k = g.getTime() - this.startTime,
                p = e.round(this.x),
                q = e.round(this.y),
                i = e.abs(p - this.startX),
                j = e.abs(q - this.startY),
                t = 0,
                m = "";
            this.isInTransition = 0;
            this.initiated = 0;
            this.endTime = g.getTime();
            if (this.resetPosition(this.options.bounceTime)) {
                return
            }
            this.scrollTo(p, q);
            if (!this.moved) {
                if (this.options.tap) {
                    g.tap(l, this.options.tap)
                }
                if (this.options.click) {
                    g.click(l)
                }
                this._execEvent("scrollCancel");
                return
            }
            if (this._events.flick && k < 200 && i < 100 && j < 100) {
                this._execEvent("flick");
                return
            }
            if (this.options.momentum && k < 300) {
                n = this.hasHorizontalScroll ? g.momentum(this.x, this.startX, k, this.maxScrollX, this.options.bounce ? this.wrapperWidth : 0, this.options.deceleration) : {
                    destination: p,
                    duration: 0
                };
                o = this.hasVerticalScroll ? g.momentum(this.y, this.startY, k, this.maxScrollY, this.options.bounce ? this.wrapperHeight : 0, this.options.deceleration) : {
                    destination: q,
                    duration: 0
                };
                p = n.destination;
                q = o.destination;
                t = e.max(n.duration, o.duration);
                this.isInTransition = 1
            }
            if (this.options.snap) {
                var s = this._nearestSnap(p, q);
                this.currentPage = s;
                t = this.options.snapSpeed || e.max(e.max(e.min(e.abs(p - s.x), 1000), e.min(e.abs(q - s.y), 1000)), 300);
                p = s.x;
                q = s.y;
                this.directionX = 0;
                this.directionY = 0;
                m = this.options.bounceEasing
            }
            if (p != this.x || q != this.y) {
                if (p > 0 || p < this.maxScrollX || q > 0 || q < this.maxScrollY) {
                    m = g.ease.quadratic
                }
                this.scrollTo(p, q, t, m);
                return
            }
            this._execEvent("scrollEnd")
        },
        _resize: function() {
            var i = this;
            clearTimeout(this.resizeTimeout);
            this.resizeTimeout = setTimeout(function() {
                i.refresh()
            }, this.options.resizePolling)
        },
        resetPosition: function(i) {
            var j = this.x,
                k = this.y;
            i = i || 0;
            if (!this.hasHorizontalScroll || this.x > 0) {
                j = 0
            } else {
                if (this.x < this.maxScrollX) {
                    j = this.maxScrollX
                }
            } if (!this.hasVerticalScroll || this.y > 0) {
                k = 0
            } else {
                if (this.y < this.maxScrollY) {
                    k = this.maxScrollY
                }
            } if (j == this.x && k == this.y) {
                return false
            }
            this.scrollTo(j, k, i, this.options.bounceEasing);
            return true
        },
        disable: function() {
            this.enabled = false
        },
        enable: function() {
            this.enabled = true
        },
        refresh: function() {
            var i = this.wrapper.offsetHeight;
            this.wrapperWidth = this.wrapper.clientWidth;
            this.wrapperHeight = this.wrapper.clientHeight;
            this.scrollerWidth = this.scroller.offsetWidth;
            this.scrollerHeight = this.scroller.offsetHeight;
            this.maxScrollX = this.wrapperWidth - this.scrollerWidth;
            this.maxScrollY = this.wrapperHeight - this.scrollerHeight;
            this.hasHorizontalScroll = this.options.scrollX && this.maxScrollX < 0;
            this.hasVerticalScroll = this.options.scrollY && this.maxScrollY < 0;
            if (!this.hasHorizontalScroll) {
                this.maxScrollX = 0;
                this.scrollerWidth = this.wrapperWidth
            }
            if (!this.hasVerticalScroll) {
                this.maxScrollY = 0;
                this.scrollerHeight = this.wrapperHeight
            }
            this.endTime = 0;
            this.directionX = 0;
            this.directionY = 0;
            this.wrapperOffset = g.offset(this.wrapper);
            this._execEvent("refresh");
            this.resetPosition()
        },
        on: function(j, i) {
            if (!this._events[j]) {
                this._events[j] = []
            }
            this._events[j].push(i)
        },
        off: function(k, i) {
            if (!this._events[k]) {
                return
            }
            var j = this._events[k].indexOf(i);
            if (j > -1) {
                this._events[k].splice(j, 1)
            }
        },
        _execEvent: function(m) {
            if (!this._events[m]) {
                return
            }
            var j = 0,
                k = this._events[m].length;
            if (!k) {
                return
            }
            for (; j < k; j++) {
                this._events[m][j].apply(this, [].slice.call(arguments, 1))
            }
        },
        scrollBy: function(k, l, j, i) {
            k = this.x + k;
            l = this.y + l;
            j = j || 0;
            this.scrollTo(k, l, j, i)
        },
        scrollTo: function(k, l, j, i) {
            i = i || g.ease.circular;
            this.isInTransition = this.options.useTransition && j > 0;
            if (!j || (this.options.useTransition && i.style)) {
                this._transitionTimingFunction(i.style);
                this._transitionTime(j);
                this._translate(k, l)
            } else {
                this._animate(k, l, j, i.fn)
            }
        },
        scrollToElement: function(j, n, k, l, i) {
            j = j.nodeType ? j : this.scroller.querySelector(j);
            if (!j) {
                return
            }
            var m = g.offset(j);
            m.left -= this.wrapperOffset.left;
            m.top -= this.wrapperOffset.top;
            if (k === true) {
                k = e.round(j.offsetWidth / 2 - this.wrapper.offsetWidth / 2)
            }
            if (l === true) {
                l = e.round(j.offsetHeight / 2 - this.wrapper.offsetHeight / 2)
            }
            m.left -= k || 0;
            m.top -= l || 0;
            m.left = m.left > 0 ? 0 : m.left < this.maxScrollX ? this.maxScrollX : m.left;
            m.top = m.top > 0 ? 0 : m.top < this.maxScrollY ? this.maxScrollY : m.top;
            n = n === undefined || n === null || n === "auto" ? e.max(e.abs(this.x - m.left), e.abs(this.y - m.top)) : n;
            this.scrollTo(m.left, m.top, n, i)
        },
        _transitionTime: function(k) {
            k = k || 0;
            this.scrollerStyle[g.style.transitionDuration] = k + "ms";
            if (!k && g.isBadAndroid) {
                this.scrollerStyle[g.style.transitionDuration] = "0.001s"
            }
            if (this.indicators) {
                for (var j = this.indicators.length; j--;) {
                    this.indicators[j].transitionTime(k)
                }
            }
        },
        _transitionTimingFunction: function(j) {
            this.scrollerStyle[g.style.transitionTimingFunction] = j;
            if (this.indicators) {
                for (var k = this.indicators.length; k--;) {
                    this.indicators[k].transitionTimingFunction(j)
                }
            }
        },
        _translate: function(k, l) {
            if (this.options.useTransform) {
                this.scrollerStyle[g.style.transform] = "translate(" + k + "px," + l + "px)" + this.translateZ
            } else {
                k = e.round(k);
                l = e.round(l);
                this.scrollerStyle.left = k + "px";
                this.scrollerStyle.top = l + "px"
            }
            this.x = k;
            this.y = l;
            if (this.indicators) {
                for (var j = this.indicators.length; j--;) {
                    this.indicators[j].updatePosition()
                }
            }
        },
        _initEvents: function(j) {
            var i = j ? g.removeEvent : g.addEvent,
                k = this.options.bindToWrapper ? this.wrapper : h;
            i(h, "orientationchange", this);
            i(h, "resize", this);
            if (this.options.click) {
                i(this.wrapper, "click", this, true)
            }
            if (!this.options.disableMouse) {
                i(this.wrapper, "mousedown", this);
                i(k, "mousemove", this);
                i(k, "mousecancel", this);
                i(k, "mouseup", this)
            }
            if (g.hasPointer && !this.options.disablePointer) {
                i(this.wrapper, "MSPointerDown", this);
                i(k, "MSPointerMove", this);
                i(k, "MSPointerCancel", this);
                i(k, "MSPointerUp", this)
            }
            if (g.hasTouch && !this.options.disableTouch) {
                i(this.wrapper, "touchstart", this);
                i(k, "touchmove", this);
                i(k, "touchcancel", this);
                i(k, "touchend", this)
            }
            i(this.scroller, "transitionend", this);
            i(this.scroller, "webkitTransitionEnd", this);
            i(this.scroller, "oTransitionEnd", this);
            i(this.scroller, "MSTransitionEnd", this)
        },
        getComputedPosition: function() {
            var i = h.getComputedStyle(this.scroller, null),
                j, k;
            if (this.options.useTransform) {
                i = i[g.style.transform].split(")")[0].split(", ");
                j = +(i[12] || i[4]);
                k = +(i[13] || i[5])
            } else {
                j = +i.left.replace(/[^-\d.]/g, "");
                k = +i.top.replace(/[^-\d.]/g, "")
            }
            return {
                x: j,
                y: k
            }
        },
        _initIndicators: function() {
            var o = this.options.interactiveScrollbars,
                k = typeof this.options.scrollbars != "string",
                n = [],
                m;
            var p = this;
            this.indicators = [];
            if (this.options.scrollbars) {
                if (this.options.scrollY) {
                    m = {
                        el: a("v", o, this.options.scrollbars),
                        interactive: o,
                        defaultScrollbars: true,
                        customStyle: k,
                        resize: this.options.resizeScrollbars,
                        shrink: this.options.shrinkScrollbars,
                        fade: this.options.fadeScrollbars,
                        listenX: false
                    };
                    this.wrapper.appendChild(m.el);
                    n.push(m)
                }
                if (this.options.scrollX) {
                    m = {
                        el: a("h", o, this.options.scrollbars),
                        interactive: o,
                        defaultScrollbars: true,
                        customStyle: k,
                        resize: this.options.resizeScrollbars,
                        shrink: this.options.shrinkScrollbars,
                        fade: this.options.fadeScrollbars,
                        listenY: false
                    };
                    this.wrapper.appendChild(m.el);
                    n.push(m)
                }
            }
            if (this.options.indicators) {
                n = n.concat(this.options.indicators)
            }
            for (var l = n.length; l--;) {
                this.indicators.push(new c(this, n[l]))
            }

            function j(q) {
                for (var r = p.indicators.length; r--;) {
                    q.call(p.indicators[r])
                }
            }
            if (this.options.fadeScrollbars) {
                this.on("scrollEnd", function() {
                    j(function() {
                        this.fade()
                    })
                });
                this.on("scrollCancel", function() {
                    j(function() {
                        this.fade()
                    })
                });
                this.on("scrollStart", function() {
                    j(function() {
                        this.fade(1)
                    })
                });
                this.on("beforeScrollStart", function() {
                    j(function() {
                        this.fade(1, true)
                    })
                })
            }
            this.on("refresh", function() {
                j(function() {
                    this.refresh()
                })
            });
            this.on("destroy", function() {
                j(function() {
                    this.destroy()
                });
                delete this.indicators
            })
        },
        _initWheel: function() {
            g.addEvent(this.wrapper, "wheel", this);
            g.addEvent(this.wrapper, "mousewheel", this);
            g.addEvent(this.wrapper, "DOMMouseScroll", this);
            this.on("destroy", function() {
                g.removeEvent(this.wrapper, "wheel", this);
                g.removeEvent(this.wrapper, "mousewheel", this);
                g.removeEvent(this.wrapper, "DOMMouseScroll", this)
            })
        },
        _wheel: function(i) {
            if (!this.enabled) {
                return
            }
            i.preventDefault();
            i.stopPropagation();
            var m, n, j, k, l = this;
            if (this.wheelTimeout === undefined) {
                l._execEvent("scrollStart")
            }
            clearTimeout(this.wheelTimeout);
            this.wheelTimeout = setTimeout(function() {
                l._execEvent("scrollEnd");
                l.wheelTimeout = undefined
            }, 400);
            if ("deltaX" in i) {
                m = -i.deltaX;
                n = -i.deltaY
            } else {
                if ("wheelDeltaX" in i) {
                    m = i.wheelDeltaX / 120 * this.options.mouseWheelSpeed;
                    n = i.wheelDeltaY / 120 * this.options.mouseWheelSpeed
                } else {
                    if ("wheelDelta" in i) {
                        m = n = i.wheelDelta / 120 * this.options.mouseWheelSpeed
                    } else {
                        if ("detail" in i) {
                            m = n = -i.detail / 3 * this.options.mouseWheelSpeed
                        } else {
                            return
                        }
                    }
                }
            }
            m *= this.options.invertWheelDirection;
            n *= this.options.invertWheelDirection;
            if (!this.hasVerticalScroll) {
                m = n;
                n = 0
            }
            if (this.options.snap) {
                j = this.currentPage.pageX;
                k = this.currentPage.pageY;
                if (m > 0) {
                    j--
                } else {
                    if (m < 0) {
                        j++
                    }
                } if (n > 0) {
                    k--
                } else {
                    if (n < 0) {
                        k++
                    }
                }
                this.goToPage(j, k);
                return
            }
            j = this.x + e.round(this.hasHorizontalScroll ? m : 0);
            k = this.y + e.round(this.hasVerticalScroll ? n : 0);
            if (j > 0) {
                j = 0
            } else {
                if (j < this.maxScrollX) {
                    j = this.maxScrollX
                }
            } if (k > 0) {
                k = 0
            } else {
                if (k < this.maxScrollY) {
                    k = this.maxScrollY
                }
            }
            this.scrollTo(j, k, 0)
        },
        _initSnap: function() {
            this.currentPage = {};
            if (typeof this.options.snap == "string") {
                this.options.snap = this.scroller.querySelectorAll(this.options.snap)
            }
            this.on("refresh", function() {
                var p = 0,
                    q, r = 0,
                    s, j, k, v = 0,
                    w, t = this.options.snapStepX || this.wrapperWidth,
                    u = this.options.snapStepY || this.wrapperHeight,
                    o;
                this.pages = [];
                if (!this.wrapperWidth || !this.wrapperHeight || !this.scrollerWidth || !this.scrollerHeight) {
                    return
                }
                if (this.options.snap === true) {
                    j = e.round(t / 2);
                    k = e.round(u / 2);
                    while (v > -this.scrollerWidth) {
                        this.pages[p] = [];
                        q = 0;
                        w = 0;
                        while (w > -this.scrollerHeight) {
                            this.pages[p][q] = {
                                x: e.max(v, this.maxScrollX),
                                y: e.max(w, this.maxScrollY),
                                width: t,
                                height: u,
                                cx: v - j,
                                cy: w - k
                            };
                            w -= u;
                            q++
                        }
                        v -= t;
                        p++
                    }
                } else {
                    o = this.options.snap;
                    q = o.length;
                    s = -1;
                    for (; p < q; p++) {
                        if (p === 0 || o[p].offsetLeft <= o[p - 1].offsetLeft) {
                            r = 0;
                            s++
                        }
                        if (!this.pages[r]) {
                            this.pages[r] = []
                        }
                        v = e.max(-o[p].offsetLeft, this.maxScrollX);
                        w = e.max(-o[p].offsetTop, this.maxScrollY);
                        j = v - e.round(o[p].offsetWidth / 2);
                        k = w - e.round(o[p].offsetHeight / 2);
                        this.pages[r][s] = {
                            x: v,
                            y: w,
                            width: o[p].offsetWidth,
                            height: o[p].offsetHeight,
                            cx: j,
                            cy: k
                        };
                        if (v > this.maxScrollX) {
                            r++
                        }
                    }
                }
                this.goToPage(this.currentPage.pageX || 0, this.currentPage.pageY || 0, 0);
                if (this.options.snapThreshold % 1 === 0) {
                    this.snapThresholdX = this.options.snapThreshold;
                    this.snapThresholdY = this.options.snapThreshold
                } else {
                    this.snapThresholdX = e.round(this.pages[this.currentPage.pageX][this.currentPage.pageY].width * this.options.snapThreshold);
                    this.snapThresholdY = e.round(this.pages[this.currentPage.pageX][this.currentPage.pageY].height * this.options.snapThreshold)
                }
            });
            this.on("flick", function() {
                var i = this.options.snapSpeed || e.max(e.max(e.min(e.abs(this.x - this.startX), 1000), e.min(e.abs(this.y - this.startY), 1000)), 300);
                this.goToPage(this.currentPage.pageX + this.directionX, this.currentPage.pageY + this.directionY, i)
            })
        },
        _nearestSnap: function(o, p) {
            if (!this.pages.length) {
                return {
                    x: 0,
                    y: 0,
                    pageX: 0,
                    pageY: 0
                }
            }
            var j = 0,
                k = this.pages.length,
                n = 0;
            if (e.abs(o - this.absStartX) < this.snapThresholdX && e.abs(p - this.absStartY) < this.snapThresholdY) {
                return this.currentPage
            }
            if (o > 0) {
                o = 0
            } else {
                if (o < this.maxScrollX) {
                    o = this.maxScrollX
                }
            } if (p > 0) {
                p = 0
            } else {
                if (p < this.maxScrollY) {
                    p = this.maxScrollY
                }
            }
            for (; j < k; j++) {
                if (o >= this.pages[j][0].cx) {
                    o = this.pages[j][0].x;
                    break
                }
            }
            k = this.pages[j].length;
            for (; n < k; n++) {
                if (p >= this.pages[0][n].cy) {
                    p = this.pages[0][n].y;
                    break
                }
            }
            if (j == this.currentPage.pageX) {
                j += this.directionX;
                if (j < 0) {
                    j = 0
                } else {
                    if (j >= this.pages.length) {
                        j = this.pages.length - 1
                    }
                }
                o = this.pages[j][0].x
            }
            if (n == this.currentPage.pageY) {
                n += this.directionY;
                if (n < 0) {
                    n = 0
                } else {
                    if (n >= this.pages[0].length) {
                        n = this.pages[0].length - 1
                    }
                }
                p = this.pages[0][n].y
            }
            return {
                x: o,
                y: p,
                pageX: j,
                pageY: n
            }
        },
        goToPage: function(m, n, l, i) {
            i = i || this.options.bounceEasing;
            if (m >= this.pages.length) {
                m = this.pages.length - 1
            } else {
                if (m < 0) {
                    m = 0
                }
            } if (n >= this.pages[m].length) {
                n = this.pages[m].length - 1
            } else {
                if (n < 0) {
                    n = 0
                }
            }
            var j = this.pages[m][n].x,
                k = this.pages[m][n].y;
            l = l === undefined ? this.options.snapSpeed || e.max(e.max(e.min(e.abs(j - this.x), 1000), e.min(e.abs(k - this.y), 1000)), 300) : l;
            this.currentPage = {
                x: j,
                y: k,
                pageX: m,
                pageY: n
            };
            this.scrollTo(j, k, l, i)
        },
        next: function(j, i) {
            var k = this.currentPage.pageX,
                l = this.currentPage.pageY;
            k++;
            if (k >= this.pages.length && this.hasVerticalScroll) {
                k = 0;
                l++
            }
            this.goToPage(k, l, j, i)
        },
        prev: function(j, i) {
            var k = this.currentPage.pageX,
                l = this.currentPage.pageY;
            k--;
            if (k < 0 && this.hasVerticalScroll) {
                k = 0;
                l--
            }
            this.goToPage(k, l, j, i)
        },
        _initKeys: function(j) {
            var l = {
                pageUp: 33,
                pageDown: 34,
                end: 35,
                home: 36,
                left: 37,
                up: 38,
                right: 39,
                down: 40
            };
            var k;
            if (typeof this.options.keyBindings == "object") {
                for (k in this.options.keyBindings) {
                    if (typeof this.options.keyBindings[k] == "string") {
                        this.options.keyBindings[k] = this.options.keyBindings[k].toUpperCase().charCodeAt(0)
                    }
                }
            } else {
                this.options.keyBindings = {}
            }
            for (k in l) {
                this.options.keyBindings[k] = this.options.keyBindings[k] || l[k]
            }
            g.addEvent(h, "keydown", this);
            this.on("destroy", function() {
                g.removeEvent(h, "keydown", this)
            })
        },
        _key: function(j) {
            if (!this.enabled) {
                return
            }
            var p = this.options.snap,
                k = p ? this.currentPage.pageX : this.x,
                l = p ? this.currentPage.pageY : this.y,
                m = g.getTime(),
                o = this.keyTime || 0,
                i = 0.25,
                n;
            if (this.options.useTransition && this.isInTransition) {
                n = this.getComputedPosition();
                this._translate(e.round(n.x), e.round(n.y));
                this.isInTransition = false
            }
            this.keyAcceleration = m - o < 200 ? e.min(this.keyAcceleration + i, 50) : 0;
            switch (j.keyCode) {
                case this.options.keyBindings.pageUp:
                    if (this.hasHorizontalScroll && !this.hasVerticalScroll) {
                        k += p ? 1 : this.wrapperWidth
                    } else {
                        l += p ? 1 : this.wrapperHeight
                    }
                    break;
                case this.options.keyBindings.pageDown:
                    if (this.hasHorizontalScroll && !this.hasVerticalScroll) {
                        k -= p ? 1 : this.wrapperWidth
                    } else {
                        l -= p ? 1 : this.wrapperHeight
                    }
                    break;
                case this.options.keyBindings.end:
                    k = p ? this.pages.length - 1 : this.maxScrollX;
                    l = p ? this.pages[0].length - 1 : this.maxScrollY;
                    break;
                case this.options.keyBindings.home:
                    k = 0;
                    l = 0;
                    break;
                case this.options.keyBindings.left:
                    k += p ? -1 : 5 + this.keyAcceleration >> 0;
                    break;
                case this.options.keyBindings.up:
                    l += p ? 1 : 5 + this.keyAcceleration >> 0;
                    break;
                case this.options.keyBindings.right:
                    k -= p ? -1 : 5 + this.keyAcceleration >> 0;
                    break;
                case this.options.keyBindings.down:
                    l -= p ? 1 : 5 + this.keyAcceleration >> 0;
                    break;
                default:
                    return
            }
            if (p) {
                this.goToPage(k, l);
                return
            }
            if (k > 0) {
                k = 0;
                this.keyAcceleration = 0
            } else {
                if (k < this.maxScrollX) {
                    k = this.maxScrollX;
                    this.keyAcceleration = 0
                }
            } if (l > 0) {
                l = 0;
                this.keyAcceleration = 0
            } else {
                if (l < this.maxScrollY) {
                    l = this.maxScrollY;
                    this.keyAcceleration = 0
                }
            }
            this.scrollTo(k, l, 0);
            this.keyTime = m
        },
        _animate: function(j, k, l, m) {
            var r = this,
                o = this.x,
                p = this.y,
                n = g.getTime(),
                i = n + l;

            function q() {
                var v = g.getTime(),
                    t, u, s;
                if (v >= i) {
                    r.isAnimating = false;
                    r._translate(j, k);
                    if (!r.resetPosition(r.options.bounceTime)) {
                        r._execEvent("scrollEnd")
                    }
                    return
                }
                v = (v - n) / l;
                s = m(v);
                t = (j - o) * s + o;
                u = (k - p) * s + p;
                r._translate(t, u);
                if (r.isAnimating) {
                    f(q)
                }
            }
            this.isAnimating = true;
            q()
        },
        handleEvent: function(i) {
            switch (i.type) {
                case "touchstart":
                case "MSPointerDown":
                case "mousedown":
                    this._start(i);
                    break;
                case "touchmove":
                case "MSPointerMove":
                case "mousemove":
                    this._move(i);
                    break;
                case "touchend":
                case "MSPointerUp":
                case "mouseup":
                case "touchcancel":
                case "MSPointerCancel":
                case "mousecancel":
                    this._end(i);
                    break;
                case "orientationchange":
                case "resize":
                    this._resize();
                    break;
                case "transitionend":
                case "webkitTransitionEnd":
                case "oTransitionEnd":
                case "MSTransitionEnd":
                    this._transitionEnd(i);
                    break;
                case "wheel":
                case "DOMMouseScroll":
                case "mousewheel":
                    this._wheel(i);
                    break;
                case "keydown":
                    this._key(i);
                    break;
                case "click":
                    if (!i._constructed) {
                        i.preventDefault();
                        i.stopPropagation()
                    }
                    break
            }
        }
    };

    function a(i, k, m) {
        var l = b.createElement("div"),
            j = b.createElement("div");
        if (m === true) {
            l.style.cssText = "position:absolute;z-index:9999";
            j.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:absolute;background:rgba(0,0,0,0.5);border:1px solid rgba(255,255,255,0.9);border-radius:3px"
        }
        j.className = "iScrollIndicator";
        if (i == "h") {
            if (m === true) {
                l.style.cssText += ";height:7px;left:2px;right:2px;bottom:0";
                j.style.height = "100%"
            }
            l.className = "iScrollHorizontalScrollbar"
        } else {
            if (m === true) {
                l.style.cssText += ";width:7px;bottom:2px;top:2px;right:1px";
                j.style.width = "100%"
            }
            l.className = "iScrollVerticalScrollbar"
        }
        l.style.cssText += ";overflow:hidden";
        if (!k) {
            l.style.pointerEvents = "none"
        }
        l.appendChild(j);
        return l
    }

    function c(l, k) {
        this.wrapper = typeof k.el == "string" ? b.querySelector(k.el) : k.el;
        this.wrapperStyle = this.wrapper.style;
        this.indicator = this.wrapper.children[0];
        this.indicatorStyle = this.indicator.style;
        this.scroller = l;
        this.options = {
            listenX: true,
            listenY: true,
            interactive: false,
            resize: true,
            defaultScrollbars: false,
            shrink: false,
            fade: false,
            speedRatioX: 0,
            speedRatioY: 0
        };
        for (var j in k) {
            this.options[j] = k[j]
        }
        this.sizeRatioX = 1;
        this.sizeRatioY = 1;
        this.maxPosX = 0;
        this.maxPosY = 0;
        if (this.options.interactive) {
            if (!this.options.disableTouch) {
                g.addEvent(this.indicator, "touchstart", this);
                g.addEvent(h, "touchend", this)
            }
            if (!this.options.disablePointer) {
                g.addEvent(this.indicator, "MSPointerDown", this);
                g.addEvent(h, "MSPointerUp", this)
            }
            if (!this.options.disableMouse) {
                g.addEvent(this.indicator, "mousedown", this);
                g.addEvent(h, "mouseup", this)
            }
        }
        if (this.options.fade) {
            this.wrapperStyle[g.style.transform] = this.scroller.translateZ;
            this.wrapperStyle[g.style.transitionDuration] = g.isBadAndroid ? "0.001s" : "0ms";
            this.wrapperStyle.opacity = "0"
        }
    }
    c.prototype = {
        handleEvent: function(i) {
            switch (i.type) {
                case "touchstart":
                case "MSPointerDown":
                case "mousedown":
                    this._start(i);
                    break;
                case "touchmove":
                case "MSPointerMove":
                case "mousemove":
                    this._move(i);
                    break;
                case "touchend":
                case "MSPointerUp":
                case "mouseup":
                case "touchcancel":
                case "MSPointerCancel":
                case "mousecancel":
                    this._end(i);
                    break
            }
        },
        destroy: function() {
            if (this.options.interactive) {
                g.removeEvent(this.indicator, "touchstart", this);
                g.removeEvent(this.indicator, "MSPointerDown", this);
                g.removeEvent(this.indicator, "mousedown", this);
                g.removeEvent(h, "touchmove", this);
                g.removeEvent(h, "MSPointerMove", this);
                g.removeEvent(h, "mousemove", this);
                g.removeEvent(h, "touchend", this);
                g.removeEvent(h, "MSPointerUp", this);
                g.removeEvent(h, "mouseup", this)
            }
            if (this.options.defaultScrollbars) {
                this.wrapper.parentNode.removeChild(this.wrapper)
            }
        },
        _start: function(i) {
            var j = i.touches ? i.touches[0] : i;
            i.preventDefault();
            i.stopPropagation();
            this.transitionTime();
            this.initiated = true;
            this.moved = false;
            this.lastPointX = j.pageX;
            this.lastPointY = j.pageY;
            this.startTime = g.getTime();
            if (!this.options.disableTouch) {
                g.addEvent(h, "touchmove", this)
            }
            if (!this.options.disablePointer) {
                g.addEvent(h, "MSPointerMove", this)
            }
            if (!this.options.disableMouse) {
                g.addEvent(h, "mousemove", this)
            }
            this.scroller._execEvent("beforeScrollStart")
        },
        _move: function(k) {
            var n = k.touches ? k.touches[0] : k,
                i, j, l, m, o = g.getTime();
            if (!this.moved) {
                this.scroller._execEvent("scrollStart")
            }
            this.moved = true;
            i = n.pageX - this.lastPointX;
            this.lastPointX = n.pageX;
            j = n.pageY - this.lastPointY;
            this.lastPointY = n.pageY;
            l = this.x + i;
            m = this.y + j;
            this._pos(l, m);
            k.preventDefault();
            k.stopPropagation()
        },
        _end: function(i) {
            if (!this.initiated) {
                return
            }
            this.initiated = false;
            i.preventDefault();
            i.stopPropagation();
            g.removeEvent(h, "touchmove", this);
            g.removeEvent(h, "MSPointerMove", this);
            g.removeEvent(h, "mousemove", this);
            if (this.scroller.options.snap) {
                var j = this.scroller._nearestSnap(this.scroller.x, this.scroller.y);
                var k = this.options.snapSpeed || e.max(e.max(e.min(e.abs(this.scroller.x - j.x), 1000), e.min(e.abs(this.scroller.y - j.y), 1000)), 300);
                if (this.scroller.x != j.x || this.scroller.y != j.y) {
                    this.scroller.directionX = 0;
                    this.scroller.directionY = 0;
                    this.scroller.currentPage = j;
                    this.scroller.scrollTo(j.x, j.y, k, this.scroller.options.bounceEasing)
                }
            }
            if (this.moved) {
                this.scroller._execEvent("scrollEnd")
            }
        },
        transitionTime: function(i) {
            i = i || 0;
            this.indicatorStyle[g.style.transitionDuration] = i + "ms";
            if (!i && g.isBadAndroid) {
                this.indicatorStyle[g.style.transitionDuration] = "0.001s"
            }
        },
        transitionTimingFunction: function(i) {
            this.indicatorStyle[g.style.transitionTimingFunction] = i
        },
        refresh: function() {
            this.transitionTime();
            if (this.options.listenX && !this.options.listenY) {
                this.indicatorStyle.display = this.scroller.hasHorizontalScroll ? "block" : "none"
            } else {
                if (this.options.listenY && !this.options.listenX) {
                    this.indicatorStyle.display = this.scroller.hasVerticalScroll ? "block" : "none"
                } else {
                    this.indicatorStyle.display = this.scroller.hasHorizontalScroll || this.scroller.hasVerticalScroll ? "block" : "none"
                }
            } if (this.scroller.hasHorizontalScroll && this.scroller.hasVerticalScroll) {
                g.addClass(this.wrapper, "iScrollBothScrollbars");
                g.removeClass(this.wrapper, "iScrollLoneScrollbar");
                if (this.options.defaultScrollbars && this.options.customStyle) {
                    if (this.options.listenX) {
                        this.wrapper.style.right = "8px"
                    } else {
                        this.wrapper.style.bottom = "8px"
                    }
                }
            } else {
                g.removeClass(this.wrapper, "iScrollBothScrollbars");
                g.addClass(this.wrapper, "iScrollLoneScrollbar");
                if (this.options.defaultScrollbars && this.options.customStyle) {
                    if (this.options.listenX) {
                        this.wrapper.style.right = "2px"
                    } else {
                        this.wrapper.style.bottom = "2px"
                    }
                }
            }
            var i = this.wrapper.offsetHeight;
            if (this.options.listenX) {
                this.wrapperWidth = this.wrapper.clientWidth;
                if (this.options.resize) {
                    this.indicatorWidth = e.max(e.round(this.wrapperWidth * this.wrapperWidth / (this.scroller.scrollerWidth || this.wrapperWidth || 1)), 8);
                    this.indicatorStyle.width = this.indicatorWidth + "px"
                } else {
                    this.indicatorWidth = this.indicator.clientWidth
                }
                this.maxPosX = this.wrapperWidth - this.indicatorWidth;
                if (this.options.shrink == "clip") {
                    this.minBoundaryX = -this.indicatorWidth + 8;
                    this.maxBoundaryX = this.wrapperWidth - 8
                } else {
                    this.minBoundaryX = 0;
                    this.maxBoundaryX = this.maxPosX
                }
                this.sizeRatioX = this.options.speedRatioX || (this.scroller.maxScrollX && (this.maxPosX / this.scroller.maxScrollX))
            }
            if (this.options.listenY) {
                this.wrapperHeight = this.wrapper.clientHeight;
                if (this.options.resize) {
                    this.indicatorHeight = e.max(e.round(this.wrapperHeight * this.wrapperHeight / (this.scroller.scrollerHeight || this.wrapperHeight || 1)), 8);
                    this.indicatorStyle.height = this.indicatorHeight + "px"
                } else {
                    this.indicatorHeight = this.indicator.clientHeight
                }
                this.maxPosY = this.wrapperHeight - this.indicatorHeight;
                if (this.options.shrink == "clip") {
                    this.minBoundaryY = -this.indicatorHeight + 8;
                    this.maxBoundaryY = this.wrapperHeight - 8
                } else {
                    this.minBoundaryY = 0;
                    this.maxBoundaryY = this.maxPosY
                }
                this.maxPosY = this.wrapperHeight - this.indicatorHeight;
                this.sizeRatioY = this.options.speedRatioY || (this.scroller.maxScrollY && (this.maxPosY / this.scroller.maxScrollY))
            }
            this.updatePosition()
        },
        updatePosition: function() {
            var i = this.options.listenX && e.round(this.sizeRatioX * this.scroller.x) || 0,
                j = this.options.listenY && e.round(this.sizeRatioY * this.scroller.y) || 0;
            if (!this.options.ignoreBoundaries) {
                if (i < this.minBoundaryX) {
                    if (this.options.shrink == "scale") {
                        this.width = e.max(this.indicatorWidth + i, 8);
                        this.indicatorStyle.width = this.width + "px"
                    }
                    i = this.minBoundaryX
                } else {
                    if (i > this.maxBoundaryX) {
                        if (this.options.shrink == "scale") {
                            this.width = e.max(this.indicatorWidth - (i - this.maxPosX), 8);
                            this.indicatorStyle.width = this.width + "px";
                            i = this.maxPosX + this.indicatorWidth - this.width
                        } else {
                            i = this.maxBoundaryX
                        }
                    } else {
                        if (this.options.shrink == "scale" && this.width != this.indicatorWidth) {
                            this.width = this.indicatorWidth;
                            this.indicatorStyle.width = this.width + "px"
                        }
                    }
                } if (j < this.minBoundaryY) {
                    if (this.options.shrink == "scale") {
                        this.height = e.max(this.indicatorHeight + j * 3, 8);
                        this.indicatorStyle.height = this.height + "px"
                    }
                    j = this.minBoundaryY
                } else {
                    if (j > this.maxBoundaryY) {
                        if (this.options.shrink == "scale") {
                            this.height = e.max(this.indicatorHeight - (j - this.maxPosY) * 3, 8);
                            this.indicatorStyle.height = this.height + "px";
                            j = this.maxPosY + this.indicatorHeight - this.height
                        } else {
                            j = this.maxBoundaryY
                        }
                    } else {
                        if (this.options.shrink == "scale" && this.height != this.indicatorHeight) {
                            this.height = this.indicatorHeight;
                            this.indicatorStyle.height = this.height + "px"
                        }
                    }
                }
            }
            this.x = i;
            this.y = j;
            if (this.scroller.options.useTransform) {
                this.indicatorStyle[g.style.transform] = "translate(" + i + "px," + j + "px)" + this.scroller.translateZ
            } else {
                this.indicatorStyle.left = i + "px";
                this.indicatorStyle.top = j + "px"
            }
        },
        _pos: function(i, j) {
            if (i < 0) {
                i = 0
            } else {
                if (i > this.maxPosX) {
                    i = this.maxPosX
                }
            } if (j < 0) {
                j = 0
            } else {
                if (j > this.maxPosY) {
                    j = this.maxPosY
                }
            }
            i = this.options.listenX ? e.round(i / this.sizeRatioX) : this.scroller.x;
            j = this.options.listenY ? e.round(j / this.sizeRatioY) : this.scroller.y;
            this.scroller.scrollTo(i, j)
        },
        fade: function(l, j) {
            if (j && !this.visible) {
                return
            }
            clearTimeout(this.fadeTimeout);
            this.fadeTimeout = null;
            var k = l ? 250 : 500,
                i = l ? 0 : 300;
            l = l ? "1" : "0";
            this.wrapperStyle[g.style.transitionDuration] = k + "ms";
            this.fadeTimeout = setTimeout((function(m) {
                this.wrapperStyle.opacity = m;
                this.visible = +m
            }).bind(this, l), i)
        }
    };
    d.utils = g;
    if (typeof module != "undefined" && module.exports) {
        module.exports = d
    } else {
        h.IScroll = d
    }
})(window, document, Math);
/*
 * jQuery Smart Banner
 * Copyright (c) 2012 Arnold Daniels <arnold@jasny.net>
 * Based on 'jQuery Smart Web App Banner' by Kurt Zenisek @ kzeni.com
 */
! function(a) {
    var b = function(d) {
        this.origHtmlMargin = parseFloat(a("html").css("margin-top"));
        this.options = a.extend({}, a.smartbanner.defaults, d);
        var e = navigator.standalone,
            f = navigator.userAgent;
        if (this.options.force) {
            this.type = this.options.force
        } else {
            if (f.match(/iPad|iPhone|iPod/i) != null) {
                if (f.match(/Safari/i) != null && (f.match(/CriOS/i) != null || window.Number(navigator.userAgent.substr(navigator.userAgent.indexOf("OS ") + 3, 3).replace("_", ".")) < 6)) {
                    this.type = "ios"
                }
            } else {
                if (f.match(/Android/i) != null) {
                    this.type = "android"
                } else {
                    if (f.match(/Windows NT 6.2/i) != null && f.match(/Touch/i) !== null) {
                        this.type = "windows"
                    }
                }
            }
        } if (!this.type || e || this.getCookie("sb-closed") || this.getCookie("sb-installed")) {
            return
        }
        this.scale = this.options.scale == "auto" ? a(window).width() / window.screen.width : this.options.scale;
        if (this.scale < 1) {
            this.scale = 1
        }
        var c = a(this.type == "android" ? 'meta[name="google-play-app"]' : this.type == "ios" ? 'meta[name="apple-itunes-app"]' : 'meta[name="msApplication-ID"]');
        if (c.length == 0) {
            return
        }
        if (this.type == "windows") {
            this.pfn = a('meta[name="msApplication-PackageFamilyName"]').attr("content");
            this.appId = c.attr("content")[1]
        } else {
            this.appId = /app-id=([^\s,]+)/.exec(c.attr("content"))[1]
        }
        this.title = this.options.title ? this.options.title : a("title").text().replace(/\s*[|\-·].*$/, "");
        this.author = this.options.author ? this.options.author : (a('meta[name="author"]').length ? a('meta[name="author"]').attr("content") : window.location.hostname);
        this.create();
        this.show();
        this.listen()
    };
    b.prototype = {
        constructor: b,
        create: function() {
            var d, f = (this.options.url ? this.options.url : (this.type == "windows" ? "ms-windows-store:PDP?PFN=" + this.pfn : (this.type == "android" ? "market://details?id=" : "https://itunes.apple.com/" + this.options.appStoreLanguage + "/app/id")) + this.appId),
                e = this.options.price ? this.options.price + " - " + (this.type == "android" ? this.options.inGooglePlay : this.type == "ios" ? this.options.inAppStore : this.options.inWindowsStore) : "",
                c = this.options.iconGloss === null ? (this.type == "ios") : this.options.iconGloss;
            a("body").append('<div id="smartbanner" class="' + this.type + '"><div class="sb-container"><a href="#" class="sb-close">&nbsp;</a><span class="sb-icon"></span><div class="sb-info"><strong>' + this.title + "</strong><span>" + this.author + "</span><span>" + e + '</span></div><a href="' + f + '" class="sb-button"><span>' + this.options.button + "</span></a></div></div>");
            if (this.options.icon) {
                d = this.options.icon
            } else {
                if (a('link[rel="apple-touch-icon-precomposed"]').length > 0) {
                    d = a('link[rel="apple-touch-icon-precomposed"]').attr("href");
                    if (this.options.iconGloss === null) {
                        c = false
                    }
                } else {
                    if (a('link[rel="apple-touch-icon"]').length > 0) {
                        d = a('link[rel="apple-touch-icon"]').attr("href")
                    } else {
                        if (a('meta[name="msApplication-TileImage"]').length > 0) {
                            d = a('meta[name="msApplication-TileImage"]').attr("content")
                        } else {
                            if (a('meta[name="msapplication-TileImage"]').length > 0) {
                                d = a('meta[name="msapplication-TileImage"]').attr("content")
                            }
                        }
                    }
                }
            } if (d) {
                a("#smartbanner .sb-icon").css("background-image", "url(" + d + ")");
                if (c) {
                    a("#smartbanner .sb-icon").addClass("gloss")
                }
            } else {
                a("#smartbanner").addClass("no-icon")
            }
            this.bannerHeight = a("#smartbanner").outerHeight() + 2;
            if (this.scale > 1) {
                a("#smartbanner").css("top", parseFloat(a("#smartbanner").css("top")) * this.scale).css("height", parseFloat(a("#smartbanner").css("height")) * this.scale);
                a("#smartbanner .sb-container").css("-webkit-transform", "scale(" + this.scale + ")").css("-msie-transform", "scale(" + this.scale + ")").css("-moz-transform", "scale(" + this.scale + ")").css("width", a(window).width() / this.scale)
            }
        },
        listen: function() {
            a("#smartbanner .sb-close").on("click", a.proxy(this.close, this));
            a("#smartbanner .sb-button").on("click", a.proxy(this.install, this))
        },
        show: function(c) {
            a("#smartbanner").stop().animate({
                top: 0
            }, this.options.speedIn).addClass("shown");
            a("html").animate({
                marginTop: this.origHtmlMargin + (this.bannerHeight * this.scale)
            }, this.options.speedIn, "swing", c);
            a("div.header").css("top", "43px")
        },
        hide: function(c) {
            a("#smartbanner").stop().animate({
                top: -1 * this.bannerHeight * this.scale
            }, this.options.speedOut).removeClass("shown");
            a("html").animate({
                marginTop: this.origHtmlMargin
            }, this.options.speedOut, "swing", c);
            a("div.header").css("top", "0")
        },
        close: function(c) {
            c.preventDefault();
            this.hide();
            this.setCookie("sb-closed", "true", this.options.daysHidden)
        },
        install: function(c) {
            this.hide();
            this.setCookie("sb-installed", "true", this.options.daysReminder)
        },
        setCookie: function(e, f, d) {
            var c = new Date();
            c.setDate(c.getDate() + d);
            f = escape(f) + ((d == null) ? "" : "; expires=" + c.toUTCString());
            document.cookie = e + "=" + f + "; path=/;"
        },
        getCookie: function(e) {
            var d, f, g, c = document.cookie.split(";");
            for (d = 0; d < c.length; d++) {
                f = c[d].substr(0, c[d].indexOf("="));
                g = c[d].substr(c[d].indexOf("=") + 1);
                f = f.replace(/^\s+|\s+$/g, "");
                if (f == e) {
                    return unescape(g)
                }
            }
            return null
        },
        switchType: function() {
            var c = this;
            this.hide(function() {
                c.type = c.type == "android" ? "ios" : "android";
                var d = a(c.type == "android" ? 'meta[name="google-play-app"]' : 'meta[name="apple-itunes-app"]').attr("content");
                c.appId = /app-id=([^\s,]+)/.exec(d)[1];
                a("#smartbanner").detach();
                c.create();
                c.show()
            })
        }
    };
    a.smartbanner = function(e) {
        var c = a(window),
            d = c.data("typeahead"),
            f = typeof e == "object" && e;
        if (!d) {
            c.data("typeahead", (d = new b(f)))
        }
        if (typeof e == "string") {
            d[e]()
        }
    };
    a.smartbanner.defaults = {
        title: null,
        author: null,
        price: "FREE",
        appStoreLanguage: "us",
        inAppStore: "On the App Store",
        inGooglePlay: "In Google Play",
        inWindowsStore: "In the Windows Store",
        icon: null,
        iconGloss: null,
        button: "VIEW",
        url: null,
        scale: "auto",
        speedIn: 300,
        speedOut: 400,
        daysHidden: 15,
        daysReminder: 90,
        force: null
    };
    a.smartbanner.Constructor = b
}(window.jQuery);
window.Modernizr = function(G, I, K) {
    function ak(b) {
        U.cssText = b
    }

    function al(c, d) {
        return ak(X.join(c + ";") + (d || ""))
    }

    function H(c, d) {
        return typeof c === d
    }

    function J(c, d) {
        return !!~("" + c).indexOf(d)
    }

    function L(c, f) {
        for (var g in c) {
            var h = c[g];
            if (!J(h, "-") && U[h] !== K) {
                return f == "pfx" ? h : !0
            }
        }
        return !1
    }

    function N(c, g, h) {
        for (var i in c) {
            var j = g[c[i]];
            if (j !== K) {
                return h === !1 ? c[i] : H(j, "function") ? j.bind(h || g) : j
            }
        }
        return !1
    }

    function P(f, g, h) {
        var i = f.charAt(0).toUpperCase() + f.slice(1),
            j = (f + " " + Z.join(i + " ") + i).split(" ");
        return H(g, "string") || H(g, "undefined") ? L(j, g) : (j = (f + " " + aa.join(i + " ") + i).split(" "), N(j, g, h))
    }
    var M = "2.6.2",
        O = {}, Q = I.documentElement,
        S = "modernizr",
        T = I.createElement(S),
        U = T.style,
        V, W = {}.toString,
        X = " -webkit- -moz- -o- -ms- ".split(" "),
        Y = "Webkit Moz O ms",
        Z = Y.split(" "),
        aa = Y.toLowerCase().split(" "),
        ab = {}, ac = {}, ad = {}, ae = [],
        af = ae.slice,
        ag, ah = function(b, f, g, o) {
            var p, q, r, s, t = I.createElement("div"),
                u = I.body,
                v = u || I.createElement("body");
            if (parseInt(g, 10)) {
                while (g--) {
                    r = I.createElement("div"), r.id = o ? o[g] : S + (g + 1), t.appendChild(r)
                }
            }
            return p = ["&#173;", '<style id="s', S, '">', b, "</style>"].join(""), t.id = S, (u ? t : v).innerHTML += p, v.appendChild(t), u || (v.style.background = "", v.style.overflow = "hidden", s = Q.style.overflow, Q.style.overflow = "hidden", Q.appendChild(v)), q = f(t, b), u ? t.parentNode.removeChild(t) : (v.parentNode.removeChild(v), Q.style.overflow = s), !! q
        }, ai = {}.hasOwnProperty,
        aj;
    !H(ai, "undefined") && !H(ai.call, "undefined") ? aj = function(c, d) {
        return ai.call(c, d)
    } : aj = function(c, d) {
        return d in c && H(c.constructor.prototype[d], "undefined")
    }, Function.prototype.bind || (Function.prototype.bind = function(a) {
        var f = this;
        if (typeof f != "function") {
            throw new TypeError
        }
        var g = af.call(arguments, 1),
            h = function() {
                if (this instanceof h) {
                    var b = function() {};
                    b.prototype = f.prototype;
                    var c = new b,
                        d = f.apply(c, g.concat(af.call(arguments)));
                    return Object(d) === d ? d : c
                }
                return f.apply(a, g.concat(af.call(arguments)))
            };
        return h
    }), ab.cssanimations = function() {
        return P("animationName")
    }, ab.csstransforms = function() {
        return !!P("transform")
    }, ab.csstransforms3d = function() {
        var b = !! P("perspective");
        return b && "webkitPerspective" in Q.style && ah("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}", function(a, d) {
            b = a.offsetLeft === 9 && a.offsetHeight === 3
        }), b
    }, ab.csstransitions = function() {
        return P("transition")
    };
    for (var R in ab) {
        aj(ab, R) && (ag = R.toLowerCase(), O[ag] = ab[R](), ae.push((O[ag] ? "" : "no-") + ag))
    }
    return O.addTest = function(c, e) {
        if (typeof c == "object") {
            for (var f in c) {
                aj(c, f) && O.addTest(f, c[f])
            }
        } else {
            c = c.toLowerCase();
            if (O[c] !== K) {
                return O
            }
            e = typeof e == "function" ? e() : e, typeof enableClasses != "undefined" && enableClasses && (Q.className += " " + (e ? "" : "no-") + c), O[c] = e
        }
        return O
    }, ak(""), T = V = null, O._version = M, O._prefixes = X, O._domPrefixes = aa, O._cssomPrefixes = Z, O.testProp = function(b) {
        return L([b])
    }, O.testAllProps = P, O.testStyles = ah, O
}(this, this.document);
/* Hammer.JS - v1.0.6dev - 2013-04-10
 * http://eightmedia.github.com/hammer.js
 *
 * Copyright (c) 2013 Jorik Tangelder <j.tangelder@gmail.com>;
 * Licensed under the MIT license */
(function(h, a) {
    function c() {
        if (!b.READY) {
            b.event.determineEventTypes();
            for (var e in b.gestures) {
                b.gestures.hasOwnProperty(e) && b.detection.register(b.gestures[e])
            }
            b.event.onTouch(b.DOCUMENT, b.EVENT_MOVE, b.detection.detect), b.event.onTouch(b.DOCUMENT, b.EVENT_END, b.detection.detect), b.READY = !0
        }
    }
    var b = function(j, i) {
        return new b.Instance(j, i || {})
    };
    b.defaults = {
        stop_browser_behavior: {
            userSelect: "none",
            touchAction: "none",
            touchCallout: "none",
            contentZooming: "none",
            userDrag: "none",
            tapHighlightColor: "rgba(0,0,0,0)"
        }
    }, b.HAS_POINTEREVENTS = navigator.pointerEnabled || navigator.msPointerEnabled, b.HAS_TOUCHEVENTS = "ontouchstart" in h, b.MOBILE_REGEX = /mobile|tablet|ip(ad|hone|od)|android/i, b.NO_MOUSEEVENTS = b.HAS_TOUCHEVENTS && navigator.userAgent.match(b.MOBILE_REGEX), b.EVENT_TYPES = {}, b.DIRECTION_DOWN = "down", b.DIRECTION_LEFT = "left", b.DIRECTION_UP = "up", b.DIRECTION_RIGHT = "right", b.POINTER_MOUSE = "mouse", b.POINTER_TOUCH = "touch", b.POINTER_PEN = "pen", b.EVENT_START = "start", b.EVENT_MOVE = "move", b.EVENT_END = "end", b.DOCUMENT = document, b.plugins = {}, b.READY = !1, b.Instance = function(k, i) {
        var j = this;
        return c(), this.element = k, this.enabled = !0, this.options = b.utils.extend(b.utils.extend({}, b.defaults), i || {}), this.options.stop_browser_behavior && b.utils.stopDefaultBrowserBehavior(this.element, this.options.stop_browser_behavior), b.event.onTouch(k, b.EVENT_START, function(e) {
            j.enabled && b.detection.startDetect(j, e)
        }), this
    }, b.Instance.prototype = {
        on: function(m, j) {
            for (var l = m.split(" "), k = 0; l.length > k; k++) {
                this.element.addEventListener(l[k], j, !1)
            }
            return this
        },
        off: function(m, j) {
            for (var l = m.split(" "), k = 0; l.length > k; k++) {
                this.element.removeEventListener(l[k], j, !1)
            }
            return this
        },
        trigger: function(l, i) {
            var j = b.DOCUMENT.createEvent("Event");
            j.initEvent(l, !0, !0), j.gesture = i;
            var k = this.element;
            return b.utils.hasParent(i.target, k) && (k = i.target), k.dispatchEvent(j), this
        },
        enable: function(e) {
            return this.enabled = e, this
        }
    };
    var f = null,
        d = !1,
        g = !1;
    b.event = {
        bindDom: function(o, j, l) {
            for (var k = j.split(" "), m = 0; k.length > m; m++) {
                o.addEventListener(k[m], l, !1)
            }
        },
        onTouch: function(l, j, k) {
            var i = this;
            this.bindDom(l, b.EVENT_TYPES[j], function(e) {
                var n = e.type.toLowerCase();
                if (!n.match(/mouse/) || !g) {
                    n.match(/touch/) || n.match(/pointerdown/) || n.match(/mouse/) && 1 === e.which ? d = !0 : n.match(/mouse/) && 1 !== e.which && (d = !1), n.match(/touch|pointer/) && (g = !0);
                    var m = 0;
                    d && (b.HAS_POINTEREVENTS && j != b.EVENT_END ? m = b.PointerEvent.updatePointer(j, e) : n.match(/touch/) ? m = e.touches.length : g || (m = n.match(/up/) ? 0 : 1), m > 0 && j == b.EVENT_END ? j = b.EVENT_MOVE : m || (j = b.EVENT_END), m || null === f ? f = e : e = f, k.call(b.detection, i.collectEventData(l, j, e)), b.HAS_POINTEREVENTS && j == b.EVENT_END && (m = b.PointerEvent.updatePointer(j, e))), m || (f = null, d = !1, g = !1, b.PointerEvent.reset())
                }
            })
        },
        determineEventTypes: function() {
            var e;
            e = b.HAS_POINTEREVENTS ? b.PointerEvent.getEvents() : b.NO_MOUSEEVENTS ? ["touchstart", "touchmove", "touchend touchcancel"] : ["touchstart mousedown", "touchmove mousemove", "touchend touchcancel mouseup"], b.EVENT_TYPES[b.EVENT_START] = e[0], b.EVENT_TYPES[b.EVENT_MOVE] = e[1], b.EVENT_TYPES[b.EVENT_END] = e[2]
        },
        getTouchList: function(e) {
            return b.HAS_POINTEREVENTS ? b.PointerEvent.getTouchList() : e.touches ? e.touches : [{
                identifier: 1,
                pageX: e.pageX,
                pageY: e.pageY,
                target: e.target
            }]
        },
        collectEventData: function(m, i, j) {
            var l = this.getTouchList(j, i),
                k = b.POINTER_TOUCH;
            return (j.type.match(/mouse/) || b.PointerEvent.matchType(b.POINTER_MOUSE, j)) && (k = b.POINTER_MOUSE), {
                center: b.utils.getCenter(l),
                timeStamp: (new Date).getTime(),
                target: j.target,
                touches: l,
                eventType: i,
                pointerType: k,
                srcEvent: j,
                preventDefault: function() {
                    this.srcEvent.preventManipulation && this.srcEvent.preventManipulation(), this.srcEvent.preventDefault && this.srcEvent.preventDefault()
                },
                stopPropagation: function() {
                    this.srcEvent.stopPropagation()
                },
                stopDetect: function() {
                    return b.detection.stopDetect()
                }
            }
        }
    }, b.PointerEvent = {
        pointers: {},
        getTouchList: function() {
            var j = this,
                i = [];
            return Object.keys(j.pointers).sort().forEach(function(e) {
                i.push(j.pointers[e])
            }), i
        },
        updatePointer: function(j, i) {
            return j == b.EVENT_END ? this.pointers = {} : (i.identifier = i.pointerId, this.pointers[i.pointerId] = i), Object.keys(this.pointers).length
        },
        matchType: function(k, i) {
            if (!i.pointerType) {
                return !1
            }
            var j = {};
            return j[b.POINTER_MOUSE] = i.pointerType == i.MSPOINTER_TYPE_MOUSE || i.pointerType == b.POINTER_MOUSE, j[b.POINTER_TOUCH] = i.pointerType == i.MSPOINTER_TYPE_TOUCH || i.pointerType == b.POINTER_TOUCH, j[b.POINTER_PEN] = i.pointerType == i.MSPOINTER_TYPE_PEN || i.pointerType == b.POINTER_PEN, j[k]
        },
        getEvents: function() {
            return ["pointerdown MSPointerDown", "pointermove MSPointerMove", "pointerup pointercancel MSPointerUp MSPointerCancel"]
        },
        reset: function() {
            this.pointers = {}
        }
    }, b.utils = {
        extend: function(l, j, e) {
            for (var k in j) {
                l[k] !== a && e || (l[k] = j[k])
            }
            return l
        },
        hasParent: function(j, i) {
            for (; j;) {
                if (j == i) {
                    return !0
                }
                j = j.parentNode
            }
            return !1
        },
        getCenter: function(o) {
            for (var j = [], l = [], k = 0, m = o.length; m > k; k++) {
                j.push(o[k].pageX), l.push(o[k].pageY)
            }
            return {
                pageX: (Math.min.apply(Math, j) + Math.max.apply(Math, j)) / 2,
                pageY: (Math.min.apply(Math, l) + Math.max.apply(Math, l)) / 2
            }
        },
        getVelocity: function(k, i, j) {
            return {
                x: Math.abs(i / k) || 0,
                y: Math.abs(j / k) || 0
            }
        },
        getAngle: function(m, j) {
            var l = j.pageY - m.pageY,
                k = j.pageX - m.pageX;
            return 180 * Math.atan2(l, k) / Math.PI
        },
        getDirection: function(l, i) {
            var j = Math.abs(l.pageX - i.pageX),
                k = Math.abs(l.pageY - i.pageY);
            return j >= k ? l.pageX - i.pageX > 0 ? b.DIRECTION_LEFT : b.DIRECTION_RIGHT : l.pageY - i.pageY > 0 ? b.DIRECTION_UP : b.DIRECTION_DOWN
        },
        getDistance: function(m, j) {
            var l = j.pageX - m.pageX,
                k = j.pageY - m.pageY;
            return Math.sqrt(l * l + k * k)
        },
        getScale: function(j, i) {
            return j.length >= 2 && i.length >= 2 ? this.getDistance(i[0], i[1]) / this.getDistance(j[0], j[1]) : 1
        },
        getRotation: function(j, i) {
            return j.length >= 2 && i.length >= 2 ? this.getAngle(i[1], i[0]) - this.getAngle(j[1], j[0]) : 0
        },
        isVertical: function(e) {
            return e == b.DIRECTION_UP || e == b.DIRECTION_DOWN
        },
        stopDefaultBrowserBehavior: function(q, j) {
            var l, k = ["webkit", "khtml", "moz", "ms", "o", ""];
            if (j && q.style) {
                for (var p = 0; k.length > p; p++) {
                    for (var m in j) {
                        j.hasOwnProperty(m) && (l = m, k[p] && (l = k[p] + l.substring(0, 1).toUpperCase() + l.substring(1)), q.style[l] = j[m])
                    }
                }
                "none" == j.userSelect && (q.onselectstart = function() {
                    return !1
                })
            }
        }
    }, b.detection = {
        gestures: [],
        current: null,
        previous: null,
        stopped: !1,
        startDetect: function(j, i) {
            this.current || (this.stopped = !1, this.current = {
                inst: j,
                startEvent: b.utils.extend({}, i),
                lastEvent: !1,
                name: ""
            }, this.detect(i))
        },
        detect: function(m) {
            if (this.current && !this.stopped) {
                m = this.extendEventData(m);
                for (var i = this.current.inst.options, j = 0, l = this.gestures.length; l > j; j++) {
                    var k = this.gestures[j];
                    if (!this.stopped && i[k.name] !== !1 && k.handler.call(k, m, this.current.inst) === !1) {
                        this.stopDetect();
                        break
                    }
                }
                return this.current && (this.current.lastEvent = m), m.eventType == b.EVENT_END && !m.touches.length - 1 && this.stopDetect(), m
            }
        },
        stopDetect: function() {
            this.previous = b.utils.extend({}, this.current), this.current = null, this.stopped = !0
        },
        extendEventData: function(u) {
            var k = this.current.startEvent;
            if (k && (u.touches.length != k.touches.length || u.touches === k.touches)) {
                k.touches = [];
                for (var l = 0, p = u.touches.length; p > l; l++) {
                    k.touches.push(b.utils.extend({}, u.touches[l]))
                }
            }
            var m = u.timeStamp - k.timeStamp,
                q = u.center.pageX - k.center.pageX,
                i = u.center.pageY - k.center.pageY,
                j = b.utils.getVelocity(m, q, i);
            return b.utils.extend(u, {
                deltaTime: m,
                deltaX: q,
                deltaY: i,
                velocityX: j.x,
                velocityY: j.y,
                distance: b.utils.getDistance(k.center, u.center),
                angle: b.utils.getAngle(k.center, u.center),
                direction: b.utils.getDirection(k.center, u.center),
                scale: b.utils.getScale(k.touches, u.touches),
                rotation: b.utils.getRotation(k.touches, u.touches),
                startEvent: k
            }), u
        },
        register: function(i) {
            var e = i.defaults || {};
            return e[i.name] === a && (e[i.name] = !0), b.utils.extend(b.defaults, e, !0), i.index = i.index || 1000, this.gestures.push(i), this.gestures.sort(function(k, j) {
                return k.index < j.index ? -1 : k.index > j.index ? 1 : 0
            }), this.gestures
        }
    }, b.gestures = b.gestures || {}, b.gestures.Hold = {
        name: "hold",
        index: 10,
        defaults: {
            hold_timeout: 500,
            hold_threshold: 1
        },
        timer: null,
        handler: function(j, i) {
            switch (j.eventType) {
                case b.EVENT_START:
                    clearTimeout(this.timer), b.detection.current.name = this.name, this.timer = setTimeout(function() {
                        "hold" == b.detection.current.name && i.trigger("hold", j)
                    }, i.options.hold_timeout);
                    break;
                case b.EVENT_MOVE:
                    j.distance > i.options.hold_threshold && clearTimeout(this.timer);
                    break;
                case b.EVENT_END:
                    clearTimeout(this.timer)
            }
        }
    }, b.gestures.Tap = {
        name: "tap",
        index: 100,
        defaults: {
            tap_max_touchtime: 250,
            tap_max_distance: 10,
            tap_always: !0,
            doubletap_distance: 20,
            doubletap_interval: 300
        },
        handler: function(l, i) {
            if (l.eventType == b.EVENT_END) {
                var j = b.detection.previous,
                    k = !1;
                if (l.deltaTime > i.options.tap_max_touchtime || l.distance > i.options.tap_max_distance) {
                    return
                }
                j && "tap" == j.name && l.timeStamp - j.lastEvent.timeStamp < i.options.doubletap_interval && l.distance < i.options.doubletap_distance && (i.trigger("doubletap", l), k = !0), (!k || i.options.tap_always) && (b.detection.current.name = "tap", i.trigger(b.detection.current.name, l))
            }
        }
    }, b.gestures.Swipe = {
        name: "swipe",
        index: 40,
        defaults: {
            swipe_max_touches: 1,
            swipe_velocity: 0.7
        },
        handler: function(j, i) {
            if (j.eventType == b.EVENT_END) {
                if (i.options.swipe_max_touches > 0 && j.touches.length > i.options.swipe_max_touches) {
                    return
                }(j.velocityX > i.options.swipe_velocity || j.velocityY > i.options.swipe_velocity) && (i.trigger(this.name, j), i.trigger(this.name + j.direction, j))
            }
        }
    }, b.gestures.Drag = {
        name: "drag",
        index: 50,
        defaults: {
            drag_min_distance: 10,
            drag_max_touches: 1,
            drag_block_horizontal: !1,
            drag_block_vertical: !1,
            drag_lock_to_axis: !1,
            drag_lock_min_distance: 25
        },
        triggered: !1,
        handler: function(j, e) {
            if (b.detection.current.name != this.name && this.triggered) {
                return e.trigger(this.name + "end", j), this.triggered = !1, a
            }
            if (!(e.options.drag_max_touches > 0 && j.touches.length > e.options.drag_max_touches)) {
                switch (j.eventType) {
                    case b.EVENT_START:
                        this.triggered = !1;
                        break;
                    case b.EVENT_MOVE:
                        if (j.distance < e.options.drag_min_distance && b.detection.current.name != this.name) {
                            return
                        }
                        b.detection.current.name = this.name, (b.detection.current.lastEvent.drag_locked_to_axis || e.options.drag_lock_to_axis && e.options.drag_lock_min_distance <= j.distance) && (j.drag_locked_to_axis = !0);
                        var i = b.detection.current.lastEvent.direction;
                        j.drag_locked_to_axis && i !== j.direction && (j.direction = b.utils.isVertical(i) ? 0 > j.deltaY ? b.DIRECTION_UP : b.DIRECTION_DOWN : 0 > j.deltaX ? b.DIRECTION_LEFT : b.DIRECTION_RIGHT), this.triggered || (e.trigger(this.name + "start", j), this.triggered = !0), e.trigger(this.name, j), e.trigger(this.name + j.direction, j), (e.options.drag_block_vertical && b.utils.isVertical(j.direction) || e.options.drag_block_horizontal && !b.utils.isVertical(j.direction)) && j.preventDefault();
                        break;
                    case b.EVENT_END:
                        this.triggered && e.trigger(this.name + "end", j), this.triggered = !1
                }
            }
        }
    }, b.gestures.Transform = {
        name: "transform",
        index: 45,
        defaults: {
            transform_min_scale: 0.01,
            transform_min_rotation: 1,
            transform_always_block: !1
        },
        triggered: !1,
        handler: function(k, e) {
            if (b.detection.current.name != this.name && this.triggered) {
                return e.trigger(this.name + "end", k), this.triggered = !1, a
            }
            if (!(2 > k.touches.length)) {
                switch (e.options.transform_always_block && k.preventDefault(), k.eventType) {
                    case b.EVENT_START:
                        this.triggered = !1;
                        break;
                    case b.EVENT_MOVE:
                        var j = Math.abs(1 - k.scale),
                            i = Math.abs(k.rotation);
                        if (e.options.transform_min_scale > j && e.options.transform_min_rotation > i) {
                            return
                        }
                        b.detection.current.name = this.name, this.triggered || (e.trigger(this.name + "start", k), this.triggered = !0), e.trigger(this.name, k), i > e.options.transform_min_rotation && e.trigger("rotate", k), j > e.options.transform_min_scale && (e.trigger("pinch", k), e.trigger("pinch" + (1 > k.scale ? "in" : "out"), k));
                        break;
                    case b.EVENT_END:
                        this.triggered && e.trigger(this.name + "end", k), this.triggered = !1
                }
            }
        }
    }, b.gestures.Touch = {
        name: "touch",
        index: -1 / 0,
        defaults: {
            prevent_default: !1,
            prevent_mouseevents: !1
        },
        handler: function(i, e) {
            return e.options.prevent_mouseevents && i.pointerType == b.POINTER_MOUSE ? (i.stopDetect(), a) : (e.options.prevent_default && i.preventDefault(), i.eventType == b.EVENT_START && e.trigger(this.name, i), a)
        }
    }, b.gestures.Release = {
        name: "release",
        index: 1 / 0,
        handler: function(j, i) {
            j.eventType == b.EVENT_END && i.trigger(this.name, j)
        }
    }, "object" == typeof module && "object" == typeof module.exports ? module.exports = b : (h.Hammer = b, "function" == typeof h.define && h.define.amd && h.define("hammer", [], function() {
        return b
    }))
})(this),
    function(b, a) {
        b !== a && (Hammer.event.bindDom = function(d, c, e) {
            b(d).on(c, function(g) {
                var f = g.originalEvent || g;
                f.pageX === a && (f.pageX = g.pageX, f.pageY = g.pageY), f.target || (f.target = g.target), f.which === a && (f.which = f.button), f.preventDefault || (f.preventDefault = g.preventDefault), f.stopPropagation || (f.stopPropagation = g.stopPropagation), e.call(this, f)
            })
        }, Hammer.Instance.prototype.on = function(c, d) {
            return b(this.element).on(c, d)
        }, Hammer.Instance.prototype.off = function(c, d) {
            return b(this.element).off(c, d)
        }, Hammer.Instance.prototype.trigger = function(c, f) {
            var d = b(this.element);
            return d.has(f.target).length && (d = b(f.target)), d.trigger({
                type: c,
                gesture: f
            })
        }, b.fn.hammer = function(c) {
            return this.each(function() {
                var e = b(this),
                    d = e.data("hammer");
                d ? d && c && Hammer.utils.extend(d.options, c) : e.data("hammer", new Hammer(this, c || {}))
            })
        })
    }(window.jQuery || window.Zepto);
(function(y) {
    var P = y.fn.domManip,
        B = "_tmplitem",
        O = /^[^<]*(<[\w\W]+>)[^>]*$|\{\{\! /,
        z = {}, D = {}, C, N = {
            key: 0,
            data: {}
        }, G = 0,
        A = 0,
        J = [];

    function E(i, b, j, f) {
        var a = {
            data: f || (f === 0 || f === false) ? f : b ? b.data : {},
            _wrap: b ? b._wrap : null,
            tmpl: null,
            parent: b || null,
            nodes: [],
            calls: S,
            nest: U,
            wrap: V,
            html: T,
            update: R
        };
        i && y.extend(a, i, {
            nodes: [],
            parent: b
        });
        if (j) {
            a.tmpl = j;
            a._ctnt = a._ctnt || a.tmpl(y, a);
            a.key = ++G;
            (J.length ? D : z)[G] = a
        }
        return a
    }
    y.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(b, a) {
        y.fn[b] = function(r) {
            var c = [],
                e = y(r),
                o, d, q, p, f = this.length === 1 && this[0].parentNode;
            C = z || {};
            if (f && f.nodeType === 11 && f.childNodes.length === 1 && e.length === 1) {
                e[a](this[0]);
                c = this
            } else {
                for (d = 0, q = e.length; d < q; d++) {
                    A = d;
                    o = (d > 0 ? this.clone(true) : this).get();
                    y(e[d])[a](o);
                    c = c.concat(o)
                }
                A = 0;
                c = this.pushStack(c, b, e.selector)
            }
            p = C;
            C = null;
            y.tmpl.complete(p);
            return c
        }
    });
    y.fn.extend({
        tmpl: function(f, e, a) {
            return y.tmpl(this[0], f, e, a)
        },
        tmplItem: function() {
            return y.tmplItem(this[0])
        },
        template: function(a) {
            return y.template(a, this[0])
        },
        domManip: function(a, p, o) {
            if (a[0] && y.isArray(a[0])) {
                var c = y.makeArray(arguments),
                    e = a[0],
                    n = e.length,
                    l = 0,
                    b;
                while (l < n && !(b = y.data(e[l++], "tmplItem"))) {}
                if (b && A) {
                    c[2] = function(d) {
                        y.tmpl.afterManip(this, d, o)
                    }
                }
                P.apply(this, c)
            } else {
                P.apply(this, arguments)
            }
            A = 0;
            !C && y.tmpl.complete(z);
            return this
        }
    });
    y.extend({
        tmpl: function(b, g, f, a) {
            var j, l = !a;
            if (l) {
                a = N;
                b = y.template[b] || y.template(null, b);
                D = {}
            } else {
                if (!b) {
                    b = a.tmpl;
                    z[a.key] = a;
                    a.nodes = [];
                    a.wrapped && L(a, a.wrapped);
                    return y(H(a, null, a.tmpl(y, a)))
                }
            } if (!b) {
                return []
            }
            if (typeof g === "function") {
                g = g.call(a || {})
            }
            f && f.wrapped && L(f, f.wrapped);
            j = y.isArray(g) ? y.map(g, function(c) {
                return c ? E(f, a, b, c) : null
            }) : [E(f, a, b, g)];
            return l ? y(H(a, null, j)) : j
        },
        tmplItem: function(a) {
            var d;
            if (a instanceof y) {
                a = a[0]
            }
            while (a && a.nodeType === 1 && !(d = y.data(a, "tmplItem")) && (a = a.parentNode)) {}
            return d || N
        },
        template: function(d, a) {
            if (a) {
                if (typeof a === "string") {
                    a = M(a)
                } else {
                    if (a instanceof y) {
                        a = a[0] || {}
                    }
                } if (a.nodeType) {
                    a = y.data(a, "tmpl") || y.data(a, "tmpl", M(a.innerHTML))
                }
                return typeof d === "string" ? (y.template[d] = a) : a
            }
            return d ? typeof d !== "string" ? y.template(null, d) : y.template[d] || y.template(null, O.test(d) ? d : y(d)) : null
        },
        encode: function(b) {
            return ("" + b).split("<").join("&lt;").split(">").join("&gt;").split('"').join("&#34;").split("'").join("&#39;")
        }
    });
    y.extend(y.tmpl, {
        tag: {
            tmpl: {
                _default: {
                    $2: "null"
                },
                open: "if($notnull_1){__=__.concat($item.nest($1,$2));}"
            },
            wrap: {
                _default: {
                    $2: "null"
                },
                open: "$item.calls(__,$1,$2);__=[];",
                close: "call=$item.calls();__=call._.concat($item.wrap(call,__));"
            },
            each: {
                _default: {
                    $2: "$index, $value"
                },
                open: "if($notnull_1){$.each($1a,function($2){with(this){",
                close: "}});}"
            },
            "if": {
                open: "if(($notnull_1) && $1a){",
                close: "}"
            },
            "else": {
                _default: {
                    $1: "true"
                },
                open: "}else if(($notnull_1) && $1a){"
            },
            html: {
                open: "if($notnull_1){__.push($1a);}"
            },
            "=": {
                _default: {
                    $1: "$data"
                },
                open: "if($notnull_1){__.push($.encode($1a));}"
            },
            "!": {
                open: ""
            }
        },
        complete: function() {
            z = {}
        },
        afterManip: function(h, a, c) {
            var g = a.nodeType === 11 ? y.makeArray(a.childNodes) : a.nodeType === 1 ? [a] : [];
            c.call(h, a);
            K(g);
            A++
        }
    });

    function H(h, j, i) {
        var a, d = i ? y.map(i, function(b) {
            return typeof b === "string" ? h.key ? b.replace(/(<\w+)(?=[\s>])(?![^>]*_tmplitem)([^>]*)/g, "$1 " + B + '="' + h.key + '" $2') : b : H(b, h, b._ctnt)
        }) : h;
        if (j) {
            return d
        }
        d = d.join("");
        d.replace(/^\s*([^<\s][^<]*)?(<[\w\W]+>)([^>]*[^>\s])?\s*$/, function(l, b, k, g) {
            a = y(k).get();
            K(a);
            if (b) {
                a = I(b).concat(a)
            }
            if (g) {
                a = a.concat(I(g))
            }
        });
        return a ? a : I(d)
    }

    function I(d) {
        var a = document.createElement("div");
        a.innerHTML = d;
        return y.makeArray(a.childNodes)
    }

    function M(a) {
        return new Function("jQuery", "$item", "var $=jQuery,call,__=[],$data=$item.data;with($data){__.push('" + y.trim(a).replace(/([\\'])/g, "\\$1").replace(/[\r\t\n]/g, " ").replace(/\$\{([^\}]*)\}/g, "{{= $1}}").replace(/\{\{(\/?)(\w+|.)(?:\(((?:[^\}]|\}(?!\}))*?)?\))?(?:\s+(.*?)?)?(\(((?:[^\}]|\}(?!\}))*?)\))?\s*\}\}/g, function(w, v, u, r, h, n, o) {
            var t = y.tmpl.tag[u],
                s, p, q;
            if (!t) {
                throw "Unknown template tag: " + u
            }
            s = t._default || [];
            if (n && !/\w$/.test(h)) {
                h += n;
                n = ""
            }
            if (h) {
                h = F(h);
                o = o ? "," + F(o) + ")" : n ? ")" : "";
                p = n ? h.indexOf(".") > -1 ? h + F(n) : "(" + h + ").call($item" + o : h;
                q = n ? p : "(typeof(" + h + ")==='function'?(" + h + ").call($item):(" + h + "))"
            } else {
                q = p = s.$1 || "null"
            }
            r = F(r);
            return "');" + t[v ? "close" : "open"].split("$notnull_1").join(h ? "typeof(" + h + ")!=='undefined' && (" + h + ")!=null" : "true").split("$1a").join(q).split("$1").join(p).split("$2").join(r || s.$2 || "") + "__.push('"
        }) + "');}return __;")
    }

    function L(d, a) {
        d._wrap = H(d, true, y.isArray(a) ? a : [O.test(a) ? a : y(a).html()]).join("")
    }

    function F(b) {
        return b ? b.replace(/\\'/g, "'").replace(/\\\\/g, "\\") : null
    }

    function Q(d) {
        var c = document.createElement("div");
        c.appendChild(d.cloneNode(true));
        return c.innerHTML
    }

    function K(q) {
        var i = "_" + A,
            d, c, f = {}, a, r, b;
        for (a = 0, r = q.length; a < r; a++) {
            if ((d = q[a]).nodeType !== 1) {
                continue
            }
            c = d.getElementsByTagName("*");
            for (b = c.length - 1; b >= 0; b--) {
                g(c[b])
            }
            g(d)
        }

        function g(s) {
            var w, n = s,
                t, l, u;
            if (u = s.getAttribute(B)) {
                while (n.parentNode && (n = n.parentNode).nodeType === 1 && !(w = n.getAttribute(B))) {}
                if (w !== u) {
                    n = n.parentNode ? n.nodeType === 11 ? 0 : n.getAttribute(B) || 0 : 0;
                    if (!(l = z[u])) {
                        l = D[u];
                        l = E(l, z[n] || D[n]);
                        l.key = ++G;
                        z[G] = l
                    }
                    A && v(u)
                }
                s.removeAttribute(B)
            } else {
                if (A && (l = y.data(s, "tmplItem"))) {
                    v(l.key);
                    z[l.key] = l;
                    n = y.data(s.parentNode, "tmplItem");
                    n = n ? n.key : 0
                }
            } if (l) {
                t = l;
                while (t && t.key != n) {
                    t.nodes.push(s);
                    t = t.parent
                }
                delete l._ctnt;
                delete l._wrap;
                y.data(s, "tmplItem", l)
            }

            function v(e) {
                e = e + i;
                l = f[e] = f[e] || E(l, z[l.parent.key + i] || l.parent)
            }
        }
    }

    function S(e, h, g, f) {
        if (!e) {
            return J.pop()
        }
        J.push({
            _: e,
            tmpl: h,
            item: this,
            data: g,
            options: f
        })
    }

    function U(f, e, a) {
        return y.tmpl(y.template(f), e, a, this)
    }

    function V(a, f) {
        var e = a.options || {};
        e.wrapped = f;
        return y.tmpl(y.template(a.tmpl), a.data, e, a.item)
    }

    function T(f, e) {
        var a = this._wrap;
        return y.map(y(y.isArray(a) ? a.join("") : a).filter(f || "*"), function(b) {
            return e ? b.innerText || b.textContent : b.outerHTML || Q(b)
        })
    }

    function R() {
        var a = this.nodes;
        y.tmpl(null, null, null, this).insertBefore(a[0]);
        y(a).remove()
    }
})(jQuery);
jQuery.cookie = function(h, m, j) {
    if (typeof m != "undefined") {
        j = j || {};
        if (m === null) {
            m = "";
            j = $.extend({}, j);
            j.expires = -1
        }
        var f = "";
        if (j.expires && (typeof j.expires == "number" || j.expires.toUTCString)) {
            var d;
            if (typeof j.expires == "number") {
                d = new Date();
                d.setTime(d.getTime() + (j.expires * 24 * 60 * 60 * 1000))
            } else {
                d = j.expires
            }
            f = "; expires=" + d.toUTCString()
        }
        var k = j.path ? "; path=" + (j.path) : "";
        var e = j.domain ? "; domain=" + (j.domain) : "";
        var l = j.secure ? "; secure" : "";
        document.cookie = [h, "=", encodeURIComponent(m), f, k, e, l].join("")
    } else {
        var c = null;
        if (document.cookie && document.cookie != "") {
            var b = document.cookie.split(";");
            for (var g = 0; g < b.length; g++) {
                var a = jQuery.trim(b[g]);
                if (a.substring(0, h.length + 1) == (h + "=")) {
                    c = decodeURIComponent(a.substring(h.length + 1));
                    break
                }
            }
        }
        return c
    }
};
var Currency;
(function(a) {
    var c = (function() {
        function e() {}
        return e
    })();
    a.CurrencyViewModel = c;
    var d = (function() {
        function e() {}
        return e
    })();
    var b = (function() {
        function e() {
            this.currencyInfo = null;
            this.currencyInfoLoading = false;
            this.observers = new Array()
        }
        e.prototype.loadInfo = function() {
            this.currencyInfoLoading = true;
            var f = this;
            $.getJSON(this.webUrl + "/_layouts/Skoda.K2/CurrencyHandler.ashx").done(function(g) {
                f.currencyInfo = g;
                f.currencyInfoLoading = false;
                f.applyFormatingToObservers()
            }).fail(function(g, h) {
                alert("Request failed: " + h)
            })
        };
        e.prototype.applyFormating = function(g, i, f) {
            var h = new d();
            h.observer = g;
            h.symbolName = i;
            h.hoverSymbolName = f;
            if (this.currencyInfoLoading) {
                this.observers.push(h)
            } else {
                if (this.currencyInfo == null) {
                    this.observers.push(h);
                    this.loadInfo()
                } else {
                    this.appendFilledTemplateToObserver(h)
                }
            }
        };
        e.prototype.applyFormatingToObservers = function() {
            for (var f = 0; f < this.observers.length; f++) {
                var g = this.observers[f];
                this.appendFilledTemplateToObserver(g)
            }
        };
        e.prototype.appendFilledTemplateToObserver = function(i) {
            var h = $(i.observer);
            var g = h.text();
            var f = this.format(g, i.symbolName, i.hoverSymbolName);
            h.empty().append(f)
        };
        e.prototype.format = function(i, o, j) {
            var n = '{{if displayCurrencyLiteral}}<span class="sa-currency-literal">${formatedCurrencyValue}</span>{{/if}}{{if displayCurrencySymbolImage}} <img class="sa-currency-symbol-image" src="${src}" sizes="" srcset="" /><img class="sa-currency-symbol-hover-image" style="display: none;" src="${hoverSrc}" sizes="" srcset="" />{{/if}}';
            $.template("currencyTemplate", n);
            var g = new c();
            var p = this.currencyInfo.CurrencyImageUrl != null && this.currencyInfo.CurrencyImageUrl.lastIndexOf("/") > 0;
            if (i == null || i == "" || i == "-") {
                g.displayCurrencyLiteral = false;
                g.displayCurrencySymbolImage = false
            } else {
                if (p) {
                    g.displayCurrencyLiteral = true;
                    g.displayCurrencySymbolImage = true;
                    g.formatedCurrencyValue = i;
                    var m = this.currencyInfo.CurrencyImageUrl.lastIndexOf(".");
                    var h = this.currencyInfo.CurrencyImageUrl.substring(m + 1, this.currencyInfo.CurrencyImageUrl.length);
                    var l = this.currencyInfo.CurrencyImageUrl.substring(0, m);
                    var f = l.lastIndexOf(".");
                    var k = l.substring(0, f);
                    g.src = k + "." + o + "." + h;
                    g.hoverSrc = k + "." + j + "." + h
                } else {
                    if (this.currencyInfo.CurrencyText != null && this.currencyInfo.CurrencyText != "") {
                        g.displayCurrencyLiteral = true;
                        g.displayCurrencySymbolImage = false;
                        g.formatedCurrencyValue = i + " " + this.currencyInfo.CurrencyText
                    } else {
                        g.displayCurrencyLiteral = true;
                        g.displayCurrencySymbolImage = false;
                        g.formatedCurrencyValue = this.currencyInfo.CurrencyFormat.replace("{0}", i)
                    }
                }
            }
            return $.tmpl("currencyTemplate", g)
        };
        return e
    })();
    a.CurrencyFormatter = b
})(Currency || (Currency = {}));
(function(a) {
    var b = new Currency.CurrencyFormatter();
    a.fn.currency = function(d) {
        var c = {
            webUrl: null,
            symbolName: null,
            hoverSymbolName: null
        };
        if (d) {
            a.extend(c, d);
            b.webUrl = c.webUrl
        }
        return this.each(function(e, f) {
            b.applyFormating(f, c.symbolName, c.hoverSymbolName)
        })
    }
})(jQuery);