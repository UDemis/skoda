$(document).ready(function(){
    // Инициализация Date Picker
    kdxTools.Form.DatePickerInit();

    // Запрещаем вводить в поле телефон что-либо не являющееся номером
    kdxTools.Form.PreventPhoneInput();
});