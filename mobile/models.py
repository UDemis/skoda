# -*- coding: utf-8 -*-
from django.db import models
from cars.models import Car
from news.models import News
from onlineforms.models import TestDrive, TechService
from actions.models import Action


class CarMobileContent(models.Model):
    car = models.ForeignKey(Car, verbose_name=u'Модель', limit_choices_to={'level': 1}, related_name='car_mobile')
    title = models.CharField(u'Название', max_length=255)
    html = models.TextField(u'HTML код', blank=True)
    position = models.PositiveSmallIntegerField(u"Позиция")

    class Meta:
        verbose_name = u'Содержание'
        verbose_name_plural = u'Описание для мобильной версии'
        ordering = ['position']

    def __unicode__(self):
        return self.title


class NewsMobile(News):
    class Meta(News.Meta):
        proxy = True
        verbose_name_plural = u'Новости для мобильной версии'


class TestDriveMobile(TestDrive):
    class Meta(TestDrive.Meta):
        proxy = True
        verbose_name_plural = u'Тест-драйв для мобильной версии'


class ActionMobile(Action):
    class Meta(Action.Meta):
        proxy = True
        verbose_name_plural = u'Акции для мобильной версии'


class TechServiceMobile(TechService):
    class Meta(TechService.Meta):
        proxy = True
        verbose_name_plural = u'Запись на ТО для мобильной версии'