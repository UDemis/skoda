# -*- coding: utf-8 -*-
from onlineforms.forms import TechServiceForm, TestDriveForm
from django import forms


class MobileTechServiceForm(TechServiceForm):
    class Meta(TechServiceForm.Meta):
        pass

    notes = forms.CharField()


class MobileTestDriveForm(TestDriveForm):
    class Meta(TestDriveForm.Meta):
        pass

    notes = forms.CharField()