# -*- coding: utf-8 -*-
from django import template
from slider.models import Slide
from news.models import News
from news import settings as news_settings
from cars.models import Car

register = template.Library()


@register.inclusion_tag("mobile/mobile_list_tag.html")
def slider_mobile():
    slides = Slide.objects.all()
    return {'object_list': slides}


@register.inclusion_tag('mobile/news_last_tag.html', takes_context=True)
def news_last_mobile(context, count=news_settings.LAST_PAGE_SIZE):
    try:
        count = int(count)
    except:
        count = news_settings.LAST_PAGE_SIZE
    last_news = News.published.all()[:count]
    context['object_list'] = last_news
    return context


@register.simple_tag()
def to_mobilelink(link):
    if link:
        link = link.replace('http://zlata-nn.ru/', 'http://zlata-nn.ru/m/')
    return link


@register.inclusion_tag("mobile/models_cars_list.html")
def cars_mobile():
    return {'object_list': Car.objects.filter(level=0).order_by('price')}

@register.assignment_tag
def slug_from_path_m(path):
    return path[3:len(path)-1]