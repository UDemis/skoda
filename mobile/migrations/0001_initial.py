# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CarMobileContent'
        db.create_table(u'mobile_carmobilecontent', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(related_name='car_mobile', to=orm['cars.Car'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('html', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('position', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'mobile', ['CarMobileContent'])


    def backwards(self, orm):
        # Deleting model 'CarMobileContent'
        db.delete_table(u'mobile_carmobilecontent')


    models = {
        u'cars.car': {
            'Meta': {'object_name': 'Car'},
            'accessories': ('snippets.core.fields.FileField', [], {'max_length': '255', 'blank': 'True'}),
            'catalog_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'characteristics': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'max_length': '100'}),
            'configurator_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'emission_co2': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'fulltext': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'img_exterior_bg': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'img_exterior_point_back': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'img_exterior_point_front': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'interior_space': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'luggage_space': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['cars.Car']"}),
            'petrol_consumption': ('django.db.models.fields.FloatField', [], {'null': 'True'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'price_to': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'view360': ('snippets.core.fields.FileField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'mobile.carmobilecontent': {
            'Meta': {'ordering': "['position']", 'object_name': 'CarMobileContent'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'car_mobile'", 'to': u"orm['cars.Car']"}),
            'html': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['mobile']