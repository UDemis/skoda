# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from snippets.pages.site import site, PageType
from mobile.models import NewsMobile, TestDriveMobile, TechServiceMobile, ActionMobile


class MobileHomePage(PageType):
    def get_path(self, *args, **kwargs):
        return reverse('show_mobile')


class NewsListMobilePage(PageType):
    def get_path(self, *args, **kwargs):
        return reverse('news_list_mobile')


class TestDriveMobilePage(PageType):
    def get_path(self, *args, **kwargs):
        return reverse('test_drive')


class TechServiceMobilePage(PageType):
    def get_path(self, *args, **kwargs):
        return reverse('online_record_sto')


class OffersMobilePage(PageType):
    def get_path(self, *args, **kwargs):
        return reverse('action_list_mobile')


site.register(NewsMobile, NewsListMobilePage)
site.register(TestDriveMobile, TestDriveMobilePage)
site.register(TechServiceMobile, TechServiceMobilePage)
site.register(ActionMobile, OffersMobilePage)
