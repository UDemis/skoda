# -*- coding: utf-8 -*-
from django.views.generic import TemplateView
from django.views.generic import ListView, DetailView
from onlineforms.views import TestDriveFormView, TechServiceFormView
from cars.views import CarList
from cars.models import CarInStock, Car
from onlineforms.models import TestDriveCar, TestDrivePlace
from actions.views import ActionList, ActionDetail
from news.views import ArchiveIndexView
from news.views import DetailView as DetailViewNews
from mobile.forms import MobileTechServiceForm, MobileTestDriveForm


class ShowMobile(TemplateView):
    template_name = 'mobile/main_page.html'

    def get_context_data(self, **kwargs):
        context = super(ShowMobile, self).get_context_data(**kwargs)
        context['template_name'] = 'main'
        return context


class AvailableCars(CarList):
    template_name = 'mobile/available_cars.html'


class AvailableCarsInStock(ListView):
    model = CarInStock
    template_name = 'mobile/available_cars_in_stock.html'

    def get_queryset(self):
        qs = super(AvailableCarsInStock, self).get_queryset()
        return qs.filter(car__slug=self.kwargs['slug'])


class AvailableCarsInStockDetailView(DetailView):
    model = CarInStock
    template_name = 'mobile/available_cars_in_stock_detail.html'


class ActionListMobile(ActionList):
    template_name = 'mobile/action_list_mobile.html'
    actual = True


class ActionDetailMobile(ActionDetail):
    template_name = 'mobile/action_detail_mobile.html'


class TestDriveFormViewMobile(TestDriveFormView):
    template_name = 'mobile/test_drive.html'
    http_method_names = ('post', 'get',)
    form_class = MobileTestDriveForm

    def get_context_data(self, **kwargs):
        context = super(TestDriveFormView, self).get_context_data(**kwargs)
        context['test_drive_cars'] = TestDriveCar.objects.all()
        context['test_drive_places'] = TestDrivePlace.objects.all()
        return context


class TechServiceFormViewMobile(TechServiceFormView):
    template_name = 'mobile/online_record_sto.html'
    http_method_names = ('post', 'get',)
    context_object_name = 'obj'
    form_class = MobileTechServiceForm

    def get_context_data(self, **kwargs):
        context = super(TechServiceFormViewMobile, self).get_context_data(**kwargs)
        if 'object' in context:
            del context['object']
        return context


class ArchiveIndexViewMobile(ArchiveIndexView):
    template_name = 'mobile/news_list.html'


class DetailViewMobile(DetailViewNews):
    template_name = 'mobile/action_detail_mobile.html'


class CarsListView(ListView):
    model = Car
    template_name = 'mobile/cars_list.html'

    def get_queryset(self):
        qs = super(CarsListView, self).get_queryset()
        newqs = qs.select_related('parent').filter(parent__slug=self.kwargs['parent'])
        return newqs

    def get_context_data(self, **kwargs):
        context = super(CarsListView, self).get_context_data(**kwargs)
        context['parent'] = self.kwargs['parent']
        return context


class CarDetailView(DetailView):
    model = Car
    template_name = 'mobile/car_detail.html'

    def get_queryset(self):
        qs = super(CarDetailView, self).get_queryset()
        return qs.prefetch_related('pricelist')