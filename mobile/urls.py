# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from mobile.views import ShowMobile, AvailableCars, ActionListMobile, ActionDetailMobile, TestDriveFormViewMobile, \
    AvailableCarsInStock,AvailableCarsInStockDetailView,TechServiceFormViewMobile,ArchiveIndexViewMobile,DetailViewMobile, CarsListView, \
    CarDetailView

urlpatterns = patterns('',
    url(r'^$', ShowMobile.as_view(), name='show_mobile'),
    url(r'^available_cars/$', AvailableCars.as_view(), name='available_cars'),
    url(r'^available_cars/(?P<slug>[-\w]+)/$', AvailableCarsInStock.as_view(), name='available_cars_in_stock'),
    url(r'^available_cars/(?P<slug>[-\w]+)/(?P<pk>\d+)/', AvailableCarsInStockDetailView.as_view(), name='available_cars_in_stock_detail'),
    url(r'^offers/$', ActionListMobile.as_view(), name='action_list_mobile'),
    url(r'^offers/(?P<slug>[-\w]+)/$', ActionDetailMobile.as_view(), name="action_detail_mobile"),
    url(r'^test-drive/', TestDriveFormViewMobile.as_view(), name='test_drive'),
    url(r'^online-record-sto/', TechServiceFormViewMobile.as_view(), name='online_record_sto'),
    url(r'^news/$', ArchiveIndexViewMobile.as_view(), name='news_list_mobile'),
    url(r'^news/(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)\.html$',
       DetailViewMobile.as_view(), name='news_detail_mobile'),
    url(r'^models/(?P<parent>[-\w]+)/$', CarsListView.as_view(), name='car_list_mobile'),
    url(r'^models/(?P<parent>[-\w]+)/(?P<slug>[-\w]+)/$', CarDetailView.as_view(), name='car_detail_mobile'),

)