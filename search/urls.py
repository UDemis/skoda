# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns
from haystack.views import search_view_factory
from haystack.forms import HighlightedSearchForm

urlpatterns = patterns('',
   url(r'^$', search_view_factory(form_class=HighlightedSearchForm), name='search')
)