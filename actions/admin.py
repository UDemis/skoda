# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.timezone import now
from snippets.core.admin import PublicationAdmin
from actions.models import Action, ServiceAction

class ActionFilter(admin.SimpleListFilter):
    title = u'Акции'
    parameter_name = 'type'

    def lookups(self, request, model_admin):
        return (
            ('actual', u'Актуальные'),
            ('regular', u'Постоянные'),
            ('archive', u'Архив'),
        )

    def queryset(self, request, queryset):
        n = now()
        if self.value() == 'actual':
            return queryset.filter(pub_date__lte=n, expiry_date__gte=n)
        if self.value() == 'regular':
            return queryset.filter(pub_date__lte=n, expiry_date=None)
        if self.value() == 'archive':
            return queryset.filter(expiry_date__lt=n)

class ActionAdmin(PublicationAdmin):
    """
        Admin for actions
    """
    list_filter = (ActionFilter,)
    fields = ('title','slug','is_published', 'pub_date','expiry_date','excerpt','content','img')

class ServiceActionAdmin(ActionAdmin):
    list_filter = []

admin.site.register(Action, ActionAdmin)
admin.site.register(ServiceAction, ServiceActionAdmin)
