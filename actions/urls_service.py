# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from actions.views import ServiceActionList, ServiceActionDetail

urlpatterns = patterns('',
   url(r'^$', ServiceActionList.as_view(), name="serviceaction_list"),
   url(r'^(?P<slug>[-\w]+)/$', ServiceActionDetail.as_view(), name="serviceaction_detail"),
)