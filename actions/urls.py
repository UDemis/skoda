# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from actions.views import ActionList, ActionIndex, ActionDetail

urlpatterns = patterns('',
   url(r'^$', ActionIndex.as_view(), name="action_index"),
   url(r'^actual/$', ActionList.as_view(actual=True), name="action_list_actual"),
   url(r'^regular/$', ActionList.as_view(regular=True), name="action_list_regular"),
   url(r'^archive/$', ActionList.as_view(), name="action_list_archive"),
   url(r'^(?P<slug>[-\w]+)/$', ActionDetail.as_view(), name="action_detail"),
)