# -*- coding: utf-8 -*-
from django.contrib.sitemaps import Sitemap
from actions.models import Action

class ActionSitemap(Sitemap):
    priority = 0.5
    changefreq = "monthly"

    def items(self):
        return Action.published.all()

    def lastmod(self, obj):
        return obj.updated

sitemaps = {
    'actions':ActionSitemap
}
