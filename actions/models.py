# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from snippets.core.models import Description, Published, TimeStamped
from snippets.core.fields import ImageField

class Action(Published, Description, TimeStamped):
    """
    Акции
    """
    title = models.CharField(_('Title'), max_length=255)
    slug = models.SlugField(_('Slug'), max_length=255, unique=True,
                            help_text='')
    img = ImageField(u'Изображение', max_length=255, upload_to='actions/', blank=True)

    class Meta:
        verbose_name = u"Акцию"
        verbose_name_plural = u"Акции"
        ordering = ('-pub_date', )

    @models.permalink
    def get_absolute_url(self):
        return ('action_detail', (), {'slug': self.slug})

class ServiceAction(Action):
    """
    Сервисные акции
    """

    class Meta(Action.Meta):
        verbose_name = u"Акцию"
        verbose_name_plural = u"Сервисные акции"

    @models.permalink
    def get_absolute_url(self):
        return ('serviceaction_detail', (), {'slug': self.slug})