# -*- coding: utf-8 -*-
from django.db.models import Q
from django.conf import settings
from django.http import HttpResponseRedirect
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.base import TemplateView
from django.utils.timezone import now
from actions.models import Action, ServiceAction


class ActionIndex(TemplateView):
    template_name = 'actions/action_index.html'


class ActionList(ListView):
    model = Action
    template_name = 'actions/action_list.html'
    actual = False
    regular = False
    paginate_by = settings.PUBLICATION_PAGE_SIZE

    def get(self, request, *args, **kwargs):
        page = self.kwargs.get(self.page_kwarg) or self.request.GET.get(self.page_kwarg)
        if page == '1':
            return HttpResponseRedirect(request.path)
        return super(ActionList, self).get(request, *args, **kwargs)

    def get_queryset(self):
        n = now()
        qs = self.model.objects.filter(is_published=True)
        if self.actual:
            # return qs.filter(pub_date__lte=n, expiry_date__gte=n)
            return qs.filter(pub_date__lte=n).filter(Q(expiry_date__gte=n) | Q(expiry_date__isnull=True))
        if self.regular:
            return qs.filter(pub_date__lte=n, expiry_date__isnull=True)
        return qs.filter(expiry_date__lt=n)

    def get_context_data(self, **kwargs):
        context = super(ActionList, self).get_context_data(**kwargs)
        context.update({'actual': self.actual,
                        'regular': self.regular})
        return context


class ActionDetail(DetailView):
    queryset = Action.objects.filter(is_published=True)
    model = Action


class ServiceActionList(ListView):
    queryset = ServiceAction.objects.filter(is_published=True)
    paginate_by = settings.PUBLICATION_PAGE_SIZE


class ServiceActionDetail(DetailView):
    model = ServiceAction