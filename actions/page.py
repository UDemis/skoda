# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from snippets.pages.site import site, PageType
from actions.models import Action, ServiceAction

class ActionPageType(PageType):

    def get_path(self, *args, **kwargs):
        return reverse('action_index')

class ServiceActionPageType(PageType):

    def get_path(self, *args, **kwargs):
        return reverse('serviceaction_list')

site.register(Action, ActionPageType)
site.register(ServiceAction, ServiceActionPageType)