# -*- coding: utf-8 -*-
from haystack import indexes
from actions.models import Action

class ActionIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    pub_date = indexes.DateTimeField(model_attr='pub_date')

    def get_model(self):
        return Action

    def index_queryset(self, using=None):
        return self.get_model().published.all()