# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from events.views import EventList, EventDetail

urlpatterns = patterns('',
   url(r'^$', EventList.as_view(), name="event_list"),
   url(r'^(?P<slug>[-\w]+)/$', EventDetail.as_view(), name="event_detail"),
)