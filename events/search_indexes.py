# -*- coding: utf-8 -*-
from haystack import indexes
from events.models import Event

class EventIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    pub_date = indexes.DateTimeField(model_attr='pub_date')

    def get_model(self):
        return Event

    def index_queryset(self, using=None):
        return self.get_model().published.all()