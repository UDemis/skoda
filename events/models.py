# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from snippets.core.models import Description, Published, TimeStamped
from snippets.core.fields import ImageField


class Event(Published, Description, TimeStamped):
    """
    Акции
    """
    title = models.CharField(_('Title'), max_length=255)
    slug = models.SlugField(_('Slug'), max_length=255, unique=True,
                            help_text='')
    img = ImageField(u'Изображение', blank=True, max_length=255)

    class Meta:
        verbose_name = u"Мероприятие"
        verbose_name_plural = u"Мероприятия"
        ordering = ('-pub_date', )

    @models.permalink
    def get_absolute_url(self):
        return ('event_detail', (), {'slug': self.slug})


class EventImage(models.Model):
    page = models.ForeignKey(Event, related_name='images')
    img = ImageField(u'Изображение', max_length=255, upload_to='events')
    title = models.CharField(u'Описание', max_length=255, blank=True)

    class Meta:
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения'