# -*- coding: utf-8 -*-
from django.contrib import admin
from snippets.core.admin import PublicationAdmin
from events.models import Event, EventImage


class EventImageInline(admin.TabularInline):
    model = EventImage
    

class EventAdmin(PublicationAdmin):
    """
        Admin for actions
    """
    fields = ('title', 'slug', 'is_published', 'pub_date', 'expiry_date', 'excerpt', 'content', 'img')
    inlines = (EventImageInline,)

admin.site.register(Event, EventAdmin)
