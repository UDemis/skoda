# -*- coding: utf-8 -*-
from django.conf import settings
from django.http import HttpResponseRedirect
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from events.models import Event


class EventList(ListView):
    model = Event
    paginate_by = settings.PUBLICATION_PAGE_SIZE

    def get(self, request, *args, **kwargs):
        page = self.kwargs.get(self.page_kwarg) or self.request.GET.get(self.page_kwarg)
        if page == '1':
            return HttpResponseRedirect(request.path)
        return super(EventList, self).get(request, *args, **kwargs)


class EventDetail(DetailView):
    model = Event