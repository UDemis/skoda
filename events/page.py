# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from snippets.pages.site import site, PageType
from events.models import Event

class EventPageType(PageType):

    def get_path(self, *args, **kwargs):
        return reverse('event_list')

site.register(Event, EventPageType)