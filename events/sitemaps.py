# -*- coding: utf-8 -*-
from django.contrib.sitemaps import Sitemap
from events.models import Event


class EventSitemap(Sitemap):

    def items(self):
        return Event.published.all()


sitemaps = {
    'events': EventSitemap
}
