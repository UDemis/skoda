# -*- coding: utf-8 -*-
from django.db import models
from snippets.core.models import Published, TimeStamped, Description


class Article(models.Model):
    title = models.CharField(u'Заголовок статьи', max_length=1000)
    slug = models.SlugField(u'Путь', max_length=255)
    zag = models.CharField(u'Заголовок H1', max_length=255)
    introtext = models.TextField(u'Краткое описание')
    fullopis = models.TextField(u'Содержимое')
    visibl = models.BooleanField(u'Отображать на сайте', default=True)
    sort = models.IntegerField(u'Порядок', default=0)
    metatitle = models.CharField(u'title SEO', max_length=1000, blank=True)
    metades = models.CharField(u'Meta des', max_length=1000, blank=True)
    metakey = models.CharField(u'Meta key', max_length=1000, blank=True)

    class Meta:
        ordering = ['sort']
        verbose_name = u'Статью'
        verbose_name_plural = u'Статьи'
        db_table = 'stati_stati'

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('article_detail', (), {'slug': self.slug})
