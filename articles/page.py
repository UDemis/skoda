# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from snippets.pages.site import site, PageType
from articles.models import Article


class ArticlePageType(PageType):
    def get_path(self, *args, **kwargs):
        return reverse('article_list')


site.register(Article, ArticlePageType)