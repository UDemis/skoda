# -*- coding: utf-8 -*-
from django.contrib import admin
from articles.models import Article


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('pk', 'title', 'sort', 'visibl')
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(Article, ArticleAdmin)