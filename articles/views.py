# -*- coding: utf-8 -*-
from django.views import generic as generic_views
from articles.models import Article


class ArticleListView(generic_views.ListView):
    queryset = Article.objects.filter(visibl=True)


class ArticleDetailView(generic_views.DetailView):
    queryset = Article.objects.filter(visibl=True)