# -*- coding: utf-8 -*-
from django.contrib import admin
from jsonld.models import MicroMarking


class MikroMarkingAdmin(admin.ModelAdmin):
    list_display = ('url', 'text')
    list_display_links = ('url', 'text')

admin.site.register(MicroMarking, MikroMarkingAdmin)