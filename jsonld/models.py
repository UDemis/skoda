# -*- coding: utf-8 -*-
from django.db import models


class MicroMarking(models.Model):
    url = models.CharField(u'URL страницы', max_length=255, unique=True)
    text = models.TextField(u'JSON-LD разметка')

    class Meta:
        verbose_name = u'Микроразметка'
        verbose_name_plural = u'Микроразметка'