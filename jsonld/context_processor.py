# -*- coding: utf-8 -*-
from jsonld.models import MicroMarking


def micro_marking(request):
    try:
        jsonld = MicroMarking.objects.get(url=request.path)
    except MicroMarking.DoesNotExist:
        return {}
    return {
        "jsonld": jsonld.text
    }
