# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, redirect
from django.views.generic import View, TemplateView


class MainPageView(TemplateView):
    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        device_check = request.GET.get('desktop')
        is_mobile = getattr(request, 'is_mobile', False)
        if is_mobile and device_check is None:
             path_mobile = reverse('show_mobile')
             return redirect(path_mobile)
        return super(MainPageView, self).render_to_response(context)