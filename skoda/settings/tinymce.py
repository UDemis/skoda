from .__init__ import *

TINYMCE_SPELLCHECKER=False

TINYMCE_PLUGINS = [
    'autolink',
    'lists',
    'spellchecker',
    # 'pagebreak',
    'style',
    'layer',
    'table',
    'advhr',
    'advimage',
    'advlink',
    #'emotions',
    'iespell',
    'inlinepopups',
    'insertdatetime',
    'preview',
    'media',
    'searchreplace',
    'print',
    'contextmenu',
    'paste',
    'directionality',
    'fullscreen',
    'noneditable',
    'visualchars',
    'nonbreaking',
    #'xhtmlxtras',
    #'template'
]



TINYMCE_DEFAULT_CONFIG={
    'theme' : "advanced",
    'width': "100%",
    'height': "auto",
    'plugins' : ",".join(TINYMCE_PLUGINS), # django-cms
    'language' : 'ru',
    'theme_advanced_buttons1' : "bold,italic,underline,strikethrough,bullist,numlist,"\
                               "|,undo,redo,|,"\
                               ",|,cleanup,removeformat,charmap,"\
                               "|,cut,copy,paste,pastetext,pasteword,|,search,replace,"\
                               "|,fullscreen,preview,code",
    'theme_advanced_buttons2' : "fontsizeselect,formatselect,|,forecolor,backcolor,|,table,|,"\
                                "delete_row,delete_table,|,row_after,row_before,|, "\
                                "link,unlink,anchor,image,|,"\
                                "outdent,indent,|,justifyleft,justifycenter,justifyright,justifyfull",
    'theme_advanced_buttons3' : "",
    'theme_advanced_buttons4' : "",
    'theme_advanced_toolbar_location' : "top",
    'theme_advanced_statusbar_location' : "bottom",
    'theme_advanced_resizing' : True,
    'table_default_cellpadding': 2,
    'table_default_cellspacing': 2,
    'cleanup_on_startup' : False,
    'cleanup' : False,
    'paste_auto_cleanup_on_paste' : False,
    'paste_block_drop' : False,
    'paste_remove_spans' : False,
    'paste_strip_class_attributes' : False,
    'paste_retain_style_properties' : "",
    'forced_root_block' : False,
    'force_br_newlines' : False,
    'force_p_newlines' : False,
    'remove_linebreaks' : False,
    'convert_newlines_to_brs' : False,
    'inline_styles' : False,
    'relative_urls' : False,
    'valid_elements' : '+*[*]',
    'valid_children' : "+pre[script]",
    'extended_valid_elements' : "script[charset|defer|async|language|src|type]",
    'verify_html' : False,
    # 'formats' : {
        # 'alignleft' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-left'},
        # 'aligncenter' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-center'},
        # 'alignright' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-right'},
        # 'alignfull' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-justify'},
        # 'strikethrough' : {'inline' : 'del'},
        # 'italic' : {'inline' : 'em'},
        # 'bold' : {'inline' : 'strong'},
        # 'underline' : {'inline' : 'u'}
    # },
    'pagebreak_separator' : ""
}

TINYMCE_SIMPLE_CONFIG={
    'theme' : "advanced",
    'height': "auto",
    'plugins': "fullscreen,paste,autoresize",
    'theme_advanced_buttons1' : "bold,italic,underline,strikethrough,bullist,numlist," \
                                "separator,undo,redo,separator,link,unlink,image" \
                                ",separator,cleanup,code,removeformat,charmap," \
                                "fullscreen,paste",
    'theme_advanced_buttons2' : "",
}
