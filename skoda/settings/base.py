# -*- coding: utf-8 -*-
"""
Django settings for skoda project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

PROJECT_NAME = os.path.basename(os.path.dirname(os.path.dirname(__file__)))
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'pwjmv2$*loit7u6n*0$94vs=-)n1o3x!5&yslzr#6qg8_nr*zo'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = []

SITE_ID = 1
# Application definition

INSTALLED_APPS = (
    'grappelli.dashboard',
    'grappelli',
    'filebrowser',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.redirects',
    # pip
    'tinymce',
    'mptt',
    'south',
    'colorful',
    'phonenumber_field',
    # 'easy_thumbnails',
    'robots',
    'haystack',
    'compressor',
    'captcha',
    # locals
    'snippets.core',
    'snippets.pages',
    'snippets.conf',
    'snippets.seo',
    'skoda',
    'news',
    'actions',
    'cars',
    'events',
    'slider',
    'onlineforms',
    'banners',
    'reviews',
    'landing',
    'mobile',
    'articles',
    'jsonld',
    'metatags',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.redirects.middleware.RedirectFallbackMiddleware',
    'django_mobileesp.middleware.UserAgentDetectionMiddleware',
    'skoda.middleware.CustomPageMiddleware',


)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
    "snippets.conf.context_processors.settings",
    "jsonld.context_processor.micro_marking",
    "metatags.context_processor.meta_tags",
)

ROOT_URLCONF = '%s.urls' % PROJECT_NAME

WSGI_APPLICATION = '%s.wsgi.application' % PROJECT_NAME


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'ru-RU'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static')

MEDIA_URL = '/media/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)

PAGE_MODEL = 'skoda.PageCustom'

PAGE_TEMPLATES = (
    ('inner.html', u'Внутренняя страница'),
    ('inner_without_img.html', u'Внутренняя страница (без картинки)'),
    ('inner_without_header.html', u'Внутренняя страница (без заголовка)'),
    ('inner_with_submenu.html', u'Внутренняя страница (c подменю)'),
    ('mobile/home.html', u'Главная мобильной версии'),
    ('mobile/inner_mobile.html', u'Внутренняя страница мобильной версии'),
)

PAGE_MENUS = (
    ('default', u'Основное меню'),
    ('footer', u'Нижнее меню'),
    ('mobile_menu', u'Меню для мобильной версии'),
    ('mobile_menu_popup', u'Всплывающее меню для мобильной версии'),
    ('mobile_menu_footer_left', u'Нижнее левое меню для мобильной версии'),
    ('mobile_menu_footer_right', u'Нижнее правое меню для мобильной версии'),
)

DEFAULT_EMAIL_FROM = 'info@local.com'

GRAPPELLI_ADMIN_TITLE = u'Официальный диллер Škoda'
GRAPPELLI_INDEX_DASHBOARD = 'skoda.dashboard.CustomIndexDashboard'

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(BASE_DIR, 'searchindex'),
    },
}
HAYSTACK_SEARCH_RESULTS_PER_PAGE = 10

STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    'compressor.finders.CompressorFinder',
)

COMPRESS_CSS_FILTERS = (
    'compressor.filters.css_default.CssAbsoluteFilter',
    'compressor.filters.cssmin.CSSMinFilter',
)

PUBLICATION_PAGE_SIZE = 10
NEWS_ARCHIVE_PAGE_SIZE = 10
NEWS_PAGE_SIZE = 10

SOUTH_MIGRATION_MODULES = {
    'captcha': 'captcha.south_migrations',
    'robots': 'robots.south_migrations',
}

CAPTCHA_NOISE_FUNCTIONS = ()

X_FRAME_OPTIONS = ''

try:
    from .tinymce import *
except ImportError:
    pass

try:
    from .filebrowser import *
except ImportError:
    pass


from django_mobileesp.detector import mobileesp_agent as agent

DETECT_USER_AGENTS = {
    'is_android': agent.detectAndroid,
    'is_ios': agent.detectIos,
    'is_windows_phone': agent.detectWindowsPhone,
    'is_mobile': agent.detectTierTablet | \
                 agent.detectTierIphone | \
                 agent.detectMobileQuick,
}