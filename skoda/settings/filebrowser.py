# -*- coding: utf-8 -*-
FILEBROWSER_EXTENSIONS = {
    'Folder': [''],
    'Image': ['.jpg','.jpeg','.gif','.png','.tif','.tiff','.ico'],
    'Document': ['.pdf','.doc','.rtf','.txt','.xls','.csv'],
    'Video': ['.mov','.wmv','.mpeg','.mpg','.avi','.rm'],
    'Audio': ['.mp3','.mp4','.wav','.aiff','.midi','.m4p']
}

FILEBROWSER_VERSIONS = {
    'admin_thumbnail': {'verbose_name': 'Admin Thumbnail', 'width': 60, 'height': 60, 'opts': ''},
    'thumbnail': {'verbose_name': 'Thumbnail (1 col)', 'width': 80, 'height': 60, 'opts': 'crop'},
    'small': {'verbose_name': 'Small (2 col)', 'width': 140, 'height': '', 'opts': ''},
    'medium': {'verbose_name': 'Medium (4col )', 'width': 300, 'height': '', 'opts': ''},
    'big': {'verbose_name': 'Big (6 col)', 'width': 460, 'height': '', 'opts': ''},
    'large': {'verbose_name': 'Large (8 col)', 'width': 680, 'height': '', 'opts': ''},
    '200x150': {'verbose_name': '200x150', 'width': 200, 'height': 150, 'opts': 'crop'},
    '300x240': {'verbose_name': '300x240', 'width': 300, 'height': 240, 'opts': 'crop'},
    '600x300': {'verbose_name': '600x300', 'width': 600, 'height': 300, 'opts': 'crop'},
    '200x100': {'verbose_name': '200x100', 'width': 200, 'height': 100, 'opts': 'crop'},
    '940x430': {'verbose_name': '940x430', 'width': 940, 'height': 430, 'opts': 'crop'},
    '940x': {'verbose_name': '940x', 'width': 940, 'height': '', 'opts': 'crop'},
    '217x136': {'verbose_name': '217x136', 'width': 217, 'height': 136, 'opts': 'crop'},
    '800x500': {'verbose_name': '800x500', 'width': 800, 'height': 500, 'opts': 'crop'},
    '125x80': {'verbose_name': '125x80', 'width': 125, 'height': 80, 'opts': 'crop'},
    '192x90': {'verbose_name': '192x90', 'width': 192, 'height': 90, 'opts': 'crop'},
    '194x92': {'verbose_name': '194x92', 'width': 194, 'height': 92, 'opts': 'crop'},
    '276x310': {'verbose_name': '276x310', 'width': 276, 'height': 310, 'opts': 'crop'},
    '270x136': {'verbose_name': '270x136', 'width': 270, 'height': 136, 'opts': 'crop'},
    '1920x556': {'verbose_name': '1920x556', 'width': 1920, 'height': 556, 'opts': 'crop'},
}

FILEBROWSER_DIRECTORY = 'uploads/'
FILEBROWSER_VERSIONS_BASEDIR = 'versions/'