# -*- coding: utf-8 -*-
from django.contrib import admin
from snippets.pages.admin import PageAdmin, ChoiceFieldPatched
from skoda.models import PageCustom, PageImage


class ChoiceField(ChoiceFieldPatched):

    def to_python(self, value):
        res = super(ChoiceFieldPatched, self).to_python(value)
        if res:
            return res.path
        return ''


class PageImageInline(admin.TabularInline):
    model = PageImage


class PageCustomAdmin(PageAdmin):
    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'is_published', 'pub_date', 'expiry_date',
                       'content', 'img', 'template', 'pagetype', 'in_menu', 'in_sitemap', 'parent')
        }),
    )
    inlines = tuple(PageAdmin.inlines) + (PageImageInline,)

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'redirect_to':
            kwargs.update({
                'form_class': ChoiceField,
                'widget': ChoiceField.widget,
                'queryset': self.model.objects.all(),
                'to_field_name': 'path'
            })
            return super(PageAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        return super(PageCustomAdmin, self).formfield_for_dbfield(db_field, **kwargs)

admin.site.register(PageCustom, PageCustomAdmin)