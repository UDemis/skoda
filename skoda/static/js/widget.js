(function() {

	var enableScrollEffect = true; // true/false - вкл/выкл эффект при скроллинге
	if (window.innerWidth <= 850) { // минимальная ширина окна в px при которой происходит переключение на мобильную версию (должно совпадать со значением в файле стилей)
		enableScrollEffect = false;
	}


	function move() {
		var widget = document.getElementById('car-fin_widget');
		var scrolled = window.pageYOffset || document.documentElement.scrollTop;
		widget.style.top = (scrolled + window.innerHeight - 310) + 'px';
	}

	setTimeout(function() {
		var widget = document.getElementById('car-fin_widget');
		widget.style.opacity = .9; // прозрачность виджета (от 0 до 1): 0 — прозрачно, 1 — непрозрачно
		if (!enableScrollEffect) {
			return;
		}
		var widgetContainer = document.getElementById('car-fin_widgetContainer');
		widgetContainer.style.position = 'absolute';
		move();
		window.onscroll = move;

	}, 1500); // скорость появления виджета
})();