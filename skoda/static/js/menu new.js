
var bwIE60 = (navigator.appVersion.indexOf("MSIE 6.0") != -1) ? "True" : "False";
var bwIE70 = (navigator.appVersion.indexOf("MSIE 7.0") != -1) ? "True" : "False";
var bwIE80 = (navigator.appVersion.indexOf("MSIE 8.0") != -1) ? "True" : "False";
var bwIE90 = (navigator.appVersion.indexOf("MSIE 9.0") != -1) ? "True" : "False";

$(document).ready(function () {								
	// top menu
	$(".hlbMenuMain li").each(function () {
		// menu SHOW
		$(this).mouseenter(function () {									
			$("a:first", this).toggleClass("hlbMenuMainSelected");
			var offset = $("a:first", this)[0].offsetLeft;
			if (bwIE60 == "True" || bwIE70 == "True" || bwIE80 == "True" || bwIE90 == "True") {
				offset = this.offsetLeft;
			}			
			$("div:first", this).css("left", offset + "px");
			$("div:first", this).show();
		});
		// menu HIDE
		$(this).mouseleave(function () {
									 //alert(document.getElementById('55').offsetWidth);
			$("div:first", this).hide();
			$("a:first", this).toggleClass("hlbMenuMainSelected");
		});
	});

	// model menu
	$(".mmMenuMain > li", this).each(function () {
		// menu SHOW
		$(this).mouseenter(function () {
			$("a:first", this).toggleClass("mmMenuMainSelected");
			$("div:first", this).show();
			var offset = $("a:first", this)[0].offsetLeft;
			var width = $("div:first", this)[0].offsetWidth;
			if (bwIE60 == "True" || bwIE70 == "True" || bwIE80 == "True" || bwIE90 == "True") {
				offset = this.offsetLeft;
			}
			if (offset + width > 900) {
				var diff = 900 - (offset + width);
				offset = offset + diff;
			}
			$("div:first", this).css("left", offset + "px");
		});
		// menu HIDE
		$(this).mouseleave(function () {
			$("div:first", this).hide();
			$("a:first", this).toggleClass("mmMenuMainSelected");
		});
	});
	
	// Vehicles menu
	$(".menuAutosItem li").each(function () {
		// menu SHOW
		$(this).mouseenter(function () {									
			$("a:first", this).toggleClass("menuAutosItemSelected");
			var offset = $("a:first", this)[0].offsetLeft;
			if (bwIE60 == "True" || bwIE70 == "True" || bwIE80 == "True" || bwIE90 == "True") {
				offset = this.offsetLeft;
			}
			$("div:first", this).css("left", offset + "px");
			$("div:first", this).show();
		});
		// menu HIDE
		$(this).mouseleave(function () {									 
			$("div:first", this).hide();
			$("a:first", this).toggleClass("menuAutosItemSelected");
		});
	});
	
	
	$('.model-list li').children('a').mouseover(function(){
		$(this).parents('.model-list').find('a').removeClass('active');
		$(this).addClass('active');
		var target = ( "ms" + '-' + $(this).attr('id').slice(3) );
		$('#' + target).css('display', 'block').siblings().css('display', 'none');
	});

	$('.tech-icons > li').tipTip({defaultPosition: 'top',delay: 200});

});

function ShowML3Submenu(idMenu, id){
	if (document.getElementById(id) != null) {
		$("#"+id).css("display", "block");
	}	
}

function HideML3Submenu(idMenu, id) {
	if (document.getElementById(id) != null) {
		$("#"+id).css("display", "none");
	}	
}

function ShowML3Menu(idMenu, id) {
	if (document.getElementById(id) != null) {		
		$("#"+id).css("display", "block");
		$("#"+idMenu).css("background-color", "#FFFFFF");
		$("#"+idMenu).css("color", "#555555");
	}	
}

function HideML3Menu(idMenu, id) {
	if (document.getElementById(id) != null) {		
		$("#"+id).css("display", "none");
		$("#"+idMenu).css("background-color", "");
		$("#"+idMenu).css("color", "");
	}	
}

function ShowML3MenuDWA(idMenu, id) {
	if (document.getElementById(id) != null) {		
		$("#"+id).css("display", "block");
		$("#"+idMenu).css("background-color", "#FF871F");
		$("#"+idMenu).css("color", "#555555");
	}	
}

function HideML3MenuDWA(idMenu, id) {
	if (document.getElementById(id) != null) {		
		$("#"+id).css("display", "none");
		$("#"+idMenu).css("background-color", "");
		$("#"+idMenu).css("color", "");
	}	
}
