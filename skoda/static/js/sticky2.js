$(function(){	  

	if($('.aside-sticky-inner').length) {
		document.getElementById('fixedMenu').style.display = 'block';
		$('#fixedMenu').parent().wrapInner('<div class="sticky-container"></div>');
		$('#fixedMenu').wrap('<div class="sticky-container2"><div class="sticky-container3"></div></div>');
		if(document.all && !document.querySelector){
			$('.car-hotspot').hover(function(){
				$('.sticky-container').addClass('z-fix');
			}, function(){
				$('.sticky-container').delay(150).removeClass('z-fix');
			});
		}

		$(window).scroll(function(){
			$('#fixedMenu').css('margin-left',-$(window).scrollLeft());
		});
		 
		$('.folding-button').on('click', function(){
			if(!$('.folding-button').hasClass('button-folded')){
				$('.folding-button').addClass('button-folded');
				$('.sticky-container2').addClass('menu-folded');
			} else {
				$('.folding-button').removeClass('button-folded');
				$('.sticky-container2').removeClass('menu-folded');
			}
		});
	}
	

	// right menu opener
	$(document).on('click','.frm_trigger',function(){
		$(this).closest('.fixed_right_menu').toggleClass('frm_closed');
	});


});