//$ ( document ).ready ( function () {
	var reverse = function ( s ) {
		s = s.toString ();
		var ret = '';

		for ( var i = s.length - 1; i >= 0; i-- ) {
			ret = ret + s.charAt ( i );
		};

		return ret;
	};
	var humanFriendlyNumberView = function ( n ) {
		var nn = reverse ( n );
		n = '';

		var c = 0;
		for ( var i = 0; i < nn.length; i++ ) {
			n = n + nn.charAt ( i );

			c = c + 1;
			if ( ( c % 3 == 0 ) && ( i < nn.length - 1 ) ) {
				n = n + ' ';
			};
		};
		return reverse ( n );
	};

	var Model = function ( scope ) {
//		this.el = $('#'+scope.id);
		this.el = $(scope.el);
		this.low = scope.low;
		this.high = scope.high;
		this.hide = function () {
			this.el.addClass ( 'modelOpacity' );
		};
		this.show = function () {
			this.el.removeClass ( 'modelOpacity' );
		};
		return this;
	};

	var Group = function ( scope ) {
		this.el = scope.el;
		this.items = [];
        this.push = function(i){
            this.items.push(i);
        };
		this.hide = function () {
			for ( var k in this.items ) {
				this.items[k].hide ();
			}
		};
		this.show = function ( scope ) {
			for ( var k in this.items ) {
                var item = this.items[k];
				if (
					( ( scope.high >= item.low ) && ( scope.low <= item.low ) )
					|| ( ( scope.low <= item.high ) && ( scope.high >= item.high ) )
					|| ( ( scope.low >= item.low ) && ( scope.high <= item.high ) )
					|| ( ( scope.low <= item.low ) && ( scope.high >= item.high ) )
				) {
                    item.show();
				}
			}
		};
		return this;
	};

	var Models = function (items, groups) {
		this.low = 0;
		this.high = Number.POSITIVE_INFINITY;

        this.groups = {};
        for ( var k=0; k<groups.length; k++ ) {
            var item = groups[k];
            this.groups[item.id] = new Group(item);
        }
		this.items = {};
        for ( var k=0; k<items.length; k++ ) {
            var item = items[k];
            var model = new Model(item);
            this.items[k] = model;
            for (var j in item.groups){
                var g = item.groups[j];
                this.groups[g].push(model);
            }
        }
		//Хэтчбек (и в этой группе будут Fabia, Fabia RS, Fabia Scout)
		//Седан/Лифтбек (Octavia, Octavia RS, Superb)
		//Универсал (Octavia Combi, Octavia Scout, Superb Combi, Fabia Combi)
		//Универсал повышенной проходимости (Octavia Scout)
		//Универсал/Минивэн (Roomster, Roomster Scout)
		//Кросовер/Внедорожник (Yeti)
		//Спорт (FabiaRS, Octavia RS)
		//4x4 (Octavia Scout, Superb Combi 4x4, Yeti 4x4)

		this.hide = function () {
			for ( var k in this.items ) {
				this.items[k].hide ();
			};
		};

		this.show = function () {
			for ( var k in this.items ) {
				this.items[k].show ();
			};
		};

		this.update = function () {
			this.hide ();
			for ( var k in this.groups ) {
				if ( this.groups[k].el.checked ) {
					this.groups[k].show ( {
						low: this.low,
						high: this.high
					});
				}
			}
		};

		for ( var k in this.groups ) {
			var self = this;
			self.groups[k].el.checked = true;
			$(self.groups[k].el).bind ('change', function(){
				self.update();
			});
		}

		this.update();
		return this;
	};
//} );
