
var bwIE60 = (navigator.appVersion.indexOf("MSIE 6.0") != -1) ? "True" : "False";
var bwIE70 = (navigator.appVersion.indexOf("MSIE 7.0") != -1) ? "True" : "False";
var bwIE80 = (navigator.appVersion.indexOf("MSIE 8.0") != -1) ? "True" : "False";
var bwIE90 = (navigator.appVersion.indexOf("MSIE 9.0") != -1) ? "True" : "False";

$(document).ready(function () {								
	// top menu
	$(".hlbMenuMain li", this).each(function () {
		// menu SHOW
		$(this).mouseenter(function () {									
			$("a:first", this).toggleClass("hlbMenuMainSelected");
            if ($("div:first", this).length){
                $("div:first", this).show();
                var offset = $("a:first", this)[0].offsetLeft;
                var width = $("div:first", this)[0].offsetWidth;
                if (bwIE60 == "True" || bwIE70 == "True") {
                    offset = this.offsetLeft;
                }
                if (offset + width > 900) {
                    var diff = 899 - (offset + width);
                    offset = offset + diff;
                }
                $("div:first", this).css("left", offset + "px");
            }
		});
		// menu HIDE
		$(this).mouseleave(function () {
									 //alert(document.getElementById('55').offsetWidth);
			$("div:first", this).hide();
			$("a:first", this).toggleClass("hlbMenuMainSelected");
		});
	});

	// model menu
	$(".mmMenuMain > li", this).each(function () {
		// menu SHOW
		$(this).mouseenter(function () {
			$("a:first", this).toggleClass("mmMenuMainSelected");
            if ($("div:first", this).length){
                $("div:first", this).show();
                var offset = $("a:first", this)[0].offsetLeft;
                var width = $("div:first", this)[0].offsetWidth;
                if (bwIE60 == "True" || bwIE70 == "True" || bwIE80 == "True" || bwIE90 == "True") {
                    offset = this.offsetLeft;
                }
                if (offset + width > 900) {
                    var diff = 900 - (offset + width);
                    offset = offset + diff;
                }
                $("div:first", this).css("left", offset + "px");
            }
		});
		// menu HIDE
		$(this).mouseleave(function () {
			$("div:first", this).hide();
			$("a:first", this).toggleClass("mmMenuMainSelected");
		});
	});
	
	// Vehicles menu
	$(".menuAutosItem li").each(function () {
		// menu SHOW
		$(this).mouseenter(function () {									
			$("a:first", this).toggleClass("menuAutosItemSelected");
			var offset = $("a:first", this)[0].offsetLeft;
			if (bwIE60 == "True" || bwIE70 == "True" || bwIE80 == "True" || bwIE90 == "True") {
				offset = this.offsetLeft;
			}
			$("div:first", this).css("left", offset + "px");
			$("div:first", this).show();
		});
		// menu HIDE
		$(this).mouseleave(function () {									 
			$("div:first", this).hide();
			$("a:first", this).toggleClass("menuAutosItemSelected");
		});
	});
	
	
	$('.model-list li').children('a').mouseover(function(){
		$(this).parents('.model-list').find('a').removeClass('active');
		$(this).addClass('active');
		var target = ( "ms" + '-' + $(this).attr('id').slice(3) );
		$('#' + target).css('display', 'block').siblings().css('display', 'none');
	});

	$('.tech-icons > li').tipTip({defaultPosition: 'top',delay: 200});

});

function ShowML3Submenu(idMenu, id){
	if (document.getElementById(id) != null) {
		$("#"+id).css("display", "block");
	}	
}

function HideML3Submenu(idMenu, id) {
	if (document.getElementById(id) != null) {
		$("#"+id).css("display", "none");
	}	
}

function ShowML3Menu(idMenu, id) {
	if (document.getElementById(id) != null) {		
		$("#"+id).css("display", "block");
		$("#"+idMenu).css("background-color", "#FFFFFF");
		$("#"+idMenu).css("color", "#555555");
	}	
}

function HideML3Menu(idMenu, id) {
	if (document.getElementById(id) != null) {		
		$("#"+id).css("display", "none");
		$("#"+idMenu).css("background-color", "");
		$("#"+idMenu).css("color", "");
	}	
}

function ShowML3MenuDWA(idMenu, id) {
	if (document.getElementById(id) != null) {		
		$("#"+id).css("display", "block");
		$("#"+idMenu).css("background-color", "#FF871F");
		$("#"+idMenu).css("color", "#555555");
	}	
}

function HideML3MenuDWA(idMenu, id) {
	if (document.getElementById(id) != null) {		
		$("#"+id).css("display", "none");
		$("#"+idMenu).css("background-color", "");
		$("#"+idMenu).css("color", "");
	}	
}


function showhide(id1, id2) {
	if (id1 != '') showhide_v(id1);
	if (id2 != '') showhide_v(id2);
}

function showhide_v(id) {	
	if (document.getElementById(id).style.display == 'none') {
		document.getElementById(id).style.display = 'block';
        document.getElementById('pic_'+id).src = '/static/pics/autominus.jpg';
	} else {
		document.getElementById(id).style.display = 'none';
        document.getElementById('pic_'+id).src = '/static/pics/autoplus.jpg';
	}
	cols_h();
}

function cols_h() {
	var exc_div_h = 1;
	var lim = 100;
	if (excel_rows) lim = excel_rows;
	for (i=1; i <= lim; i++) {
		if (document.getElementById('col1_'+i)) {
			var col1h = document.getElementById('col1_'+i).clientHeight;
			var col2h = document.getElementById('col2_'+i).clientHeight;
			
			//alert(''+col1h+' - '+col2h);
			if (col1h <= col2h) {
				document.getElementById('col1_'+i).style.height = ''+col2h+'px';
				exc_div_h = exc_div_h + col2h + 1;
			} else {
				document.getElementById('col2_'+i).style.height = ''+col1h+'px';
				exc_div_h = exc_div_h + col1h + 1;
			}
		}
	}
	//alert(''+exc_div_h);
	if (document.getElementById('exc_div')) document.getElementById('exc_div').style.height = ''+(exc_div_h+16)+'px';
}
