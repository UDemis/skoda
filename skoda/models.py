# -*- coding: utf-8 -*-
from django.db import models
from snippets.pages.models import PageBase
from snippets.core.fields import ImageField


class PageCustom(PageBase):
    redirect_to_out = models.URLField(u'Перенаправление на внешний источник', blank=True)
    img = ImageField(u'Изображение', max_length=255, upload_to='pages', blank=True)

    class Meta:
        db_table = 'pages_pagecustom'
        verbose_name = u'Страницу'
        verbose_name_plural = u'Страницы'
        
    def get_absolute_url(self):
        if self.pagetype == '~~' and self.redirect_to_out:
            return self.redirect_to_out
        return super(PageCustom, self).get_absolute_url()


class PageImage(models.Model):
    page = models.ForeignKey(PageCustom, related_name='images')
    img = ImageField(u'Изображение', max_length=255, upload_to='pages')
    title = models.CharField(u'Описание', max_length=255, blank=True)

    class Meta:
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения'
