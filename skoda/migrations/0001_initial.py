# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PageCustom'
        db.create_table('pages_pagecustom', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('pub_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('expiry_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('pagetype', self.gf('django.db.models.fields.CharField')(default='~', max_length=100)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=255, blank=True)),
            ('path', self.gf('django.db.models.fields.SlugField')(max_length=2000, blank=True)),
            ('redirect_to', self.gf('django.db.models.fields.CharField')(max_length=2000, blank=True)),
            ('parent', self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='children', null=True, to=orm['skoda.PageCustom'])),
            ('content', self.gf('tinymce.models.HTMLField')(blank=True)),
            ('template', self.gf('django.db.models.fields.CharField')(default='inner.html', max_length=255)),
            ('in_menu', self.gf('django.db.models.fields.CharField')(default='default', max_length=255, blank=True)),
            ('in_sitemap', self.gf('django.db.models.fields.BooleanField')(default=True)),
            (u'lft', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            (u'rght', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            (u'tree_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            (u'level', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('img', self.gf('snippets.core.fields.ImageField')(max_length=255, blank=True)),
        ))
        db.send_create_signal(u'skoda', ['PageCustom'])


    def backwards(self, orm):
        # Deleting model 'PageCustom'
        db.delete_table('pages_pagecustom')


    models = {
        u'skoda.pagecustom': {
            'Meta': {'object_name': 'PageCustom', 'db_table': "'pages_pagecustom'"},
            'content': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'expiry_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'in_menu': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '255', 'blank': 'True'}),
            'in_sitemap': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'pagetype': ('django.db.models.fields.CharField', [], {'default': "'~'", 'max_length': '100'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['skoda.PageCustom']"}),
            'path': ('django.db.models.fields.SlugField', [], {'max_length': '2000', 'blank': 'True'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'redirect_to': ('django.db.models.fields.CharField', [], {'max_length': '2000', 'blank': 'True'}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255', 'blank': 'True'}),
            'template': ('django.db.models.fields.CharField', [], {'default': "'inner.html'", 'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        }
    }

    complete_apps = ['skoda']