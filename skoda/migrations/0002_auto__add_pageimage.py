# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PageImage'
        db.create_table(u'skoda_pageimage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('page', self.gf('django.db.models.fields.related.ForeignKey')(related_name='images', to=orm['skoda.PageCustom'])),
            ('img', self.gf('snippets.core.fields.ImageField')(max_length=255)),
        ))
        db.send_create_signal(u'skoda', ['PageImage'])


    def backwards(self, orm):
        # Deleting model 'PageImage'
        db.delete_table(u'skoda_pageimage')


    models = {
        u'skoda.pagecustom': {
            'Meta': {'object_name': 'PageCustom', 'db_table': "'pages_pagecustom'"},
            'content': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'expiry_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'in_menu': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '255', 'blank': 'True'}),
            'in_sitemap': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'pagetype': ('django.db.models.fields.CharField', [], {'default': "'~'", 'max_length': '100'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['skoda.PageCustom']"}),
            'path': ('django.db.models.fields.SlugField', [], {'max_length': '2000', 'blank': 'True'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'redirect_to': ('django.db.models.fields.CharField', [], {'max_length': '2000', 'blank': 'True'}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255', 'blank': 'True'}),
            'template': ('django.db.models.fields.CharField', [], {'default': "'inner.html'", 'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        u'skoda.pageimage': {
            'Meta': {'object_name': 'PageImage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['skoda.PageCustom']"})
        }
    }

    complete_apps = ['skoda']