# -*- coding: utf-8 -*-
from snippets.conf import register_setting


register_setting(
    "DEALER_NAME",
    u"Название диллера Skoda",
    editable=True,
    default=u'АВС Моторс',
)

register_setting(
    "DEALER_ADDRESS",
    u"Адрес диллера Skoda",
    editable=True,
    description=u'Указанный адрес отображается на главной странице в блоке контакты',
    default=u'Москва, м. Сокол, Волоколамское шоссе, д. 3, корп. 4',
    )

register_setting(
    "DEALER_PHONE",
    u"Телефон диллера Skoda",
    editable=True,
    description=u'Указанный телефон отображается на главной странице в блоке контакты',
    default=u'(831) 299-93-90',
    )

register_setting(
    "DEALER_EMAIL",
    u"E-mail диллера Skoda",
    editable=True,
    description=u'Указанный e-mail отображается на главной странице в блоке контакты',
    default=u'info@dealers-name.ru',
    )

register_setting(
    "DEALER_EMAIL_FROM",
    u"E-mail диллера Skoda для отправки писем",
    editable=True,
    description=u'Указанный e-mail используется для отправки писем с сайта',
    default=u'info@dealers-name.ru',
    )

register_setting(
    "DEALER_WORKTIME",
    u"Часы работы диллера Skoda",
    editable=True,
    description=u'Указанное время отображается на главной странице в блоке контакты',
    default=u'С 8 до 20, ежедневно',
    )

register_setting(
    "DEALER_FB_LINK",
    u"Диллер на facebook",
    editable=True,
    description=u'Указанная ссылка отображается в подвале сайта',
    default=u'',
    )

register_setting(
    "DEALER_VK_LINK",
    u"Диллер вконтакте",
    editable=True,
    description=u'Указанная ссылка отображается в подвале сайта',
    default=u'',
    )

register_setting(
    "DEALER_SCHEME_LINK",
    u"Схема проезда",
    editable=True,
    description=u'Указанная ссылка отображается на главной странице в блоке контакты',
    default=u'',
    )

register_setting(
    "SITE_BASE_TITLE",
    u"Базовый заголовок страницы",
    editable=True,
    description=u'Используется в качестве префикса тега title',
    default=u'Официальный дилер SKODA AUTO Россия',
    )

register_setting(
    "SITE_BASE_KEYWORDS",
    u"Базовые ключевые слова страницы",
    editable=True,
    description=u'Используются в meta-теге keywords',
    default=u'Skoda, Шкода, Октавия, Octavia, Фабия, Fabia, СуперБ, Superb, Йети, Yeti, Румстер, Roomster, запчасти, запасные части, ремонт, техническое обслуживание, сервис',
    )

register_setting(
    "SITE_BASE_DESCRIPTION",
    u"Базовое описание страницы",
    editable=True,
    description=u'Используется в meta-теге description',
    default=u'Продажа в кредит / лизинг, сервисное обслуживание автомобилей Skoda / Шкода: Октавия / Octavia, Фабия / Fabia, Йети / Yeti, Румстер / Roomster и СуперБ / Superb. Запчасти и аксессуары.',
    )