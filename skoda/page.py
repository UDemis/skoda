# -*- coding: utf-8 -*-
from snippets.pages.site import site, RedirectPageType


class CustomRedirectPageType(RedirectPageType):
    fields = ('slug', 'redirect_to', 'redirect_to_out')


site._registry['~~'] = CustomRedirectPageType()
