# -*- coding: utf-8 -*-
from snippets.pages.sitemaps import PageSitemap
from cars.sitemaps import sitemaps as cars_sitemaps
from actions.sitemaps import sitemaps as actions_sitemaps
from events.sitemaps import sitemaps as events_sitemaps
from news.sitemaps import sitemaps as news_sitemaps


class PageSitemapCustom(PageSitemap):
    priority = None
    changefreq = None

    def items(self):
        items = super(PageSitemapCustom, self).items().filter(in_sitemap=True)
        return items.exclude(pagetype='~~')

    def lastmod(self, obj):
        return None


SITEMAPS = {
    'pages': PageSitemapCustom,
}

SITEMAPS.update(cars_sitemaps)
SITEMAPS.update(actions_sitemaps)
SITEMAPS.update(events_sitemaps)
SITEMAPS.update(news_sitemaps)