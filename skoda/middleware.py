# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import redirect
from snippets.pages.middleware import PageMiddleware
from snippets.pages.views import page as page_view
from snippets.pages import get_page_model

Page = get_page_model()


class CustomPageMiddleware(PageMiddleware):
    def process_view(self, request, view_func, view_args, view_kwargs):
        self.page = None
        if request.path_info.startswith(settings.STATIC_URL) or request.path_info.startswith(settings.MEDIA_URL):
            return None
        path = request.path_info
        try:
            page = Page.objects.get(path=path)
        except Page.DoesNotExist:
            return None
        if not page.is_published:
            raise Http404
        if page.pagetype == '~~' and page.redirect_to:
            return HttpResponseRedirect(page.redirect_to)
        if page.pagetype == '~~' and page.redirect_to_out:
            return HttpResponseRedirect(page.redirect_to_out)
        if view_func == page_view:
            view_kwargs.setdefault("extra_context", {})
            view_kwargs["extra_context"]["page"] = page
            response = view_func(request, *view_args, **view_kwargs)
            return response
        self.page = page
        return None