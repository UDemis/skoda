from django.conf.urls import patterns, include, url
from django.conf import settings
from django.views.generic.base import TemplateView
from django.contrib import admin
from skoda.sitemaps import SITEMAPS
from skoda.views import MainPageView

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', MainPageView.as_view(template_name='home.html'), name='home'),
    url(r'^m/', include('mobile.urls')),
    url(r'^news/', include('news.urls')),
    url(r'^stati/', include('articles.urls')),
    url(r'^models/', include('cars.urls')),
    url(r'^landing/', include('landing.urls')),
    url(r'^offers/service', include('actions.urls_service')),
    url(r'^offers/', include('actions.urls')),
    url(r'^events/', include('events.urls')),
    url(r'^reviews/', include('reviews.urls')),
    url(r'^onlineforms/', include('onlineforms.urls')),
    url(r'^search/', include('search.urls')),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^sitemap/$', TemplateView.as_view(template_name='sitemap.html')),
    url(r'^robots\.txt$', include('robots.urls')),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': SITEMAPS})
)

if 'grappelli' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
        url(r'^grappelli/', include('grappelli.urls')),
    )

if 'filebrowser' in settings.INSTALLED_APPS:
    from filebrowser.sites import site
    urlpatterns += patterns('',
        url(r'^admin/filebrowser/', include(site.urls)),
    )

if 'tinymce' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
        url(r'^tinymce/', include('tinymce.urls')),
    )

if settings.DEBUG:
    urlpatterns += patterns('',
       url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
           {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    )

if settings.DEBUG and 'debug_toolbar' in settings.INSTALLED_APPS:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )

if 'snippets.pages' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
        url(r'^', include('snippets.pages.urls')),
    )
