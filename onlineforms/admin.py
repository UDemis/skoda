# -*- coding: utf-8 -*-
from django.contrib import admin
from onlineforms.models import TestDrive, TechService, Feedback, TestDrivePlace, TestDriveCar, Offer

class TestDriveAdmin(admin.ModelAdmin):
    list_display = ('car','name','phone','created')
    list_select_related = ('car',)

class TechServiceAdmin(admin.ModelAdmin):
    list_display = ('car','name','phone','created')

class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('subject','name','email')

class TestDriveCarInline(admin.TabularInline):
    model = TestDriveCar

class TestDrivePlaceAdmin(admin.ModelAdmin):
    inlines = [TestDriveCarInline]

class OfferAdmin(admin.ModelAdmin):
    list_display = ('car','name','phone','created')
    list_select_related = ('car',)

admin.site.register(TestDrive, TestDriveAdmin)
admin.site.register(TechService, TechServiceAdmin)
admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(TestDrivePlace, TestDrivePlaceAdmin)
admin.site.register(Offer, OfferAdmin)
