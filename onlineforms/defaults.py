# -*- coding: utf-8 -*-
from snippets.conf import register_setting

register_setting(
    "ONLINE_FORMS_TESTDRIVE_EMAIL",
    u'E-mail (Тест-драйв)',
    description=u"E-mail для отправки заявок на тест-драйв",
    editable=True,
    default=u'',
    )

register_setting(
    "ONLINE_FORMS_TECHSERVICE_EMAIL",
    u'E-mail (ТО)',
    description=u"E-mail для отправки заявок на ТО",
    editable=True,
    default=u'',
    )

register_setting(
    "ONLINE_FORMS_FEEDBACK_EMAIL",
    u'E-mail (Обратная связь)',
    description=u"E-mail для отправки сообщений с сайта",
    editable=True,
    default=u'',
    )

register_setting(
    "ONLINE_FORMS_OFFER_EMAIL",
    u'E-mail (Индивидуальные предложения)',
    description=u"E-mail для отправки сообщений с сайта",
    editable=True,
    default=u'',
    )

register_setting(
    "ONLINE_FORMS_AGREEMENT_LINK",
    u'Адрес страницы "О персональных данных"',
    description=u'Адрес страницы "О персональных данных"',
    editable=True,
    default=u'',
    )

register_setting(
    "ONLINE_FORMS_PROMO_AGREEMENT_LINK",
    u'Адрес страницы "О рекламной коммуникации"',
    description=u'Адрес страницы "О рекламной коммуникации"',
    editable=True,
    default=u'',
    )

