# -*- coding: utf-8 -*-
from django.db import models
from snippets.core.models import TimeStamped
from phonenumber_field.modelfields import PhoneNumberField


class TestDrivePlace(models.Model):
    title = models.CharField(u'Автомобиль', max_length=255)

    class Meta:
        verbose_name = u'Автоцентр'
        verbose_name_plural = u'Автоцентры диллера'

    def __unicode__(self):
        return self.title


class TestDriveCar(models.Model):
    title = models.CharField(u'Автомобиль', max_length=255)
    place = models.ForeignKey(TestDrivePlace, verbose_name=u'Автоцентр', related_name='cars')

    class Meta:
        verbose_name = u'Автомобиль'
        verbose_name_plural = u'Автомобили для тест-драйва'

    def __unicode__(self):
        return self.title


class TestDrive(TimeStamped):
    place = models.ForeignKey(TestDrivePlace, verbose_name=u'Автоцентр', related_name='requests')
    car = models.ForeignKey(TestDriveCar, verbose_name=u'Автомобиль')
    name = models.CharField(u'Имя', max_length=255)
    phone = PhoneNumberField(u'Телефон')
    notes = models.TextField(u'Примечание')

    class Meta:
        verbose_name = u'Запись'
        verbose_name_plural = u'Тест-драйв'

    def __unicode__(self):
        return u'%s - %s' % (self.car, self.name)


class TechService(TimeStamped):
    car = models.CharField(u'Автомобиль', max_length=255)
    state_number = models.CharField(u'Гос. номер', max_length=255, blank=True)
    year = models.CharField(u'Год выпуска', max_length=255, blank=True)
    name = models.CharField(u'ФИО', max_length=255)
    phone = PhoneNumberField(u'Телефон')
    notes = models.TextField(u'Примечание', blank=True)
    activity_list = models.TextField(u'Список работ')

    class Meta:
        verbose_name = u'Запись'
        verbose_name_plural = u'Тех. обслуживание'

    def __unicode__(self):
        return u'%s - %s' % (self.car, self.name)


class Feedback(TimeStamped):
    SUBJECT_CHOICES = (
        ('5', u'Покупка нового автомобиля'),
        ('6', u'Покупка авто с пробегом'),
        ('7', u'Работа с корпоративными клиентами'),
        ('8', u'Сервис'),
        ('9', u'Кредитование'),
        ('10', u'Страхование'),
        ('11', u'Запчасти и аксессуары'),
    )
    name = models.CharField(u'Имя', max_length=255)
    phone = PhoneNumberField(u'Телефон')
    email = models.EmailField(u'E-mail', max_length=255)
    subject = models.CharField(u'Тема', max_length=20, choices=SUBJECT_CHOICES)
    message = models.TextField(u'Сообщение')

    class Meta:
        verbose_name = u'Сообщение'
        verbose_name_plural = u'Обратная связь'

    def __unicode__(self):
        return u'%s - %s' % (self.subject, self.name)


class Offer(TimeStamped):
    car = models.ForeignKey(TestDriveCar, verbose_name=u'Автомобиль')
    name = models.CharField(u'Имя', max_length=255)
    phone = PhoneNumberField(u'Телефон')

    class Meta:
        verbose_name = u'Запись'
        verbose_name_plural = u'Индивидуальные предложения'

    def __unicode__(self):
        return u'%s - %s' % (self.car, self.name)


class CreditApp(TimeStamped):
    name = models.CharField(u'Имя', max_length=255)
    phone = PhoneNumberField(u'Телефон')
    email = models.EmailField(u'E-mail', max_length=255)
    subject = models.CharField(u'Тема', max_length=20, default='Кредитование')
    message = models.TextField(u'Сообщение')

    class Meta:
        verbose_name = u'Заявка'
        verbose_name_plural = u'Заявка на кредит'

    def __unicode__(self):
        return u'%s - %s' % (self.subject, self.name)