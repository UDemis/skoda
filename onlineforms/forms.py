# -*- coding: utf-8 -*-
from django import forms
from captcha.fields import CaptchaField
from onlineforms.models import TestDrive, TechService, Feedback, Offer, TestDriveCar, CreditApp


class TestDriveForm(forms.ModelForm):
    captcha = CaptchaField(label=u'Код с картинки')
    agreement = forms.BooleanField()
    promo_agreement = forms.BooleanField()

    class Meta:
        model = TestDrive

    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'testdrive'
        super(TestDriveForm, self).__init__(*args, **kwargs)
        self.fields['place'].empty_label = None


class TechServiceForm(forms.ModelForm):
    captcha = CaptchaField(label=u'Код с картинки')
    agreement = forms.BooleanField()
    promo_agreement = forms.BooleanField()

    class Meta:
        model = TechService

    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'techservice'
        super(TechServiceForm, self).__init__(*args, **kwargs)


class FeedbackForm(forms.ModelForm):
    captcha = CaptchaField(label=u'Код с картинки')
    agreement = forms.BooleanField()
    promo_agreement = forms.BooleanField()

    class Meta:
        model = Feedback

    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'feedback'
        super(FeedbackForm, self).__init__(*args, **kwargs)


class OfferForm(forms.ModelForm):
    captcha = CaptchaField(label=u'Код с картинки')
    agreement = forms.BooleanField()
    promo_agreement = forms.BooleanField()

    class Meta:
        model = Offer

    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'offer'
        super(OfferForm, self).__init__(*args, **kwargs)
        tmp = []
        out = []
        for choice in self.fields['car'].choices:
            if choice[1] not in tmp:
                out.append(choice)
                tmp.append(choice[1])
        self.fields['car'].choices = out

class CreditAppForm(forms.ModelForm):
    captcha = CaptchaField(label=u'Код с картинки')
    agreement = forms.BooleanField()
    promo_agreement = forms.BooleanField()

    class Meta:
        model = CreditApp

    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'creditapp'
        super(CreditAppForm, self).__init__(*args, **kwargs)