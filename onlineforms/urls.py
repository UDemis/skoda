# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from onlineforms.views import TestDriveFormView, TechServiceFormView, FeedbackFormView, OfferView, CreditAppView

urlpatterns = patterns('',
    url(r'^testdrive/$', TestDriveFormView.as_view(), name='onlineforms_testdrive'),
    url(r'^techservice/$', TechServiceFormView.as_view(), name='onlineforms_techservice'),
    url(r'^feedback/$', FeedbackFormView.as_view(), name='onlineforms_feedback'),
    url(r'^offer/$', OfferView.as_view(), name='onlineforms_offer'),
    url(r'^credit/$', CreditAppView.as_view(), name='onlineforms_credit'),
)