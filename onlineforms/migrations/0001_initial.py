# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TestDrive'
        db.create_table(u'onlineforms_testdrive', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cars.Car'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('notes', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'onlineforms', ['TestDrive'])

        # Adding model 'TechService'
        db.create_table(u'onlineforms_techservice', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('car', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('state_number', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('year', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('notes', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('activity_list', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'onlineforms', ['TechService'])

        # Adding model 'Feedback'
        db.create_table(u'onlineforms_feedback', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=255)),
            ('subject', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('message', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'onlineforms', ['Feedback'])


    def backwards(self, orm):
        # Deleting model 'TestDrive'
        db.delete_table(u'onlineforms_testdrive')

        # Deleting model 'TechService'
        db.delete_table(u'onlineforms_techservice')

        # Deleting model 'Feedback'
        db.delete_table(u'onlineforms_feedback')


    models = {
        u'cars.car': {
            'Meta': {'object_name': 'Car'},
            'accessories': ('snippets.core.fields.FileField', [], {'max_length': '255', 'blank': 'True'}),
            'catalog_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'characteristics': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'max_length': '100'}),
            'configurator_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'emission_co2': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'fulltext': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'img_exterior_bg': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'img_exterior_point_back': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'img_exterior_point_front': ('snippets.core.fields.ImageField', [], {'max_length': '255', 'blank': 'True'}),
            'interior_space': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'luggage_space': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['cars.Car']"}),
            'petrol_consumption': ('django.db.models.fields.FloatField', [], {'null': 'True'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'price_to': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'view360': ('snippets.core.fields.FileField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'onlineforms.feedback': {
            'Meta': {'object_name': 'Feedback'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'onlineforms.techservice': {
            'Meta': {'object_name': 'TechService'},
            'activity_list': ('django.db.models.fields.TextField', [], {}),
            'car': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'notes': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'state_number': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'year': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'onlineforms.testdrive': {
            'Meta': {'object_name': 'TestDrive'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cars.Car']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'notes': ('django.db.models.fields.TextField', [], {}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['onlineforms']