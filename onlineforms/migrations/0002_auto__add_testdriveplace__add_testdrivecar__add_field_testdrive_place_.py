# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TestDrivePlace'
        db.create_table(u'onlineforms_testdriveplace', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'onlineforms', ['TestDrivePlace'])

        # Adding model 'TestDriveCar'
        db.create_table(u'onlineforms_testdrivecar', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('place', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cars', to=orm['onlineforms.TestDrivePlace'])),
        ))
        db.send_create_signal(u'onlineforms', ['TestDriveCar'])

        # Adding field 'TestDrive.place'
        db.add_column(u'onlineforms_testdrive', 'place',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='requests', to=orm['onlineforms.TestDrivePlace']),
                      keep_default=False)


        # Changing field 'TestDrive.car'
        db.alter_column(u'onlineforms_testdrive', 'car_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['onlineforms.TestDriveCar']))

    def backwards(self, orm):
        # Deleting model 'TestDrivePlace'
        db.delete_table(u'onlineforms_testdriveplace')

        # Deleting model 'TestDriveCar'
        db.delete_table(u'onlineforms_testdrivecar')

        # Deleting field 'TestDrive.place'
        db.delete_column(u'onlineforms_testdrive', 'place_id')


        # Changing field 'TestDrive.car'
        db.alter_column(u'onlineforms_testdrive', 'car_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cars.Car']))

    models = {
        u'onlineforms.feedback': {
            'Meta': {'object_name': 'Feedback'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'onlineforms.techservice': {
            'Meta': {'object_name': 'TechService'},
            'activity_list': ('django.db.models.fields.TextField', [], {}),
            'car': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'notes': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'state_number': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'year': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'onlineforms.testdrive': {
            'Meta': {'object_name': 'TestDrive'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['onlineforms.TestDriveCar']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'notes': ('django.db.models.fields.TextField', [], {}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'requests'", 'to': u"orm['onlineforms.TestDrivePlace']"}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'onlineforms.testdrivecar': {
            'Meta': {'object_name': 'TestDriveCar'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cars'", 'to': u"orm['onlineforms.TestDrivePlace']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'onlineforms.testdriveplace': {
            'Meta': {'object_name': 'TestDrivePlace'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['onlineforms']