# -*- coding: utf-8 -*-
from django.views.generic.edit import CreateView
from onlineforms.models import TestDrive, TechService, Feedback, Offer, CreditApp
from onlineforms.forms import TestDriveForm, TechServiceForm, FeedbackForm, OfferForm, CreditAppForm
from onlineforms.utils import send_mail
from snippets.conf import settings


def get_emails(email_str):
    if not email_str:
        return None
    return email_str.split(',')


class TestDriveFormView(CreateView):
    model = TestDrive
    form_class = TestDriveForm
    http_method_names = ('post',)

    def form_valid(self, form):
        self.object = form.save()
        emails = get_emails(settings.ONLINE_FORMS_TESTDRIVE_EMAIL)
        if emails:
            send_mail(u'Запись на тест-драйв', 'onlineforms/mail/testdrive.html',
                      {'object': self.object}, emails, from_email=settings.DEALER_EMAIL_FROM)
        form = self.form_class()
        return self.render_to_response(self.get_context_data(form=form, success=True))

    def form_invalid(self, form):
        print form.errors
        return super(TestDriveFormView, self).form_invalid(form)


class TechServiceFormView(CreateView):
    model = TechService
    form_class = TechServiceForm
    http_method_names = ('post',)

    def form_valid(self, form):
        self.object = form.save()
        emails = get_emails(settings.ONLINE_FORMS_TECHSERVICE_EMAIL)
        if emails:
            send_mail(u'Запись на ТО', 'onlineforms/mail/techservice.html',
                      {'object': self.object}, emails, from_email=settings.DEALER_EMAIL_FROM)
        form = self.form_class()
        return self.render_to_response(self.get_context_data(form=form, success=True))

    def form_invalid(self, form):
        print form.errors
        return super(TechServiceFormView, self).form_invalid(form)


class FeedbackFormView(CreateView):
    model = Feedback
    form_class = FeedbackForm
    http_method_names = ('post',)

    def form_valid(self, form):
        self.object = form.save()
        emails = get_emails(settings.ONLINE_FORMS_FEEDBACK_EMAIL)
        if emails:
            send_mail(u'Обратная связь', 'onlineforms/mail/feedback.html', {'object': self.object},
                      emails, from_email=settings.DEALER_EMAIL_FROM)
        form = self.form_class()
        return self.render_to_response(self.get_context_data(form=form, success=True))


class OfferView(CreateView):
    model = Offer
    form_class = OfferForm
    http_method_names = ('post',)

    def form_valid(self, form):
        self.object = form.save()
        emails = get_emails(settings.ONLINE_FORMS_OFFER_EMAIL)
        if emails:
            send_mail(u'СРОЧНО ПОЗВОНИТЬ КЛИЕНТУ', 'onlineforms/mail/offer.html',
                      {'object': self.object}, emails, from_email=settings.DEALER_EMAIL_FROM)
        form = self.form_class()
        return self.render_to_response(self.get_context_data(form=form, success=True))


class CreditAppView(CreateView):
    model = CreditApp
    form_class = CreditAppForm
    http_method_names = ('post',)
    template_name = 'onlineforms/credit_form.html'

    def form_valid(self, form):
        self.object = form.save()
        emails = get_emails(settings.ONLINE_FORMS_FEEDBACK_EMAIL)
        if emails:
            send_mail(u'Заявка на кредит', 'onlineforms/mail/credit.html', {'object': self.object},
                      emails, from_email=settings.DEALER_EMAIL_FROM)
        form = self.form_class()
        return self.render_to_response(self.get_context_data(form=form, success=True))