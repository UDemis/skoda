# -*- coding: utf-8 -*-
from django import template
from onlineforms.forms import TestDriveForm, TechServiceForm, FeedbackForm, OfferForm, CreditAppForm

register = template.Library()


@register.inclusion_tag("onlineforms/testdrive_form.html", takes_context=True)
def onlineforms_testdrive(context):
    form = TestDriveForm()
    context.update({
        'form': form
    })
    return context


@register.inclusion_tag("onlineforms/techservice_form.html", takes_context=True)
def onlineforms_techservice(context):
    form = TechServiceForm()
    context.update({
        'form': form
    })
    return context


@register.inclusion_tag("onlineforms/feedback_form.html", takes_context=True)
def onlineforms_feedback(context):
    form = FeedbackForm()
    context.update({
        'form': form
    })
    return context

@register.inclusion_tag("onlineforms/credit_form.html", takes_context=True)
def onlineforms_credit(context):
    form = CreditAppForm()
    context.update({
        'form': form
    })
    return context


@register.inclusion_tag("onlineforms/offer_form.html", takes_context=True)
def onlineforms_offer(context):
    form = OfferForm()
    context.update({
        'form': form
    })
    return context


@register.filter
def add_class(field, classes):
    field.field.widget.attrs['class'] = classes
    return field

@register.filter
def add_placeholder(field, placeholder):
    field.field.widget.attrs['placeholder'] = placeholder
    return field

@register.filter
def required(field):
    field.field.widget.attrs['required'] = required
    return field