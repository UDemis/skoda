# -*- coding: utf-8 -*-
from django.conf.urls import *
from reviews.views import ReviewListView, ReviewCreateView


urlpatterns = patterns('',
    url(r'^$', ReviewListView.as_view(), name='review_list'),
    url(r'^create/$', ReviewCreateView.as_view(), name='review_create'),
)