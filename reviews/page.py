# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from snippets.pages.site import site, PageType
from reviews.models import Review


class ReviewPageType(PageType):

    def get_path(self, *args, **kwargs):
        return reverse('review_list')

site.register(Review, ReviewPageType)