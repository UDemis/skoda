# -*- coding: utf-8 -*-
from django.contrib import admin
from reviews.models import Review


class ReviewAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'is_published', 'datetime_create',)


admin.site.register(Review, ReviewAdmin)