# -*- coding: utf-8 -*-
from snippets.conf import register_setting

register_setting(
    "REVIEWS_EMAIL",
    u'E-mail для отзывов с сайта',
    description=u"E-mail для отправки новых отзывов с сайта",
    editable=True,
    default=u'',
)

