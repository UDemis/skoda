# -*- coding: utf-8 -*-
from django.db import models


class Review(models.Model):
    name = models.CharField(u'Имя', max_length=255)
    email = models.EmailField(u'E-mail', blank=True)
    text = models.TextField(u'Текст отзыва')
    is_published = models.BooleanField(u'Опубликован', default=False)
    datetime_create = models.DateTimeField(u'Дата и время создания', auto_now_add=True)

    class Meta:
        verbose_name = u'Отзыв'
        verbose_name_plural = u'Отзывы'
        ordering = ['-datetime_create']

    def __unicode__(self):
        if self.email:
            return u'%s (%s)' % (self.name, self.email)
        return self.name