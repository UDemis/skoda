# -*- coding: utf-8 -*-
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.conf import settings
from django.utils.html import strip_tags
from reviews.models import Review
from reviews.forms import ReviewForm
from snippets.conf import settings as site_settings


class ReviewListView(ListView):
    queryset = Review.objects.filter(is_published=True)

    def get_context_data(self, **kwargs):
        kwargs.update({
            'review_form': ReviewForm()
        })
        return super(ReviewListView, self).get_context_data(**kwargs)


class ReviewCreateView(CreateView):
    model = Review
    form_class = ReviewForm
    http_method_names = ('post',)

    def send_mail(self, subject, template, context, recipient_list, from_email=settings.DEFAULT_FROM_EMAIL,
                  fail_silently=not settings.DEBUG):
        html_content = render_to_string(template, context)
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, recipient_list)
        msg.attach_alternative(html_content, "text/html")
        return msg.send(fail_silently)

    def get_emails(self, email_str):
        if not email_str:
            return None
        return email_str.split(',')

    def form_valid(self, form):
        self.object = form.save()
        emails = self.get_emails(site_settings.REVIEWS_EMAIL)
        if emails:
            self.send_mail(u'НОВЫЙ ОТЗЫВ', 'reviews/mail/body.html',
                           {'object': self.object}, emails, from_email=site_settings.DEALER_EMAIL_FROM)
        form = self.form_class()
        return self.render_to_response(self.get_context_data(form=form, success=True))