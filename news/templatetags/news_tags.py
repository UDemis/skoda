# -*- coding: utf-8 -*-
from django import template
from news.models import News
from news import settings as news_settings

register = template.Library()

@register.inclusion_tag('news/news_other_tag.html', takes_context=True)
def news_other(context, current):
    if not isinstance(current, News):
        raise template.TemplateSyntaxError('%s is not instance of %s' % (current, News))
    other_news = News.published.exclude(pk=current.pk)[:news_settings.OTHER_PAGE_SIZE]
    context['object_list'] = other_news
    return context

@register.inclusion_tag('news/news_last_tag.html', takes_context=True)
def news_last(context, count=news_settings.LAST_PAGE_SIZE):
    try:
        count = int(count)
    except:
        count=news_settings.LAST_PAGE_SIZE
    last_news = News.published.all()[:count]
    context['object_list'] = last_news
    return context

