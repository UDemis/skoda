# -*- coding: utf-8 -*-
from django.conf import settings

ARCHIVE_PAGE_SIZE = getattr(settings, 'NEWS_ARCHIVE_PAGE_SIZE', 15)
PAGE_SIZE = getattr(settings, 'NEWS_PAGE_SIZE', 15)
OTHER_PAGE_SIZE = getattr(settings, 'NEWS_OTHER_PAGE_SIZE', 3)
LAST_PAGE_SIZE = getattr(settings, 'NEWS_LAST_PAGE_SIZE', 3)