# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from snippets.pages.site import site, PageType
from news.models import News


class NewsPageType(PageType):
    def get_path(self, *args, **kwargs):
        return reverse('news_archive_index')


site.register(News, NewsPageType)