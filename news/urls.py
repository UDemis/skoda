# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from news import views

urlpatterns = patterns('',
    url(r'^$',
       views.ArchiveIndexView.as_view(), name='news_archive_index'),

    # url(r'^(?P<year>\d{4})/$',
    #     views.YearArchiveView.as_view(), name='news_archive_year'),
    #
    # url(r'^(?P<year>\d{4})/(?P<month>\d{2})/$',
    #     views.MonthArchiveView.as_view(), name='news_archive_month'),
    #
    # url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/$',
    #     views.DayArchiveView.as_view(), name='news_archive_day'),

    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)\.html$',
       views.DetailView.as_view(), name='news_detail'),
)