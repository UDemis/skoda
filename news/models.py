# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from snippets.core.models import Published, TimeStamped, Description
from snippets.core.fields import ImageField


class News(Published, Description, TimeStamped):
    """
    News
    """
    KIND_CHOICES = (
        ('common', u'Общие новости'),
        ('dealer', u'Новости диллера'),
    )

    title = models.CharField(_('Title'), max_length=255)
    slug = models.SlugField(_('Slug'), max_length=255, unique_for_date='pub_date',
                            help_text=_('A slug is a short name which uniquely identifies the news item for this day'))

    img = ImageField(_('Image'), blank=True, max_length=255, upload_to='news')

    link = models.URLField(_('Link'), blank=True, null=True,
                           help_text=_('This link will be used a absolute url'
                                       ' for this item and replaces the view logic. <br />Note that by'
                                       ' default this only applies for items with an empty "content"'
                                       ' field.'))

    kind = models.CharField(u'Тип новости', max_length=10, choices=KIND_CHOICES, default=KIND_CHOICES[0][0],
                            editable=False)

    class Meta:
        verbose_name = _('News')
        verbose_name_plural = _('News')
        ordering = ('-pub_date', )

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        if self.link:
            return self.link
        return self.get_origin_absolute_url()

    @models.permalink
    def get_origin_absolute_url(self):
        return ('news_detail', (), {'year': self.pub_date.strftime("%Y"),
                                    'month': self.pub_date.strftime("%m"),
                                    'day': self.pub_date.strftime("%d"),
                                    'slug': self.slug})

    def get_absolute_url_mobile(self):
        if self.link:
            return self.link
        return self.get_origin_absolute_url_mobile()

    @models.permalink
    def get_origin_absolute_url_mobile(self):
        return ('news_detail_mobile', (), {'year': self.pub_date.strftime("%Y"),
                                           'month': self.pub_date.strftime("%m"),
                                           'day': self.pub_date.strftime("%d"),
                                           'slug': self.slug})
