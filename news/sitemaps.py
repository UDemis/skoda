# -*- coding: utf-8 -*-
from django.contrib.sitemaps import Sitemap
from django.db import models
from news.models import News


class NewsSitemap(Sitemap):

    def items(self):
        return News.published.filter(models.Q(link=None) | models.Q(link=''))


sitemaps = {
    'news': NewsSitemap
}
