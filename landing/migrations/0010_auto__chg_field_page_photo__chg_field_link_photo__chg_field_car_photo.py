# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Page.photo'
        db.alter_column(u'landing_page', 'photo', self.gf('snippets.core.fields.ImageField')(max_length=255))

        # Changing field 'Link.photo'
        db.alter_column(u'landing_link', 'photo', self.gf('snippets.core.fields.ImageField')(max_length=255))

        # Changing field 'Car.photo'
        db.alter_column(u'landing_car', 'photo', self.gf('snippets.core.fields.ImageField')(max_length=255))

    def backwards(self, orm):

        # Changing field 'Page.photo'
        db.alter_column(u'landing_page', 'photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100))

        # Changing field 'Link.photo'
        db.alter_column(u'landing_link', 'photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100))

        # Changing field 'Car.photo'
        db.alter_column(u'landing_car', 'photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100))

    models = {
        u'landing.car': {
            'Meta': {'object_name': 'Car'},
            'desc': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cars'", 'to': u"orm['landing.Page']"}),
            'photo': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'landing.link': {
            'Meta': {'object_name': 'Link'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'links'", 'to': u"orm['landing.Page']"}),
            'photo': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'landing.page': {
            'Meta': {'object_name': 'Page'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'photo': ('snippets.core.fields.ImageField', [], {'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'stock': ('tinymce.models.HTMLField', [], {}),
            'text': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['landing']