# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Page.photo'
        db.add_column(u'landing_page', 'photo',
                      self.gf('django.db.models.fields.files.ImageField')(default='no', max_length=100),
                      keep_default=False)

        # Adding field 'Page.stock'
        db.add_column(u'landing_page', 'stock',
                      self.gf('tinymce.models.HTMLField')(default='no'),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Page.photo'
        db.delete_column(u'landing_page', 'photo')

        # Deleting field 'Page.stock'
        db.delete_column(u'landing_page', 'stock')


    models = {
        u'landing.car': {
            'Meta': {'object_name': 'Car'},
            'desc': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cars'", 'to': u"orm['landing.Page']"}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'landing.link': {
            'Meta': {'object_name': 'Link'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'links'", 'to': u"orm['landing.Page']"}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'landing.page': {
            'Meta': {'object_name': 'Page'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'stock': ('tinymce.models.HTMLField', [], {}),
            'text': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['landing']