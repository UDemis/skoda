# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from landing.views import PageDetailView, LandingFormView, IndexView


urlpatterns = patterns('',
    url(r'^$', IndexView.as_view(), name='landing_list'),
    url(r'^form/$', LandingFormView.as_view(), name='landing_form'),
    url(r'^(?P<slug>[-\w]+)/$', PageDetailView.as_view(), name='landing_detail'),
)