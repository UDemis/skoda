# -*- coding: utf-8 -*-
from django.db import models
from snippets.core.fields import RichTextField, ImageField


class Page(models.Model):
    class Meta():
        verbose_name = u'Лендинг'
        verbose_name_plural = u'Лендинги'

    name = models.CharField(u'Название страницы', max_length=128)
    slug = models.SlugField(u'Слаг страницы')
    text = models.TextField(u'Текст вверху страницы')
    photo = ImageField(u'Фото вверху страницы', max_length=255, upload_to='landing')
    stock = RichTextField(u'Содержание акций')
    is_published = models.BooleanField(u'Опубликовать', default=False)

    def __unicode__(self):
        return "%s" % self.name


class Car(models.Model):
    class Meta():
        verbose_name = u'Автомобиль'
        verbose_name_plural = u'Автомобили'

    name = models.CharField(u'Название автомобиля', max_length=128)
    desc = models.CharField(u'Комплектация автомобиля', max_length=128)
    photo = ImageField(u'Фото автомобиля', max_length=255, upload_to='landing')
    price = models.CharField(u'Цена автомобиля', max_length=20)
    page = models.ForeignKey(Page, verbose_name='Страница', related_name='cars')


class Link(models.Model):
    page = models.ForeignKey(Page, verbose_name='Страница', related_name='links')
    photo = ImageField(u'Картинка', max_length=255, upload_to='landing')
    name = models.CharField(u'Заголовок ссылки', max_length=128)
    text = models.TextField(u'Описание')
    link = models.URLField(u'Ссылка')