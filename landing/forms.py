# -*- coding: utf-8 -*-
from django import forms


class LandingForm(forms.Form):
    name = forms.CharField(label=u'Имя')
    phone = forms.CharField(label=u'Телефон для связи')

