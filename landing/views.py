from django.shortcuts import render
from django.http import Http404, HttpResponseRedirect
from django.views.generic import ListView, DetailView, View
from django.views.generic.edit import FormView
from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string
from django.conf import settings
from django.utils.html import strip_tags
from snippets.conf import settings as site_settings
from landing.forms import LandingForm
from landing.models import Page


class PageListView(ListView):
    model = Page
    queryset = Page.objects.filter(is_published=True)


class PageDetailView(DetailView):
    model = Page


class SendMailMixin(object):
    template_mail_name = None

    def send_mail(self, mail_to, subject, context):
        if mail_to:
            recipient_list = [i.strip() for i in mail_to.split(',')]
            html_content = render_to_string(self.template_mail_name, context)
            text_content = strip_tags(html_content)
            try:
                msg = EmailMultiAlternatives(subject, text_content,
                                             site_settings.DEALER_EMAIL_FROM or settings.DEFAULT_EMAIL_FROM,
                                             recipient_list)
                msg.attach_alternative(html_content, "text/html")
                return msg.send(not settings.DEBUG)
            except Exception as e:
                print e

        return False


class LandingFormView(SendMailMixin, FormView):
    form_class = LandingForm
    template_name = 'landing/landing_form.html'
    template_mail_name = 'landing/mail_landing_form.html'

    def form_valid(self, form):
        self.send_mail(site_settings.LANDING_EMAIL, site_settings.LANDING_EMAIL_SUBJECT, {'form': form})
        return self.render_to_response(self.get_context_data(form=form, success=True))


class IndexView(View):
    def get(self, *args, **kwargs):
        page = Page.objects.first()
        if page:
            return HttpResponseRedirect(reverse('landing_detail', kwargs={'slug': page.slug}))
        raise Http404
