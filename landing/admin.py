from django.contrib import admin
from landing.models import Page, Car, Link


class CarInLine(admin.TabularInline):
    model = Car
    extra = 6


class LinkInLine(admin.TabularInline):
    model = Link
    extra = 3


class PageAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    inlines = [CarInLine, LinkInLine]


admin.site.register(Page, PageAdmin)