# -*- coding: utf-8 -*-
from snippets.conf import register_setting


register_setting(
    "LANDING_EMAIL",
    u"E-mail для лендингов",
    editable=True,
    description=u'На указанный e-mail приходят заявки с лендингов',
    default=u'info@dealers-name.ru',
)

register_setting(
    "LANDING_EMAIL_SUBJECT",
    u"Тема E-mail сооющений с лендингов",
    editable=True,
    description=u'Тема указанного e-mail лендинга, с которого приходят заявки',
    default=u'Заявки с лендингов',
)